﻿// THIS FILE IS AN EMPTY STUB, IT IS YET TO BE FILLED WITH CODE

/**
 *  file       : id 20220829°1121 — gitlab.com/normai/cheeseburger …/cs/cs343textfile.cs
 *  version    : • 20xxxxxx°xxxx v0.x.x Filling • 20220829°1121 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  summary    : This demonstrates ...
 */
using System;

class Program
{
    const string sVERSION = "v0.0.0";
    static string sDOTNETV = "(C# " + typeof(int).Assembly.ImageRuntimeVersion + ")";

    public static void Main(String[] args)
    {
        Console.WriteLine($"*** Pryvit, tse `cs343textfile.cs` {sVERSION} — Textfiles {sDOTNETV} ***");  "Привіт, це [Pryvit, tse]"





        Console.WriteLine("Do pobachennya.");  // "до побачення [Do pobachennya]."
    }
}
