﻿/**
 *  file       : id 20221002°0921 — gitlab.com/normai/cheeseburger …/cs/cs127ternary.cs
 *  version    : • 20221227°1613 v0.1.9 Filling • 20221002°0921 v0.1.8 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate ternary operator
 *  userstory  : Roll dice and tell whether it was greater than three or not
 */
using System;

class Program
{
   const string sVERSION = "v0.1.9";
   static string sDOTNETV = "(C# " + typeof(int).Assembly.ImageRuntimeVersion + ")";

   public static void Main(String[] args)
   {
      Console.WriteLine($"*** Hej, det här är `cs127ternary.cs` {sVERSION} — Ternary operator {sDOTNETV} ***");


      // Preparation — Roll dice
      Random random = new System.Random();
      int iRand = random.Next(1, 7);                                   // From 1 though 6
      Console.WriteLine("(1.1) You rolled " + iRand.ToString());

      // The demo line
      Console.WriteLine("(1.2) You rolled greater three : " + (iRand > 3 ? "Yes" : "No"));


      Console.WriteLine("Adjö.");
   }
}
