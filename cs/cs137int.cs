﻿/**
 *  file       : id 20221012°1021 — gitlab.com/normai/cheeseburger …/cs/cs137int.cs
 *  version    : • 20221018°1121 v0.1.8 Filling • 20221012°1021 v0.1.7 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate Integer Types
 *  compile    : > csc.exe /reference:System.Numerics.dll cs137int.cs
 *  summary    : This shows
 *                • All primitive C# integer types, including char
 *                • BigInteger class
 *                • Boxing/unboxing
 *                • PadRight()
 *                • Min/MaxValue()
 *                • GetType()
 *                • Array of strings
 *  userstory  :
 *  ref        : https://www.dotnetperls.com/int-maxvalue [ref 20221016°1242]
 *  ref        : https://thedeveloperblog.com/byte [ref 20221016°1243] Material seems stolen from ref 20221016°1247 dotnetperls
 *  ref        : https://www.dotnetperls.com/byte [ref 20221016°1247]
 *  ref        : https://blog.udemy.com/c-sharp-char/ [ref 20221016°1252]
 *  ref        : http://ctp.mkprog.com/en/csharp/unsigned_integers/ [ref 20221016°1812]
 *  ref        : https://learn.microsoft.com/en-us/dotnet/api/system.numerics.biginteger [ref 20221016°1832]
 *  ref        : https://www.tutorialsteacher.com/articles/biginteger-type-in-csharp [ref 20221016°1833]
 *  ref        : https://www.universetoday.com/36302/atoms-in-the-universe/ [ref 20221016°1842]
 *  ref        : https://en.wikipedia.org/wiki/Age_of_the_universe [ref 20221016°1852]
 *  ref        :
 *  status     :
 */
using System;
using System.Numerics;                                                  // BigInteger, needs /reference:System.Numerics.dll

class Program
{
   const string sVERSION = "v0.1.8";
   static string sDOTNETV = "(C# " + typeof(int).Assembly.ImageRuntimeVersion + ")";

   public static void Main(String[] args)
   {
      Console.WriteLine($"*** [Nǐ hǎo, zhè shì] `cs137int.cs` {sVERSION} — Integer Types {sDOTNETV} ***");  // "你好，这是" does not print nice

      // () 1
      sbyte i11 = -128;
      sbyte i12 = 127;
      sbyte i13 = (sbyte) (i12 + 1);
      byte i21 = 255;
      byte i22 = (byte) (i21 + 1);

      // () 2
      char i31 = (char) 65535;
      char i32 = unchecked((char) (i31 + 1));

      // () 3
      short i41 = -32_768;
      short i42 = 32_767;
      short i43 = (short) (i42 + 1);
      ushort i51 = 65535;
      ushort i52 = (ushort) (i51 + 1);

      // () 4
      int i61 = -2_147_483_648;
      int i62 = 2_147_483_647;
      int i63 = (int) (i62 + 1);
      uint i71 = 4294967295;
      uint i72 = (uint) (i71 + 1);

      // () 5
      long i81 = long.MinValue;
      long i82 = long.MaxValue;
      long i83 = (long) (i82 + 1);
      ulong i91 = 18446744073709551615;
      ulong i92 = (ulong) (i91 + 1);

      // () 6
      BigInteger iB1 = 0, iB2 = 0;
      try {
         string [] ar = { "120000000000000000000000"                    // Number of stars in the universe (see ref 20221016°1842)
                         , "434786832000000000000123"                   // 13_787_000_000 * 365 * 24 * 60 * 60 * 1000 * 1000 Microseconds since big bang (see ref 20221016°1852)
                          };
         iB1 = BigInteger.Parse(ar[1]);
      } catch (FormatException ex) {
         Console.WriteLine("Exception :" + ex.Message);
      }
      iB2 = iB1 * iB1;

      Console.WriteLine("(A.1.1) sbyte i11          = " + i11.ToString().PadRight(25) + " " + i11.GetType());
      Console.WriteLine("(A.1.2) sbyte i12          = " + i12.ToString().PadRight(25) + " " + i12.GetType());
      Console.WriteLine("(A.1.3) sbyte i12 + 1      = " + i13.ToString().PadRight(25) + " " + i13.GetType());
      Console.WriteLine("(A.2.1) byte i21           = " + i21.ToString().PadRight(25) + " " + i21.GetType());
      Console.WriteLine("(A.2.2) byte i21 + 1       = " + i22.ToString().PadRight(25) + " " + i22.GetType());

      Console.WriteLine("(A.3.1) (int) char i31     = " + ((int) i31).ToString().PadRight(25) + " " + i31.GetType());
      Console.WriteLine("(A.3.2) unchecked((char) (i31 + 1)) = " + ((int) i32).ToString().PadRight(16) + " " + i32.GetType());

      Console.WriteLine("(A.4.1) short i41          = " + i41.ToString().PadRight(25) + " " + i41.GetType());
      Console.WriteLine("(A.4.2) short i42          = " + i42.ToString().PadRight(25) + " " + i42.GetType());
      Console.WriteLine("(A.4.3) short i42 + 1      = " + i43.ToString().PadRight(25) + " " + i43.GetType());
      Console.WriteLine("(A.5.1) ushort i21         = " + i51.ToString().PadRight(25) + " " + i51.GetType());
      Console.WriteLine("(A.5.2) ushort i21 + 1     = " + i52.ToString().PadRight(25) + " " + i52.GetType());

      Console.WriteLine("(A.6.1) int i61            = " + i61.ToString().PadRight(25) + " " + i61.GetType());
      Console.WriteLine("(A.6.2) int i62            = " + i62.ToString().PadRight(25) + " " + i62.GetType());
      Console.WriteLine("(A.6.3) int i62 + 1        = " + i63.ToString().PadRight(25) + " " + i63.GetType());
      Console.WriteLine("(A.7.1) uint i71           = " + i71.ToString().PadRight(25) + " " + i71.GetType());
      Console.WriteLine("(A.7.2) uint i71 + 1       = " + i72.ToString().PadRight(25) + " " + i72.GetType());

      Console.WriteLine("(A.8.1) long i81           = " + i81.ToString().PadRight(25) + " " + i81.GetType());
      Console.WriteLine("(A.8.2) long i82           = " + i82.ToString().PadRight(25) + " " + i82.GetType());
      Console.WriteLine("(A.8.3) long i82 + 1       = " + i83.ToString().PadRight(25) + " " + i83.GetType());
      Console.WriteLine("(A.9.1) ulong i91          = " + i91.ToString().PadRight(25) + " " + i91.GetType());
      Console.WriteLine("(A.9.2) ulong i91 + 1      = " + i92.ToString().PadRight(25) + " " + i92.GetType());

      Console.WriteLine("(B.1) BigInteger iB1       = " + iB1.ToString().PadRight(25) + " " + iB1.GetType());
      Console.WriteLine("(B.2) iB1 * iB1            = " + iB2.ToString());

      Console.WriteLine("(C.1) sbyte.Min/MaxValue   = " + (sbyte.MinValue + " / " + sbyte.MaxValue).PadRight(25) + " " + sbyte.MaxValue.GetType());
      Console.WriteLine("(C.2) byte.Min/MaxValue    = " + (byte.MinValue + " / " + byte.MaxValue).PadRight(25) + " " + byte.MaxValue.GetType());
      Console.WriteLine("(C.3) char.Min/MaxValue    = " + ("'" + char.MinValue + "' / '" + char.MaxValue + "'").PadRight(25) + " " + char.MaxValue.GetType());
      Console.WriteLine("(C.4) short.Min/MaxValue   = " + (short.MinValue + " / " + short.MaxValue).PadRight(25) + " " + short.MaxValue.GetType());
      Console.WriteLine("(C.5) ushort.Min/MaxValue  = " + (ushort.MinValue + " / " + ushort.MaxValue).PadRight(25) + " " + ushort.MaxValue.GetType());
      Console.WriteLine("(C.6) int.Min/MaxValue     = " + (int.MinValue + " / " + int.MaxValue).PadRight(25) + " " + int.MaxValue.GetType());
      Console.WriteLine("(C.7) uint.Min/MaxValue    = " + (uint.MinValue + " / " + uint.MaxValue).PadRight(25) + " " + uint.MaxValue.GetType());
      Console.WriteLine("(C.8) long.Min/MaxValue    = " + (long.MinValue + " / " + long.MaxValue).PadRight(25) + " " + long.MaxValue.GetType());
      Console.WriteLine("(C.9) ulong.Min/MaxValue   = " + (ulong.MinValue + " / " + ulong.MaxValue).PadRight(25) + " " + ulong.MaxValue.GetType());

      Console.WriteLine("Zàijiàn.");  // "再见" does not print nice
   }
}
