﻿// EMPTY STUB, YET TO BE FILLED WITH CODE

/**
 *  file       : id 20221222°1921 — gitlab.com/normai/cheeseburger …/cs/cs341dir.cs
 *  version    : • 20xxxxxx°xxxx v0.x.x Filling • 20221222°1921 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate directory listing
 *  status     :
 */
using System;

class Program
{
   const string sVERSION = "v0.0.0";
   static string sDOTNETV = "(C# " + typeof(int).Assembly.ImageRuntimeVersion + ")";

   public static void Main(String[] args)
   {
      Console.WriteLine($"*** To jest `cs341dir.cs` {sVERSION} — Directory listing {sDOTNETV} ***");





      Console.WriteLine("Żegnajcie.");
   }
}
