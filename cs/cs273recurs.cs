﻿// EMPTY STUB, YET TO BE FILLED WITH CODE

/**
 *  file       : id 20221222°1821 — gitlab.com/normai/cheeseburger …/cs/cs273recurs.cs
 *  version    : • 20xxxxxx°xxxx v0.x.x Filling • 20221222°1821 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate recursion
 *  status     :
 */
using System;

class Program
{
   const string sVERSION = "v0.0.0";
   static string sDOTNETV = "(C# " + typeof(int).Assembly.ImageRuntimeVersion + ")";

   public static void Main(String[] args)
   {
      Console.WriteLine($"*** Yō 'cs273recurs.cs' hō {sVERSION} — Recursion {sDOTNETV} ***");  "***  यो 'cs273recurs.cs' हो [Yō 'cs273recurs.cs' hō]"





      Console.WriteLine("Namastē.");  // "नमस्ते [Namastē]."
   }
}
