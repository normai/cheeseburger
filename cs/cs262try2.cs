﻿/**
 *  file       : id 20220827°0921 — gitlab.com/normai/cheeseburger …/cs/cs262try2.cs
 *  version    : • 20220915°1031 v0.1.8 Filling • 20220827°0921 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subjec     : Demonstrates C# exception handling
 *  summary    : This is one single try block with a sequence of tasks. With
 *               each pass, one more case succeeds to execute. The sequence is:
 *                • (1) Division (fails if divisor is zero)
 *                • (2) Calculate square root (fails if done with negative number)
 *                • (3) Array access (fails if index out of bounds)
 *                • (4) Open file (fails if file does not exist)
 *                • (5) Output all runs fine (only reached if non of above failed)
 *               The catch clause is divided into a catch cascade for specific exceptions.
 */

using System;
using System.IO;

class Program
{
    const string sVERSION = "v0.1.8";
    static string sDOTNETV = "(C# " + typeof(int).Assembly.ImageRuntimeVersion + ")";

    public static void Main(String[] args)
    {
        Console.WriteLine($"*** Sveiki, šis ir `cs262try2.cs` {sVERSION} — Exception Handling (full) {sDOTNETV} ***");


      // Process five cases
      int[] canaries = {0, -2, 3, 4, 1};
      foreach (int iCanary  in canaries)
      {
         Console.WriteLine();
         Console.WriteLine("Case with number = " + iCanary.ToString() + ":");

         try
         {
            //----------------------------------------------
            // (1) Try division
            Console.WriteLine("(1.1) Try division");
            Double nRatio = 123.4 / iCanary;
            Console.WriteLine("(1.2) Calculation: " + (123.4).ToString() + " / " + iCanary.ToString() + " = " + nRatio.ToString());
            if (Double.IsInfinity(nRatio))
            {
               throw new DivisionByZeroException("Division yielded unusable result.");
            }
            //----------------------------------------------
            // (2) Try calculate square root
            Console.WriteLine("(2.1) Try square root");
            double dSqrt = Math.Sqrt(iCanary);
            Console.WriteLine("(2.2) Calculation: Square root of " + iCanary.ToString() + " = " + dSqrt.ToString());
            if (Double.IsNaN(dSqrt))
            {
               throw new NumberIsNanException("Calculation yielded unusable result.");
            }

            //----------------------------------------------
            // (3) Array out of bounds
            Console.WriteLine("(3) Array bounds");
            string[] ar = {"Oins", "Zwoi", "Droi"};
            if (iCanary > ar.Length)                                            // Missing the true boundary by one
            {
               Console.WriteLine("(3.1)   Array access skipped because index is greater than array.");
            }
            else
            {
               Console.WriteLine("(3.2)   ar[" + iCanary.ToString() + "] = " + ar[iCanary]);
            }

            //----------------------------------------------
            // (4) Try read file
            String sFile = iCanary == 4 ? ".\\detjibbetnich.txt" : ".\\cs262try2.cs";
            Console.WriteLine("(4.1) Try open file \"" + sFile);
            String sLine;
            using(StreamReader file = new StreamReader(sFile))
            {
                sLine = file.ReadLine();
            }
            Console.WriteLine("(4.2) Read first line: \"" + sLine + "\"");

            //----------------------------------------------
            // (5) All fine
            Console.WriteLine("(5) All was fine, no exception was thrown.");

         }
         catch (DivisionByZeroException ex)
         {
            Console.WriteLine("(6.1) Caught " + ex.Message);
         }
         catch (Exception ex)
         {
            Console.WriteLine("(6.2) Caught " + ex.Message);
         }
         finally {
            Console.WriteLine("(7) Finally.");
         }
      }

        Console.WriteLine("Uz redzēšanos.");
    }
}

// Custom exception
class DivisionByZeroException : Exception
{ 
   public DivisionByZeroException(String sMsg)
      : base(sMsg) {}
}

// Custom exception
class NumberIsNanException : Exception
{ 
   public NumberIsNanException(String sMsg)
      : base(sMsg) {}
}
