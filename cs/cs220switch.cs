﻿/**
 *  file       : id 20220826°1421 — gitlab.com/normai/cheeseburger …/cs/cs220switch.cs
 *  version    : • 20221001°1021 v0.1.8 Filling • 20220826°1421 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  summary    : This demonstrates ...
 */
using System;

class Program
{
   const string sVERSION = "v0.1.8";
   static string sDOTNETV = "(C# " + typeof(int).Assembly.ImageRuntimeVersion + ")";

   public static void Main(String[] args)
   {
      Console.WriteLine($"*** Aloha, o kēia `cs220switch.cs` {sVERSION} — Switch {sDOTNETV} ***");  // "Aloha, ʻo kēia"

      Random random = new Random();
      Console.WriteLine("Your dice is rolling ...");
      int randi = random.Next(1, 8);                                // Numbers 1 through 7
      switch (randi)
      {
         case 1 : Console.WriteLine("Look, a one"); break;
         case 2 : Console.WriteLine("Whoops, a two"); break;
         case 3 : Console.WriteLine("Uh-huh, a three"); break;
         case 4 : Console.WriteLine("Holla, a four"); break;
         case 5 : Console.WriteLine("Oh, a five"); break;
         case 6 : Console.WriteLine("Hooray, a six"); break;
         default : Console.WriteLine("Your dice seems broken, you rolled " + Convert.ToString(randi)); break;
      }

      Console.WriteLine("Aloha mai.");
   }
}
