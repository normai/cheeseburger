﻿/**
 *  file       : id 20221224°1221 — gitlab.com/normai/cheeseburger …/cs/cs715assert.cs
 *  version    : • 20221224°1217 v0.1.9 Filling • 20221224°1221 v0.1.6 Stub
 *  subject    : Demonstrate assertions
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  compile    : VS-Dev-Prompt > csc.exe cs715assert.cs -r:"C:\Program Files\Microsoft Visual Studio\2022\Community\Common7\IDE\PublicAssemblies\Microsoft.VisualStudio.QualityTools.UnitTestFramework.dll"
 *  run        : First provide the UnitTestFramework Dll with one of two options:
 *               1.1. Copy file Microsoft.VisualStudio.QualityTools.UnitTestFramework.dll into the folder with the executable
 *               1.2. Install Microsoft.VisualStudio.QualityTools.UnitTestFramework.dll
 *                  with admin rights into the Global Assembly Cache (see screenshot 20221224°1042):
 *                  > gacutil.exe /i "C:\Program Files\Microsoft Visual Studio\2022\Community\Common7\IDE\PublicAssemblies\Microsoft.VisualStudio.QualityTools.UnitTestFramework.dll"
 *               2. Then run the executable:
 *                   VS-Dev-Prompt > cs715assert.exe
 *  todo       : Use e.g. Assert.ThrowsException<ArgumentOutOfRangeException>(() => RollDummy(0));  // Since VS 2017
 *  status     :
 */
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;  // Assert

class Program
{
   const string sVERSION = "v0.1.9";
   static string sDOTNETV = "(C# " + typeof(int).Assembly.ImageRuntimeVersion + ")";

   public static void Main(String[] args)
   {
      Console.WriteLine($"*** Bu `cs715assert.cs` {sVERSION} — Assertions {sDOTNETV} ***");


      int i = 123;
      try {
         Assert.IsTrue(i > 100);                       // Nothing happens
         Assert.IsTrue(i < 100);
      }
      catch (Exception e) {
         Console.WriteLine($"Exception thrown: {e.Message}");
      }


      Console.WriteLine("Güle güle.");
   }
}
