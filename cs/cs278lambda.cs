﻿// EMPTY STUB, YET TO BE FILLED WITH CODE

/**
 *  file       : id 20221213°1721 — gitlab.com/normai/cheeseburger …/cs/cs278lambda.cs
 *  version    : • 20xxxxxx°xxxx v0.x.x Filling • 20221213°1721 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate lambda expressions
 *  userstory  :
 *  summary    :
 *  status     :
 *  ref        :
 */
using System;

class Program
{
   const string sVERSION = "v0.0.0";
   static string sDOTNETV = "(C# " + typeof(int).Assembly.ImageRuntimeVersion + ")";

   public static void Main(String[] args)
   {
      Console.WriteLine($"*** Bongiorno, sono `cs278lambda.cs` {sVERSION} — Lambda expressions {sDOTNETV} ***");





      Console.WriteLine("Arrivederci.");
   }
}
