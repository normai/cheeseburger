﻿// THIS FILE IS AN EMPTY STUB, IT IS YET TO BE FILLED WITH CODE

/**
 *  file       : id 20221116°1621 — gitlab.com/normai/cheeseburger …/cs/cs319kbd3menu.cs
 *  version    : • 20xxxxxx°xxxx v0.x.x Filling • 20221116°1621 v0.1.8 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate keyboard menu using a GUI function
 *  userstory  :
 *  summary    :
 */
using System;

class Program
{
    const string sVERSION = "v0.0.0";
    static string sDOTNETV = "(C# " + typeof(int).Assembly.ImageRuntimeVersion + ")";

    public static void Main(String[] args)
    {
        Console.WriteLine($"*** Aftó eínai to `cs319kbd3menu.cs` {sVERSION} — Keyboard menu (GUI function) {sDOTNETV} ***");  // "Αυτό είναι το [Aftó eínai to]"





        Console.WriteLine("Antio sas.");  // "Αντίο σας [Antio sas]"
    }
}
