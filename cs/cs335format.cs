﻿// THIS FILE IS AN EMPTY STUB, IT IS YET TO BE FILLED WITH CODE

/**
 *  file       : id 20220828°1021 — gitlab.com/normai/cheeseburger …/cs/cs260format.cs
 *  version    : • 20xxxxxx°xxxx v0.x.x Filling • 20220828°1021 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate C# string formatting
 *  summary    : This demonstrates string formatting in C#
 *  ref        : https://stackoverflow.com/questions/2954962/convert-integer-to-binary-in-c-sharp [ref 20220907°1312]
 *  ref        : https://www.delftstack.com/howto/csharp/convert-int-to-binary-in-csharp/ [ref 20220907°1313]
 *  ref        : https://gist.github.com/luizcentennial/c6353c2ae21815420e616a6db3897b4c [ref 20220907°1314] C# String Formatting Cheat Sheet
 *  ref        : https://docs.microsoft.com/en-us/dotnet/api/system.string.format?view=net-6.0 [ref 20220907°1315]
 *  ref        : https://docs.microsoft.com/en-us/dotnet/standard/base-types/padding [ref 20220907°1316]
 *  chain      : String formatting usage examples see file 20220826°1111 cs125bitwise.cs
 */
using System;

class Program
{
    const string sVERSION = "v0.0.0";
    static string sDOTNETV = "(C# " + typeof(int).Assembly.ImageRuntimeVersion + ")";

    public static void Main(String[] args)
    {
        Console.WriteLine($"*** Dobrý deň, toto je `cs260format.cs` {sVERSION} — Format functions {sDOTNETV} ***");





        Console.WriteLine("Dovidenia.");
    }
}
