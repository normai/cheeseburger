﻿// THIS FILE IS AN EMPTY STUB, IT IS YET TO BE FILLED WITH CODE

/**
 *  file       : id 20220828°0921 — gitlab.com/normai/cheeseburger …/cs/cs338stdio.cs
 *  version    : • 20xxxxxx°xxxx v0.x.x Filling • 20220828°0921 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  summary    : This demonstrates ...
 */
using System;

class Program
{
    const string sVERSION = "v0.0.0";
    static string sDOTNETV = "(C# " + typeof(int).Assembly.ImageRuntimeVersion + ")";

    public static void Main(String[] args)
    {
        Console.WriteLine($"*** Privet, eto `cs338stdio.cs` {sVERSION} — Standard I/O {sDOTNETV} ***");  // "Привет, это [Privet, eto]"





        Console.WriteLine("Do svidaniya.");  // "до свидания [Do svidaniya]."
    }
}
