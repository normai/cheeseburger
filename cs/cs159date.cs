﻿/**
 *  file       : id 20221012°1821 — gitlab.com/normai/cheeseburger …/cs/cs159date.cs
 *  version    : • 20221230°1411 v0.1.9 Filling • 20221012°1821 v0.1.8 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate Date and Time Types
 *  userstory  : Set and print dates in various formats and calculate time spans
 */
using System;

class Program
{
   const string sVERSION = "v0.1.9";
   static string sDOTNETV = "(C# " + typeof(int).Assembly.ImageRuntimeVersion + ")";

   public static void Main(String[] args)
   {
      Console.WriteLine($"*** Tere, see on `cs159date.cs` {sVERSION} — Date/Time Types {sDOTNETV} ***");
      Console.WriteLine();

      DateTime dtStart = DateTime.Now;                         // For the calculation far below

      // Limits
      Console.WriteLine("(1) DateTime limits:");
      DateTime dtMin = DateTime.MinValue;
      DateTime dtMax = DateTime.MaxValue;
      Console.WriteLine("   1. DT Min = " + dtMin);
      Console.WriteLine("   2. DT Max = " + dtMax);

      // Galileo
      Console.WriteLine("(2) Galileo birthday:");
      DateTime dtGal1 = new DateTime(1564, 2, 16);
      DateTime dtGal2 = new DateTime(1564, 2, 16, 13, 14, 15);
      DateTime dtGal3 = new DateTime(1564, 2, 16, 13, 14, 15, 123);
      String sCustom = dtGal3.ToString("yyyy'-'MM'-'dd'`'HH':'mm':'ss'.'ffffff");
      Console.WriteLine("    2. DT 1   = " + dtGal1 + "          -- date only");
      Console.WriteLine("    3. DT 2   = " + dtGal2 + "          -- with seconds");
      Console.WriteLine("    4. DT 3   = " + dtGal3 + "          -- with milliseconds");
      Console.WriteLine("    5. Custom = " + sCustom + "   -- with milliseconds");

      // Now
      Console.WriteLine("(3) Now:");
      DateTime dtNow = DateTime.Now;
      String sNow = dtNow.ToString("yyyy'-'MM'-'dd'`'HH':'mm':'ss'.'ffffff");
      Console.WriteLine("    2. LDT    = " + dtNow);
      Console.WriteLine("    3. Custom = " + sNow);

      // Halley
      Console.WriteLine("(4) Next Halley approach:");
      DateTime dtHal = new DateTime(2061, 7, 29);
      String sHalley = dtHal.ToString("yyyy'-'MM'-'dd'`'HH':'mm':'ss'.'ffffff");
      Console.WriteLine("    1. DT     = " + dtHal);
      Console.WriteLine("    2. Custom = " + sHalley);

      // Calculate large scale time span
      System.TimeSpan tsWait = dtHal - dtNow;
      Console.WriteLine($"(5) Wait for Halley : {tsWait.Days} days, {tsWait.Hours} h, {tsWait.Minutes} m, {tsWait.Seconds} s, {tsWait.Milliseconds} ms");


      // Calculate short scale time span
      // Empirical finding. Successive executions result in either 15 ms or
      //    0 ms, but noting in between [issue 20221230°1313 'cs millisecs']
      DateTime dtStop = DateTime.Now;
      System.TimeSpan dura = dtStop - dtStart;
      Console.WriteLine($"(6) Runtime duration : {dura} .. {dura.Milliseconds} ms");  // Microseconds μs available in .NET 7


      Console.WriteLine();
      Console.WriteLine("Hüvasti.");
   }
}
