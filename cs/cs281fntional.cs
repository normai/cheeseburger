﻿// EMPTY STUB, YET TO BE FILLED WITH CODE

/**
 *  file       : id 20221213°1821 — gitlab.com/normai/cheeseburger …/cs/cs281fntional.cs
 *  version    : • 20xxxxxx°xxxx v0.x.x Filling • 20221213°1821 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate functional programming
 *  userstory  :
 *  summary    :
 *  status     :
 *  ref        :
 */
using System;

class Program
{
   const string sVERSION = "v0.0.0";
   static string sDOTNETV = "(C# " + typeof(int).Assembly.ImageRuntimeVersion + ")";

   public static void Main(String[] args)
   {
      Console.WriteLine($"*** Kon'nichiwa, desu `cs281fntional.cs` {sVERSION} — Functional programming {sDOTNETV} ***");  // こんにちは、です [Kon'nichiwa, desu]





      Console.WriteLine("Sayōnara.");  // さようなら [Sayōnara]
   }
}
