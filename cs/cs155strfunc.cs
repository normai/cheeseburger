﻿/**
 *  file       : id 20221012°1721 — gitlab.com/normai/cheeseburger …/cs/cs155strfunc.cs
 *  version    : • 20221013°1121 v0.1.8 Filling • 20221012°1721 v0.1.7 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate String Functions
 *  compile    : On VS console: > csc.exe cs155strfunc.cs
 *  userstory  :
 *  summary    :
 */
using System;

class Program
{
   const string sVERSION = "v0.1.8";
   static string sDOTNETV = "(C# " + typeof(int).Assembly.ImageRuntimeVersion + ")";

   public static void Main(String[] args)
   {
      Console.WriteLine($"*** Hallo, dit is `cs155strfunc.cs` — String Functions {sDOTNETV} ***");

      // () Fodder
      String sWord = "SilzuZankunKrei";

      // (.) String Functions
      // (.1) String length
      Console.WriteLine("(3.1) Length of '" + sWord + "' is " + sWord.Length);

      // (.2) Character code to string
      Console.WriteLine("(3.2) Code to char : " + (char) 36 + (char) 37 + (char) 38 + (char) 96 + (char) 97 + (char) 98 + (char) 122 + (char) 123 + (char) 124 + (char) 125 + '\u007e' + '\u007f' + ".");

      // (.3) Substrings
      Console.WriteLine("(3.3) Substrings : " + sWord.Substring(0, 5) + " + " + sWord.Substring(5, sWord.Length - 4 - 5) + " + " + sWord.Substring(sWord.Length - 4));

      // (.4) Upper/Lower
      Console.WriteLine("(3.4) Upper/Lower : " + sWord.ToLower() + " / " + sWord.ToUpper());  // See ref 20221010°1052. There exist also ToLowerInvariant() and ToUpperInvariant()

      // (.5) Trim
      String sBreezy = "    Breeze    ";
      Console.WriteLine("(3.5) Trim \"" + sBreezy + "\" : \"" + sBreezy.TrimStart() + "\", \"" + sBreezy.TrimEnd() + "\", \"" + sBreezy.Trim() + "\"");




      Console.WriteLine("Tot ziens.");
   }
}
