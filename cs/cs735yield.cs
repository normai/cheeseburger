﻿// EMPTY STUB, YET TO BE FILLED WITH CODE

/**
 *  file       : id 20221227°1021 — gitlab.com/normai/cheeseburger …/cs/cs735yield.cs
 *  version    : • 20xxxxxx°xxxx v0.x.x Filling • 20221227°1021 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate the yield keyword
 *  status     :
 */
using System;

class Program
{
   const string sVERSION = "v0.0.0";
   static string sDOTNETV = "(C# " + typeof(int).Assembly.ImageRuntimeVersion + ")";

   public static void Main(String[] args)
   {
      Console.WriteLine($"*** Tse `cs735yield.cs` {sVERSION} — Yield {sDOTNETV} ***");  // "Це [Tse]"





      Console.WriteLine("Do pobachennya.");  // "до побачення [Do pobachennya]."
   }
}
