﻿/**
 *  file       : id 20221116°1421 — gitlab.com/normai/cheeseburger …/cs/cs311kbd1menu.cs
 *  version    : • 20221117°0914 v0.1.8 Filling • 20221116°1421 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate keyboard menu with pressing Enter
 *  userstory  :
 *  summary    :
 *  status     : Good enough.
 *  ref        :
 */
using System;

class Program
{
   const string sVERSION = "v0.1.8";
   static string sDOTNETV = "(C# " + typeof(int).Assembly.ImageRuntimeVersion + ")";

   public static void Main(String[] args)
   {
      Console.WriteLine($"*** Hei, tämä on `cs311kbd1menu.cs` {sVERSION} — Keyboard menu (with Enter) {sDOTNETV} ***");

      bool bContinue = true;
      while (bContinue)
      {
         Console.WriteLine("Menu: a = Anton, k = Kylie, s = Santa, x = Exit");
         String sKey = Console.ReadLine();

         switch (sKey)
         {
             case "a" :
                Console.WriteLine("This is Anton speaking");
                break;
             case "k" :
                Console.WriteLine("This is Kylie singing");
                break;
             case "s" :
                Console.WriteLine("This is Santa Claus ringing");
                break;
             case "x" :
                bContinue = false;
                break;
             default :
                Console.WriteLine("Invalid selection: \"" + sKey + "\"");
                break;
         }
      }

      Console.WriteLine("Näkemiin.");
   }
}
