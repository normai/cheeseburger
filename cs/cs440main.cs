﻿// THIS FILE IS AN EMPTY STUB, IT IS YET TO BE FILLED WITH CODE

/**
 *  file       : id 20220828°1421 — gitlab.com/normai/cheeseburger …/cs/cs440main.cs
 *  version    : • 20xxxxxx°xxxx v0.x.x Filling • 20220828°1421 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  summary    : This demonstrates ...
 */
using System;

class Program
{
    const string sVERSION = "v0.0.0";
    static string sDOTNETV = "(C# " + typeof(int).Assembly.ImageRuntimeVersion + ")";

    public static void Main(String[] args)
    {
        Console.WriteLine($"*** Hej, det här är `cs440main.cs` {sVERSION} — Main function {sDOTNETV} ***");





        Console.WriteLine("Adjö.");
    }
}
