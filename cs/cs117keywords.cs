﻿/**
 *  file       : id 20221217°0921 — gitlab.com/normai/cheeseburger …/cs/cs117keywords.cs
 *  version    : • 20221227°0921 v0.1.9 Filling • 20221217°0921 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate keywords, here using the contextual keyword 'yield' as variable name
 *  status     :
 */
using System;
using System.Collections.Generic;

// Workaround on compiler error after [ref 20221227°0918]
//  https://stackoverflow.com/questions/64749385/predefined-type-system-runtime-compilerservices-isexternalinit-is-not-defined
namespace System.Runtime.CompilerServices {
   internal static class IsExternalInit {}  // Dummy class to prevent compiler error "CS0518 Predefined type …"
}

class Program
{
   const string sVERSION = "v0.1.9";
   static string sDOTNETV = "(C# " + typeof(int).Assembly.ImageRuntimeVersion + ")";

   // Compare snippet in middle [ref 20221227°0912]
   //  https://learn.microsoft.com/en-us/dotnet/csharp/language-reference/statements/yield
   public readonly record struct Numbers()
   {
      public IEnumerator<int> GetEnumerator()
      {
         int yield = 12;
         yield return yield;                           // Contextual keyword used plus abused
         yield return 345;
         yield return 6789;
      }
   }


   public static void Main(String[] args)
   {
      Console.WriteLine($"*** Ko tenei `cs117keywords.cs` {sVERSION} — Keywords {sDOTNETV} ***");


      int yield = 5;                                   // Contextual keyword used as identifier
      Console.WriteLine($"(1) Contextual keyword name = {nameof(yield)}, value = {yield}");

      Console.WriteLine("(2) Retrieve values from an IEnumerator via 'yield':");
      Numbers numbers = new Numbers();
      foreach (int i in numbers) {
         Console.WriteLine($" - {i}");
      }


      Console.WriteLine("Tēnā koe.");
   }
}
