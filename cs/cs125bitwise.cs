﻿/**
 *  file       : id 20220826°1121 — gitlab.com/normai/cheeseburger …/cs/cs125bitwise.cs
 *  version    : • 20220908°1731 v0.1.7 Filling • 20220826°1121 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    :
 *  summary    : This demonstrates ...
 *  ref        : See file 20220828°1015 cs335format.cs
 *  ref        :
 */
using System;

class Program
{
    const string sVERSION = "v0.1.7";
    static string sDOTNETV = "(C# " + typeof(int).Assembly.ImageRuntimeVersion + ")";

    public static void Main(String[] args)
    {
        Console.WriteLine($"*** Hei, tämä on `cs125bitwise.cs` {sVERSION} — Bitwise operators {sDOTNETV} ***");


        int i1 = 43690;
        int i2 = 3855;

        Console.WriteLine("(1) Bitwise AND");
        Console.WriteLine("(1.1) i1       =  " + String.Format("{0}", Convert.ToString(i1, 2)).PadLeft(33)      + " " + String.Format("{0:X}", i1).PadLeft(10)      + " " + String.Format("{0:D}", i1).PadLeft(8));
        Console.WriteLine("(1.2) i2       =  " + String.Format("{0}", Convert.ToString(i2, 2)).PadLeft(33)      + " " + String.Format("{0:X}", i2).PadLeft(10)      + " " + String.Format("{0:D}", i2).PadLeft(8));
        Console.WriteLine("(1.3) i1 & i2  =  " + String.Format("{0}", Convert.ToString(i1 & i2, 2)).PadLeft(33) + " " + String.Format("{0:X}", i1 & i2).PadLeft(10) + " " + String.Format("{0:D}", i1 & i2).PadLeft(8));

        Console.WriteLine("(2) Bitwise OR");
        Console.WriteLine("(2.1) i1       =  " + String.Format("{0}", Convert.ToString(i1, 2)).PadLeft(33)      + " " + String.Format("{0:X}", i1).PadLeft(10)      + " " + String.Format("{0:D}", i1).PadLeft(8));
        Console.WriteLine("(2.2) i2       =  " + String.Format("{0}", Convert.ToString(i2, 2)).PadLeft(33)      + " " + String.Format("{0:X}", i2).PadLeft(10)      + " " + String.Format("{0:D}", i2).PadLeft(8));
        Console.WriteLine("(2.3) i1 | i2  =  " + String.Format("{0}", Convert.ToString(i1 | i2, 2)).PadLeft(33) + " " + String.Format("{0:X}", i1 | i2).PadLeft(10) + " " + String.Format("{0:D}", i1 | i2).PadLeft(8));

        Console.WriteLine("(3) Bitwise XOR");
        Console.WriteLine("(3.1) i1       =  " + String.Format("{0}", Convert.ToString(i1, 2)).PadLeft(33)      + " " + String.Format("{0:X}", i1).PadLeft(10)      + " " + String.Format("{0:D}", i1).PadLeft(8));
        Console.WriteLine("(3.2) i2       =  " + String.Format("{0}", Convert.ToString(i2, 2)).PadLeft(33)      + " " + String.Format("{0:X}", i2).PadLeft(10)      + " " + String.Format("{0:D}", i2).PadLeft(8));
        Console.WriteLine("(3.3) i1 ^ i2  =  " + String.Format("{0}", Convert.ToString(i1 ^ i2, 2)).PadLeft(33) + " " + String.Format("{0:X}", i1 ^ i2).PadLeft(10) + " " + String.Format("{0:D}", i1 ^ i2).PadLeft(8));

        Console.WriteLine("(4) Bitwise NOT");
        Console.WriteLine("(4.1) i1       =  " + String.Format("{0}", Convert.ToString(i1, 2)).PadLeft(33)      + " " + String.Format("{0:X}", i1).PadLeft(10)      + " " + String.Format("{0:D}", i1).PadLeft(8));
        Console.WriteLine("(4.2) ~i1      =  " + String.Format("{0}", Convert.ToString(~i1, 2)).PadLeft(33)     + " " + String.Format("{0:X}", ~i1).PadLeft(10)     + " " + String.Format("{0:D}", ~i1).PadLeft(8));

        Console.WriteLine("(5) Bit Shift Left");
        Console.WriteLine("(5.1) i1       =  " + String.Format("{0}", Convert.ToString(i1, 2)).PadLeft(33)      + " " + String.Format("{0:X}", i1).PadLeft(10)      + " " + String.Format("{0:D}", i1).PadLeft(8));
        Console.WriteLine("(5.2) i1 << 1  =  " + String.Format("{0}", Convert.ToString(i1 << 1, 2)).PadLeft(33) + " " + String.Format("{0:X}", i1 << 1).PadLeft(10) + " " + String.Format("{0:D}", i1 << 1).PadLeft(8));

        Console.WriteLine("(6) Bit Shift Right");
        Console.WriteLine("(6.1) i1       =  " + String.Format("{0}", Convert.ToString(i1, 2)).PadLeft(33)      + " " + String.Format("{0:X}", i1).PadLeft(10)      + " " + String.Format("{0:D}", i1).PadLeft(8));
        Console.WriteLine("(6.2) i1 >> 1  =  " + String.Format("{0}", Convert.ToString(i1 >> 1, 2)).PadLeft(33) + " " + String.Format("{0:X}", i1 >> 1).PadLeft(10) + " " + String.Format("{0:D}", i1 >> 1).PadLeft(8));

        Console.WriteLine("Näkemiin.");
    }
}
