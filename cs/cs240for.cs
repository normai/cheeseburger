﻿/**
 *  file       : id 20220826°1721 — gitlab.com/normai/cheeseburger …/cs/cs240for.cs
 *  version    : • 20220919°1921 v0.1.8 Filling • 20220826°1721 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate the for loop
 *  ref        : https://learn.microsoft.com/en-us/dotnet/api/system.math.pow?view=net-6.0 []
 */
using System;

class Program
{
   const string sVERSION = "v0.1.8";
   static string sDOTNETV = "(C# " + typeof(int).Assembly.ImageRuntimeVersion + ")";

   public static void Main(String[] args)
   {
      Console.WriteLine($"*** Halo, ini adalah `cs240for.cs` {sVERSION} — For-Loop {sDOTNETV} ***");

      Console.WriteLine("(1) Two power 6 through 10");
      for (int i = 6; i <= 10; i++)
      {
         int iPow = (int) Math.Pow(2, i);
         Console.WriteLine("   - " + i.ToString() + " " + iPow.ToString());
      }

      Console.WriteLine("(2) And the same backward");
      for (int i = 10; i >= 6; i--)
      {
         int iPow = (int) Math.Pow(2, i);
         Console.WriteLine("   - " + i.ToString() + " " + iPow.ToString());
      }

      Console.WriteLine("Alavida.");  // "अलविदा " does not print nice
   }
}
