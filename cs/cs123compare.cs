﻿/**
 *  file       : id 20220826°1021 — gitlab.com/normai/cheeseburger …/cs/cs123compare.cs
 *  version    : • 20220908°1631 v0.1.7 Filling • 20220826°1021 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : This demonstrates comparison operators in C#
 *  summary    :
 *  ref        : https://www.w3schools.com/cs/cs_operators.php [ref 20220906°1342] Gerneral operator overview
 *  ref        :
 *  ref        :
 */
using System;

class Program
{
    const string sVERSION = "v0.1.7";
    static string sDOTNETV = "(C# " + typeof(int).Assembly.ImageRuntimeVersion + ")";

    public static void Main(String[] args)
    {
        const double THRESHOLD_1 = .000_000_000_000_000_1; // Problem — The tolerance has to be different, depending on the magnitude of the number!

        Console.WriteLine($"*** Tere, see on `cs123compare.cs` {sVERSION} — Comparisons {sDOTNETV} ***");

        Console.WriteLine("(1.1)  \"Ahoj\" == \"Aloha\"         = " + ("Ahoj" == "Aloha"));
        Console.WriteLine("(1.2)  12345  == 23456           = " + (12345  == 23456));
        Console.WriteLine("(1.3)  3.4567 == 4.5678          = " + (3.4567 == 4.5678));
        Console.WriteLine("(1.4)  true   == false           = " + (true   == false));
        Console.WriteLine();
        Console.WriteLine("(2.1)  \"Ahoj\" != \"Aloha\"         = " + ("Ahoj" != "Aloha"));
        Console.WriteLine("(2.2)  12345  != 23456           = " + (12345  != 23456));
        Console.WriteLine("(2.3)  3.4567 != 4.5678          = " + (3.4567 != 4.5678));
        Console.WriteLine("(2.4)  true   != false           = " + (true   != false));
        Console.WriteLine();
        Console.WriteLine("(3.1)  \"Ahoj\".CompareTo(\"Aloha\") = " + "Ahoj".CompareTo("Aloha"));
        Console.WriteLine("(3.2)  12345  >  23456           = " + (12345  > 23456));
        Console.WriteLine("(3.3)  3.4567 >  4.5678          = " + (3.4567 > 4.5678));
//      Console.WriteLine("(3.4)  true   >  false           = " + (true   > false));
        Console.WriteLine();
//      Console.WriteLine("(4.1)  \"Ahoj\" >= \"Aloha\"         = " + ("Ahoj" >= "Aloha"));
        Console.WriteLine("(4.2)  12345  >= 23456           = " + (12345  >= 23456));
        Console.WriteLine("(4.3)  3.4567 >= 4.5678          = " + (3.4567 >= 4.5678));
//      Console.WriteLine("(4.4)  true   >= false           = " + (true   >= false));
        Console.WriteLine();
//      Console.WriteLine("(5.1)  \"Ahoj\" <  \"Aloha\"         = " + ("Ahoj" < "Aloha"));
        Console.WriteLine("(5.2)  12345  <  23456           = " + (12345  < 23456));
        Console.WriteLine("(5.3)  3.4567 <  4.5678          = " + (3.4567 < 4.5678));
//      Console.WriteLine("(5.4)  true   <  false           = " + (true   < false));
        Console.WriteLine();
//      Console.WriteLine("(6.1)  \"Ahoj\" >= \"Aloha\"         = " + ("Ahoj" >= "Aloha"));
        Console.WriteLine("(6.2)  12345  >= 23456           = " + (12345  >= 23456));
        Console.WriteLine("(6.3)  3.4567 >= 4.5678          = " + (3.4567 >= 4.5678));
//      Console.WriteLine("(6.4)  true   >= false           = " + (true   >= false));
        Console.WriteLine();
        Console.WriteLine("(7.1)  0.1 + 0.2 == 0.3          = " + (0.1 + 0.2 == 0.3));
        Console.WriteLine("(7.2)  Math.Abs((0.1 + 0.2) - 0.3) < THRESHOLD_1 = " + (Math.Abs((0.1 + 0.2) - 0.3) < THRESHOLD_1));
        Console.WriteLine();
        Console.WriteLine("(8.1)  \"Ahoj\".CompareTo(\"Aloha\") = " + "Ahoj".CompareTo("Aloha"));
        Console.WriteLine("(8.1)  \"Aloha\".CompareTo(\"Ahoj\") = " + "Aloha".CompareTo("Ahoj"));
        Console.WriteLine("(8.1)  \"Ahoj\".CompareTo(\"Ahoj\")  = " + "Ahoj".CompareTo("Ahoj"));

        Console.WriteLine("Hüvasti.");
    }
}
