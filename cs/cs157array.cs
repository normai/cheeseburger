﻿/**
 *  file       : id 20220831°0821 — gitlab.com/normai/cheeseburger …/cs/cs157array.cs
 *  version    : • 20220908°1111 v0.1.7 Filling • 20220831°0821 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate C# arrays
 *  summary    : This demonstrates C# arrays in three servings:
 *                (1) Plain array with single-elements-definition
 *                (2) Plain array with bulk definition
 *                (3) List class, the nearest container class array equivalent
 *  todo       : Possibly also provide a serving for an unmanaged array, then using Marshal.SizeOf() [todo 20220902°1121]
 */
using System;                                                          // Console, Environment.NewLine
using System.Collections.Generic;                                      // List

class Program
{
   const string sVERSION = "v0.1.7";
   static string sDOTNETV = "(C# " + typeof(int).Assembly.ImageRuntimeVersion + ")";

    public static void Main(String[] args)
    {
        Console.WriteLine($"*** Zdraveĭte `cs157array.cs` {sVERSION} — Arrays {sDOTNETV} ***");  // "Здравейте [Zdraveĭte]"

        // =====================================================
        // (1) Archaic array, separate declaration plus bulky single-elements-definition
        // (1.1) Define some array
        string[] mountains = new string[4];
        mountains[0] = "Zugspitze";
        mountains[1] = "Schneefernerkopf";
        mountains[2] = "Weather spike";
        mountains[3] = "Middle Höllental Peak";                        // BTW. "Hochwanner" is missing

        // (1.2) Retrieve size info
        Console.WriteLine( "(1.2.1) Mountains array memory consumption: "  // Memory consumption of managed array is hard to retrieve
                          /* + Marshal.SizeOf(mountains) */ + "?" + " bytes"  // Marshal.SizeOf() works only with unmanaged objects
                           );
        int iNumberOfMountains = mountains.Length;
        Console.WriteLine("(1.2.2) Number of mountains: " + iNumberOfMountains);

        // (1.3) Iterate over and output all elements
        for (int i = 0; i < iNumberOfMountains; i++)
        {
           Console.WriteLine(" - " + mountains[i]);
        }

        // =====================================================
        // (2) Archaic array, declaration and definition in one line
        // (2.1) Define some array
        string[] rivers = {"Danube", "Elbe", "Main", "Rhine", "Weser"};

        // (2.2) Retrieve size info
        Console.WriteLine("(2.2.1) Rivers array memory consumption: ? bytes");  // This information is hard to retrieve for managed objects
        int iNumberOfRivers = rivers.Length;
        Console.WriteLine("(2.2.2) Number of rivers: " + iNumberOfRivers);

        // (2.3) Iterate over and output all elements
        for (int i = 0; i < rivers.Length; i++)
        {
           Console.WriteLine(" - " + rivers[i]);
        }

        // =====================================================
        // (3) List class, the nearest container class array equivalent
        // (3.1) Create list of lakes
        List<String> lakes = new List<String>();
        lakes.Add("Bodensee");
        lakes.Add("Müritz");
        lakes.Add("Chiemsee");
        lakes.Add("Schweriner See");
        lakes.Add("Starnberger See");
        lakes.Add("Ammersee");

        // (3.2) Retrieve size info
        Console.WriteLine( "(3.2.1) Lakes vector memory consumption: "
                          /* + sizeof(lakes) */ + "?" + " bytes"
                           );
        int iNumberOfLakes = lakes.Count;
        Console.WriteLine("(3.2.2) Number of lakes: " + iNumberOfLakes);

        // (3.3) Iterate over and output all elements
        foreach (string s in lakes)
        {
           Console.WriteLine(" - " + s);
        }

        Console.WriteLine("Dovizhdane.");  // "Довиждане [Dovizhdane]."
    }
}
