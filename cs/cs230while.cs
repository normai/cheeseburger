﻿/**
 *  file       : id 20220826°1521 — gitlab.com/normai/cheeseburger …/cs/cs230while.cs
 *  version    : • 20220910°1021 v0.1.7 Filling • 20220826°1521 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrates C# While Loop
 *  summary    :
 *  ref        : About random see cs761random.cs
 *  ref        :
 */

using System;

class Program
{
   const string sVERSION = "v0.1.7";
   static string sDOTNETV = "(C# " + typeof(int).Assembly.ImageRuntimeVersion + ")";

   public static void Main(String[] args)
   {
      Console.WriteLine($"*** Hailo, yah hai `cs230while.cs` {sVERSION} — While-Loop {sDOTNETV} ***");  // "*** हैलो, यह है  [Hailo, yah hai]" is not printed nice

      // (1) Roll dice first time
      Console.WriteLine("Roll the number six!");
      Random random = new Random();
      int iDicenum = random.Next(1, 7);
      int iTimes = 1;
      Console.WriteLine("Your first number is " + Convert.ToString(iDicenum));

      // (2) Continue as long no six is rolled
      while (iDicenum != 6)
      {
         iDicenum = random.Next(1, 7);
         iTimes += 1;
         Console.WriteLine(" - next number " + Convert.ToString(iDicenum));
      }

      Console.WriteLine("You rolled " + Convert.ToString(iTimes) + " times to yield the six.");

      Console.WriteLine("Alavida.");  // "अलविदा  [Alavida]." is not printed nice
   }
}
