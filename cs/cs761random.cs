﻿// THIS FILE IS AN EMPTY STUB, IT IS YET TO BE FILLED WITH CODE

/**
 *  file       : id 20220910°1321 — gitlab.com/normai/cheeseburger …/cs/cs761random.cs
 *  version    : • 20xxxxxx°xxxx v0.x.x Filling • 20220910°1321 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate C# Random Functions
 *  summary    :
 *  ref        : https://learn.microsoft.com/en-us/dotnet/api/system.random.next?view=net-7.0 []
 *  ref        : https://stackify.com/csharp-random-numbers/ [ref 20220910°1117]
 *  ref        : https://code-maze.com/csharp-convert-int-to-string/ [ref 20220910°1122]
 *  ref        : https://github.com/CodeMazeBlog/CodeMazeGuides [ref 20220910°1125]
 */
using System;

class Program
{
    const string sVERSION = "v0.0.0";
    static string sDOTNETV = "(C# " + typeof(int).Assembly.ImageRuntimeVersion + ")";

    public static void Main(String[] args)
    {
        Console.WriteLine($"*** Privet, eto `cs761random.cs` {sVERSION} — Random Functions {sDOTNETV} ***");  // "Привет, это [Privet, eto]"





        Console.WriteLine("Do svidaniya.");  // "до свидания [Do svidaniya]."
    }
}
