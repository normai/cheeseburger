﻿// THIS FILE IS AN EMPTY STUB, IT IS YET TO BE FILLED WITH CODE

/**
 *  file       : id 20220830°1621 — gitlab.com/normai/cheeseburger …/cs/cs720global.cs
 *  version    : • 20xxxxxx°xxxx v0.x.x Filling • 20220830°1621 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  summary    : This demonstrates ...
 */
using System;

class Program
{
    const string sVERSION = "v0.0.0";
    static string sDOTNETV = "(C# " + typeof(int).Assembly.ImageRuntimeVersion + ")";

    public static void Main(String[] args)
    {
        Console.WriteLine($"*** Sveiki, šis ir `cs720global.cs` — Global keyword {sDOTNETV} ***");





        Console.WriteLine("Uz redzēšanos.");
    }
}
