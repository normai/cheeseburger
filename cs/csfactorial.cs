﻿/**
 *  file       : id 20221215°1121 — gitlab.com/normai/cheeseburger …/cs/csfactorial.cs
 *  version    : • 20221216°0923 v0.1.8 Filling • 20221215°1121 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate factorial calculation
 *  status     : Too complicated. The checking is not yet correct.
 *  note       : With a long, the last correct value is: 20! = 2_432_902_008_176_640_000
 *  note       : long.MaxValue = 9223372036854775807
 */

using System;
using System.Collections.Generic;                                      // For the delegates list

delegate long Factorial(long x);

class Program
{
   const string sVERSION = "v0.1.8";
   static string sDOTNETV = "(C# " + typeof(int).Assembly.ImageRuntimeVersion + ")";

   // Function 20221215°1311 Traditional loop
   static long Factorial1(long iFact) {
      long iVal = 1;
      long iCnt = 0;
      while (iCnt <= iFact) {
         iVal = iVal * (iCnt < 1 ? 1 : iCnt);
         iCnt++;
      }
      return iVal;
   }

   // Function 20221215°1321 Recursion naive
   static long Factorial2(long iVal) {
      if (iVal > 1) {
         iVal = iVal * Factorial2(iVal - 1);
         return iVal;
      }
      else {
         return 1;
      }
   }

   // Function 20221215°1331 Recursion trying to check
   // Note: The checking is not yet correct
   static long Factorial3(long iVal) {
      if (iVal > 1) {
         try {
            iVal = checked (iVal * Factorial3(iVal - 1));
            return iVal;
         }
         catch(Exception) {
            return -1;
         }
      }
      else if (iVal < 0) {                             //
         return -1;                                    //
      }                                                //
      else {
         return 1;
      }
   }

   public static void Main(String[] args)
   {
      Console.WriteLine($"*** Sveiki, tai `csfactorial.cs` {sVERSION} — Factorial {sDOTNETV} ***");

      Factorial f1 = Program.Factorial1;
      Factorial f2 = Program.Factorial2;
      Factorial f3 = Program.Factorial3;
      List<Factorial> facts = new List<Factorial> {f1, f2, f3};

      int iCnt = 1;
      foreach (var f in facts) {
         Console.WriteLine($"({iCnt}) Factorial series {iCnt++}:");
         for (long iFact = -1; iFact < 24 ; iFact++) {
            long iResult = f(iFact);
            ////if (iFact > 4 && (iFact < 8 || iFact > 17)) {          // Skip boring ranges
            if (iFact < 8 || iFact > 17) {                             // Skip boring ranges
               Console.WriteLine($" — {iFact}! = {iResult}");
            }
         }
         Console.WriteLine();
      }

      Console.WriteLine("Iki pasimatymo.");
   }
}
