/**
 *  file       : id 20220215°0921 — gitlab.com/normai/cheeseburger …/cs/cs113aloha.cs
 *  version    : • 20220908°0811 v0.1.7 Filling • 20220215°0921 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate basic program building elements
 *  summary    : This demonstrates basic elements like • Variables • Operators • Strings • Built-in function calls
 *  reference  : https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/strings/ [ref 20220806°1224]
 */

using System;                                                  // Console

class Program
{
   const string sVERSION = "v0.1.7";
   static string sDOTNETV = "(C# " + typeof(int).Assembly.ImageRuntimeVersion + ")";

   public static void Main(string[] args)
   {
      Console.WriteLine($"*** O kēia `cs113aloha.cs` {sVERSION} — Aloha {sDOTNETV} ***");   // "ʻO kēia"

      // Define some variables and use an operator
      int a = 123;
      int b = 345;
      int c = a + b;

      // Output with a bulky statement
      Console.WriteLine( "(1) Sum of " + Convert.ToString(a)
                        + " and " + Convert.ToString(b)
                         + " is " + Convert.ToString(c)
                          );

      // Output with a summarized statement
      string s = "(2) Sum of " + Convert.ToString(a)
                + " and " + Convert.ToString(b)
                 + " is " + Convert.ToString(c)
                  ;
      Console.WriteLine(s);

      // Output numbers in various notations
      string sBin = Convert.ToString(a * -1, 2);
      string sHex = Convert.ToString(a, 16);
      Console.WriteLine("(3) Bin : " + sBin + ", Hex : " + sHex);

      // Show difference between integers, floats and strings
      Console.WriteLine("(4) Get variable types:");
      int i = 12345;
      double f = 12.345;
      s = "A character chain";
      Console.WriteLine(" - i = " + Convert.ToString(i) + " " + i.GetType().Name);
      Console.WriteLine(" - f = " + Convert.ToString(f) + " " + f.GetType().Name);
      Console.WriteLine(" - s = \"" + Convert.ToString(s) + "\" " + s.GetType().Name);

      Console.WriteLine("Aloha mai.");
   }
}
