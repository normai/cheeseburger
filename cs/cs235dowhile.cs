﻿/**
 *  file       : id 20220826°1621 — gitlab.com/normai/cheeseburger …/cs/cs235dowhile.cs
 *  version    : • 20221001°1121 v0.1.8 Filling • 20220826°1621 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Foot-controlled Do/While loop
 */
using System;

class Program
{
   const string sVERSION = "v0.1.8";
   static string sDOTNETV = "(C# " + typeof(int).Assembly.ImageRuntimeVersion + ")";

   public static void Main(String[] args)
   {
      Console.WriteLine($"*** Hello, ez `cs235dowhile.cs` {sVERSION} — Do/While-Loop {sDOTNETV} ***");

      Console.WriteLine("Loop until you rolled a six :");
      Random random = new Random();
      int randi;                                                   // Variable needs still be known behind the do block
      do
      {
         randi = random.Next(1, 7);                                // Numbers 1 through 6
         Console.WriteLine(" — You rolled " + randi);
      }
      while (randi != 6);

      Console.WriteLine("Viszlát.");
   }
}
