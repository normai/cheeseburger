﻿/**
 *  file       : id 20220828°1921 — gitlab.com/normai/cheeseburger …/cs/cs165dict.cs
 *  version    : • 20220908°1411 v0.1.7 Filling • 20220828°1921 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  summary    : This demonstrates a Dictionary in C#
 *  ref        : https://www.c-sharpcorner.com/uploadfile/mahesh/dictionary-in-C-Sharp/
 *  ref        : https://www.w3schools.blog/c-sharp-dictionary [] -- Note, this is not the usual W3Schools site
 *  ref        : https://www.w3schools.com/asp/asp_ref_dictionary.asp [] -- The W3Schools C# section has no Dictionary chapter, only the ASP section
 *  ref        : https://www.delftstack.com/howto/csharp/how-to-convert-int-to-string-in-csharp/ [] -- Lists five ways to convert Int to String
 */
using System;
using System.Collections.Generic;

class Program
{
    const string sVERSION = "v0.1.7";
    static string sDOTNETV = "(C# " + typeof(int).Assembly.ImageRuntimeVersion + ")";

    public static void Main(String[] args)
    {
        Console.WriteLine($"*** Hej, das ist `cs165dict.cs` {sVERSION} — Dictionaries {sDOTNETV} ***");

        // (1) Create
        Dictionary<string, string> being = new Dictionary<string, string>();
        being.Add("kingdom", "Animals");
        being.Add("class", "Reptiles");
        being.Add("genus", "Crocodile");
        being.Add("species", "Nile crocodile");
        being.Add("name", "Schnappi");
        being.Add("color", "Green");
        being.Add("legs", "Four");
        being.Add("food", "Meat");
        Console.WriteLine("(1) Created = " + being.ToString());

        // (2) Info
        Console.WriteLine("(2) Dictionary size = " + being.Count.ToString());

        // (3) Iterate
        Console.WriteLine("(3) Iterate:");
        foreach (KeyValuePair<string,string> kv in being)
        {
            Console.WriteLine(string.Format(" - {0} : {1}", kv.Key.PadRight(8), kv.Value));
        }

        Console.WriteLine("Farvel.");
    }
}
