﻿// THIS FILE IS AN EMPTY STUB, IT IS YET TO BE FILLED WITH CODE

/**
 *  file       : id 20220830°1521 — gitlab.com/normai/cheeseburger …/cs/cs724range.cs
 *  version    : • 20xxxxxx°xxxx v0.x.x Filling • 20220830°1521 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  summary    : This demonstrates ...
 */
using System;

class Program
{
    const string sVERSION = "v0.0.0";
    static string sDOTNETV = "(C# " + typeof(int).Assembly.ImageRuntimeVersion + ")";

    public static void Main(String[] args)
    {
        Console.WriteLine($"*** Kon'nichiwa, desu `cs724range.cs` {sVERSION} — Ranges .. {sDOTNETV} ***");  // "こんにちは、です [Kon'nichiwa, desu]"





        Console.WriteLine("Sayōnara.");  // "さようなら [Sayōnara]."
    }
}
