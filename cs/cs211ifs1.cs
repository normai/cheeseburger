﻿/**
 *  file       : id 20220826°1221 — gitlab.com/normai/cheeseburger …/cs/cs211ifs1.cs
 *  version    : • 20220922°1821 v0.1.8 Filling • 20220826°1221 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate branching with if, else if, else
 *  summary    : This demonstrates If/ElseIf/Else with four cases:
 *                • Unilateral branching
 *                • Bilateral branching
 *                • Multilateral branching
 *                • Wrong branching logic
 *               The sequence goes like this (1) Create random integer (2) Let the user
 *               overwrite this number (3) Do four different evaluations on that number
 *  usage      : VS Developer Command Prompt "csc.exe cs211ifs1.cs", then "cs211ifs1.exe"
 */
using System;

class Program
{
   const string sVERSION = "v0.1.8";
   static string sDOTNETV = "(C# " + typeof(int).Assembly.ImageRuntimeVersion + ")";

   public static void Main(String[] args)
   {
      Console.WriteLine($"*** Hallo, hier ist `cs211ifs1.cs` {sVERSION} — If/ElseIf/Else {sDOTNETV} ***");

      // (A.1) Preparation — Get a random number rolled
      Random random = new System.Random();
      int randi = random.Next(1, 7);                                       // From 1 though 6
      Console.WriteLine("You rolled number " + randi.ToString());

      // (A.2) Optionally allow to manually choose the number
      bool bHelp = false;                                                  // Avoid warning 'Unreachable code detected'
      if (bHelp) {                                                         // Optionally toggle this
         Console.WriteLine("Overwrite the rolled number with 1..6 >");
         String s = Console.ReadLine();
         try {
            randi = int.Parse(s);
            randi = randi < 1 ? 1 : randi;
            randi = randi > 6 ? 6 : randi;
         }
         catch (Exception xcp) {
            Console.WriteLine("Your input was not a number. " + xcp.Message);
         }
         Console.WriteLine("Your number now is '" + randi.ToString() + "'.");
      }

      // (B.1) Unilateral branching
      Console.WriteLine("(1) Unilateral branching");
      if (randi > 3) {
         Console.WriteLine("(1.1) : Your number is bigger than three");
      }

      // (B.2) Two-way branching
      Console.WriteLine("(2) Bilateral branching");
      if (randi == 3) {
         Console.WriteLine("(2.1) : The number is three");
      }
      else {
         Console.WriteLine("(2.2) : The number is not three");
      }

      // (B.3) Multilateral branching — Only one branch will be executed
      Console.WriteLine("(3) Multilateral branching");
      if (randi > 5) {
         Console.WriteLine("(3.1) : The number is six");
      }
      else if (randi > 4) {
         Console.WriteLine("(3.2) : The number is five");
      }
      else if (randi > 3) {
         Console.WriteLine("(3.3) : The number is four");
      }
      else if (randi > 2) {
         Console.WriteLine("(3.4) : The number is three");
      }
      else {
         Console.WriteLine("(3.5) : The number is one or two");
      }

      // (B.4) A typical beginners mistake, looks very similar like above,
      //  has multiple if instead elif statements, will not work as intended.
      Console.WriteLine("(4) Wrong logic");
      if (randi > 5) {
         Console.WriteLine("(4.1) : The number is six");
      }
      if (randi > 4) {
         Console.WriteLine("(4.2) : The number is five or six");
      }
      if (randi > 3) {
         Console.WriteLine("(4.3) : The number is four to six");
      }
      if (randi > 2) {
         Console.WriteLine("(4.4) : The number is three to six");
      }
      else {
         Console.WriteLine("(4.5) : The number is one or two");
      }

      Console.WriteLine("Auf Wiedersehen.");
   }
}
