﻿// THIS FILE IS AN EMPTY STUB, IT IS YET TO BE FILLED WITH CODE

/**
 *  file       : id 20221001°1421 — gitlab.com/normai/cheeseburger …/cs/cs765unittest.cs
 *  version    : • 20xxxxxx°xxxx v0.x.x Filling • 20221001°1421 v0.1.8 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate
 *  userstory  :
 *  summary    :
 */
using System;

class Program
{
    const string sVERSION = "v0.0.0";
    static string sDOTNETV = "(C# " + typeof(int).Assembly.ImageRuntimeVersion + ")";

    public static void Main(String[] args)
    {
        Console.WriteLine($"*** Hola, este es `cs765unittest.cs` {sVERSION} — UnitTest {sDOTNETV} ***");





        Console.WriteLine("Adiós.");
    }
}
