﻿/**
 *  file       : id 20141128°1431 — gitlab.com/normai/cheeseburger …/cs/cs111hello.cs
 *  version    : • 20220908°0711 v0.1.7 Filling • 20141128°1431 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate a most simple C# program with user input
 *  compile    : VS-Dev-Prompt > csc.exe cs111hello.cs
 *  ref        : https://www.w3schools.com/cs/cs_user_input.php [ref 20220907°1243]
 */

using System;

namespace Helo
{
   class Program
   {
      const string sVERSION = "v0.1.7";
      static string sDOTNETV = "(C# " + typeof(int).Assembly.ImageRuntimeVersion + ")";

      public static void Main(string[] args)
      {
         Console.WriteLine($"*** This is `cs111hello.cs` {sVERSION} — Hello {sDOTNETV} ***");

         Console.Write("Please enter your name > ");
         String sWord;
         sWord = Console.ReadLine();
         Console.WriteLine("Good morning \"" + sWord + "\"!");

         Console.WriteLine("Bye. Press any key to exit ...");
         Console.ReadKey(true);
      }
   }
}
