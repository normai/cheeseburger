﻿/**
 *  file       : id 20220828°1721 — gitlab.com/normai/cheeseburger …/cs/cs161list.cs
 *  version    : • 20220908°1211 v0.1.7 Filling • 20220828°1721 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate C# list class
 *  summary    : This demonstrates C# lists in two flavours
 *                (1) Simple
 *                (2) Sophisticated
 *  reference  : https://docs.microsoft.com/en-us/dotnet/api/system.collections.generic.list-1 []
 *  reference  : https://docs.microsoft.com/en-us/dotnet/csharp/tour-of-csharp/tutorials/arrays-and-collections []
 *                 — Note the top box about .NET 6.0 compatibility
 *  reference  : https://www.w3schools.com/cs/cs_for_loop.php []
 *  reference  : https://www.w3schools.blog/c-sharp-list []
 */

using System;
using System.Collections.Generic;

class Program
{
    const string sVERSION = "v0.1.7";
    static string sDOTNETV = "(C# " + typeof(int).Assembly.ImageRuntimeVersion + ")";

    public static void Main(String[] args)
    {
        Console.WriteLine($"*** Nǐ hǎo `cs161list.cs` {sVERSION} — Lists {sDOTNETV} ***");  // "你好 [Nǐ hǎo]"

        // (1) Simple list
        // (1.1) Create
        List<String> islands = new List<String>();
        islands.Add ("Rügen");
        islands.Add ("Usedom");
        islands.Add ("Fehmarn");
        islands.Add ("Sylt");
        islands.Add ("Föhr");
        islands.Add ("Pellworm");

        // (1.2) Print some infos
        Console.WriteLine("(1.1) Type = " + islands.GetType());
        Console.WriteLine("(1.2) Length =" + islands.Count);
        Console.WriteLine("(1.3) Content = " + islands);

        // (1.3) Iterate
        for (int iNdx = 0; iNdx < islands.Count; iNdx++)
        {
             Console.WriteLine("   - " + islands[iNdx]);
        }

        // (2) Sophisticated list
        // (2.1) Create
        List<Object> mixture = new List<Object>();             // Use Object as a workaround
        mixture.Add("Pink");
        mixture.Add(true);
        mixture.Add(123);
        mixture.Add(2.34);
        List<String> helplist = new List<String>();
        helplist.Add("Aha");
        helplist.Add("Oha");
        helplist.Add("Uhu");
        mixture.Add(helplist);

        // (2.2) Print some infos
        Console.WriteLine("(2.1) Type = " + mixture.GetType());
        Console.WriteLine("(2.2) Length =" + mixture.Count);
        Console.WriteLine("(2.3) Content = " + mixture);

        // (2.3) Iterate
        foreach (Object o in mixture)
        {
             Console.WriteLine("   - " + o + " (type = " + o.GetType() + ")");
        }

        Console.WriteLine("Zàijiàn.");  // "再见 [Zàijiàn]."
    }
}
