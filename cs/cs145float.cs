﻿/**
 *  file       : id 20221012°1121 — gitlab.com/normai/cheeseburger …/cs/cs145float.cs
 *  version    : • 20221216°1112 v0.1.8 Filling • 20221012°1121 v0.1.7 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate Float Type
 *  status     : Has room for enhancement. Should use string formatting for number output.
 */
using System;

class Program
{
    const string sVERSION = "v0.1.8";
    static string sDOTNETV = "(C# " + typeof(int).Assembly.ImageRuntimeVersion + ")";

    public static void Main(String[] args)
    {
        Console.WriteLine($"*** Dobrý den, toto je `cs145float.cs` {sVERSION} — Float Type {sDOTNETV} ***");


        Console.WriteLine("(1) Print some numbers:");
        double f1 = 123_456.789;
        double f2 = -123_456.789;
        double f3 = 123_456_777_777_777_777_777_777_777.789;
        double f4 = -123_456_777_777_777_777_777_777_777.789;
        Console.WriteLine($"(1.1) f1 = {f1} {f1.GetType()}");
        Console.WriteLine($"(1.2) f2 = {f2} {f2.GetType()}");
        Console.WriteLine($"(1.3) f3 = {f3} {f3.GetType()}");
        Console.WriteLine($"(1.4) f4 = {f4} {f4.GetType()}");


        Console.WriteLine("Nashledanou.");
    }
}
