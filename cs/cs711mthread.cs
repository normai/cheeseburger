﻿// EMPTY STUB, YET TO BE FILLED WITH CODE

/**
 *  file       : id 20221224°1121 — gitlab.com/normai/cheeseburger …/cs/cs711mthread.cs
 *  version    : • 20xxxxxx°xxxx v0.x.x Filling • 20221224°1121 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate multithreading
 *  status     :
 */
using System;

class Program
{
   const string sVERSION = "v0.0.0";
   static string sDOTNETV = "(C# " + typeof(int).Assembly.ImageRuntimeVersion + ")";

   public static void Main(String[] args)
   {
      Console.WriteLine($"*** Det här är `cs711mthread.cs` {sVERSION} — Multithreading {sDOTNETV} ***");





      Console.WriteLine("Adjö.");
   }
}
