﻿// THIS FILE IS AN EMPTY STUB, IT IS YET TO BE FILLED WITH CODE

/**
 *  file       : id 20221116°1721 — gitlab.com/normai/cheeseburger …/cs/cs722varargs.cs
 *  version    : • 20xxxxxx°xxxx v0.x.x Filling • 20221116°1721 v0.1.7 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate VarArgs
 *  userstory  :
 *  summary    :
 */
using System;

class Program
{
    const string sVERSION = "v0.0.0";
    static string sDOTNETV = "(C# " + typeof(int).Assembly.ImageRuntimeVersion + ")";

    public static void Main(String[] args)
    {
        Console.WriteLine($"*** Aloha, o kēia `cs722varargs.cs` {sVERSION} — VarArgs {sDOTNETV} ***");  // "Aloha, ʻo kēia"





        Console.WriteLine("Aloha mai.");
    }
}
