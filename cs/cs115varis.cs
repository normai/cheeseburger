/**
 *  file       : id 20220825°1421 — gitlab.com/normai/cheeseburger …/cs/cs13varis.cs
 *  version    : • 20220908°0911 v0.1.7 Filling • 20220825°1421 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate variables
 */

using System;                                          // Console

class Program
{
   const string sVERSION = "v0.1.7";
   static string sDOTNETV = "(C# " + typeof(int).Assembly.ImageRuntimeVersion + ")";

   public static void Main(String[] args)
   {
      Console.WriteLine($"*** Hello, this is `cs115varis.cs` {sVERSION} — Variables {sDOTNETV} ***");

      int a = 123;                                     // Integer
      double b = 2.34;                                 // Float
      String c = "Ahoj";                               // String
      String d = c + " = ";
      double e = a + b;
      Console.WriteLine(d + e.ToString());

      Console.WriteLine("Good bye.");
   }
}
