/**
 *  file       : id 20220806°1251 — gitlab.com/normai/cheeseburger …/cs/cs121operas.cs
 *  version    : • 20220908°1531 v0.1.7 Filling • 20220806°1251 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate some operators
 */

using System;

class Program
{
   const string sVERSION = "v0.1.7";
   static string sDOTNETV = "(C# " + typeof(int).Assembly.ImageRuntimeVersion + ")";

   public static void Main(String[] args)
   {
      Console.WriteLine($"*** Hello, this is `cs121operas.cs` {sVERSION} — Operators {sDOTNETV} ***");

      // (1) Assignment operators
      int v, w, x, y, z;
      v = w = x = y = z = 5;
      w += 2;
      x -= 2;
      y *= 2;
      y /= 2;
      Console.WriteLine("(1.1) 'v = w = x = y = z = 5;'            : " + v.ToString());
      Console.WriteLine("(1.2) 'w += 2' identical with 'w = w + 2' : " + w.ToString());
      Console.WriteLine("(1.3) 'x += 2' identical with 'x = x + 2' : " + x.ToString());
      Console.WriteLine("(1.4) 'y *= 2' identical with 'y = y * 2' : " + y.ToString());
      Console.WriteLine("(1.5) 'z *= 2' identical with 'z = z * 2' : " + z.ToString());
      Console.WriteLine();

      // (2) Some remarkable operators
      double x1 = (double) 100 / 30;                            // Note the extra cast "(double)"
      int x2 = 100 / 30;                                        // Integer division, in Python "//"
      double x3 = 100 % 30;                                     // Modulo
      double x4 = Math.Pow(16, 3);                              // Power, in Python operator "16 ** 3"
      int x5 = 1 > 2 ? 1234 : 5678;                             // Ternary operator
      Console.WriteLine("(2.1) 100 / 30            = (double) " + x1.ToString() + " " + x1.GetType().Name);
      Console.WriteLine("(2.2) 100 / 30            = " + x2.ToString() + " " + x2.GetType().Name);
      Console.WriteLine("(2.3) 100 % 30            = " + x3.ToString() + " " + x3.GetType().Name);
      Console.WriteLine("(2.4) 16 ** 3             = " + x4.ToString() + " " + x4.GetType().Name);
      Console.WriteLine("(2.5) 1 > 2 ? 1234 : 5678 = " + x5.ToString() + " " + x5.GetType().Name);
      Console.WriteLine();

      // (3) The format function introduces the member operator '.'
      int i1 = 1; float f1 = 2.3f; double d1 = 3.456789;
      String s = String.Format("(7) Take {0,3} or {1,3} or {2,3}", i1, f1, d1);
      Console.WriteLine(s);

      Console.WriteLine("Good bye.");
   }
}
