﻿/**
 *  file       : id 20220826°1921 — gitlab.com/normai/cheeseburger …/cs/cs261try1.cs
 *  version    : • 20220910°1521 v0.1.7 Filling • 20220826°1921 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate C# Exception Handling
 *  summary    :
 *  ref        : https://www.w3schools.com/cs/cs_exceptions.php [ref 20220910°1415]
 *  ref        : https://docs.microsoft.com/en-us/dotnet/api/system.math.sqrt?view=net-6.0 [ref 20220910°1416]
 *  status     : Example stories not yet settled. But the square root is a nice
 *               example in so far that it is practicable in some languages, but
 *               not in others. This will be similar with division by zero.
 */

using System;

class Program
{
   const string sVERSION = "v0.1.7";
   static string sDOTNETV = "(C# " + typeof(int).Assembly.ImageRuntimeVersion + ")";

   public static void Main(String[] args)
   {
      Console.WriteLine($"*** {Kon'nichiwa, desu} `cs261try1.cs` {sVERSION} — Exception Handling {sDOTNETV} ***");  // "こんにちは、です [Kon'nichiwa, desu]" does not print nice

      // (1) Try adding mismatching types
      Console.WriteLine("(1) Adding mismatching types cannot be demonstrated in Java because of strict typing");

      // (2) Calculate square root, sometimes succeed, sometimes fail
      Random random = new Random();
      int iRand = random.Next(-9, 9);                                   // Shall be from -9 to +9 -- VERIFY THIS

      Console.WriteLine("\n(2) Calculate sqare root of " + iRand);
      double fRoot;
      try
      {
          if (iRand < 0)
          {
             throw new ArithmeticException("The root of a negative number is NaN, not a number.");  // Workaround. Todo: Craft another example, which throws itself [todo 20220910°1421]
          }
          fRoot = Math.Sqrt(iRand);
          Console.WriteLine("    => Sqare root of " + iRand + " is " + fRoot);
      }
      catch (Exception e)
      {
          Console.WriteLine("    => Sqare root of " + iRand + " calculation failed: " + e.Message);
      }

      Console.WriteLine("Sayōnara.");  // "さようなら [Sayōnara]" does not print nice
   }
}
