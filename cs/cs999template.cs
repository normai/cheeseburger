﻿// EMPTY STUB, YET TO BE FILLED WITH CODE

/**
 *  file       : id 20220828°1621 — gitlab.com/normai/cheeseburger …/cs/cs999template.cs
 *  version    : • 20xxxxxx°xxxx v0.x.x Filling • 20220828°1621 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate ...
 *  status     :
 */
using System;

class Program
{
   const string sVERSION = "v0.0.0";
   static string sDOTNETV = "(C# " + typeof(int).Assembly.ImageRuntimeVersion + ")";

   public static void Main(String[] args)
   {
      Console.WriteLine($"*** Bună, acesta este `cs999template.cs` {sVERSION} — Template {sDOTNETV} ***");





      Console.WriteLine("La revedere.");
   }
}
