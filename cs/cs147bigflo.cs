﻿/**
 *  file       : id 20221117°1121 — gitlab.com/normai/cheeseburger …/cs/cs147bigflo.cs
 *  version    : • 20221227°1911 v0.1.9 Filling • 20221117°1121 v0.1.8 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate big floatingpoint numbers
 *  userstory  : Calculate and output some popular non-terminating decimals
 *  note       : C# has no BigFloat class, so we suffice with the Decimal class
 */
using System;

class Program
{
   const string sVERSION = "v0.1.9";
   static string sDOTNETV = "(C# " + typeof(int).Assembly.ImageRuntimeVersion + ")";

   public static void Main(String[] args)
   {
      Console.WriteLine($"*** Hailo, yah hai `cs147bigflo.cs` {sVERSION} — Big integer types {sDOTNETV} ***");  // "हैलो, यह है [Hailo, yah hai]"


      Console.WriteLine("(1) Try some doubles:");
      double d1 = 10.0 / 7.0;
      double d2 = 10.0 / 3.0;
      double d3 = 10.0 / 1.5;
      Console.WriteLine($"(1.1.1) d1  = {d1}");
      Console.WriteLine($"(1.1.2) d1  = {d1.ToString()}");
      Console.WriteLine($"(1.2.1) d2  = {d2}");
      Console.WriteLine($"(1.2.2) d2  = {d2.ToString()}");
      Console.WriteLine($"(1.3.1) d3  = {d3}");
      Console.WriteLine($"(1.3.2) d3  = {d3.ToString()}");

      Console.WriteLine("(2) Try some Decimals:");
      Decimal bd1 = 10.0M;
      bd1 = bd1 / 7;
      Decimal bd2 = 10.0M;
      bd2 = bd2 / 3;
      Decimal bd3 = 10.0M;
      bd3 = bd3 / 1.5M;
      Console.WriteLine($"(2.1.1) bd1 = {bd1}");
      Console.WriteLine($"(2.1.2) bd1 = {bd1.ToString("N12")}");
      Console.WriteLine($"(2.2.1) bd1 = {bd2}");
      Console.WriteLine($"(2.2.2) bd2 = {bd2.ToString("N24")}");
      Console.WriteLine($"(2.3.1) bd1 = {bd3}");
      Console.WriteLine($"(2.3.2) bd3 = {bd3.ToString("N56")}");  // "0.########################################################"


      Console.WriteLine("Alavida.");  // "अलविदा [Alavida]"
   }
}
