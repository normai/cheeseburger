﻿// EMPTY STUB, YET TO BE FILLED WITH CODE

/**
 *  file       : id 20221213°1621 — gitlab.com/normai/cheeseburger …/cs/cs276anonfs.cs
 *  version    : • 20xxxxxx°xxxx v0.x.x Filling • 20221213°1621 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate anonymous functions
 *  userstory  :
 *  summary    :
 *  status     :
 *  ref        :
 */
using System;

class Program
{
   const string sVERSION = "v0.0.0";
   static string sDOTNETV = "(C# " + typeof(int).Assembly.ImageRuntimeVersion + ")";

   public static void Main(String[] args)
   {
      Console.WriteLine($"*** Halo, ini adalah `cs276anonfs.cs` {sVERSION} — Anonymous functions {sDOTNETV} ***");





      Console.WriteLine("Alavida.");  // "अलविदा [Alavida]."
   }
}
