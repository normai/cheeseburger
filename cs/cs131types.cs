﻿/**
 *  file       : id 20220215°0915 — gitlab.com/normai/cheeseburger …/cs/131types.cs
 *  version    : • 20220908°1011 v0.1.7 Filling • 20220215°0915 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  summary    : Demonstrate Some Types
 *  ref        : http://ctp.mkprog.com/en/csharp/menu/types_variables/ [ref 20221016°1814]
 *  status     :
 */

using System;                                                           // For String

class Program
{
    const string sVERSION = "v0.1.7";
    static string sDOTNETV = "(C# " + typeof(int).Assembly.ImageRuntimeVersion + ")";

    public static void Main(String[] args)
    {
        Console.WriteLine($"*** Abhinandan `cp131types.cs` {sVERSION} — Types {sDOTNETV} ***");  // "अभिनंदन [Abhinandan]"

        // Some declarations
        byte iByte1 = 127;
        short iShort1 = 32767;
        int iInt1 = 2147483647;
        long iLong1 = Int64.MaxValue;
        float f1 = 1.23f;                                               // The 'f' postfix prevents error 'lossy conversion'
        double d1 = double.MaxValue;
        bool b1 = true;
        char c1 = 'x';
        String s1 = "Holla";

        // Some operations
        byte iByte2 = (byte) (iByte1 + ((byte) 1));                     // Force the lossy conversion from int to byte
        short iShort2 = (short) (iShort1 + ((short) 1));                // Force lossy conversion from int to short
        int iInt2 = iInt1 + 1;
        long iLong2 = iLong1 + 1;
        double f2 = f1 + 1;
        double d2 = d1 + 1;
        bool b2 = !b1;                                                  // The plus operator is not possible with boolean
        char c2 = (char) (c1 + ((char) 1));                             // Note the tricky parentheses placement
        String s2 = s1 + 1;                                             // Note the automatic conversion of '1' from int to String

        // Display the results
        Console.WriteLine("(1) " + iByte1 + " + 1 = " + iByte2 + " (Byte.MaxValue = " + Byte.MaxValue + ")");
        Console.WriteLine("(2) " + iShort1 + " + 1 = " + iShort2 + " (Int16.MaxValue = " + Int16.MaxValue + ")");
        Console.WriteLine("(3) " + iInt1 + " + 1 = " + iInt2);
        Console.WriteLine("(4) " + iLong1 + " + 1 = " + iLong2);
        Console.WriteLine("(5) " + f1 + " + 1 = " + f2);
        Console.WriteLine("(6) " + d1 + " + 1 = " + d2);
        Console.WriteLine("(7) !" + b1 + " = " + b2);
        Console.WriteLine("(8) " + s1 + " + 1 = " + s2);

        Console.WriteLine("Alavida.");  // "अलविदा [Alavida]."
    }
}
