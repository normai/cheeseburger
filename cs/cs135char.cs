﻿/**
 *  file       : id 20221012°0921 — gitlab.com/normai/cheeseburger …/cs/cs135char.cs
 *  version    : •  20221016°1121 v0.1.8 Filling • 20221012°0921 v0.1.7 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate Character Type
 *  userstory  :
 *  summary    :
 *  ref        : https://www.codevscolor.com/c-sharp-iterate-string-characters [ref 20221015°1132] (👍)
 *  ref        : https://blog.udemy.com/c-sharp-char/ [ref 20221016°1252]
 *  ref        :
 *  note       : Interstingly, UTF-8 chars like '…' or greek chars can be handled
 *                on the console, but when printed from C# here, they fail.
 *                Can there anything be done, so they will print from C#?
 *  status     :
 */
using System;

class Program
{
   const string sVERSION = "v0.1.8";
   static string sDOTNETV = "(C# " + typeof(int).Assembly.ImageRuntimeVersion + ")";

   public static void Main(String[] args)
   {
      Console.WriteLine($"*** Zdraveite, tova e `cs135char.cs` {sVERSION} — Character Type {sDOTNETV} ***");  // "Здравейте, това е [Zdraveĭte, tova e]" does not print nice

      // (A) Naive lines
      char c11 = 'a';
      char c12 = '\u0061';                                              // Use UTF-8 code
      char c13 = '\x0061';                                              // Use Hexadecimal
      char c14 = (char) 97;
      char c21 = 'é';
      char c22 = '\u00e9';                                              // 'é'
      char c23 = '\x00e9';
      char c24 = (char) 233;
      char c31 = '…';
      char c32 = '\u2026';                                              // '…' ellipsis
      char c33 = '\x2026';
      char c34 = (char) 8230;
      Console.WriteLine("(A.1) c1x  : " + c11 + " " + c12 + " " + c13 + " " + c14 + " " + (int) c11);
      Console.WriteLine("(A.2) c2x  : " + c21 + " " + c22 + " " + c23 + " " + c24 + " " + (int) c21);
      Console.WriteLine("(A.3) c3x  : " + c31 + " " + c32 + " " + c33 + " " + c34 + " " + (int) c31);

      // (B) String elements
      // (B.1) Codes most below 128
      string s1 = "Abc é … xyz";
      Console.Write("(B.1) s1   : " + s1 + " —");
      foreach (char c in s1) {
          Console.Write(" " + (int) c);
      }
      Console.WriteLine();

      // (B.2) Greek UTF-8 chars
      string s2 = "Ακούσατε";
      Console.Write("(B.2) s2   : " + s2 + " —");
      foreach (char c in s2) {
          Console.Write(" " + (int) c);
      }
      Console.WriteLine();

      // (B.3) Hindi UTF-8 chars
      string s3 = "नमस्ते";                                                // "Namastē"
      Console.Write("(B.3) s3   : " + s3 + " —");
      foreach (char c in s3) {
          Console.Write(" " + (int) c);
      }
      Console.WriteLine();

      Console.WriteLine("Dovizhdane.");  // "Довиждане [Dovizhdane]" does not print nice
   }
}
