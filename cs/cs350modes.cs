﻿// THIS FILE IS AN EMPTY STUB, IT IS YET TO BE FILLED WITH CODE

/**
 *  file       : id 20220829°1321 — gitlab.com/normai/cheeseburger …/cs/cs350modes.cs
 *  version    : • 20xxxxxx°xxxx v0.x.x Filling • 20220829°1321 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  summary    : This demonstrates ...
 */
using System;

class Program
{
    const string sVERSION = "v0.0.0";
    static string sDOTNETV = "(C# " + typeof(int).Assembly.ImageRuntimeVersion + ")";

    public static void Main(String[] args)
    {
        Console.WriteLine($"*** Nǐ hǎo, zhè shì `cs350modes.cs` {sVERSION} — File access modes {sDOTNETV} ***");  // "你好，这是 [Nǐ hǎo, zhè shì]"





        Console.WriteLine("Zàijiàn.");  // "再见 [Zàijiàn]."
    }
}
