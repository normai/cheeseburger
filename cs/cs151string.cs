﻿/**
 *  file       : id 20221001°1321 — gitlab.com/normai/cheeseburger … cs/cs151string.cs
 *  version    : • 20221009°1721 v0.1.8 Filling • 20221001°1321 v0.1.7 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  compile    : > csc.exe cs151string.cs -langversion:preview
 *  subject    : Demonstrate string syntax
 *  ref        : https://kodify.net/csharp/strings/uppercase-lowercase/ [ref 20221010°1052]
 *  ref        : https://code-maze.com/csharp-convert-int-to-string/ [ref 20221010°1112]
 *  ref        : https://www.tutorialsteacher.com/articles/convert-string-to-int [ref 20221010°1113]
 *  todo       : Supplment some date in the string interpolation
 */
using System;
using System.Collections.Generic;                                       // List

class Program
{
   const string sVERSION = "v0.1.8";
   static string sDOTNETV = "(C# " + typeof(int).Assembly.ImageRuntimeVersion + ")";

   public static void Main(String[] args)
   {
      Console.WriteLine($"*** Pozdravljeni, tukaj `cs151string.cs` {sVERSION} — Strings {sDOTNETV} ***");

      // ====================================================
      // (1) String Syntax

      // (1.1) Syntax — Quote types (and newline)
      Console.Write("(1.1) Use double-quotes \" for strings, single-quotes \' for chars" + Environment.NewLine);

      // (1.2) Syntax — Mulitiline string                               // Error CS8652 "The feature 'raw string literals' is currently in Preview .."
      String sMulti = """
Ein Wiesel
saß auf einem Kiesel
inmitten Bachgeriesel.
""";
      Console.WriteLine("(1.2) " + sMulti);

      // (1.3) Syntax — String concatenation (is demonstrated again below)
      String sHello = "Hello";
      String sJenny = "Jenny";
      String sGreet = sHello + ' ' + sJenny;
      Console.WriteLine("(1.3) " + sGreet);

      // (1.4) Syntax — String as array of chars
      String sWord = "SilzuZankunKrei";
      char[] chars = sWord.ToCharArray();
      Console.Write("(1.4) String as Array: ");
      foreach (char c in chars ) {
         Console.Write(c + ".");
      }
      Console.WriteLine();

      // ==============================================
      // (2) Conversions

      Console.WriteLine("(2.1) Number to string: \"" + 1234 + "\", \"" + (2.345).ToString() + "\"");  // See ref 20221010°1112

      Console.WriteLine("(2.2) String to number:");
      List<String> nums = new List<String>();
      nums.Add("1234");
      nums.Add("2.345");
      nums.Add("Tree hundred twenty five");
      Object o;
      foreach (String sNum in nums) {
         try {
            o = Int32.Parse(sNum);
         }
         catch (Exception e1) {
            try {
               o = Double.Parse(sNum);
            }
            catch (Exception e2) {
               Console.WriteLine("      Two exceptions thrown: " + (uint) e1.HResult + " and " + (uint) e2.HResult);
               o = "Not a number";
            }
         }
         Console.WriteLine("      - \"" + sNum + "\" => " + o + " (" + o.GetType() + ")");
      }

      Console.WriteLine("Nasvidenje.");
   }
}
