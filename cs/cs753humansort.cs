﻿// THIS FILE IS AN EMPTY STUB, IT IS YET TO BE FILLED WITH CODE

/**
 *  file       : id 20220830°1821 — gitlab.com/normai/cheeseburger …/cs/cs753humansort.cs
 *  version    : • 20xxxxxx°xxxx v0.x.x Filling • 20220830°1821 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate …
 *  summary    : This demonstrates …
 *  ref        : https://github.com/CodeMazeBlog/CodeMazeGuides [ref 20220910°1125]
 *                then https://github.com/CodeMazeBlog/CodeMazeGuides/tree/main/csharp-algorithms
 *  ref        :
 */
using System;

class Program
{
    const string sVERSION = "v0.0.0";
    static string sDOTNETV = "(C# " + typeof(int).Assembly.ImageRuntimeVersion + ")";

    public static void Main(String[] args)
    {
        Console.WriteLine($"*** Kia ora, ko tenei `cs753humansort.cs` {sVERSION} — Selection Sort {sDOTNETV} ***");





        Console.WriteLine("Tēnā koe.");
    }
}
