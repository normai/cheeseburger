﻿/**
 *  file       : id 20220828°1815 — gitlab.com/normai/cheeseburger …/cs/cs530sublis.cs
 *  version    : • 20220908°1311 v0.1.7 Filling • 20220828°1815 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  summary    : This demonstrates ...
 *  reference  : https://docs.microsoft.com/en-us/dotnet/api/system.collections.generic.list-1.getrange?view=net-6.0 []7
 */
using System;
using System.Collections.Generic;

class Program
{
    const string sVERSION = "v0.1.7";
    static string sDOTNETV = "(C# " + typeof(int).Assembly.ImageRuntimeVersion + ")";

    public static void Main(String[] args)
    {
        Console.WriteLine($"*** Ahoj `cs530sublis.cs` {sVERSION} — Sublists .. {sDOTNETV} ***");

        string[] nums = {"Uno", "Due", "Tre", "Quattro"};
        List<String> l = new List<String>(nums);
        l.Add ("Cinque");
        l.Add ("Sei");
        l.Add ("Sette");

        Console.WriteLine("(1) l                             = " + string.Join(" ", l));
        Console.WriteLine("(2) l.GetRange(0, 2)              = " + string.Join(" ", l.GetRange(0, 2))); // Params: index, count
        Console.WriteLine("(3) l.GetRange(2, (l.Count - 2))) = " + string.Join(" ", l.GetRange(2, (l.Count - 2))));
        Console.WriteLine("(4) l.GetRange(0, (l.Count - 2))) = " + string.Join(" ", l.GetRange(0, (l.Count - 2))));
        Console.WriteLine("(5) l.GetRange(2, (5 - 2)))       = " + string.Join(" ", l.GetRange(2, (5 - 2))));

        Console.WriteLine("Nashledanou.");
    }
}
