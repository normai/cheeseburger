﻿/**
 *  file       : id 20220928°1821 — gitlab.com/normai/cheeseburger … cs/cscircles1.cs
 *  version    : • 20220928°1821 v0.1.8 Initial
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Draw ASCII Circles and other patterns
 *  usage      : On Visual Studio Developer Command Prompt, in this folder
 *               type "csc.exe cscircles1.cs" then "cscircles1.exe"
 */
using System;

class Program {

   const string sVERSION = "v0.0.0";
   static string sDOTNETV = "(C# " + typeof(int).Assembly.ImageRuntimeVersion + ")";

   public static bool IsPointOnCirlce(int r, int x, int y) {
      int tolerance = r;                                            // Empirical value for line width
      if (Math.Abs(Math.Pow(x, 2) + Math.Pow(y, 2) - Math.Pow(r, 2)) < tolerance) {
         return true;
      }
      else {
         return false;
      }
   }

   public static bool IsPointOnDiagonal(int x, int y, int size) {
      if (x == size - y + 1) {                                      // Add 1 empirically
         return true;
      }
      else {
         return false;
      }
   }

   public static void Main(String[] args) {

      Console.WriteLine($"*** Dobrý deň, toto je `cscircles1.cs` {sVERSION} — Draw ASCII Circles {sDOTNETV} ***");

      int[] sizes = {7, 17};
      foreach (int size in sizes) {                                 // Paint as many rectangles as sizes are given

         // Prepare convenient variables
         // Note how the integer division for the radius looks different in each language
         int radius = (size / 2) - 1;                               // Make size of circle slightly smaller than the rectangle
         int shiftx = radius + 2;                                   // Shift circle center from rectangle left to rectangle center
         int shifty = radius + 2;                                   // Shift circle center from rectangle top to rectangle center

         // Iterate over lines and columns
         for (int line = 1; line <= size; line++) {
            for (int col = 1; col <= size; col++) {
               if (IsPointOnCirlce(radius, line - shiftx, col - shifty)) {  // Is this point on the circle?
                  Console.Write("@ ");
               }
               else {
                  if (IsPointOnDiagonal(line, col, size)) {         // Is this point on the diagonal?
                     Console.Write("# ");
                  }
                  else {                                            // All other cases
                     Console.Write(". ");
                  }
               }
            }
            Console.WriteLine();                                    // Goto next line
         }
      }

      Console.WriteLine("Dovidenia.");
   }
}
