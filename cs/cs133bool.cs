﻿/**
 *  file       : id 20221012°0821 — gitlab.com/normai/cheeseburger …/cs/cs133bool.cs
 *  version    : • 20221014°1121 v0.1.8 Filling • 20221012°0821 v0.1.7 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate Boolean Type
 *  compile    : VS Dev Cmd Prompt >
 *  userstory  :
 *  summary    :
 */

using System;
using System.Collections.Generic;                                       // List

class Program
{
   const string sVERSION = "v0.1.8";
   static string sDOTNETV = "(C# " + typeof(int).Assembly.ImageRuntimeVersion + ")";

   public static void Main(String[] args)
   {
      Console.WriteLine($"*** Pryvit, tse `cs133bool.cs` {sVERSION} — Boolean Type {sDOTNETV} ***");  // "Привіт, це" does not print nice

      // (1) The naive test series
      Console.WriteLine("(1) Naive test series:");
      bool bFalse = false;
      bool bTrue = true;
      //int iMinus = -1;
      //int iZero = 0;
      //int iOne = 1;
      //int iTwo = 2;
      //double fMinus = -0.1;
      //double fZero = 0.0;
      //double fOne = 0.1;
      //double fTwo = 1.23;
      //Console.WriteLine("(1.1)  (boolean) None = " + (boolean) bNone);
      Console.WriteLine("(1.2)  False          = " + bFalse);
      Console.WriteLine("(1.3)  True           = " + bTrue);
      //Console.WriteLine("(1.4)  (bool) -1   = " + (bool) iMinus); // Compiler error "Cannot convert type 'int' to 'bool'"
      //Console.WriteLine("(1.5)  (bool) 0    = " + (bool) iZero);
      //Console.WriteLine("(1.6)  (bool) 1    = " + (bool) iOne);
      //Console.WriteLine("(1.7)  (bool) 2    = " + (bool) iTwo);
      //Console.WriteLine("(1.8)  (bool) -0.1 = " + (bool) fMinus);  // Compiler error "Cannot convert type 'double' to 'bool'"
      //Console.WriteLine("(1.9)  (bool) 0.0  = " + (bool) fZero);
      //Console.WriteLine("(1.10) (bool) 0.1  = " + (bool) fOne);
      //Console.WriteLine("(1.11) (bool) 1.23 = " + (bool) fTwo);
      //Console.WriteLine("(1.12) (bool) ''   = " + (bool) "");     // Compiler error "Cannot convert type 'string' to 'bool'"
      //Console.WriteLine("(1.13) (bool) 'x'  = " + (bool) "x");

      // (2) Targeted conversion
      Console.WriteLine("(2) Targeted conversion:");
      List<String> trues = new List<String>();
      trues.Add("true");                                                // Todo: Use shorter initialization
      trues.Add("1");
      trues.Add("j");
      trues.Add("ja");
      trues.Add("t");
      trues.Add("true");
      trues.Add("y");
      trues.Add("yes");

      List<String> inputs = new List<String>();
      inputs.Add("");                                                    // Todo: Use shorter initialization
      inputs.Add("Bla");
      inputs.Add("Ja");
      inputs.Add("No");
      inputs.Add("True");
      inputs.Add("Yes");

      foreach (String x in inputs) {
         bool b = trues.Contains(x.ToLower());
         Console.WriteLine(" -" + x + " -> " + b);
      }

      Console.WriteLine("Do pobachennya.");                   // "до побачення" does not print nice
   }
}
