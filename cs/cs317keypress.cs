﻿// THIS FILE IS AN EMPTY STUB, IT IS YET TO BE FILLED WITH CODE

/**
 *  file       : id 20220827°1621 — gitlab.com/normai/cheeseburger …/cs/cs317keypress.cs
 *  version    : • 20xxxxxx°xxxx v0.x.x Filling • 20220827°1621 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  summary    : This demonstrates ...
 */
using System;

class Program
{
    const string sVERSION = "v0.0.0";
    static string sDOTNETV = "(C# " + typeof(int).Assembly.ImageRuntimeVersion + ")";

    public static void Main(String[] args)
    {
        Console.WriteLine($"*** Bună, acesta este `cs317keypress.cs` {sVERSION} — KeyPress .. {sDOTNETV} ***");





        Console.WriteLine("La revedere.");
    }
}
