﻿/**
 *  file       : id 20220829°0921 — gitlab.com/normai/cheeseburger …/cs/cs167tuple.cs
 *  versions   : • 20220906°0830 v0.1.7 Filling • 20220829°0921 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrates a C-Sharp Tuple
 *  summary    : This demonstrates tuples in C-Sharp. Cornerstones:
 *                • Tuples are value types (see ref 20220906°0914)
 *                • It looks like they cannot incorporate other tuples (?)
 *                • They are suited to serve as return type for multiple values (like in Python)
 *                • The fields can optionally be named (since about C# 7.4)
 *  servings   : This should serve three flavours:
 *                (1) Create tuple using just the parenthesis (shown)
 *                (3) Create tuple using a constructor (not shown here, see e.g. ref 20220906°0922)
 *                (2) Create tuple with the Create method (not shown here, see e.g. ref 20220906°0922)
 *  ref        : https://en.wikipedia.org/wiki/Tuple [ref 20220906°0932]
 *  ref        : https://de.wikipedia.org/wiki/Tupel [ref 20220906°0933]
 *  ref        : https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/builtin-types/value-tuples [ref 20220906°0912] Tuples can also have fieldnames
 *  ref        : https://docs.microsoft.com/en-us/dotnet/api/system.tuple?view=net-6.0 [ref 20220906°0913]
 *  ref        : https://docs.microsoft.com/en-us/archive/msdn-magazine/2018/june/csharp-tuple-trouble-why-csharp-tuples-get-to-break-the-guidelines [ref 20220906°0914]
 *  ref        : https://www.c-sharpcorner.com/article/c-sharp-introduction-of-tuple/ [ref 20220906°0922]
 *  ref        : https://www.c-sharpcorner.com/article/tuples-in-c-sharp/ [ref 20220906°0923]
 *  ref        : https://stackoverflow.com/questions/7631327/number-of-elements-in-tuple [ref 20220906°0942] How to get the number of fields. The Length property does not work.
 *  ref        : https://stackoverflow.com/questions/43341177/how-to-iterate-over-tuple-items [ref 20220906°0943]
 *  ref        :
 */
using System;
using System.Runtime.CompilerServices;                          // For ITuple

class Program
{
    const string sVERSION = "v0.1.7";
    static string sDOTNETV = "(C# " + typeof(int).Assembly.ImageRuntimeVersion + ")";

    public static void Main(String[] args)
    {
        Console.WriteLine($"*** Hallo, dit is `cs167tuple.cs` {sVERSION} — Tuples {sDOTNETV} ***");

        // (1) Create -- Subtuples seem not be possible, so Ding is just a string
        (string, int, double, bool, string) tupl1 = ("Values", 1234, 2.345, true, "Ding");
        Console.WriteLine("(1) tupl1 = " + tupl1.ToString());

        // (2) Info
        // About Length, see ref 20220906°0942. Another way to make Length available is via ITuple, see below.
        Console.WriteLine("(2.1) Type = " + tupl1.GetType());
        Console.WriteLine("(2.2) Size = " + tupl1.GetType().GetGenericArguments().Length.ToString());

        // (3) Iterate -- Compare ref 20220906°0943
        ITuple itu = tupl1 as ITuple;
        Console.WriteLine("(3) Iterate:");
        for (int i = 0; i < itu.Length; i++)
        {
            Console.WriteLine("     - " + itu[i].ToString().PadRight(12) + itu[i].GetType());
        }

        // (4) Pick by index
        Console.WriteLine("(4.1) Third item = " + tupl1.Item3);
        Console.WriteLine("(4.2) Fifth      = " + tupl1.Item5);  // We have no tuple in tuple, thus just a string!

        Console.WriteLine("Tot ziens.");
    }
}
