﻿// EMPTY STUB, YET TO BE FILLED WITH CODE

/**
 *  file       : id 20221227°1121 — gitlab.com/normai/cheeseburger …/cs/cs767docoms.cs
 *  version    : • 20xxxxxx°xxxx v0.x.x Filling • 20221227°1121 v0.1.8 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate documentation comments
 *  status     :
 */
using System;

class Program
{
   const string sVERSION = "v0.0.0";
   static string sDOTNETV = "(C# " + typeof(int).Assembly.ImageRuntimeVersion + ")";

   public static void Main(String[] args)
   {
      Console.WriteLine($"*** Това е [Tova e] `cs767docoms.cs` {sVERSION} — Documentation comments {sDOTNETV} ***");





      Console.WriteLine("Довиждане [Dovizhdane].");
   }
}
