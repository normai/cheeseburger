rem file 20220215`1925 (after 20211103`1121) -- Run all
rem usage : Run this from a Visual Studio Command Prompt
@echo off

set list=
set list=%list%;cs111hello
set list=%list%;cs113aloha
set list=%list%;cs115varis
set list=%list%;cs117keywords
set list=%list%;cs121operas
set list=%list%;cs123compare
set list=%list%;cs125bitwise
set list=%list%;cs127ternary
set list=%list%;cs131types
set list=%list%;cs133bool
set list=%list%;cs135char

rem Needs special commanline: > csc.exe /reference:System.Numerics.dll cs137int.cs
rem set list=%list%;cs137int

rem Needs > csc.exe cs139bigint.cs -r:System.Numerics.dll
rem set list=%list%;cs139bigint

set list=%list%;cs145float
set list=%list%;cs147bigflo
set list=%list%;cs151string
set list=%list%;cs153stripol
set list=%list%;cs155strfunc
set list=%list%;cs157array
set list=%list%;cs161list
set list=%list%;cs163sublist
set list=%list%;cs165dict
set list=%list%;cs167tuple
set list=%list%;cs171print
set list=%list%;cs173input
set list=%list%;cs211ifs1
set list=%list%;cs220switch
set list=%list%;cs230while
set list=%list%;cs235dowhile
set list=%list%;cs240for
set list=%list%;cs250foreach
set list=%list%;cs261try1
set list=%list%;cs262try2
set list=%list%;cs267goto
set list=%list%;cs271funcs
set list=%list%;cs273recurs
set list=%list%;cs276anonfs
set list=%list%;cs278lambda
set list=%list%;cs281fntional
set list=%list%;cs286compars
set list=%list%;cs311kbd1menu
set list=%list%;cs313getkey
set list=%list%;cs315kbd2menu
set list=%list%;cs317keypress
set list=%list%;cs319kbd3menu
set list=%list%;cs335format
set list=%list%;cs338stdio
set list=%list%;cs341dir
set list=%list%;cs343textfile
set list=%list%;cs346binfile
set list=%list%;cs350modes
set list=%list%;cs410import
set list=%list%;cs420export
set list=%list%;cs430import2
set list=%list%;cs440main
set list=%list%;cs450lib
set list=%list%;cs510class
set list=%list%;cs520access
set list=%list%;cs530inherit
set list=%list%;cs540aggregate
set list=%list%;cs550polymo
set list=%list%;cs560iface
set list=%list%;cs610create
set list=%list%;cs620insert
set list=%list%;cs630upsala
set list=%list%;cs640select
set list=%list%;cs642relation
set list=%list%;cs644many
set list=%list%;cs646joins
set list=%list%;cs651nosql
set list=%list%;cs661linq
set list=%list%;cs711mthread

rem > csc.exe cs715assert.cs -r:"C:\Program Files\Microsoft Visual Studio\2022\Community\Common7\IDE\PublicAssemblies\Microsoft.VisualStudio.QualityTools.UnitTestFramework.dll"
rem set list=%list%;cs715assert

set list=%list%;cs720global
set list=%list%;cs722varargs
set list=%list%;cs724range
set list=%list%;cs726regex
set list=%list%;cs730del
set list=%list%;cs733sleep
set list=%list%;cs735yield
set list=%list%;cs740args
set list=%list%;cs751bubblesort
set list=%list%;cs753humansort
set list=%list%;cs761random
set list=%list%;cs765unittest
set list=%list%;cs771sound
set list=%list%;cs810window
set list=%list%;cs813input1
set list=%list%;cs815input2
set list=%list%;cs830treeview
set list=%list%;cs999template
set list=%list%;cscircles1
set list=%list%;csfactorial

goto fin
:fin

@echo off
for %%a in (%list%) do (
   echo --------------------- %%a.cs ---------------------
   csc.exe %%a.cs
   %%a.exe
   pause
   echo.
)

echo --------------------------------------------------------
pause
