﻿// EMPTY STUB, YET TO BE FILLED WITH CODE

/**
 *  file       : id 20221223°1321 — gitlab.com/normai/cheeseburger …/cs/cs661linq.cs
 *  version    : • 20xxxxxx°xxxx v0.x.x Filling • 20221223°1321 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate LINQ
 *  status     :
 */
using System;

class Program
{
   const string sVERSION = "v0.0.0";
   static string sDOTNETV = "(C# " + typeof(int).Assembly.ImageRuntimeVersion + ")";

   public static void Main(String[] args)
   {
      Console.WriteLine($"*** Esto es `cs661linq.cs` {sVERSION} — LINQ {sDOTNETV} ***");





      Console.WriteLine("Adiós.");
   }
}
