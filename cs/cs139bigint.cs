﻿/**
 *  file       : id 20221117°1021 — gitlab.com/normai/cheeseburger …/cs/cs139bigint.cs
 *  version    : • 20221216°0913 v0.1.8 Filling • 20221117°1021 v0.1.7 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate big integer types
 *  compile    : .NET-Fw-4.8.1 > csc.exe cs139bigint.cs -r:System.Numerics.dll
 *  status     : Too simple?
 */
using System;
using System.Numerics;

class Program
{
   const string sVERSION = "v0.1.8";
   static string sDOTNETV = "(C# " + typeof(int).Assembly.ImageRuntimeVersion + ")";

   public static void Main(String[] args)
   {
      Console.WriteLine($"*** Hello, ez `cs139bigint.cs` {sVERSION} — Big integers {sDOTNETV} ***");

      BigInteger iBig = new BigInteger(2);
      for (int i = 2; i < 11; i++) {
         iBig = BigInteger.Pow(iBig, 2);
         Console.WriteLine($" — {i} {iBig}");
      }

      Console.WriteLine("Viszlát.");
   }
}
