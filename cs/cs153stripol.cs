﻿/**
 *  file       : id 20221012°1621 — gitlab.com/normai/cheeseburger …/cs/cs153stripol.cs
 *  version    : • 20221013°1021 v0.1.8 Filling • 20221012°1621 v0.1.7 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate String Interpolation
 *  userstory  :
 *  summary    :
 *  ref        : https://learn.microsoft.com/en-us/dotnet/csharp/tutorials/string-interpolation [ref 20221010°1012]
 *  ref        : https://devblogs.microsoft.com/dotnet/string-interpolation-in-c-10-and-net-6/ [ref 20221010°1013]
 *  ref        : https://thedotnetguide.com/string-interpolation-in-csharp/ [ref 20221010°1022]
 *  ref        : https://qawithexperts.com/article/c-sharp/string-interpolation-in-c-with-examples/308 [ref 20221010°1032]
 *  ref        : https://www.dummies.com/article/technology/programming-web-design/csharp/stringbuilder-manipulating-c-strings-efficiently-249269/ [ref 20221010°1042]
 *  ref        : https://dev.to/dealeron/advanced-string-templates-in-c-2eh2 [ref 20221010°1052] About FormattableString type
 */
using System;
using System.Text;                                                      // StringBuilder

class Program
{
   const string sVERSION = "v0.1.8";
   static string sDOTNETV = "(C# " + typeof(int).Assembly.ImageRuntimeVersion + ")";

   public static void Main(String[] args)
   {
      Console.WriteLine($"*** Hej, det er `cs153stripol.cs` {sVERSION} — String Interpolation {sDOTNETV} ***");

      // Provide fodder
      String sHello = "Hello";
      String sJenny = "Jenny";
      int iX = 234;

      // (x) String interpolation
      // (x.1)
      Console.WriteLine("(2.1) Concatenation       : " + sHello + " " + iX + " " + sJenny + ".");

      // (x.2)
      Console.WriteLine(String.Format("(2.2) Format method       : {0} {1} {2}.", sHello, iX, sJenny));

      // (x.3)
      Console.WriteLine($"(2.3) Dollar-string       : {sHello} {iX} {sJenny}.");  // Introduced in C# 6.0

      // (x.4)
      StringBuilder sbldr = new StringBuilder("(2.4) StringBuilder       : ").Append(sHello).Append(" ").Append(iX).Append(" ").Append(sJenny).Append(".");
      Console.WriteLine(sbldr);

      // (x.5)
      var sFmt = (FormattableString) $"{sHello} {iX} {sJenny}.";        // See ref 20221010°1052
      Console.WriteLine("(2.5.1) FormattableString : " + sFmt);
      Console.WriteLine("(2.5.2) Format string     : " + sFmt.Format);
      Console.WriteLine("(2.5.3) Arguments         : " + sFmt.GetArgument(0) + ", " + sFmt.GetArgument(1) + ", " + sFmt.GetArgument(2));

      Console.WriteLine("Farvel.");
   }
}
