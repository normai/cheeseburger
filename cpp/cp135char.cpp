﻿/**
 *  file       : id 20221012°0935 — gitlab.com/normai/cheeseburger …/cpp/cp135char.cpp
 *  version    : • 20221016°1135 v0.1.8 Filling • v0.1.6 20221012°0935 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate Character Type
 *  compile    : VS Dev Cmd Prompt > cl.exe cp135char.cpp /EHsc /wd4566
 *  userstory  :
 *  summary    :
 *  ref        : https://learn.microsoft.com/en-us/cpp/build/reference/compiler-options-listed-alphabetically [ref 20221015°1732] Found '/wd4566' to disable specific warning
 *  ref        : https://blog.katastros.com/a?ID=00400-3fd4c4f4-f107-4a5f-bcb8-d414762487a9 []
 *  ref        : https://cplusplus.com/reference/cstdlib/wcstombs/ [ref 20221015°1733]
 *  ref        : https://cplusplus.com/forum/windows/48175/ [ref 20221015°1734]
 *  ref        : https://stackoverflow.com/questions/50403342/how-do-i-properly-use-stdstring-on-utf-8-in-c [ref 20221015°1742]
 *  ref        : https://stackoverflow.com/questions/2122194/how-i-print-utf-8-characters-c [ref 20221015°1743]
 *  ref        : https://stackoverflow.com/questions/17103925/how-well-is-unicode-supported-in-c11/17106065 [ref 20221015°1744]
 *  ref        : https://utf8everywhere.org/ [ref 20221015°1745]
 *  ref        : https://stackoverflow.com/questions/45874857/how-to-print-u32string-and-u16string-to-the-console-in-c [ref 20221015°1752]
 *  ref        : https://stackoverflow.com/questions/8788057/how-to-initialize-and-print-a-stdwstring [ref 20221015°1753]
 *  ref        :
 *  status     : Some pieces are collected. Far from systematically ready.
 */
#define sVERSION "v0.1.8"

#include <iostream>
#include <string>

#include <locale>              // For converter
#include <codecvt>             // For converter

#include <io.h>                // For  _setmode()
#include <fcntl.h>             // For _O_U16TEXT

int main()
{
   // Using the wanted "*** Здравейте [Zdraveĭte] cp157array.cpp ***" causes compiler
   //  warning 'Character .. cannot be represented in the current code page (1252)'
   std::cout << "*** Zdraveite, tova e 'cp135char.cpp' " << sVERSION << " -- Character Type ***" << std::endl;  // The 'i' should be an 'ĭ'

   // (A.1) Naive probe
   std::string sProbe = "Abcdé…xyz 👍";                                 // Compiler warnings C4566 "character … cannot be represented in the current code page (1252)"
   std::cout << "(A.1) Plain string         : \"" << sProbe << "\", length = " << sProbe.length() << std::endl;

   // (A.2)
   std::string s2 = u8"Abcdé…xyz 👍";
   std::cout << "(A.2) Probe u8 prefix      : \"" << s2 << "\", length = " << s2.length() << std::endl;

   //printf("%ls\n", s2);
   //printf("%s\n", s2);

   // (A.3)
   std::u16string sPrb16 = u"Abcdé…xyz_👍";
   std::wstring_convert<std::codecvt_utf8<char16_t>, char16_t> converter16;
   std::cout << "(A.3) Probe u16string      : \"" << converter16.to_bytes(sPrb16) << "\", length = " << sPrb16.length() << std::endl;

   // (A.4)
   std::u32string sPrb32 = U"Abcdé…xyz_👍";
   std::wstring_convert<std::codecvt_utf8<char32_t>, char32_t> converter32;
   std::cout << "(A.4) Probe u32string      : \"" << converter32.to_bytes(sPrb32) << "\", length = " << sPrb32.length() << std::endl;

   // (A.5)
   auto PrevTransMode = _setmode(_fileno(stdout), _O_U16TEXT);          // Set translation mode for wstrings
   std::wstring sPrbWs = L"Abcdé…xyz_👍";
   std::wstring_convert<std::codecvt_utf8<char16_t>, char16_t> converterWs;
   std::wcout << "(A.5) Probe wstring        : \"" << sPrbWs << "\", length = " << sPrbWs.length() << "\n";

   // (B.1)
   // After ref 20221015°1734 https://cplusplus.com/forum/windows/48175/
   // Box edges and corners (all double thick line)
   // T for top, B for bottom, L for left, R for right
   const wchar_t TB = L'\x2550';
   const wchar_t LR = L'\x2551';
   const wchar_t TL = L'\x2554';
   const wchar_t TR = L'\x2557';
   const wchar_t BL = L'\x255A';
   const wchar_t BR = L'\x255D';
   std::wstring hello  = L"Hello World!";
   std::wstring margin = L"  ";
   std::wstring line(hello.length() + 2 * margin.length(), TB);
   std::wcout << TL << line << TR << std::endl;
   std::wcout << LR << margin << hello << margin << LR << std::endl;
   std::wcout << BL << line << BR << std::endl;

   // Reset translation mode
   _setmode(_fileno(stdout), PrevTransMode);

   // (X.x)
   // After ref 20221015°1733 https://cplusplus.com/reference/cstdlib/wcstombs/
   std::cout << "(X.x) Other ...      : \"" << std::endl;
   const wchar_t str[] = L"Abcdé…xyz_👍";
   char buffer[32];
   int ret;
   //printf ("(X.x) wchar_t string: %ls \n\n", str);
   ret = wcstombs ( buffer, str, sizeof(buffer) );
   if (ret == 32) buffer[31] = '\0';
   if (ret) printf ("(X.y) Multibyte string: %s \n",buffer);

   std::cout << "Dovizhdane. ***" << std::endl;                   // Should be "Довиждане [Dovizhdane]."
}
