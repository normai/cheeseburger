﻿/**
 *  file       : id 20221002°0935 — gitlab.com/normai/cheeseburger …/cpp/cp127ternary.cpp
 *  version    : • 20221227°1616 v0.1.9 Filling • v0.1.8 20221002°0935 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate ternary operator
 *  userstory  : Roll dice and tell whether it was greater than three or not
 */
#define sVERSION "v0.1.9"

#include <iostream>
#include <random>
#include <string>

int main()
{
   std::cout << "*** Hej, det haer aer `cp127ternary.cpp` " << sVERSION << " -- Ternary operator ***" << std::endl;  // "Hej, det här är"


   // Preparation — Roll dice
   std::random_device rd;
   std::mt19937 mt(rd());                                              // Mersenne Twister with non-deterministic 32-bit seed
   std::uniform_int_distribution<int> dist(1, 6);
   int randi = dist(mt);
   std::cout << "You rolled " << std::to_string(randi) << std::endl;

   // The demo line
   std::cout << "You rolled greater three : " << (randi > 3 ? "Yes" : "No") << std::endl;


   std::cout << "Adjoe." << std::endl;  // "Adjö"
}
