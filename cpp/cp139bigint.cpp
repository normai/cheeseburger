﻿/**
 *  file       : id 20221117°1035 — gitlab.com/normai/cheeseburger …/cpp/cp139bigint.cpp
 *  version    : • 20221216°0918 v0.1.8 Filling • 20221117°1035 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate big integer types
 *  compile    : VS_Dev_Prompt > cl.exe cp139bigint.cpp /EHsc
 *  status     : Needs to use a library, e.g. GMP
 */
#define sVERSION "v0.1.8"

#include <iostream>
//#include <string>

int main()
{
   std::cout << "*** Hello, ez 'cp139bigint.cpp' " << sVERSION << " -- Big integers ***" << std::endl;
   std::cout << "Hint. Maximum value of a long = " << std::numeric_limits<long>::max() << std::endl;
   std::cout << "Note. Last correct result is 65536" << std::endl;


   long iBig = 2;
   for (int i = 2; i < 11; i++) {
      iBig = pow(iBig, 2);
      std::cout << " - " << i << " " << iBig << std::endl;
   }


   std::cout << "Viszlat." << std::endl;  // "Viszlát"
}
