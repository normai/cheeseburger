﻿// EMPTY STUB, YET TO BE FILLED WITH CODE

/**
 *  file       : id 20221213°1835 — gitlab.com/normai/cheeseburger …/cpp/cp281fntional.cpp
 *  version    : • 20xxxxxx°xxxx v0.x.x Filling • 20221213°1835 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate functional programming
 *  compile    : cl.exe 281fntional.cpp /EHsc
 *  summary    :
 *  userstory  :
 *  status     :
 *  ref        :
 */
#define sVERSION "v0.0.0"

#include <iostream>
//#include <string>

int main()
{
   std::cout << "*** Kon'nichiwa, desu `cp281fntional.cpp` " << sVERSION << " -- Functional programming ***" << std::endl;  // こんにちは、です [Kon'nichiwa, desu]





   std::cout << "Sayonara." << std::endl;  // さようなら [Sayōnara]
}
