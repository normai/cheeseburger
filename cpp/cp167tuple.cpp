﻿/**
 *  file       : id 20220829°0935 — gitlab.com/normai/cheeseburger/ …/cpp/cp167tuple.cpp
 *  versions   : • 20220906°1127 v0.1.7 Filling • 20220829°0935 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate C++ Tuples
 *  summary    : Some facts
 *                • The C++ tuple is a a bulky beast, and not a container type at all
 *                • No conversion to string
 *                • No foreach loop
 *                • Not sure wether subtuples are possibl
 *                •
 *  compile    : cl.exe cp167tuple.cpp /EHsc /std:c++17
 *  ref        : https://cplusplus.com/reference/tuple/ [ref 20220906°1012]
 *  ref        : https://cplusplus.com/reference/tuple/tuple/ [ref 20220906°1013]
 *  ref        : https://en.cppreference.com/w/cpp/utility/tuple [ref 20220906°1014]
 *  ref        : https://stackoverflow.com/questions/23436406/converting-tuple-to-string [ref 20220906°1032]
 *  ref        : https://stackoverflow.com/questions/25635503/how-do-you-iterate-over-tuple [ref 20220906°1033] Todo: Check that "auto l = {0, ((std::cout << args), 0)...};" [todo 20220906°1034]
 *  ref        : https://www.cppstories.com/2022/tuple-iteration-basics/ [ref 20220906°1042]
 *                then https://www.cppstories.com/2022/tuple-iteration-basics/#operator-
 *  ref        : https://www.quora.com/How-can-we-iterate-over-the-items-of-a-tuple-in-C-directly-If-there-is-no-easy-way-to-do-that-what-is-the-alternative-of-using-a-tuple [ref 20220906°1052]
 *  ref        : https://www.youtube.com/watch?v=7S4wB_jf9NA [ref 20220906°1121] "Any Data Type To String Conversion In C++"
 */
#define sVERSION "v0.1.7"

#include <iostream>                                             // std::cout, std::endl
#include <ostream>                                              // std::ostream& operator
#include <sstream>                                              // std::stringstream
#include <string>                                               // std::string
#include <tuple>                                                // std::tuple
//using namespace std;

// () Convert almost any value to string [func 20220906°1123] Code after ref 20220906°1121
template<typename T>
std::string itos(T x)
{
   std::stringstream s;
   s << x;
   return s.str();
}

//-----------------------------------------------------
// Function group after https://www.cppstories.com/2022/tuple-iteration-basics/#printing-stdtuple [ref 20220906°1042]

// () Func one [func 20220906°1046]
template <typename TupleT, std::size_t... Is>
std::ostream& printTupleImp(std::ostream& os, const TupleT& tp, std::index_sequence<Is...>) {
    size_t index = 0;
    auto printElem = [&index, &os](const auto& x) {
        if (index++ > 0)
            os << ", ";
        os << x;
    };

    os << "(";
    (printElem(std::get<Is>(tp)), ...);                                 // C++17
    os << ")";
    return os;
}

// () Func two [func 20220906°1045]
template <typename TupleT, std::size_t TupSize = std::tuple_size<TupleT>::value>
std::ostream& operator <<(std::ostream& os, const TupleT& tp) {
    return printTupleImp(os, tp, std::make_index_sequence<TupSize>{});
}
//-----------------------------------------------------


// () [func 20220906°1111] Original code after ref 20220906°1052
template<typename ... Ts>
void output_tuple(std::tuple<Ts...> const &tpl) {
    std::size_t length = sizeof...(Ts);
    std::apply(
        [length](auto const &...ps) {
            std::cout << "[ ";
            int k = 0;
            ((std::cout << ps << (++k == length ? "" : "; ")), ...);
            std::cout << " ]";
        },
        tpl);
}

// () [func 20220906°1113]
template<typename ... Ts>
std::size_t sizeOfTupel(std::tuple<Ts...> const &tpl) {
    return sizeof...(Ts);
}

// () Extract one item of the tuple as string, done brute force [func 20220906°1131]
// Summary: This fully iterates over the tuple with each call. Just it returns a string
//  with the string-conversion of the given indexed element. Much room for refinement!
//  Hint. Use c++ stream manipulator std::left!
template<typename ... Ts>
std::string getFromTuple(std::tuple<Ts...> const &tpl, int iNdx) {
    std::string sOut = "";
    std::size_t length = sizeof...(Ts);
    std::apply(
        [iNdx, length, &sOut](auto const &...ps) {
            int k = 0;
            (( sOut = (k++ == iNdx ? itos(ps) : sOut) ), ...);          // The alternative two values have to be of the same type
        },
        tpl);
    return sOut;
}

int main()
{
    std::cout << "*** Hallo, dit is 'cp167tuple.cpp' " << sVERSION << " -- Tuples ***" << std::endl;

    // (1) Create
    std::tuple<std::string, int, double, bool, std::string> tupl ("Values", 1234, 2.345, true, "Ding");
    std::cout << "(1.1) tupl = " << tupl << std::endl;                          // Stream output operator provided by func 20220906°1045
    std::cout << "(1.2) tupl = "; output_tuple(tupl); std::cout << std::endl;   // Using function 20220906°1111

    // (2) Info
    std::cout << "(2.1) Type = " << typeid(tupl).name() << std::endl;
    std::cout << "(2.2) Size = " << sizeOfTupel(tupl) << std::endl;

    // (3) Iterate
    std::cout << "(3) Iterate:" << std::endl;
    for (int iNdx = 0; iNdx < sizeOfTupel(tupl); iNdx++)
    {
        std::cout << " - " << getFromTuple(tupl, iNdx) << std::endl;
    }

    // (4) Pick by index
    // The get<>() function is evaluated at compile time, thus will not work
    //  with variable indices, means it was not helpful with above iteration
    std::cout << "(4.1) Third item = " << std::get<2>(tupl) << std::endl;
    std::cout << "(4.2) Fifth item = " << std::get<4>(tupl) << std::endl;

    std::cout << "Tot ziens." << std::endl;
}
