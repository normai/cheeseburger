﻿/**
 * file        : id 20220831°0835 — gitlab.com/normai/cheeseburger/ …/cpp/cp157array.cpp
 *  version    : • 20220908°1101 v0.1.7 Filling • 20220831°0835 v0.1.6 Stub
 * license     : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 * subject     : Demonstrate C++ arrays
 * summary     : This demonstrates C++ arrays in three servings:
 *                (1) Plain array with single-elements-definition
 *                (2) Plain array with bulk definition
 *                (3) Vector class, the common use C++ style array
 * reference   : https://www.w3schools.com/cpp/cpp_arrays.asp []
 */
#define sVERSION "v0.1.7"

#include <iostream>
#include <string>
#include <vector>

int main()
{
   // Using the wanted "*** Здравейте [Zdraveĭte] cp157array.cpp ***" causes compiler
   //  warning 'Character .. cannot be represented in the current code page (1252)'
   std::cout << "*** Zdraveite 'cp157array.cpp' " << sVERSION << " -- Array ***" << std::endl;     // The 'i' should be an 'ĭ' -- 

   // ==========================================================
   // (1) Archaic array, separate declaration plus bulky single-elements-definition
   // (1.1) Define some array
   std::string mountains[4];                                            // Rather use a constant for the size
   mountains[0] = "Zugspitze";
   mountains[1] = "Schneefernerkopf";
   mountains[2] = "Weather spike";
   mountains[3] = "Middle Höllental Peak";                              // BTW. "Hochwanner" is missing

   // (1.2) Retrieve size info
   std::cout << "(1.2.1) Mountains array memory consumption: " << sizeof mountains
              << " / " << sizeof(mountains) << " bytes" << std::endl
               ;
   int iNumberOfMountains = std::size(mountains);                      // Since C++17
   std::cout << "(1.2.2) Number of mountains: " << iNumberOfMountains << std::endl;

   // (1.3) Iterate over and output all elements
   for (int i = 0; i < iNumberOfMountains; i++)
   {
      std::cout << " - " << mountains[i] << std::endl;
   }

   // ==========================================================
   // (2) Archaic array, declaration and definition in one line
   // (2.1) Define some array
   std::string rivers[] = {"Danube", "Elbe", "Main", "Rhine", "Weser"};

   // (2.2) Retrieve size info
   std::cout << "(2.2.1) Rivers array memory consumption: " << sizeof rivers
              << " / " << sizeof(rivers) << " bytes" << std::endl
               ;
   int iNumberOfRivers = std::size(rivers);                             // Since C++17
   std::cout << "(2.2.2) Number of rivers: " << iNumberOfRivers << std::endl;

   // (2.3) Iterate over and output all elements
   for (int i = 0; i < std::size(rivers); i++)
   {
      std::cout << " - " << rivers[i] << std::endl;
   }

   // ==========================================================
   // (3) Vector class, the C++ style array
   // (3.1) Define some array
   std::vector<std::string> lakes;
   lakes.push_back("Bodensee");
   lakes.push_back("Müritz");
   lakes.push_back("Chiemsee");
   lakes.push_back("Schweriner See");
   lakes.push_back("Starnberger See");
   lakes.push_back("Ammersee");

   // (3.2) Retrieve size info
   std::cout << "(3.2.1) Lakes vector memory consumption: " << sizeof lakes
              << " / " << sizeof(lakes) << " bytes" << std::endl
               ;
   int iNumberOfLakes = std::size(lakes);
   std::cout << "(3.2.2) Number of lakes: " << iNumberOfLakes << std::endl;

   // (3.3) Iterate over and output all elements
   for (std::vector<std::string>::iterator it = lakes.begin(); it < lakes.end(); it++)
   {
      std::cout << " - " << *it << std::endl;
   }

   std::cout << "Dovizhdane." << std::endl;                           // Should be "Довиждане [Dovizhdane]."
}
