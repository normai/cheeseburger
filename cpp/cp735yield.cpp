﻿// EMPTY STUB, YET TO BE FILLED WITH CODE

/**
 *  file       : id 20221227°1035 — gitlab.com/normai/cheeseburger …/cpp/cp735yield.cpp
 *  version    : • 20xxxxxx°xxxx v0.x.x Filling • 20221227°1035 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate the yield keyword
 *  status     :
 */
#define sVERSION "v0.0.0"

#include <iostream>
//#include <string>

int main()
{
   std::cout << "*** Tse 'cp735yield.cpp' " << sVERSION << " -- Yield ***" << std::endl;  // "Це [Tse]"





   std::cout << "Do pobachennya." << std::endl;  // "до побачення [Do pobachennya]"
}
