﻿/**
 *  file       : id 20221117°1135 — gitlab.com/normai/cheeseburger …/cpp/cp147bigflo.cpp
 *  version    : • 20221228°1521 v0.1.8 Filling • 20221117°1135 v0.1.8 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate big integer types
 *  userstory  :
 *  dependency : ttmath.h [dld 20221228°1425]
 */
#define sVERSION "v0.1.8"

#include <iostream>
#include "libs/ttmath/ttmath.h"
#include <string>                                              // to_string

int main()
{
   std::cout << "*** Hailo, yah hai `cp147bigflo.cpp` " << sVERSION << " -- Big floats ***" << std::endl;  // हैलो, यह है [Hailo, yah hai]


   // Todo : Elaborate the different precision on the output [todo 20221228°1221]
   std::cout << "(1) CPP native types:" << std::endl;
   float f1 = 10.0 / 7.0;
   float f2 = 10.0 / 3.0;
   float f3 = 10.0 / 1.5;
   double d1 = 10.0 / 7.0;
   double d2 = 10.0 / 3.0;
   double d3 = 10.0 / 1.5;
   long double l1 = 10.0 / 7.0;
   long double l2 = 10.0 / 3.0;
   long double l3 = 10.0 / 1.5;
   std::cout << "(1.1) Floats:" << std::endl;
   std::cout << "(1.1.1) 10 / 7   = " << std::to_string(f1) << std::endl;
   std::cout << "(1.1.2) 10 / 3   = " << std::to_string(f2) << std::endl;
   std::cout << "(1.1.3) 10 / 1.5 = " << std::to_string(f3) << std::endl;
   std::cout << "(1.2) Doubles:" << std::endl;
   std::cout << "(1.2.1) 10 / 7   = " << std::to_string(d1) << std::endl;
   std::cout << "(1.2.2) 10 / 3   = " << std::to_string(d2) << std::endl;
   std::cout << "(1.2.3) 10 / 1.5 = " << std::to_string(d3) << std::endl;
   std::cout << "(1.3) Long doubles:" << std::endl;
   std::cout << "(1.3.1) 10 / 7   = " << std::to_string(l1) << std::endl;
   std::cout << "(1.3.2) 10 / 3   = " << std::to_string(l2) << std::endl;
   std::cout << "(1.3.3) 10 / 1.5 = " << std::to_string(l3) << std::endl;
   std::cout << std::endl;


   std::cout << "(2) Big<6,6> type from TTMath:" << std::endl;
   //ttmath::Big<1,2> bigTen = "10.0";                         // Big<exponent, mantissa>
   ttmath::Big<6,6> bigTen = "10.0";                           // Big<exponent, mantissa>
   ttmath::Big<6,6> bigSeven = "7.0";
   ttmath::Big<6,6> bigThree = "3.0";
   ttmath::Big<6,6> bigOneFive = "1.5";
   ttmath::Big<6,6> bigTenBySeven = bigTen / bigSeven;
   ttmath::Big<6,6> bigTenByThree = bigTen / bigThree;
   ttmath::Big<6,6> bigTenByOneFive = bigTen / bigOneFive;
   std::cout << "(2.1.1) 10 / 7        = " << bigTenBySeven << std::endl;
   std::cout << "(2.1.2) 10 / 3        = " << bigTenByThree << std::endl;
   std::cout << "(2.1.3) 10 / 1.5      = " << bigTenByOneFive << std::endl;
   std::cout << std::endl;


   ttmath::Big<6,6> bigPi100 = "3.1415926535897932384626433832795028841971693993751058209749445923078164062862089986280348253421170679"; // Hundred decimals [ref 20221216°1734]
   std::cout << "(2.2.1) Pi-100        = " << bigPi100 << std::endl;

   ttmath::Big<6,6> a, b, c, d, c1, c2;
   a = "22.0";
   b = "7.0";
   c = "355.0";
   d = "113.0";
   c1 = "62832.0";
   c2 = "20000.0";
   std::cout << "(2.2.2) 22 / 7        = " << a / b << std::endl;
   std::cout << "(2.2.3) 355 / 113     = " << c / d << std::endl;
   std::cout << "(2.2.4) 62832 / 20000 = " << c1 / c2 << std::endl;


   std::cout << "Alavida." << std::endl;  // अलविदा [Alavida]
}
