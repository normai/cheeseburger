﻿/**
 *  file       : id 20220215°0935 — gitlab.com/normai/cheeseburger … cpp/cp131types.cpp
 *  version    : • 20220908°1001 v0.1.7 Filling • 20220215°0935 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate some operators
 *  summary    : This demonstrates some operators
 *  compile    :
 *  ref        :
 *  status     : NOT READY, DOES NOT COMPILE
 */
#define sVERSION "v0.1.7"

#include <iostream>
#include <limits>                                      // LONG_MAX, std::numeric_limits<>::max()
#include <string>

int main()
{
   std::cout << "*** Abhinandan 'cp131types.cpp' " << sVERSION << " -- Demonstrate some operators ***" << std::endl;  // "अभिनंदन"  cannot be represented …

   // Some declarations
   char iByte1 = 127;
   short iShort1 = 32767;
   int iInt1 = 2147483647;
   long iLong1 = LONG_MAX;
   float f1 = 1.23f;                                   // The 'f' postfix prevents error 'lossy conversion'
   double d1 = std::numeric_limits<double>::max(); //// double.MaxValue;
   bool b1 = true;
   char c1 = 'x';
   std::string s1 = "Holla";

   // Some operations
   char iByte2 = (char) (iByte1 + ((char) 1));         // Force the lossy conversion from int to byte
   short iShort2 = (short) (iShort1 + ((short) 1));    // Force lossy conversion from int to short
   int iInt2 = iInt1 + 1;
   long iLong2 = iLong1 + 1;
   double f2 = f1 + 1;
   double d2 = d1 + 1;
   bool b2 = !b1;                                      // The plus operator is not possible with boolean
   char c2 = (char) (c1 + ((char) 1));                 // Note the tricky parentheses placement
   std::string s2 = s1 + std::to_string(1);            //

   // Display the results
   //// std::cout << "(1) " << iByte1 + " + 1 = " << iByte2 << " (Byte.MaxValue = " << Byte.MaxValue << ")" << std::endl;
   //// std::cout << "(2) " << iShort1 << " + 1 = " << iShort2 << " (Int16.MaxValue = " << Int16.MaxValue << ")" << std::endl;
   std::cout << "(3) " << iInt1 << " + 1 = " << iInt2 << std::endl;
   std::cout << "(4) " << iLong1 << " + 1 = " << iLong2 << std::endl;
   std::cout << "(5) " << f1 << " + 1 = " << f2 << std::endl;
   std::cout << "(6) " << d1 << " + 1 = " << d2 << std::endl;
   std::cout << "(7) !" << b1 << " = " << b2 << std::endl;
   std::cout << "(8) " << s1 << " + 1 = " << s2 << std::endl;

   std::cout << "Alavida." << std::endl;               // "अलविदा" cannot be represented …
}
