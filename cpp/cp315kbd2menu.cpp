﻿// THIS FILE IS AN EMPTY STUB, YET TO BE FILLED WITH CODE

/**
 *  file       : id 20221116°1535 — gitlab.com/normai/cheeseburger …/cpp/cp315kbd2menu.cpp
 *  version    : • 20xxxxxx°xxxx v0.x.x Filling • 20221116°1535 v0.1.8 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate keyboard menu without pressing Enter
 *  userstory  :
 *  summary    :
 */
#define sVERSION "v0.0.0"

#include <iostream>
//#include <string>

int main()
{
   std::cout << "*** Hallo, hier ist `cp315kbd2menu.cpp` " << sVERSION << " -- Keyboard menu (without Enter) ***" << std::endl;





   std::cout << "Auf Wiedersehen." << std::endl;
}
