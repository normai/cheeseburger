﻿// EMPTY STUB, YET TO BE FILLED WITH CODE

/**
 *  file       : id 20221213°1735 — gitlab.com/normai/cheeseburger …/cpp/cp278lambda.cpp
 *  version    : • 20xxxxxx°xxxx v0.x.x Filling • 20221213°1735 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate lambda expressions
 *  compile    : cl.exe 278lambda.cpp /EHsc
 *  summary    :
 *  userstory  :
 *  status     :
 *  ref        :
 */
#define sVERSION "v0.0.0"

#include <iostream>
//#include <string>

int main()
{
   std::cout << "*** Bongiorno, sono `cp278lambda.cpp` " << sVERSION << " -- Lambda expressions ***" << std::endl;





   std::cout << "Arrivederci." << std::endl;
}
