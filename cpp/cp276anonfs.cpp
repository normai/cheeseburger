﻿// EMPTY STUB, YET TO BE FILLED WITH CODE

/**
 *  file       : id 20220828°1635 — gitlab.com/normai/cheeseburger …/cpp/cp276anonfs.cpp
 *  version    : • 20xxxxxx°xxxx v0.x.x Filling • 20220828°1635 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate anonymous functions
 *  compile    : cl.exe cp276anonfs.cpp /EHsc
 *  summary    :
 *  userstory  :
 *  status     :
 *  ref        :
 */
#define sVERSION "v0.0.0"

#include <iostream>
//#include <string>

int main()
{
   std::cout << "*** Buna, acesta este 'cp276anonfs.cpp' " << sVERSION << " -- Anonymous functions ***" << std::endl;  // "Bună, acesta este"





   std::cout << "La revedere." << std::endl;
}
