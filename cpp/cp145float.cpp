﻿/**
 *  file       : id 20221012°1135 — gitlab.com/normai/cheeseburger …/cpp/cp145float.cpp
 *  subject    : Demonstrate Float Types
 *  version    : • 20221216°1117 v0.1.8 Filling • 20221012°1135 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  compile    : VS-Dev-Prompt > cl.exe cp145float.cpp /EHsc
 *  status     : Has room for enhancement. Should use string formatting for number output.
 */
#define sVERSION "v0.1.8"

#include <iostream>

int main()
{
    std::cout << "*** Dobry den, toto je 'cp145float.cpp' " << sVERSION << " -- Float Types ***" << std::endl;  // "Dobrý den, toto je"


    std::cout << "(1) Print some numbers:" << std::endl;
    double f1 = 123456.789;
    double f2 = -123456.789;
    double f3 = 123456777777777777777777777.789;
    double f4 = -123456777777777777777777777.789;
    std::cout << "(1.1) f1 = " << f1 << " " << typeid(f1).name() << std::endl;
    std::cout << "(1.2) f2 = " << f2 << " " << typeid(f2).name() << std::endl;
    std::cout << "(1.3) f3 = " << f3 << " " << typeid(f3).name() << std::endl;
    std::cout << "(1.4) f4 = " << f4 << " " << typeid(f4).name() << std::endl;


    std::cout << "Nashledanou." << std::endl;
}
