﻿/**
 * file        : id 20220828°1735 — gitlab.com/normai/cheeseburger/ …/cpp/cp161list.cpp
 * version     : • 20220908°1201 v0.1.7 Filling • 20220828°1735 v0.1.6 Stub
 * license     : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 * subject     : Demonstrate C++ list class
 * ref         : https://cplusplus.com/reference/list/list/ []
 * ref         : https://stackoverflow.com/questions/58170838/index-based-access-to-a-c-list [] C++ list has no index based access
 * ref         : https://www.guru99.com/cpp-list.html [] Guru99 article "std::list in C++ with Example" by Barbara Thompson on 2022-Aug-25
 * ref         : https://en.cppreference.com/w/cpp/utility/any []
 * compile     : ">cl.exe /EHsc /std:c++17 cp161list.cpp"
 * todo        : Outsource the complex part, so the other can be compiled with standard C++14, and only
 *                the complex part needs the compiler commandline args for C++17 [todo 20220901°1021]
 */
#define sVERSION "v0.1.7"

#include <any>                                                 // Since C++17
#include <iostream>
#include <list>
#include <string>

int main()
{
   std::cout << "*** Ni hao 'cp161list.cpp' " << sVERSION << " -- List ***" << std::endl;      // "你好 (Nǐ hǎo)" Compiler "warning C4566: character represented by universal-character-name '\u4F60' cannot be represented in the current code page (1252)"

   // (1) Simple list
   // (1.1) Create
   std::list<std::string> islands;
   islands.push_back ("Rügen");
   islands.push_back ("Usedom");
   islands.push_back ("Fehmarn");
   islands.push_back ("Sylt");
   islands.push_back ("Föhr");
   islands.push_back ("Pellworm");

   // (1.2) Print some infos
   std::cout << "(1.1) Type = " << typeid(islands).name() << std::endl;
   std::cout << "(1.2) Length =" << islands.size() << std::endl;
   ////std::cout << "(1.3) Content = " << std::to_string(islands) << std::endl;

   // (1.3) Iterate
   std::list<std::string>::iterator it1;
   for (it1 = islands.begin(); it1 != islands.end(); it1++)
   {
      std::cout << " - " << *it1 << std::endl;
   }

   // (2) Complex list
   // (2.1) Create
   std::list<std::any> mixture;
   mixture.push_back("Pink");
   mixture.push_back(true);
   mixture.push_back(123);
   mixture.push_back(2.34);
   std::list<std::string> helplist = { "Aha", "Oha", "Uhu"};
   mixture.push_back(helplist);

   // (2.2) Print some infos
   std::cout << "(2.1) Type = " << typeid(mixture).name() << std::endl;
   std::cout << "(2.2) Length =" << mixture.size() << std::endl;

   // (2.3) Iterate
   std::list<std::any>::iterator it2;
   for (it2 = mixture.begin(); it2 != mixture.end(); it2++)
   {
      std::cout << " - " << (*it2).type().name() << std::endl;
   }

   std::cout << "Zaijian." << std::endl;                       // "再见 [Zàijiàn]."
}
