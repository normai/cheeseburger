﻿/**
 *  file       : id 20220825°1435 — gitlab.com/normai/cheeseburger … cpp/cp115varis.cpp
 *  version    : • 20220908°0901 v0.1.7 Filling • 20220825°1435 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  summary    :
 */
#define sVERSION "v0.1.7"

#include <iostream>                                    // std::cout
#include <string>                                      // for to_string()

int main()
{
   std::cout << "*** Hello, this is `cp115varis.cpp` " << sVERSION << " -- Variables ***" << std::endl;

   int a = 123;                                        // Integer
   double b = 2.34;                                    // Float
   std::string c = "Ahoj";                             // String
   std::string d = c + " = ";
   double e = a + b;
   std::cout << d << e << std::endl;

   std::cout << "Good bye." << std::endl;
}
