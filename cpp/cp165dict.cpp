﻿/**
 *  file       : id 20220828°1935 — gitlab.com/normai/cheeseburger/ …/cpp/cp165dict.cpp
 *  version    : • 20220908°1401 v0.1.7 Filling • 20220828°1935 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate dictionary in CPP
 *  summary    :
 *  ref        : https://www.codespeedy.com/dictionary-in-cpp/ [ref 20220904°0922]
 *  ref        : https://www.techiedelight.com/how-to-pad-strings-in-cpp/ [ref 20220904°0923] -- About string padding. Has not what I want.
 *  ref        : https://stackoverflow.com/questions/36908994/using-sprintf-with-stdstring-in-c [ref 20220904°0924]
 *                contribution https://stackoverflow.com/a/36909699
 *  ref        : https://cplusplus.com/reference/cstdio/snprintf/ [ref 20220904°0925]
 *  ref        : https://cplusplus.com/reference/cstdio/printf/ [ref 20220904°0926] -- Here are the format specifiers listed
 *  todo       : std::setw() offers only left padding. Does it? Have you tried negative values?! [todo 20220904°1932]
 */
#define sVERSION "v0.1.7"

//#include <iomanip>                                           // std::setw() — Offers only left padding.
#include <iostream>
#include <map>
#include <string>

int main()
{
    std::cout << "*** Hej, das ist 'cp165dict.cpp' " << sVERSION << " -- Dictionary ***" << std::endl;

    // (1) Create
    std::map<std::string, std::string> being;
    being["kingdom"] = "Animals";
    being["class"]   = "Reptiles";
    being["genus"]   = "Crocodile";
    being["species"] = "Nile crocodile";
    being["name"]    = "Schnappi";
    being["color"]   = "Green";
    being["legs"]    = "Four";
    being["food"]    = "Meat";
    std::cout << "(1) Created = " << typeid(being).name() << std::endl;

    // (2) Info
    std::cout << "(2) Map size = " << being.size() << std::endl;

    // (3) Iterate
    std::cout << "(3) Iterate:" << std::endl;
    std::map<std::string, std::string>::iterator it;
    for (it = being.begin(); it != being.end(); it++)
    {
        // Works, but has left padding, we want right padding
        //std::cout << " - " << std::setw(10) << (*it).first << " : " << (*it).second << std::endl;

        // Note. C++ offers std::setw() for left padding. Buit it has nothing to
        // offer for right-padding. Thus we want facilitate the C string formatting
        // feature. Only there is great fuzz because that one needs char* type.

        // (3.1) Seqence after ref 20220904°0924 contribution https://stackoverflow.com/a/36909699
        auto format = " - %-8s : %s";
        char buf [111];                                        // Replacement for the nullptr used in ref 20220904°0924
        const char * c1 = (*it).first.c_str();
        const char * c2 = (*it).second.c_str();
        auto size = std::snprintf(buf, 111, format, c1, c2);   // Does not work with nullptr as told in ref 20220904°0924
        std::string output(size + 1, '\0');
        std::sprintf(&output[0], format, c1, c2);

        // (3.2) Final output including right padding
        std::cout << output << std::endl;
    }

    std::cout << "Farvel." << std::endl;
}
