﻿// THIS FILE IS AN EMPTY STUB, YET TO BE FILLED WITH CODE

/**
 *  file       : id 20221116°1735 — gitlab.com/normai/cheeseburger …/cpp/cp722varargs.cpp
 *  version    : • 20xxxxxx°xxxx v0.x.x Filling • 20221116°1735 v0.1.8 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate VarArgs
 *  userstory  :
 *  summary    :
 */
#define sVERSION "v0.0.0"

#include <iostream>
//#include <string>

int main()
{
   std::cout << "*** Aloha, o keia `cp722varargs.cpp` " << sVERSION << " -- VarArgs ***" << std::endl;  // "Aloha, ʻo kēia"





   std::cout << "Aloha mai." << std::endl;
}
