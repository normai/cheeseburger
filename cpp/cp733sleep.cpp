﻿// EMPTY STUB, YET TO BE FILLED WITH CODE

/**
 *  file       : id 20221213°1935 — gitlab.com/normai/cheeseburger …/cpp/cp733sleep.cpp
 *  version    : • 20xxxxxx°xxxx v0.x.x Filling • 20221213°1935 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate a sleep function
 *  compile    : VS-Dev-Prompt > cl.exe 733sleep.cpp /EHsc
 *  summary    :
 *  userstory  :
 *  status     :
 *  ref        :
 */
#define sVERSION "v0.0.0"

#include <iostream>
//#include <string>

int main()
{
   std::cout << "*** Sis ir `cp733sleep.cpp` " << sVERSION << " -- Sleep function ***" << std::endl;  // "Šis ir"





   std::cout << "Uz redzesanos." << std::endl;  // "Uz redzēšanos"
}
