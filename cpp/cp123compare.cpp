﻿/**
 *  file       : id 20220826°1035 — gitlab.com/normai/cheeseburger …/cpp/cp123compare.cpp
 *  version    : • 20220908°1621 v0.1.7 Filling • 20220826°1035 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate C++ comparison operators
 *  summary    :
 *  ref        : https://www.w3schools.com/cpp/cpp_operators.asp [ref 20220906°1422]
 *  ref        : https://www.w3schools.com/cpp/cpp_operators_comparison.asp [ref 20220906°1423]
 *  status     :
 */
#define sVERSION "v0.1.7"

#include <iostream>
#include <string>

int main()
{
   const double THRESHOLD_1 = 0.0000000001;  // Problem — The tolerance has to be different, depending on the magnitude of the number!

   std::cout << "*** Tere, see on 'cp123compare.cpp' " << sVERSION << " -- Comparison operators ***" << std::endl;

   std::cout << "(1.1)  \"Ahoj\" == \"Aloha\"         = " << ("Ahoj" == "Aloha") << std::endl;
   std::cout << "(1.2)  12345  == 23456           = " << (12345  == 23456) << std::endl;
   std::cout << "(1.3)  3.4567 == 4.5678          = " << (3.4567 == 4.5678) << std::endl;
   std::cout << "(1.4)  true   == false           = " << (true   == false) << std::endl;
   std::cout << std::endl;
   std::cout << "(2.1)  \"Ahoj\" != \"Aloha\"         = " << ("Ahoj" != "Aloha") << std::endl;
   std::cout << "(2.2)  12345  != 23456           = " << (12345  != 23456) << std::endl;
   std::cout << "(2.3)  3.4567 != 4.5678          = " << (3.4567 != 4.5678) << std::endl;
   std::cout << "(2.4)  true   != false           = " << (true   != false) << std::endl;
   std::cout << std::endl;
   std::cout << "(3.1)  strcmp(\"Ahoj\", \"Aloha\") = " << strcmp("Ahoj", "Aloha") << std::endl;
   std::cout << "(3.2)  12345  >  23456           = " << (12345  > 23456) << std::endl;
   std::cout << "(3.3)  3.4567 >  4.5678          = " << (3.4567 > 4.5678) << std::endl;
   std::cout << "(3.4)  true   >  false           = " << (true   > false) << std::endl;
   std::cout << std::endl;
   std::cout << "(4.1)  \"Ahoj\" >= \"Aloha\"         = " << ("Ahoj" >= "Aloha") << std::endl;
   std::cout << "(4.2)  12345  >= 23456           = " << (12345  >= 23456) << std::endl;
   std::cout << "(4.3)  3.4567 >= 4.5678          = " << (3.4567 >= 4.5678) << std::endl;
   std::cout << "(4.4)  true   >= false           = " << (true   >= false) << std::endl;
   std::cout << std::endl;
   std::cout << "(5.1)  \"Ahoj\" <  \"Aloha\"         = " << ("Ahoj" < "Aloha") << std::endl;
   std::cout << "(5.2)  12345  <  23456           = " << (12345  < 23456) << std::endl;
   std::cout << "(5.3)  3.4567 <  4.5678          = " << (3.4567 < 4.5678) << std::endl;
   std::cout << "(5.4)  true   <  false           = " << (true   < false) << std::endl;
   std::cout << std::endl;
   std::cout << "(6.1)  \"Ahoj\" >= \"Aloha\"         = " << ("Ahoj" >= "Aloha") << std::endl;
   std::cout << "(6.2)  12345  >= 23456           = " << (12345  >= 23456) << std::endl;
   std::cout << "(6.3)  3.4567 >= 4.5678          = " << (3.4567 >= 4.5678) << std::endl;
   std::cout << "(6.4)  true   >= false           = " << (true   >= false) << std::endl;
   std::cout << std::endl;
   std::cout << "(7.1)  0.1 + 0.2 == 0.3          = " << (0.1 + 0.2 == 0.3) << std::endl;
   std::cout << "(7.2)  abs((0.1 + 0.2) - 0.3) < THRESHOLD_1 = " << (abs((0.1 + 0.2) - 0.3) < THRESHOLD_1) << std::endl;
   std::cout << std::endl;
   std::cout << "(8.1)  strcmp(\"Ahoj\", \"Aloha\") = " << strcmp("Ahoj", "Aloha") << std::endl;
   std::cout << "(8.1)  strcmp(\"Aloha\", \"Ahoj\") = " << strcmp("Aloha", "Ahoj") << std::endl;
   std::cout << "(8.1)  strcmp(\"Ahoj\", \"Ahoj\")  = " << strcmp("Ahoj", "Ahoj") << std::endl;

   std::cout << "Huevasti." << std::endl;  // "Hüvasti"
}
