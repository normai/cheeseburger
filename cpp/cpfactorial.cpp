﻿/**
 *  file       : id 20221215°1135 — gitlab.com/normai/cheeseburger …/cpp/cpfactorial.cpp
 *  version    : • 20221216°0926 v0.1.8 Filling • 20221215°1135 v0.1.6 Stub
 *  subject    : Demonstrate factorial calculation
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  compile    : VS_Dev_Prompt > "cl.exe cpfactorial.cpp /EHsc"
 *  status     :
 */
#define sVERSION "v0.1.8"

#include <iostream>

// Function 20221215°1431 Recursion naive
long Factorial(long iVal) {
   if (iVal > 1) {
      iVal = iVal * Factorial(iVal - 1);
      return iVal;
   }
   else {
      return 1;
   }
}

int main()
{
   std::cout << "*** Sveiki, tai 'cpfactorial.cpp' " << sVERSION << " -- Factorial ***" << std::endl;
   std::cout << "Hint: C+ long maximum value = " << std::numeric_limits<long>::max() << "." << std::endl;


for (long i = -1; i < 24; i++) {
   long iFact = Factorial(i);
   std::cout << " - " << i << "! = " << iFact << std::endl;
}


   std::cout << "Iki pasimatymo." << std::endl;
}
