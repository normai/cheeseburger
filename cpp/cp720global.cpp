﻿// THIS FILE IS AN EMPTY STUB, YET TO BE FILLED WITH CODE

/**
 *  file       : id 20220830°1635 — gitlab.com/normai/cheeseburger/ …/cpp/cp720global.cpp
 *  version    : • 20xxxxxx°xxxx v0.x.x Filling • 20220830°1635 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate
 */
#define sVERSION "v0.0.0"

#include <iostream>
#include <string>

int main()
{
   std::cout << "*** Sveiki, sis ir `cp720global.cpp` " << sVERSION << " -- Global keyword ***" << std::endl;  // Errors "character cannot be represented …" in "Sveiki, šis ir …"




   std::cout << "Uz redzesanos." << std::endl;  // Errors "character cannot be represented …" in "Uz redzēšanos."
}
