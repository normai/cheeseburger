﻿// EMPTY STUB, YET TO BE FILLED WITH CODE

/**
 *  file       : id 20221227°1135 — gitlab.com/normai/cheeseburger …/cpp/cp767docoms.cpp
 *  version    : • 20xxxxxx°xxxx v0.x.x Filling • 20221227°1135 v0.1.8 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate documentation comments
 *  status     :
 */
#define sVERSION "v0.0.0"

#include <iostream>
//#include <string>

int main()
{
   std::cout << "*** Tova e 'cp767docoms.cpp' " << sVERSION << " -- Documentation comments ***" << std::endl;  // "Това е [Tova e]"





   std::cout << "Dovizhdane." << std::endl;  // "Довиждане [Dovizhdane]"
}
