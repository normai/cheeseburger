﻿/**
 *  file       : id 20221012°1635 — gitlab.com/normai/cheeseburger …/cpp/cp153stripol.cpp
 *  version    : • 20221013°1035 v0.1.8 Filling • 20221012°1635 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate String Interpolation
 *  compile    : > cl.exe cp153stripol.cpp /EHsc /std:c++20
 *  userstory  :
 *  summary    :
 *  ref        : https://stackoverflow.com/questions/63121776/simplest-syntax-for-string-interpolation-in-c [ref 20221013°0914]
 *  ref        : https://stackoverflow.com/questions/37956090/how-to-construct-a-stdstring-with-embedded-values-i-e-string-interpolation [ref 20221013°0915]
 */
#define sVERSION "v0.1.8"

#include <format>                                                       // std::format -- Needs compiler option /std:c++20
#include <iostream>
#include <string>

int main()
{
   std::cout << "*** Hej, det er 'cp153stripol.cpp' " << sVERSION << " -- String Interpolation ***" << std::endl;

   // Provide fodder
   int iX = 234;
   std::string sHello = "Hello";
   std::string sJenny = "Jenny";

   // (.) String interpolation
   // (.1)
   std::cout << "(2.1) Output chaining           : ";
   std::cout << sHello << " " << iX << " " << sJenny << "." << std::endl;

   // (.2)
   std::cout << "(2.2) String concatenation      : ";
   std::string s22 = sHello + " " + std::to_string(iX) + " " + sJenny + ".";
   std::cout << s22 << std::endl;

   // (.3)
   std::cout << "(2.3) C++20 format w/o indices  : ";
   std::cout << std::format("{} {} {}.", sHello, iX, sJenny) << std::endl;  // Since C++20, Python style formatting

   // (.4)
   std::cout << "(2.4) C++20 format with indices : ";
   std::cout << std::format("{0} {1} {2}.", sHello, iX, sJenny) << std::endl;  // Since C++20, Python style formatting

   // (.5)
   std::cout << "(2.5) C printf                  : ";
   printf("%s %i %s.", sHello.c_str(), iX, sJenny.c_str());
   std::cout << std::endl;

   std::cout << "Farvel." << std::endl;
}
