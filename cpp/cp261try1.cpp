﻿// THIS FILE IS AN EMPTY STUB, YET TO BE FILLED WITH CODE

/**
 *  file       : id 20220826°1935 — gitlab.com/normai/cheeseburger/ …/cpp/cp261try1.cpp
 *  version    : • 20220910°1531 v0.x.x Filling • 20220826°1935 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate
 */
#define sVERSION "v0.0.0"

#include <iostream>
#include <string>

int main()
{
   std::cout << "*** Kon'nichiwa, desu `cp261try1.cpp` " << sVERSION << " -- Try (simple) ***" << std::endl;  // Errors "character cannot be represented …" in "こんにちは、です [Kon'nichiwa, desu] …"



   // (1) Try adding mismatching types -- NOT AN EXCEPTION



   // (2) Calculate square root, sometimes succeed, sometimes fail



   std::cout << "Sayonara." << std::endl;  // Errors "character cannot be represented …" in "さようなら [Sayōnara]."
}
