﻿/**
 *  file       : id 20160216°1221 — gitlab.com/normai/cheeseburger …/cpp/cp111hello.cpp
 *  version    : • 20220908°0701 v0.1.7 Filling • 20160216°1221 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate a most simple C++ program with user input
 *  compile    : VS-Dev-Prompt > cl.exe cp111hello.cpp /EHsc
 */
#define sVERSION "v0.1.7"

#include <iostream>
#include <string>

int main()
{
   std::cout << "*** Hello, this is cp111hello.cpp " << sVERSION << " -- Hello World ***" << std::endl;

   std::cout << "Please enter your name > ";
   std::string sWord;
   std::cin >> sWord;
   std::cout << "Good morning \"" << sWord << "\"!" << std::endl;

   std::cout << "Bye." << std::endl;
   return 0;
}
