﻿// THIS FILE IS AN EMPTY STUB, YET TO BE FILLED WITH CODE

/**
 *  file       : id 20221116°1635 — gitlab.com/normai/cheeseburger …/cpp/cp319kbd3menu.cpp
 *  version    : • 20xxxxxx°xxxx v0.x.x Filling • 20221116°1635 v0.1.8 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate keyboard menu using a GUI function
 *  userstory  :
 *  summary    :
 */
#define sVERSION "v0.0.0"

#include <iostream>
//#include <string>

int main()
{
   std::cout << "*** Aftó eínai to 'cp319kbd3menu.cpp' " << sVERSION << " -- Keyboard menu (GUI function) ***" << std::endl;  // "Γεια σας, αυτό είναι το"





   std::cout << "Antio sas." << std::endl;  // "Αντίο σας"
}
