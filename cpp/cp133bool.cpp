﻿/**
 *  file       : id 20221012°0835 — gitlab.com/normai/cheeseburger …/cpp/cp133bool.cpp
 *  version    : • 20221014°1135 v0.1.8 Filling • 20221012°0835 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate Boolean Type
 *  compile    : VS Dev Cmd Prompt > cl.exe cp133bool.cpp /EHsc /std:c++17
 *  userstory  :
 *  summary    :
 *  ref        : https://btechgeeks.com/cpp-list-find-contains-how-to-search-an-element-in-stdlist/ [ref 20221014°1012]
 *  ref        : https://en.cppreference.com/w/cpp/algorithm/find [ref 20221014°1022]
 *  ref        : https://en.cppreference.com/w/cpp/algorithm/transform [ref 20221014°1023]
 */
#define sVERSION "v0.1.8"

#include <algorithm>                                                    // std::transform
#include <execution>                                                    // Since c++17
#include <iostream>
#include<list>
#include <string>

int main()
{
   std::cout << "*** Pryvit, tse 'cp133bool.cpp' " << sVERSION << " -- Template code ***" << std::endl;

   // (1) The naive test series
   std::cout << "(1) Naive test series:" << std::endl;
   int * iNone = nullptr;
   bool bFalse = false;
   bool bTrue = true;
   int iMinus = -1;
   int iZero = 0;
   int iOne = 1;
   int iTwo = 2;
   double fMinus = -0.1;
   double fZero = 0.0;
   double fOne = 0.1;
   double fTwo = 1.23;
   std::cout << "(1.1)  (bool) nullptr = " << (bool) iNone << std::endl;
   std::cout << "(1.2)  False         = " << bFalse << std::endl;
   std::cout << "(1.3)  True         = " << bTrue << std::endl;
   std::cout << "(1.4)  (bool) -1   = " << (bool) iMinus << std::endl;
   std::cout << "(1.5)  (bool) 0    = " << (bool) iZero << std::endl;
   std::cout << "(1.6)  (bool) 1    = " << (bool) iOne << std::endl;
   std::cout << "(1.7)  (bool) 2    = " << (bool) iTwo << std::endl;
   std::cout << "(1.8)  (bool) -0.1 = " << (bool) fMinus << std::endl;
   std::cout << "(1.9)  (bool) 0.0  = " << (bool) fZero << std::endl;
   std::cout << "(1.10) (bool) 0.1  = " << (bool) fOne << std::endl;
   std::cout << "(1.11) (bool) 1.23 = " << (bool) fTwo << std::endl;
   std::cout << "(1.12) (bool) ''   = " << (bool) "" << std::endl;
   std::cout << "(1.13) (bool) 'x'  = " << (bool) "x" << std::endl;

   // (2) Targeted conversion
   std::cout << "(2) Targeted conversion:" << std::endl;
   std::list<std::string> truevals = { "1", "j", "ja", "t", "true", "y", "yes" } ;
   std::list<std::string> inputs = {"", "Bla", "Ja", "No", "True", "Yes"} ;
   for (std::string x : inputs) {
      std::transform(std::execution::par, x.begin(), x.end(), x.begin(), ::tolower);  // Compare in cp155strfunc.cpp solution after ref 20221013°0923 https://stackoverflow.com/a/735215
      auto r = std::find(truevals.begin(), truevals.end(), x);
      bool b = r != std::end(truevals) ? true : false;
      std::cout << " -" << x << " -> " << b << std::endl;
   }

   std::cout << "Do pobachennya. ***" << std::endl;
}
