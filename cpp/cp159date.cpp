﻿// THIS FILE IS AN EMPTY STUB, YET TO BE FILLED WITH CODE

/**
 *  file       : id 20221012°1835 — gitlab.com/normai/cheeseburger …/cpp/cp159date.cpp
 *  version    : • 20xxxxxx°xxxx v0.x.x Filling • 20221012°1835 v0.1.7 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate Date and Time Types
 *  userstory  : Set and print dates in various formats and calculate time spans
 */
#define sVERSION "v0.0.0"

#include <chrono>
#include <ctime>
#include <iostream>
//#include <string>

int main()
{
   std::cout << "*** Tere, see on 'cp159date.cpp' " << sVERSION << " -- Date and Time ***" << std::endl;
   std::cout << std::endl;

   // () Remember start time
   auto chrStart = std::chrono::system_clock::now();             // Type: "std::chrono::time_point<struct std::chrono::system_clock,class std::chrono::duration<__int64,struct std::ratio<1,10000000>>>"
   if (false) {
      std::cout << "   Type: " << typeid(chrStart).name() << std::endl;
   }
   std::time_t timeNow = std::chrono::system_clock::to_time_t(chrStart);
   std::cout << "(.) system_clock now : " << std::ctime(&timeNow) << std::endl;


   // (1) Limits


   // (2) Galileo


   // (3) Now


   // (4) Halley


   // (5) Calculate large scale time span


   // (6) Calculate short scale time span






   // Calculate runtime
   auto chrStop = std::chrono::system_clock::now();
   std::chrono::duration<double> dElapsedSeconds = chrStop - chrStart;
   std::time_t chrEndTime = std::chrono::system_clock::to_time_t(chrStop);
   if (false) {
   std::cout << "End time : " << std::ctime(&chrEndTime);
   }
   std::cout << "(.) Duration : " << dElapsedSeconds.count() << " s" << std::endl;


   std::cout << std::endl;
   std::cout << "Huevasti." << std::endl;  // "Hüvasti"
}
