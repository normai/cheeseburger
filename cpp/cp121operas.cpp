﻿/**
 *  file       : id 20220825°1535 — gitlab.com/normai/cheeseburger …/cpp/cp121operas.cpp
 *  version    : • 20220908°1521 v0.1.7 Filling • 20220825°1535 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate some C++ operators
 *  summary    :
 *  ref        : https://www.w3schools.com/cpp/cpp_operators.asp [ref 20220906°1422]
 *  ref        : https://stackoverflow.com/questions/14626960/why-doesnt-c-have-a-power-operator [ref 20220825°1442 →←]
 */
#define sVERSION "v0.1.7"

#include <iostream>
#include <string>

int main()
{
   std::cout << "*** Hello, this is cp121operas.cpp " << sVERSION << " -- Some operators ***" << std::endl;

   // (1) Assignment operators
   int v, w, x, y, z;
   v = w = x = y = z = 5;
   x += 2;
   x -= 2;
   y *= 2;
   y /= 2;
   std::cout << "(1.1) 'v = w = x = y = z = 5'             : " << std::to_string(v) << std::endl;
   std::cout << "(1.2) 'w += 2' identical with 'w = w + 2' : " << std::to_string(w) << std::endl;
   std::cout << "(1.3) 'x -= 2' identical with 'x = x - 2' : " << std::to_string(x) << std::endl;
   std::cout << "(1.4) 'y *= 2' identical with 'y = y * 2' : " << std::to_string(y) << std::endl;
   std::cout << "(1.5) 'z /= 2' identical with 'z = z / 2' : " << std::to_string(z) << std::endl;
   std::cout << std::endl;

   // (2) Some remarkable operators
   double x1 = (double) 100 / 30;                               // Note the extra cast "(double)"
   int x2 = 100 / 30;                                           // Integer division, in Python "//"
   double x3 = 100 % 30;                                        // Modulo
   double x4 = pow(16, 3);                                      // Power
   int x5 = 1 > 2 ? 1234 : 5678;                                // Ternary operator
   std::cout << "(2.1) 100 / 30 = (double)" << std::to_string(x1) << " " << typeid(x1).name() << std::endl;
   std::cout << "(2.2) 100 / 30 = " << std::to_string(x2) << " " << typeid(x2).name() << std::endl;
   std::cout << "(2.3) 100 % 30 = " << std::to_string(x3) << " " << typeid(x3).name() << std::endl;
   std::cout << "(2.4) 16 ** 3 = " << std::to_string(x4) << " " << typeid(x4).name() << std::endl;
   std::cout << "(2.5) x5 = 1 > 2 ? 1234 : 5678 = " << std::to_string(x5) << " " << typeid(x5).name() << std::endl;
   std::cout << std::endl;

   // (3) String interpolation
   int i1 = 1;
   int i2 = 2;
   double f1 = 3.4;
   char mTmpBuffer [128];
   int iTmpSuccess = sprintf(mTmpBuffer, "(3) Take %d or %d or %f.", i1, i2, f1); // Since C++20 we had the format() function
   std::string s = mTmpBuffer;
   std::cout << s << std::endl;

   std::cout << "Good bye." << std::endl;
}
