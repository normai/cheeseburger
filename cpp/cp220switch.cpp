﻿/**
 *  file       : id 20220826°1435 — gitlab.com/normai/cheeseburger/ …/cpp/cp220switch.cpp
 *  version    : • 20221001°1035 v0.1.8 Filling • 20220826°1435 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate the Switch controll structure
 *  usage      : On Visual Studio Developer Command Prompt, in this folder
 *                type "cl.exe cp220switch.cpp /EHsc" then "cp220switch.exe"
 */
#define sVERSION "v0.1.8"

#include <iostream>
#include <random>

int main()
{
   std::cout << "*** Aloha, o keia 'cp220switch.cpp' " << sVERSION << " -- Switch ***" << std::endl;  // "ʻo kēia" causes warnings "character cannot be represented …"

   std::cout << "Your dice is rolling ..." << std::endl;
   std::random_device rd;
   std::mt19937 mt(rd());                                       // Mersenne Twister with non-deterministic 32-bit seed
   std::uniform_int_distribution<int> dist(1, 7);               // Numbers 1 through 7
   int rolled = dist(mt);

   switch (rolled)
   {
      case 1 : std::cout << "Look, a one" << std::endl; break;
      case 2 : std::cout << "Whoops, a two" << std::endl; break;
      case 3 : std::cout << "Uh-huh, a three" << std::endl; break;
      case 4 : std::cout << "Holla, a four" << std::endl; break;
      case 5 : std::cout << "Oh, a five" << std::endl; break;
      case 6 : std::cout << "Hooray, a six" << std::endl; break;
      default : std::cout << "Your dice seems broken, you rolled " << rolled << std::endl; break;
   }

   std::cout << "Aloha mai." << std::endl;
}
