﻿/**
 *  file       : id 20221012°1035 — gitlab.com/normai/cheeseburger …/cpp/cp137int.cpp
 *  version    : • 20221018°1135 v0.1.8 Filling • v0.1.6 20221012°1035 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate C++ Integer Types
 *  summary    :
 *  compile    : > cl.exe cp137int.cpp /EHsc /std:c++17
 *  userstory  :
 *  note       : This shall be relatively analogous to file 20221012°1021 cs137int.cs
 *  todo       : Also supplement types • size_t, • ptrdiff_t
 *  todo       : Also supplement types • uint8_t, • uint16_t, • uint32_t, • uint64_t
 *  todo       : Use • sizeof() • and possibly o.type().name()
 *  todo       : Craft some demo for • big integers • possibly with GMP
 *  status     : It works.
 */
#define sVERSION "v0.1.8"

#include <cstddef>                                                     // enum class byte -- C++17
#include <iostream>
#include <iomanip>                                                     // std::setw()
#include <limits>                                                      // std::numeric_limits<x>::min/max()
#include <string>                                                      // ?

int main()
{
   std::cout << "*** Ni hao 'cp137int.cpp' " << sVERSION << " -- Integer Types ***" << std::endl;  // "你好 (Nǐ hǎo)" Compiler "warning C4566: character represented by universal-character-name '\u4F60' cannot be represented in the current code page (1252)"

   // (1) Byte
   std::byte b11 {0};
   std::byte b12 {255};
   std::byte b13 = b12 << 1;
   std::byte b14 = b12 << 2;
   std::byte b15 = b12 << 3;

   // (2) Char
   signed char i21 = -150;
   signed char i22 = i21 + 1;
   unsigned char i23 = 362;
   unsigned char i24 = i23 + 1;

   // (3) Short
   signed short i31 = -32768;
   signed short i32 = 32767;
   signed short i33 = i32 + 1;
   unsigned short i34 = 65535;
   unsigned short i35 = i34 + 1;

   // (4) Int
   signed int i41 = -2147483648;
   signed int i42 = 2147483647;
   signed int i43 = i42 + 1;
   unsigned int i44 = 4294967295;
   unsigned int i45 = i44 + 1;

   // (5) Long
   signed long i51 = std::numeric_limits<long>::min();
   signed long i52 = std::numeric_limits<long>::max();
   signed long i53 = i52 + 1;
   unsigned long i54 = 18446744073709551615;
   unsigned long i55 = i54 + 1;

   // (6) Long long
   signed long long i61 = std::numeric_limits<long long>::min();
   signed long long i62 = std::numeric_limits<long long>::max();
   signed long long i63 = i62 + 1;
   unsigned long long i64 = std::numeric_limits<unsigned long long>::max();
   unsigned long long i65 = i64 + 1;

   std::cout << "(A.1.1) byte b11                      = " << std::left << std::setw(22) << std::to_integer<int>(b11) << typeid(b11).name() << std::endl;
   std::cout << "(A.1.2) byte b12                      = " << std::left << std::setw(22) << std::to_integer<int>(b12) << typeid(b12).name() << std::endl;
   std::cout << "(A.1.3) byte b12 << 1                 = " << std::left << std::setw(22) << std::to_integer<int>(b13) << typeid(b13).name() << std::endl;
   std::cout << "(A.1.4) byte b12 << 2                 = " << std::left << std::setw(22) << std::to_integer<int>(b14) << typeid(b14).name() << std::endl;
   std::cout << "(A.1.5) byte b12 << 3                 = " << std::left << std::setw(22) << std::to_integer<int>(b15) << typeid(b15).name() << std::endl;

   std::cout << "(A.2.1) signed char i21               = " << i21 << " " << std::left << std::setw(20) << (int) i21 << typeid(i21).name() << std::endl;
   std::cout << "(A.2.2) signed char i21 + 1           = " << i22 << " " << std::left << std::setw(20) << (int) i22 << typeid(i22).name() << std::endl;
   std::cout << "(A.2.3) unsigned char i23             = " << i23 << " " << std::left << std::setw(20) << (int) i23 << typeid(i23).name() << std::endl;
   std::cout << "(A.2.4) unsigned char i23 + 1         = " << i24 << " " << std::left << std::setw(20) << (int) i24 << typeid(i24).name() << std::endl;

   std::cout << "(A.3.1) signed short i31              = " << std::left << std::setw(22) << i31 << typeid(i31).name() << std::endl;
   std::cout << "(A.3.2) signed short i32              = " << std::left << std::setw(22) << i32 << typeid(i32).name() << std::endl;
   std::cout << "(A.3.3) signed short i32 + 1          = " << std::left << std::setw(22) << i33 << typeid(i33).name() << std::endl;
   std::cout << "(A.3.4) unsigned short i34            = " << std::left << std::setw(22) << i34 << typeid(i34).name() << std::endl;
   std::cout << "(A.3.5) unsigned short i34 + 1        = " << std::left << std::setw(22) << i35 << typeid(i35).name() << std::endl;

   std::cout << "(A.4.1) signed int i41                = " << std::left << std::setw(22) << i41 << typeid(i41).name() << std::endl;
   std::cout << "(A.4.2) signed int i42                = " << std::left << std::setw(22) << i42 << typeid(i42).name() << std::endl;
   std::cout << "(A.4.3) signed int i42 + 1            = " << std::left << std::setw(22) << i43 << typeid(i43).name() << std::endl;
   std::cout << "(A.4.4) unsigned int i44              = " << std::left << std::setw(22) << i44 << typeid(i44).name() << std::endl;
   std::cout << "(A.4.5) unsigned int i44 + 1          = " << std::left << std::setw(22) << i45 << typeid(i45).name() << std::endl;

   std::cout << "(A.5.1) signed long i51               = " << std::left << std::setw(22) << i51 << typeid(i51).name() << std::endl;
   std::cout << "(A.5.2) signed long i52               = " << std::left << std::setw(22) << i52 << typeid(i52).name() << std::endl;
   std::cout << "(A.5.3) signed long i52 + 1           = " << std::left << std::setw(22) << i53 << typeid(i53).name() << std::endl;
   std::cout << "(A.5.4) unsigned long i54             = " << std::left << std::setw(22) << i54 << typeid(i54).name() << std::endl;
   std::cout << "(A.5.5) unsigned long i54 + 1         = " << std::left << std::setw(22) << i55 << typeid(i55).name() << std::endl;

   std::cout << "(A.6.1) signed long long i61          = " << std::left << std::setw(22) << i61 << typeid(i61).name() << std::endl;
   std::cout << "(A.6.2) signed long long i62          = " << std::left << std::setw(22) << i62 << typeid(i62).name() << std::endl;
   std::cout << "(A.6.3) signed long long i62 + 1      = " << std::left << std::setw(22) << i63 << typeid(i63).name() << std::endl;
   std::cout << "(A.6.4) unsigned long long i64        = " << std::left << std::setw(22) << i64 << typeid(i64).name() << std::endl;
   std::cout << "(A.6.5) unsigned long long i65 + 1    = " << std::left << std::setw(22) << i65 << typeid(i65).name() << std::endl;

   std::cout << "(B.1.1.1) signed char min/max()       = " << std::setw(15) << std::numeric_limits<signed char>::min() << " / " << std::numeric_limits<signed char>::max() << std::endl;
   std::cout << "(B.1.1.2) (int) signed char min/max() = " << std::setw(15) << (int) std::numeric_limits<signed char>::min() << " / " << (int) std::numeric_limits<signed char>::max() << std::endl;
   std::cout << "(B.1.2) (int) unsigned char min/max() = " << std::setw(15) << (int) std::numeric_limits<unsigned char>::min() << " / " << (int) std::numeric_limits<unsigned char>::max() << std::endl;
   std::cout << "(B.2.1) signed short min/max()        = " << std::setw(15) << std::numeric_limits<signed short>::min() << " / " << std::numeric_limits<signed short>::max() << std::endl;
   std::cout << "(B.2.2) unsigned short min/max()      = " << std::setw(15) << std::numeric_limits<unsigned short>::min() << " / " << std::numeric_limits<unsigned short>::max() << std::endl;
   std::cout << "(B.3.1) signed int min/max()          = " << std::setw(15) << std::numeric_limits<signed int>::min() << " / " << std::numeric_limits<signed int>::max() << std::endl;
   std::cout << "(B.3.2) unsigned int min/max()        = " << std::setw(15) << std::numeric_limits<unsigned int>::min() << " / " << std::numeric_limits<unsigned int>::max() << std::endl;
   std::cout << "(B.4.1) signed long min/max()         = " << std::setw(15) << std::numeric_limits<signed long>::min() << " / " << std::numeric_limits<signed long>::max() << std::endl;
   std::cout << "(B.4.2) unsigned long min/max()       = " << std::setw(15) << std::numeric_limits<unsigned long>::min() << " / " << std::numeric_limits<unsigned long>::max() << std::endl;
   std::cout << "(B.5.1) signed long long min/max()    = " << std::setw(15) << std::numeric_limits<signed long long>::min() << " / " << std::numeric_limits<signed long long>::max() << std::endl;
   std::cout << "(B.5.2) unsigned long long min/max()  = " << std::setw(15) << std::numeric_limits<unsigned long long>::min() << " / " << std::numeric_limits<unsigned long long>::max() << std::endl;

   std::cout << "Zaijian." << std::endl;                     // "再见 [Zàijiàn]."
}
