﻿/**
 *  file       : id 20221116°1435 — gitlab.com/normai/cheeseburger …/cpp/cp311kbd1menu.cpp
 *  version    : • 20221117°0917 v0.1.8 Filling • 20221116°1435 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate keyboard menu with pressing Enter
 *  compile    : VS-DevCmdPrompt > cl.exe cp311kbd1menu.cpp /EHsc
 *  status     : Good enough.
 *  todo       : Do flush input buffer. Type multiple chars to see why. [todo 20221116°1952]
 *  userstory  :
 *  summary    :
 *  ref        : https://stackoverflow.com/questions/49246768/safe-operation-to-clear-the-empty-input-buffer-in-c [ref 20221116°1942]
 *  ref        : https://www.geeksforgeeks.org/clearing-the-input-buffer-in-cc/ [ref 20221116°1944]
 *  ref        :
 */
#define sVERSION "v0.1.8"

#include <iostream>

int main()
{
   std::cout << "*** Hei, tama on `cp311kbd1menu.cpp` " << sVERSION << " -- Keyboard menu (with Enter) ***" << std::endl; // "Hei, tämä on"

   bool bContinue = true;
   while (bContinue)
   {
      char cKey;
      std::cout << "Menu: a = Anton, k = Kylie, s = Santa, x = Exit" << std::endl;
      std::cin >> cKey;
      cKey = tolower(cKey);
      ////std::cin.sync();                                                 // Does not work as expected. See todo 20221116°1952.
      ////std::cin >> std::ws;                                             // Does not work either
      ////fflush(stdin);                                                   // Does not work either

      switch (cKey)
      {
          case 'a' :
             std::cout << "This is Anton speaking" << std::endl;
             break;
          case 'k' :
             std::cout << "This is Kylie singing" << std::endl;
             break;
          case 's' :
             std::cout << "This is Santa Claus ringing" << std::endl;
             break;
          case 'x' :
             bContinue = false;
             break;
          default :
             std::cout << "Invalid selection: \"" << cKey << "\"" << std::endl;
             break;
      }
   }

   std::cout << "Nakemiin." << std::endl;  // "Näkemiin"
}
