﻿// THIS FILE IS AN EMPTY STUB, YET TO BE FILLED WITH CODE

/**
 *  file       : id 20220910°1335 — gitlab.com/normai/cheeseburger/ …/cpp/cp761random.cpp
 *  version    : • 20xxxxxx°xxxx v0.x.x Filling • 20220831°1135 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate C++ Random Functions
 *  ref        : https://www.w3schools.blog/c-random-number-generator [ref 20220910°1152]
 *  ref        : https://cplusplus.com/reference/random/ [ref 20220910°1153]
 *  ref        : https://cplusplus.com/reference/cstdlib/rand/ [ref 20220910°1154]
 *  ref        : https://en.cppreference.com/w/cpp/numeric/random/rand [ref 20220910°1155]
 *  ref        : https://stackoverflow.com/questions/13445688/how-to-generate-a-random-number-in-c [ref 20220910°1156]
 *  ref        : https://stackoverflow.com/questions/7114043/random-number-generation-in-c11-how-to-generate-how-does-it-work [ref 20220910°1212]
 *  ref        : https://stackoverflow.com/questions/19665818/generate-random-numbers-using-c11-random-library [ref 20220910°1213]
 *  ref        : https://docs.microsoft.com/en-us/events/goingnative-2013/rand-considered-harmful [ref 20220910°1214] Talk 31 minutes nice!
 *  ref        : https://docs.microsoft.com/en-us/events/goingnative-2013/ [ref 20220910°1216]
 *  usage      : E.g. in file cp230while.cpp
 */
#define sVERSION "v0.0.0"

#include <iostream>
#include <string>

int main()
{
   std::cout << "*** Privet, eto `cp761random.cpp` " << sVERSION << " -- Random Functions ***" << std::endl;  // Errors "character cannot be represented …" in "Привет, это [Privet, eto] …"





   std::cout << "Do svidaniya." << std::endl;  // Errors "character cannot be represented …" in "до свидания [Do svidaniya]."
}
