﻿/**
 *  file       : id 20220825°1335 — gitlab.com/normai/cheeseburger …/cpp/cp113aloha.cpp
 *  version    : • 20220908°0801 v0.1.7 Filling • 20220825°1335 v0.1.6 Stub
 *  subject    :
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  compile    : VS-Dev-Prompt > cl.exe cp113aloha.cpp /EHsc
 */
#define sVERSION "v0.1.7"

#include <bitset>                                              //
#include <iostream>
#include <string>                                              // for to_string()

int main()
{
   std::cout << "*** O keia cp113aloha.cpp " << sVERSION << " -- Aloha ***" << std::endl;  // "ʻO kēia"

   // Define some variables and use an operator
   int a = 123;
   int b = 345;
   int c = a + b;

   // Output with a bulky statement
   std::cout << "(1) Sum of " << a << " and " << b << " is " << c << std::endl;

   // Output with a summarized statement
   std::string s = "(2) Sum of " + std::to_string(a) + " and " + std::to_string(b) + " is " + std::to_string(c);
   std::cout << s << std::endl;

   // Output numbers in various notations
   std::string sBin = std::bitset<8>(a).to_string();
   char buffer [50];
   int iTmp = sprintf(buffer, "%04x", a);                      // In C++20 we had `std::format("{:x}", a);` 
   std::string sHex = buffer;
   std::cout << "(3) Bin : " << sBin << ", Hex : " << sHex << std::endl;

   // Show difference between integers, floats and strings
   std::cout << "(4) Get variable types:" << std::endl;
   int i = 12345;
   double f = 12.345;
   s = "Chain of chars";
   std::cout << "  - i = " << i << " : " << typeid(i).name() << std::endl;
   std::cout << "  - f = " << f << " : " << typeid(f).name() << std::endl;
   std::cout << "  - s = \"" << s << "\" : " << typeid(s).name() << std::endl;

   std::cout << "Aloha mai.";
   return 0;
}
