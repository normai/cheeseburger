﻿/**
 *  file       : id 20220826°1535 — gitlab.com/normai/cheeseburger/ …/cpp/cp230while.cpp
 *  version    : • 20220910°1035 v0.1.7 Filling • 20220826°1535 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate C++ while loop
 *  summary    :
 *  ref        : About random see cp761random.cpp
 *  note       : Random code written after ref 20220910°1214 talk by Stephan T. Lavavej, slide at minute 15
 */
#define sVERSION "v0.1.7"

#include <iostream>
#include <random>

int main()
{
   std::cout << "*** Hailo, yah hai 'cp230while.cpp' " << sVERSION << " -- While loop ***" << std::endl;  // Warning C4566 'character cannot be represented ..' with "हैलो, यह है"
   std::cout << "Roll the number six!\n";

   // (1) Roll dice first time
   std::random_device rd;
   std::mt19937 mt(rd());                                       // Mersenne Twister with non-deterministic 32-bit seed
   std::uniform_int_distribution<int> dist(1, 6);
   int iRolled = dist(mt);
   std::cout << "Your first number is " << iRolled << "\n";

   // (2) Continue as long no six is rolled
   int iTimes = 1;
   while (iRolled != 6)
   {
      iRolled = iRolled = dist(mt);
      iTimes += 1;
      std::cout << " - then " << iRolled << "\n";
   }

   std::cout << "You rolled " << iTimes << " times until number six.\n";

   std::cout << "Alavida." << std::endl;  // Warning C4566 'character cannot be represented ..' with "अलविदा"
}
