﻿/**
 *  file       : id 20220826°1735 — gitlab.com/normai/cheeseburger/ …/cpp/cp240for.cpp
 *  version    : • 20220919°1935 v0.1.8 Filling • 20220826°1753 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate the for loop
 *  compile    : cl.exe cp240for.cpp /EHsc
 *  ref        : https://stackoverflow.com/questions/14626960/why-doesnt-c-have-a-power-operator []
 */
#define sVERSION "v0.1.8"

#include <iostream>
#include <string>

int main()
{
   std::cout << "*** Halo, ini adalah 'cp240for.cpp' " << sVERSION << " -- For-Loop ***" << std::endl;

   std::cout << "(1) Two power 6 through 10" << std::endl;
   for (int i = 6; i <= 10; i++)
   {
      int iPow = pow(2, i);
      std::cout << "   - " << i << " " << iPow << std::endl;
   }

   std::cout << "(2) And the same backward" << std::endl;
   for (int i = 10; i >= 6; i--)
   {
      int iPow = pow(2, i);
      std::cout << "   - " << i << " " << iPow << std::endl;
   }

   std::cout << "Alavida." << std::endl;  // Errors "character cannot be represented …" in "अलविदा  [Alavida]."
}
