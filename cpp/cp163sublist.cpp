﻿/**
 * file        : id 20220828°1835 — gitlab.com/normai/cheeseburger/ …/cpp/cp163sublist.cpp
 * version     : • 20220908°1301 v0.1.7 Filling • 20220828°1835 v0.1.6 Stub
 * license     : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 * subject     : Demonstrate sublists or slice
 * summary     : The CPP List is very different from the Python List, notably it has
 *                no index based access, thus no chance to just map a part of the list.
 * overview    : There are four servings:
 *                (1) Slice from simple C++ array
 *                (2) Slice from Array class — Using copy during construction
 *                (3) Slice from Vector class — Using copy during construction
 *                (4) Slice from List class  — Using the range constructor
 * reference   : https://cplusplus.com/reference/list/list/list/ []
 * reference   : https://www.techiedelight.com/get-slice-sub-vector-from-vector-cpp/ [ref 20220903°1532]
 * ref         : https://stackoverflow.com/questions/16642286/slicing-stdarray []
 * todo        : Streamline indexing artithmetics [todo 20220903°1551]
 */
#define sVERSION "v0.1.7"

#include <array>
#include <iostream>
#include <list>
#include <string>
#include <vector>
using namespace std;

int main()
{
   cout << "*** Dobry den, toto je `cp163sublist.cpp` " << sVERSION << " -- Sublist ***" << endl;  // "Dobrý den, toto je"


   // ==========================================================
   // (1) Classic array
   // (1.1) Create source
   string nums1[7] = { "Uno", "Due", "Tre", "Quattro", "Cinque"};
   nums1[5] = "Sei";
   nums1[6] = "Sette";

   // (1.2) Make slice — Use some counting arithmetic
   const int iStartIndex = 2;
   const int iEndIndex = 5;
   int iNewIndex = 0;
   string slice1[iEndIndex - iStartIndex + 1];
   for (int i = iStartIndex; i <= iEndIndex; i++)
   {
      slice1[iNewIndex] = nums1[i];
      iNewIndex++;
   }

   // (1.3) Output slice
   cout << "(1) Plain array slice  =";
   for (int i = 0; i < size(slice1); i++)
   {
      cout << " " << slice1[i];
   }
   cout << endl;

   // ==========================================================
   // (2) Array class
   // (2.1) Create source
   array<string, 7> nums2 = { "Uno", "Due", "Tre", "Quattro", "Cinque"};
   nums2[5] = "Sei";
   nums2[6] = "Sette";

   // (2.2) Make slice — Using copy during construction
   array<string, 7> slice2;
   copy(nums2.begin() + 2, nums2.end() - 1, slice2.begin());

   // (2.3) Output slice
   cout << "(2) Array class slice  =";
   for (int i = 0; i < size(slice2); i++)
   {
      cout << " " << slice2[i];
   }
   cout << endl;

   // ==========================================================
   // (3) Vector class
   // (3.1) Create source
   vector<string> nums3 = { "Uno", "Due", "Tre", "Quattro", "Cinque"};
   nums3.push_back("Sei");
   nums3.push_back("Sette");

   // (3.2) Make slice — Use copy during construction
   // Also the range constructor should be available, in fact much more simple as in
   //  below list demjo, becaus the vector allows for iterators with index arithmetics.
   vector<string> slice3(4);
   copy(nums3.begin() + 2, nums3.end() - 1, slice3.begin());

   // (3.3) Output slice
   cout << "(3) Vector class slice =";
   for (auto item : slice3)
   {
      cout << " " << item;
   }
   cout << endl;


   // ==========================================================
   // (4) List class
   // (4.1) Create source
   list<string> nums4 = { "Uno", "Due", "Tre", "Quattro", "Cinque"};
   nums4.push_back("Sei");
   nums4.push_back("Sette");

   // (4.2) Make slice — Use range constructor
   list<string>::iterator itNums = nums4.begin();
   list<string>::iterator itStart;
   list<string>::iterator itStop;
   for (int i = 0; i < nums4.size(); i++, itNums++)
   {
      if (i == 2)
      {
         itStart = itNums;
      }
      if (i == 6)
      {
         itStop = itNums;
      }
   }
   list<string> slice4 (itStart, itStop);

   // (4.3) Output slice
   cout << "(4) List class slice   =";
   for (auto item : slice4)
   {
      cout << " " << item;
   }
   cout << endl;

   cout << "Nashledanou." << endl;
}
