﻿/**
 *  file       : id 20220826°1135 — gitlab.com/normai/cheeseburger/ …/cpp/cp125bitwise.cpp
 *  version    : • 20220908°1721 v0.1.7 Filling • 20220826°1135 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate bitwise operators in C++
 *  summary    :
 *  ref        : https://www.codecademy.com/resources/docs/cpp/bitwise-operators [ref 20220907°0942] Overview
 *  ref        : http://w3schools.org.in/c&cc/bitwise.html [ref 20220907°0932]
 *  ref        : https://www.w3schools.com/cpp/cpp_operators_logical.asp [ref 20220907°0931] For another demo
 *  todo       : See file 20151024°0731 /vehicles231/Vehicle.cpp, which uses the bits of
 *                an unsigned int as flags to craft a dedicated demo [todo 20220907°0911]
 *  todo       : Produce the identical output but with only one print sequence, that is in
 *                one (or two) loop(s), feed by according parameters [todo 20220907°0951]
 */
#define sVERSION "v0.1.7"

#include <bitset>                                               // For bitset()
#include <iomanip>                                              // std::setw()
#include <iostream>
#include <string>

int main()
{
   std::cout << "*** Hei, taemae on 'cp125bitwise.cpp' " << sVERSION << " -- ***" << std::endl;  // "Hei, tämä on"

   unsigned short int i1 = 43690;                               // 0xaaaa, 0b_1010_1010_1010_1010
   unsigned short int i2 = 3855;                                // 0x0f0f, 0b_0000_1111_0000_1111

   std::cout << "(1) Bitwise AND" << std::endl;
   std::cout << "(1.1) i1      =   " << std::bitset<16>(i1)      << std::setw(11) << std::hex << i1        << std::setw(9) << std::dec << i1        << std::endl;
   std::cout << "(1.2) i2      =   " << std::bitset<16>(i2)      << std::setw(11) << std::hex << i2        << std::setw(9) << std::dec << i2        << std::endl;
   std::cout << "(1.3) i1 & i2 =   " << std::bitset<16>(i1 & i2) << std::setw(11) << std::hex << (i1 & i2) << std::setw(9) << std::dec << (i1 & i2) << std::endl;

   std::cout << "(2) Bitwise OR" << std::endl;
   std::cout << "(2.1) i1      =   " << std::bitset<16>(i1)      << std::setw(11) << std::hex << i1        << std::setw(9) << std::dec << i1        << std::endl;
   std::cout << "(2.2) i2      =   " << std::bitset<16>(i2)      << std::setw(11) << std::hex << i2        << std::setw(9) << std::dec << i2        << std::endl;
   std::cout << "(2.3) i1 | i2 =   " << std::bitset<16>(i1 | i2) << std::setw(11) << std::hex << (i1 | i2) << std::setw(9) << std::dec << (i1 | i2) << std::endl;

   std::cout << "(3) Bitwise XOR" << std::endl;
   std::cout << "(3.1) i1      =   " << std::bitset<16>(i1)      << std::setw(11) << std::hex << i1        << std::setw(9) << std::dec << i1        << std::endl;
   std::cout << "(3.2) i2      =   " << std::bitset<16>(i2)      << std::setw(11) << std::hex << i2        << std::setw(9) << std::dec << i2        << std::endl;
   std::cout << "(3.3) i1 ^ i2 =   " << std::bitset<16>(i1 ^ i2) << std::setw(11) << std::hex << (i1 ^ i2) << std::setw(9) << std::dec << (i1 ^ i2) << std::endl;

   std::cout << "(4) Bitwise NOT (Complement)" << std::endl;
   std::cout << "(4.1) i1      =   " << std::bitset<16>(i1)      << std::setw(11) << std::hex << i1        << std::setw(9) << std::dec << i1        << std::endl;
   std::cout << "(4.2) ~i1     =   " << std::bitset<16>(~i1)     << std::setw(11) << std::hex << (~i1)     << std::setw(9) << std::dec << (~i1)     << std::endl;

   std::cout << "(5) Bit Shift Left" << std::endl;
   std::cout << "(5.1) i2      =   " << std::bitset<16>(i2)      << std::setw(11) << std::hex << i2        << std::setw(9) << std::dec << i2        << std::endl;
   std::cout << "(5.2) i2 << 1 =   " << std::bitset<16>(i2 << 1) << std::setw(11) << std::hex << (i2 << 1) << std::setw(9) << std::dec << (i2 << 1) << std::endl;

   std::cout << "(6) Bit Shift Right" << std::endl;
   std::cout << "(6.1) i2      =   " << std::bitset<16>(i2)      << std::setw(11) << std::hex << i2        << std::setw(9) << std::dec << i2        << std::endl;
   std::cout << "(6.3) i2 >> 1 =   " << std::bitset<16>(i2 >> 1) << std::setw(11) << std::hex << (i2 >> 1) << std::setw(9) << std::dec << (i2 >> 1) << std::endl;

   std::cout << "Naekemiin." << std::endl;  // "Näkemiin"
}
