﻿// THIS FILE IS AN EMPTY STUB, YET TO BE FILLED WITH CODE

/**
 *  file       : id 20220830°1235 — gitlab.com/normai/cheeseburger/ …/cpp/cp540aggregate.cpp
 *  version    : • 20xxxxxx°xxxx v0.x.x Filling • 20220830°1235 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate
 */
#define sVERSION "v0.0.0"

#include <iostream>
#include <string>

int main()
{
   std::cout << "*** Hello, this is `cp540aggregate.cpp` " << sVERSION << " -- Aggregate ***";




   std::cout << "Good bye." << std::endl;
}
