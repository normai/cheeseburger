﻿/**
 *  file       : id 20220827°0935 — gitlab.com/normai/cheeseburger/ …/cpp/cp262try2.cpp
 *  version    : • 20220913°0931 v0.1.7 Filling • 20220827°0935 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate C++ exception handling in detail
 *  summary    : Shown is
 *                • A single try block with multiple chances to fail
 *                • A catch cascade, catching the right exception for each fail case
 *  usage      : cl.exe cp262try2.cpp /EHsc /std:c++17 
 *  ref        : https://www.w3schools.com/cpp/cpp_exceptions.asp [ref 20220912°1413]
 *  ref        : https://cplusplus.com/doc/tutorial/exceptions/ [ref 20220912°1412]
 *  ref        : https://docs.microsoft.com/en-us/cpp/cpp/errors-and-exception-handling-modern-cpp [ref 20220912°1414]
 *  ref        : https://en.cppreference.com/w/cpp/error/exception [ref 20220912°1415]
 *  ref        : https://stackoverflow.com/questions/8480640/how-to-throw-a-c-exception [ref 20220912°1416]
 *  ref        : http://shanekirk.com/2015/06/c-exceptions-the-good-the-bad-and-the-ugly/ [ref 20220912°1422]
 *  ref        : https://www.scaler.com/topics/square-root-in-cpp/ [ref 20220913°1012]
 *  todo       : In the catch cascade, the Exception class with derrived
 *                classes should be demonstrated. [todo 20220913°1033]
 *  note       : Why are all the cases stuffed into one single try block?
 *               To be able to demonstrate the catch cascade at the end.
 *  todo       : Adjust cases. See Py/Java/Cs. [todo 20220915°1041]
 */
#define sVERSION "v0.1.7"

#include <cmath>                                                // sqrt()
#include <filesystem>                                           // std::exists since C++17
#include <fstream>                                              // ifstream
#include <iostream>
#include <random>
#include <string>

int main()
{
   std::cout << "*** Sveiki, sis ir `cp262try2.cpp` " << sVERSION << " -- Try (full-blown) ***" << std::endl;  // Errors "character cannot be represented …" in "Sveiki, šis ir …"

   // Process four cases
   for (int iCase = 1; iCase < 5; iCase++)
   {
      int iNumber = iCase * 3 - 6;
      std::cout << std::endl;
      std::cout << "Case " << iCase << ", Number = " << iNumber << ":" << std::endl;

      try
      {
          //----------------------------------------------
          // (1) Division by zero
          std::cout << "(1.1) Try division" << std::endl;
          if (iNumber == 0)
          {
             std::string sEx = "Attempted forbidden division by zero.";
             throw 123; // sEx;
          }
          // If iNumber were a constant, this were a already a compiler error
          // If division by zero were executed, the program aborts with %errorlevel% = 1
          double nRatio = 123.4 / iNumber;
          std::cout << "(1.2) Calculation: " << 123.4 << " / " << iNumber << " = " << nRatio << std::endl;

          //----------------------------------------------
          // (2) Calculate square root of negative number
          std::cout << "(2.1) Try square root" << std::endl;
          if (iNumber < 0)
          {
             std::string sEx = "Attempt forbidden calculation sqare root of " + std::to_string(iNumber) + ".";
             throw sEx;
          }
          double dSqrt = std::sqrt(iNumber);
          std::cout << "(2.2) Calculation: Square root of " << iNumber << " = " << dSqrt << std::endl;

          //----------------------------------------------
          // (3) Try open non-existing file
          std::string sFile = ".\\detjibbetnich.txt"; // ".\\detjibbetnich.txt"; // ".\\cp262try2.cpp"
          if (iNumber >= 6)
          {
             sFile = ".\\cp262try2.cpp";
          }
          std::cout << "(3.1) Try open file \"" << sFile << "\"" << std::endl;
          if (! std::filesystem::exists(sFile))
          {
             char * cXcpt = "Datei existiert nicht";                       // Quickly declared without the filename, but pretty inconvenieht to add the C++ style filename
             throw(cXcpt);
          }

          std::ifstream ifs(sFile);
          std::string sLine;
          ////while (getline(ifs, sLine))
          ////{
          ////   std::cout << sLine << std::endl;
          ////}
          getline(ifs, sLine);
          std::cout << "(3.2) Read first line: \"" << sLine << "\"" << std::endl;
      }
      catch (int iEx)
      {
          std::cout << "(4.1) Caught exception: " << iEx << " 'forbidden division by zero'." << std::endl;
      }
      catch (std::string sEx)
      {
          std::cout << "(4.2) Caught exception: " << sEx << std::endl;
      }
      catch (char * cEx)
      {
          std::cout << "(4.3) Caught exception: " << cEx << std::endl;
      }
   }

   std::cout << "Uz redzesanos." << std::endl;  // Errors "character cannot be represented …" in "Uz redzēšanos."
}
