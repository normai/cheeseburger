﻿// THIS FILE IS AN EMPTY STUB, YET TO BE FILLED WITH CODE

/**
 *  file       : id 20220828°1035 — gitlab.com/normai/cheeseburger/ …/cpp/cp335format.cpp
 *  version    : • 20xxxxxx°xxxx v0.x.x Filling • 20220828°1035 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate C++ string formatting
 *  summary    :
 *  ref        :
 *  ref        :
 *  ref        :
 */
#define sVERSION "v0.0.0"

#include <iostream>
#include <string>

int main()
{
   std::cout << "*** Dobry den, toto je 'cp335format.cpp' " << sVERSION << " -- Format function ***" << std::endl;  // Errors "character cannot be represented …" in "Dobrý deň, toto je …"





   std::cout << "Dovidenia." << std::endl;
}
