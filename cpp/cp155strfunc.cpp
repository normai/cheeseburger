﻿/**
 *  file       : id 20221012°1735 — gitlab.com/normai/cheeseburger …/cpp/cp155strfunc.cpp
 *  version    : • 20221013°1135 v0.1.8 Filling • 20221012°1735 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate String Functions
 *  compile    : On VS console > cl.exe cp155strfunc.cpp /EHsc
 *  userstory  :
 *  summary    :
 *  ref        : https://www.positioniseverything.net/cpp-string-trim [ref 20221013°0932]
 *  ref        : https://www.delftstack.com/howto/cpp/how-to-trim-a-string-cpp/ [ref 20221013°0933]
 *  ref        : https://stackoverflow.com/questions/23418390/how-to-convert-a-c-string-to-uppercase [ref 20221013°0922]
 *  ref        : https://stackoverflow.com/questions/735204/convert-a-string-in-c-to-upper-case [ref 20221013°0923]
 */
#define sVERSION "v0.1.8"

#include <algorithm>                                                    // std::transform?
#include <iostream>
#include <string>

// Trim function using STL, inspired by ref 20221013°0932 https://www.positioniseverything.net/cpp-string-trim
std::string strTrim(std::string str, bool bLeft, bool bRight) {
   const char* cWhitespaces = " \t\n\r";                                // Original " tnrfv"
   if (bLeft) {
      str.erase(0, str.find_first_not_of(cWhitespaces));
   }
   if (bRight) {
      str.erase(str.find_last_not_of(cWhitespaces) + 1);
   }
   return str;
}

int main()
{
   std::cout << "*** Hallo, dit is 'cp155strfunc.cpp' " << sVERSION << " -- String Functions ***" << std::endl;

   // () Fodder
   const char * cWord = "SilzuZankunKrei";
   std::string sWord = "SilzuZankunKrei";

   // (.) String Functions
   // (.1) String length
   std::cout << "(3.1.1) Length of string '" << sWord << "' is " << sWord.length() << std::endl;  // Note. size() is an alias of length()
   std::cout << "(3.1.2) Length of char*  '" << cWord << "' is " << strlen(cWord) << std::endl;

   // (.2) Character code to string
   std::cout << "(3.2) Code to char : " << (char) 36 << (char) 37 << (char) 38 << (char) 96 << (char) 97 << (char) 98 << (char) 122 << (char) 123 << (char) 124 << (char) 125 << '\u007e' << '\u007f' << "." << std::endl;

   // (.3) Substrings
   std::cout << "(3.3) Substrings : " << sWord.substr(0, 5) << " << " << sWord.substr(5, sWord.length() - 4 - 5) << " + " + sWord.substr(sWord.length() - 4) << std::endl;

   // (.4) Upper/Lower
   std::string sLower = sWord;
   std::string sUpper = sWord;
   //// Todo: CHECK NEXT TWO LINES. Compare cp133bool.cpp transform usage
   std::transform(sLower.begin(), sUpper.end(), sLower.begin(), ::tolower);  // Solution after ref 20221013°0923 https://stackoverflow.com/a/735215
   std::transform(sUpper.begin(), sUpper.end(), sUpper.begin(), ::toupper);
   std::cout << "(3.4) Upper/Lower : " << sLower << " / " << sUpper << std::endl;

   // (.5) Trim
   std::string sBreezy = "    Breeze    ";
   std::cout << "(3.5) Trim \"" << sBreezy << "\" : \"" << strTrim(sBreezy, true, false) << "\", \"" << strTrim(sBreezy, false, true) << "\", \"" << strTrim(sBreezy, true, true) << "\"" << std::endl;

   std::cout << "Tot ziens." << std::endl;
}
