﻿/**
 *  file       : id 20221001°1335 — gitlab.com/normai/cheeseburger … cpp/cp151string.cpp
 *  version    : • 20221009°1735 v0.1.8 Filling • 20221001°1335 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate string syntax
 *  compile    : > cl.exe cp151string.cpp /EHsc /std:c++17
 *  ref        : https://www.delftstack.com/howto/cpp/cpp-multiline-string-cpp/ [ref 20221013°0912]
 *  ref        : https://www.techiedelight.com/iterate-over-characters-string-cpp/ [ref 20221013°0913] Demonstrated all except operator overload
 *  ref        : https://en.cppreference.com/w/cpp/utility/any [ref 20221013°0942]
 *  ref        : https://www.techiedelight.com/determine-if-a-string-is-numeric-in-cpp/ [ref 20221013°0943]
 */
#define sVERSION "v0.1.8"

#include <algorithm>                                                    // std::for_each
#include <any>                                                          // std::any -- C++17
#include <cstdio>                                                       // for printf()
#include <iostream>
#include <list>                                                         // std::list
#include <string>

int main()
{
   std::cout << "*** Pozdravljeni, tukaj 'cp151string.cpp' " << sVERSION << " -- Strings ***" << std::endl;

   // ====================================================
   // (1) String Syntax

   // (1.1) Syntax — Quote types (and newline)
   std::cout << "(1.1) Use double-quotes \" for strings, single-quotes \' for chars" << std::endl;

   // (1.2) Syntax — Mulitiline string one
   std::string sMulti = "Ein Wiesel\n"
                        "saß auf einem Kiesel\n"
                        "inmitten Bachgeriesel.";
   std::cout << "(1.2) " << sMulti << std::endl;

   // (1.3) Syntax — Mulitiline string two
   const char * cMulti = "Ein Wiesel\n"
                        "saß auf einem Kiesel\n"
                        "inmitten Bachgeriesel.";
   std::cout << "(1.3) " << cMulti << std::endl;

   // (1.4) Syntax — Mulitiline string three
   const char * c2 = "Ein Wiesel\n\
saß auf einem Kiesel\n\
inmitten Bachgeriesel.";
   std::cout << "(1.4) " << c2 << std::endl;

   // (1.5) Syntax — String concatenation (is demonstrated again below)
   std::string sHello = "Hello";
   std::string sJenny = "Jenny";
   double dNum = 1.234;
   std::string sGreet = sHello + ' ' + std::to_string(dNum) + ' ' + sJenny;
   std::cout << "(1.5) " << sGreet << std::endl;

   // (1.6) Syntax — String as array of chars -- 1. Naive solution
   std::string sWord = "SilzuZankunKrei";
   std::cout << "(1.6) String as Array: ";
   for (std::string::size_type i = 0; i < sWord.size(); i++) {
      std::cout << sWord[i] << '.';
   }
   std::cout << std::endl;

   // (1.7) Syntax — String as array of chars -- 2. Range based for-loop
   std::cout << "(1.7) String as Array: ";
   for (char const &c : sWord) {
      std::cout << c << '.';
   }
   std::cout << std::endl;

   // (1.8) Syntax — String as array of chars -- 3. Using iterators
   std::cout << "(1.8) String as Array: ";
   for (auto it = sWord.cbegin() ; it != sWord.cend(); ++it) {
      std::cout << *it << '.';
   }
   std::cout << std::endl;

   // (1.9) Syntax — String as array of chars -- 4. for_each with lambda
   std::cout << "(1.9) String as Array: ";
   std::for_each(sWord.begin(), sWord.end(), [] (char const &c) {
      std::cout << c << '.';
   });
   std::cout << std::endl;

   // (1.10) Syntax — String as array of chars -- 5. Using C style string
   std::cout << "(1.10) String as Array: ";
   const char * cWord = "SilzuZankunKrei";
   const char * i3 = cWord;
   while (*i3 != '\0') {
      std::cout << *i3 << '.';
      i3++;
   }
   std::cout << " [" << typeid(i3).name() << "] " << std::endl;

   // ==============================================
   // (2) Conversions

   std::cout << "(2.1) Number to string: \"" << 1234 << "\", \"" << std::to_string(2.345) << "\"" << std::endl;

   // Todo : Refactor to be simpler and shorter, e.g. merge the if/else and the try/catch sequences [todo 20221013°0951 'merge if and try']
   std::cout << "(2.2) String to number:" << std::endl;
   std::list<std::string> nums = {"1234", "2.345", "Tree hundred twenty five" };
   std::any o;
   std::string s33;
   for (std::string sNum : nums) {
      if (! sNum.empty() && sNum.find_first_not_of("0123456789") == std::string::npos) {
         int i = std::stoi(sNum);
         o = std::any_cast<int>(i);
      }
      else if (! sNum.empty() && sNum.find_first_not_of(".0123456789") == std::string::npos) {
         double d = std::stod(sNum);
         o = std::any_cast<double>(d);
      }
      else {
         o = "Not a number";
      }
      try {
         s33 = std::to_string(std::any_cast<int>(o));
      }
      catch (const std::bad_any_cast& e1) {
         try {
            s33 = std::to_string(std::any_cast<double>(o));
         }
         catch (const std::bad_any_cast& e2) {
            s33 = "Not a number";
         }
      }
      std::cout << "      - \"" << sNum << "\" => " << s33  << " (" << o.type().name() << ")" << std::endl;
   }
   std::cout << "Nasvidenje" << std::endl;
}
