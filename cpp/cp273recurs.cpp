﻿// EMPTY STUB, YET TO BE FILLED WITH CODE

/**
 *  file       : id 20221222°1835 — gitlab.com/normai/cheeseburger …/cpp/cp273recurs.cpp
 *  version    : • 20xxxxxx°xxxx v0.x.x Filling • 20221222°1835 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate recursion
 *  status     :
 */
#define sVERSION "v0.0.0"

#include <iostream>
//#include <string>

int main()
{
   std::cout << "Yo `cp273recurs.cpp` ho " << sVERSION << " -- Recursion ***" << std::endl;  // "यो 'Xxx' हो [Yō 'Xxx' hō]"





   std::cout << "Namaste." << std::endl;  // "नमस्ते [Namastē]"
}
