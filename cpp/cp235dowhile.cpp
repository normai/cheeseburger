﻿/**
 *  file       : id 20220826°1635 — gitlab.com/normai/cheeseburger/ …/cpp/cp235dowhile.cpp
 *  version    : • 20221001°1135 v0.1.8 Filling • 20220826°1635 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate Do/While loop
 *  userstory  : Roll dice until you rolled a six
 *  usage      : "cp235dowhile.cpp /EHsc" then "cp235dowhile.exe"
 */
#define sVERSION "v0.1.8"

#include <iostream>
#include <random>

int main()
{
   std::cout << "*** Hello, ez 'cp235dowhile.cpp' " << sVERSION << " -- C++ Do/While ***" << std::endl;

   std::cout << "Loop until you rolled a six :" << std::endl;
   std::random_device rd;
   std::mt19937 mt(rd());                                              // Mersenne Twister with non-deterministic 32-bit seed
   std::uniform_int_distribution<int> dist(1, 6);                      // Numbers 1 through 6
   int randi;                                                          // Variable needs still be known behind the do block
   do
   {
      randi = dist(mt);                                                // Numbers 1 through 6, as configured above
      std::cout << " - You rolled " << randi << std::endl;
   }
   while (randi != 6);

   std::cout << "Viszlat" << std::endl;                                // "Viszlát" does not print nice
}
