﻿/**
 *  file       : id 20221217°0935 — gitlab.com/normai/cheeseburger …/cpp/cp117keywords.cpp
 *  version    : • 20221227°1311 v0.1.9 Filling • 20221217°0935 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate keywords, using 'const_cast' as example
 *  note       : Code inspired by www.bogotobogo.com/cplusplus/typecast.php [ref 20221226°0947]
 *  status     :
 */
#define sVERSION "v0.1.9"

#include <cstring>
#include <iostream>
#include <string>

int main()
{
   std::cout << "*** Ko tenei 'cp117keywords.cpp' " << sVERSION << " -- Keywords ***" << std::endl;


   std::cout << "(1) Type conversion with 'const_cast<>'" << std::endl;

   std::string s("A1234567");                                  // Use a C++ string as origin
   const char *cstr1 = s.c_str();                              // Define a constant C-string
   std::cout << "(1.1) Constant string  = \"" << cstr1 << "\"" << std::endl;

   //char * cstr2 = cstr1;                                     // Error C2440 "'initializing': cannot convert from 'const char *' to 'char *'"
   char * cstr2 = const_cast<char *> (cstr1);                  // Type conversion possible
   * cstr2 = 'X';
   * (cstr2 + 1) = 'y';
   * (cstr2 + 2) = 'z';

   std::cout << "(1.2) No-more-constant = \"" << cstr2 << "\"" << std::endl;


   std::cout << "Tena koe." << std::endl;                      // "Tēnā koe"
   return 0;
}
