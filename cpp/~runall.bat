@echo off
rem file 20220215`1923 (after 20211103`1121) -- Run all
rem usage : Run this inside a Visual Studio Command Prompt
rem         Run a portion of 16 by uncommenting one of the 'goto spot_' lines

set delim================================================================================

rem goto spot1
goto spot2
rem goto spot3
rem goto spot4
rem goto spot5
rem goto spot6
rem goto spot7

:spot1

echo.
echo %delim%
cl.exe cp111hello.cpp /EHsc
cp111hello.exe
pause

echo.
echo %delim%
cl.exe cp113aloha.cpp /EHsc
cp113aloha.exe
pause

echo.
echo %delim%
cl.exe cp115varis.cpp /EHsc
cp115varis.exe
pause

echo.
echo %delim%
cl.exe cp117keywords.cpp /EHsc
cp117keywords.exe
pause

echo.
echo %delim%
cl.exe cp121operas.cpp /EHsc
cp121operas.exe
pause

echo.
echo %delim%
cl.exe cp123compare.cpp /EHsc
cp123compare.exe
pause

echo .
echo %delim%
cl.exe cp125bitwise.cpp /EHsc
cp125bitwise.exe
pause

echo .
echo %delim%
cl.exe cp127ternary.cpp /EHsc
cp127ternary.exe
pause

echo .
echo %delim%
cl.exe cp131types.cpp /EHsc
cp131types.exe
pause

echo .
echo %delim%
cl.exe cp133bool.cpp /EHsc /std:c++17
cp133bool.exe
pause

rem goto fin
:spot2

echo .
echo %delim%
cl.exe cp135char.cpp /EHsc
cp135char.exe
pause

echo .
echo %delim%
cl.exe cp137int.cpp /EHsc /std:c++17
cp137int.exe
pause

echo .
echo %delim%
cl.exe cp139bigint.cpp /EHsc
cp139bigint.exe
pause

echo .
echo %delim%
cl.exe cp145float.cpp /EHsc
cp145float.exe
pause

echo .
echo %delim%
cl.exe cp147bigflo.cpp /EHsc
cp147bigflo.exe
pause

echo .
echo %delim%
cl.exe cp151string.cpp /EHsc /std:c++17
cp151string.exe
pause

rem goto fin
:spot3

echo .
echo %delim%
cl.exe cp153stripol.cpp /EHsc /std:c++20
cp153stripol.exe
pause

echo .
echo %delim%
cl.exe cp155strfunc.cpp /EHsc
cp155strfunc.exe
pause

echo .
echo %delim%
cl.exe cp157array.cpp /EHsc
cp157array.exe
pause

echo .
echo %delim%
cl.exe cp161list.cpp /EHsc /std:c++17
cp161list.exe
pause

echo .
echo %delim%
cl.exe cp163sublist.cpp /EHsc
cp163sublist.exe
pause

echo .
echo %delim%
cl.exe cp165dict.cpp /EHsc
cp165dict.exe
pause

echo .
echo %delim%
cl.exe cp167tuple.cpp /EHsc /std:c++17
cp167tuple.exe
pause

echo .
echo %delim%
cl.exe cp171print.cpp /EHsc
cp171print.exe
pause

echo .
echo %delim%
cl.exe cp173input.cpp /EHsc
cp173input.exe
pause

echo .
echo %delim%
cl.exe cp211ifs1.cpp /EHsc
cp211ifs1.exe
pause

echo .
echo %delim%
cl.exe cp220switch.cpp /EHsc
cp220switch.exe
pause

echo .
echo %delim%
cl.exe cp230while.cpp /EHsc
cp230while.exe
pause

echo .
echo %delim%
cl.exe cp235dowhile.cpp /EHsc
cp235dowhile.exe
pause

echo .
echo %delim%
cl.exe cp240for.cpp /EHsc
cp240for.exe
pause

echo .
echo %delim%
cl.exe cp250foreach.cpp /EHsc
cp250foreach.exe
pause

echo .
echo %delim%
cl.exe cp261try1.cpp /EHsc
cp261try1.exe
pause

rem goto fin
:spot4

echo .
echo %delim%
cl.exe cp262try2.cpp /EHsc /std:c++17
cp262try2.exe
pause

echo .
echo %delim%
cl.exe cp267goto.cpp /EHsc
cp267goto.exe
pause

echo .
echo %delim%
cl.exe cp271funcs.cpp /EHsc
cp271funcs.exe
pause

echo .
echo %delim%
cl.exe cp273recurs.cpp /EHsc
cp273recurs.exe
pause

echo .
echo %delim%
cl.exe cp276anonfs.cpp /EHsc
cp276anonfs.exe
pause

echo .
echo %delim%
cl.exe cp278lambda.cpp /EHsc
cp278lambda.exe
pause

echo .
echo %delim%
cl.exe cp281fntional.cpp /EHsc
cp281fntional.exe
pause

echo .
echo %delim%
cl.exe cp286compars.cpp /EHsc
cp286compars.exe
pause

echo .
echo %delim%
cl.exe cp311kbd1menu.cpp /EHsc
cp311kbd1menu.exe
pause

echo .
echo %delim%
cl.exe cp313getkey.cpp /EHsc
cp313getkey.exe
pause

echo .
echo %delim%
cl.exe cp315kbd2menu.cpp /EHsc
cp315kbd2menu.exe
pause

echo .
echo %delim%
cl.exe cp317keypress.cpp /EHsc
cp317keypress.exe
pause

echo .
echo %delim%
cl.exe cp319kbd3menu.cpp /EHsc
cp319kbd3menu.exe
pause

echo .
echo %delim%
cl.exe cp335format.cpp /EHsc
cp335format.exe
pause

echo .
echo %delim%
cl.exe cp338stdio.cpp /EHsc
cp338stdio.exe
pause

rem goto fin
:spot5

echo .
echo %delim%
cl.exe cp341dir.cpp /EHsc
cp341dir.exe
pause

echo .
echo %delim%
cl.exe cp343textfile.cpp /EHsc
cp343textfile.exe
pause

echo .
echo %delim%
cl.exe cp346binfile.cpp /EHsc
cp346binfile.exe
pause

echo .
echo %delim%
cl.exe cp350modes.cpp /EHsc
cp350modes.exe
pause

echo .
echo %delim%
cl.exe cp410import.cpp /EHsc
cp410import.exe
pause

echo .
echo %delim%
cl.exe cp420export.cpp /EHsc
cp420export.exe
pause

echo .
echo %delim%
cl.exe cp430import2.cpp /EHsc
cp430import2.exe
pause

echo .
echo %delim%
cl.exe cp440main.cpp /EHsc
cp440main.exe
pause

echo .
echo %delim%
cl.exe cp450lib.cpp /EHsc
cp450lib.exe
pause

echo .
echo %delim%
cl.exe cp510class.cpp /EHsc
cp510class.exe
pause

echo .
echo %delim%
cl.exe cp520access.cpp /EHsc
cp520access.exe
pause

echo .
echo %delim%
cl.exe cp530inherit.cpp /EHsc
cp530inherit.exe
pause

echo .
echo %delim%
cl.exe cp540aggregate.cpp /EHsc
cp540aggregate.exe
pause

echo .
echo %delim%
cl.exe cp550polymo.cpp /EHsc
cp550polymo.exe
pause

echo .
echo %delim%
cl.exe cp560iface.cpp /EHsc
cp560iface.exe
pause

rem goto fin
:spot6

echo .
echo %delim%
cl.exe cp610create.cpp /EHsc
cp610create.exe
pause

echo .
echo %delim%
cl.exe cp620insert.cpp /EHsc
cp620insert.exe
pause

echo .
echo %delim%
cl.exe cp630upsala.cpp /EHsc
cp630upsala.exe
pause

echo .
echo %delim%
cl.exe cp640select.cpp /EHsc
cp640select.exe
pause

echo .
echo %delim%
cl.exe cp642relation.cpp /EHsc
cp642relation.exe
pause

echo .
echo %delim%
cl.exe cp644many.cpp /EHsc
cp644many.exe
pause

echo .
echo %delim%
cl.exe cp646joins.cpp /EHsc
cp646joins.exe
pause

echo .
echo %delim%
cl.exe cp651nosql.cpp /EHsc
cp651nosql.exe
pause

echo .
echo %delim%
cl.exe cp661linq.cpp /EHsc
cp661linq.exe
pause

echo .
echo %delim%
cl.exe cp711mthread.cpp /EHsc
cp711mthread.exe
pause

echo .
echo %delim%
cl.exe cp715assert.cpp /EHsc
cp715assert.exe
pause

echo .
echo %delim%
cl.exe cp720global.cpp /EHsc
cp720global.exe
pause

echo .
echo %delim%
cl.exe cp722varargs.cpp /EHsc
cp722varargs.exe
pause

echo .
echo %delim%
cl.exe cp724range.cpp /EHsc
cp724range.exe
pause

echo .
echo %delim%
cl.exe cp726regex.cpp /EHsc
cp726regex.exe
pause

echo .
echo %delim%
cl.exe cp730del.cpp /EHsc
cp730del.exe
pause

rem goto fin
:spot7

echo .
echo %delim%
cl.exe cp733sleep.cpp /EHsc
cp733sleep.exe
pause

echo .
echo %delim%
cl.exe cp735yield.cpp /EHsc
cp735yield.exe
pause

echo .
echo %delim%
cl.exe cp740args.cpp /EHsc
cp740args.exe
pause

echo .
echo %delim%
cl.exe cp751bubblesort.cpp /EHsc
cp751bubblesort.exe
pause

echo .
echo %delim%
cl.exe cp753humansort.cpp /EHsc
cp753humansort.exe
pause

echo .
echo %delim%
cl.exe cp761random.cpp /EHsc
cp761random.exe
pause

echo .
echo %delim%
cl.exe cp765unittest.cpp /EHsc
cp765unittest.exe
pause

echo .
echo %delim%
cl.exe cp771sound.cpp /EHsc
cp771sound.exe
pause

echo .
echo %delim%
cl.exe cp810window.cpp /EHsc
cp810window.exe
pause

echo .
echo %delim%
cl.exe cp813input1.cpp /EHsc
cp813input1.exe
pause

echo .
echo %delim%
cl.exe cp815input2.cpp /EHsc
cp815input2.exe
pause

echo .
echo %delim%
cl.exe cp830treeview.cpp /EHsc
cp830treeview.exe
pause

echo .
echo %delim%
cl.exe cp999template.cpp /EHsc
cp999template.exe
pause

echo .
echo %delim%
cl.exe cpcircles1.cpp /EHsc
cpcircles1.exe
pause

echo .
echo %delim%
cl.exe cpfactorial.cpp /EHsc
cpfactorial.exe
pause

:fin

echo .
echo Compile series finished.
pause
