﻿/**
 *  file       : id 20220928°1835 — gitlab.com/normai/cheeseburger …/cpp/cpcircles1.cpp
 *  version    : • 20220928°1835 v0.1.8 Initial filling
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Draw ASCII Circles and other patterns
 *  usage      : On Visual Studio Developer Command Prompt, in this folder
 *                type "cl.exe cpcircles1.cpp /EHsc" then "cpcircles1.exe"
 */
#define sVERSION "v0.0.0"

#include <iostream>

bool IsPointOnCirlce(int r, int x, int y) {
   int tolerance = r;                                               // Empirical value for line width
   if (abs(pow(x, 2) + pow(y, 2) - pow(r, 2)) < tolerance) {
      return true;
   }
   else {
      return false;
   }
}

bool IsPointOnDiagonal(int x, int y, int size) {
   if (x == size - y + 1) {                                         // Add 1 empirically
      return true;
   }
   else {
      return false;
   }
}

int main()
{
   std::cout << "*** Dobry den, toto je 'cpcircles1.cpp' " << sVERSION << " -- Draw ASCII Circles ***" << std::endl;  // "Dobrý deň" causes warnings "character cannot be represented …"

   int sizes[] = {7, 17};
   for (int size : sizes) {                                         // Paint as many rectangles as sizes are given

      // Prepare convenient variables
      // Note how the integer division for the radius looks different in each language
      int radius = (size / 2) - 1;                                  // Make size of circle slightly smaller than the rectangle
      int shiftx = radius + 2;                                      // Shift circle center from rectangle left to rectangle center
      int shifty = radius + 2;                                      // Shift circle center from rectangle top to rectangle center

      // Iterate over lines and columns
      for (int line = 1; line <= size; line++) {
         for (int col = 1; col <= size; col++) {
            if (IsPointOnCirlce(radius, line - shiftx, col - shifty)) {  // Is this point on the circle?
               std::cout << "@ ";
            }
            else {
               if (IsPointOnDiagonal(line, col, size)) {            // Is this point on the diagonal?
                  std::cout << "# ";
               }
               else {                                               // All other cases
                  std::cout << ". ";
               }
            }
         }
         std::cout << std::endl;                                    // Goto next line
      }
   }

   std::cout << "Dovidenia." << std::endl;
}
