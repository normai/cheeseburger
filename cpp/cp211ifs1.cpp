﻿/**
 *  file       : id 20220826°1235 — gitlab.com/normai/cheeseburger/ …/cpp/cp211ifs1.cpp
 *  version    : • 20220922°1835 v0.1.8 Filling • 20220826°1235 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
    *  subject : Demonstrate branching with if, else if, else
    *  summary : This demonstrates If/ElseIf/Else with four cases:
    *             • Unilateral branching
    *             • Bilateral branching
    *             • Multilateral branching
    *             • Wrong branching logic
    *            The sequence goes like this (1) Create random integer (2) Let the user
    *            overwrite this number (3) Do four different evaluations on that number
 *  usage      : VS Developer Command Prompt "cl.exe cp211ifs1.cpp /EHsc", then "cp211ifs1.exe"
 *  ref        : https://cplusplus.com/reference/string/stoi/ []
 */
#define sVERSION "v0.1.8"

#include <iostream>
#include <random>
#include <string>

int main()
{
   std::cout << "*** Hallo, hier ist 'cp211ifs1.cpp' " << sVERSION << " -- If/ElseIf/Else ***" << std::endl;

   // (A.1) Preparation — Get a random number rolled
   std::random_device rd;
   std::mt19937 mt(rd());                                                  // Mersenne Twister with non-deterministic 32-bit seed
   std::uniform_int_distribution<int> dist(1, 6);
   int randi = dist(mt);
   std::cout << "You rolled number " << std::to_string(randi) << std::endl;

   // (A.2) Optionally allow to manually choose the number
   if (true) {                                                             // Optionally toggle this
      std::cout << "Overwrite the rolled number with 1..6 >" << std::endl;
      std::string s;
      std::cin >> s;
      try {
         randi = stoi(s);
         randi = randi < 1 ? 1 : randi;
         randi = randi > 6 ? 6 : randi;
      }
      catch (const std::invalid_argument& iaxcp) {
         std::cout << "Your input was not a number. " << iaxcp.what() << std::endl;
      }
      std::cout << "Your number now is '" << std::to_string(randi) << "'." << std::endl;;
   }

   // (B.1) Unilateral branching
   std::cout << "(1) Unilateral branching" << std::endl;
   if (randi > 3) {
      std::cout << "(1.1) : Your number is bigger than three" << std::endl;
   }

   // (B.2) Two-way branching
   std::cout << "(2) Bilateral branching" << std::endl;
   if (randi == 3) {
      std::cout << "(2.1) : The number is three" << std::endl;
   }
   else {
      std::cout << "(2.2) : The number is not three" << std::endl;
   }

   // (B.3) Multilateral branching — Only one branch will be executed
   std::cout << "(3) Multilateral branching" << std::endl;
   if (randi > 5) {
      std::cout << "(3.1) : The number is six" << std::endl;
   }
   else if (randi > 4) {
      std::cout << "(3.2) : The number is five" << std::endl;
   }
   else if (randi > 3) {
      std::cout << "(3.3) : The number is four" << std::endl;
   }
   else if (randi > 2) {
      std::cout << "(3.4) : The number is three" << std::endl;
   }
   else {
      std::cout << "(3.5) : The number is one or two" << std::endl;
   }

   // (B.4) A typical beginners mistake, looks very similar like above,
   //  has multiple if instead elif statements, will not work as intended.
   std::cout << "(4) Wrong logic" << std::endl;
   if (randi > 5) {
      std::cout << "(4.1) : The number is six" << std::endl;
   }
   if (randi > 4) {
      std::cout << "(4.2) : The number is five or six" << std::endl;
   }
   if (randi > 3) {
      std::cout << "(4.3) : The number is four to six" << std::endl;
   }
   if (randi > 2) {
      std::cout << "(4.4) : The number is three to six" << std::endl;
   }
   else {
      std::cout << "(4.5) : The number is one or two" << std::endl;
   }

   std::cout << "Auf Wiedersehen." << std::endl;
}
