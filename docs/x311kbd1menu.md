﻿# Demonstrate Keyboard Menu (with pressing Enter)

If a console program shall be interactively controllable by the user
 during run time, we have to establish some more or less comfortable way
 for the user to steer the program flow.

## Python &nbsp; <sup><sub>[py311kbd1menu.py](./../py/py311kbd1menu.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

<a href="./runs/py311kbd1menu.png">
 <img src ="./runs/py311kbd1menu.png" width="650" height="295" data-dims="x1084y0492" alt="Run py311kbd1menu.py">
 </a>

&nbsp;


## Java &nbsp; <sup><sub>[jv311kbd1menu.java](./../java/jv311kbd1menu.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jv311kbd1menu.png">
 <img src ="./runs/jv311kbd1menu.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv311kbd1menu.java">
 </a --->

&nbsp;


## C-Sharp &nbsp; <sup><sub>[cs311kbd1menu.cs](./../cs/cs311kbd1menu.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs311kbd1menu.png">
 <img src ="./runs/cs311kbd1menu.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs311kbd1menu.cs">
 </a --->

&nbsp;


## JavaScript &nbsp; <sup><sub>[js311kbd1menu.js](./../js/js311kbd1menu.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js311kbd1menu.png">
 <img src ="./runs/js311kbd1menu.png" width="650" height="33" data-dims="x1084y0033" alt="Run js311kbd1menu.js">
 </a --->

&nbsp;


## PHP &nbsp; <sup><sub>[ph311kbd1menu.php](./../php/ph311kbd1menu.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph311kbd1menu.png">
 <img src ="./runs/ph311kbd1menu.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph311kbd1menu.php">
 </a --->

&nbsp;


## CPP &nbsp; <sup><sub>[cp311kbd1menu.cpp](./../cpp/cp311kbd1menu.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp311kbd1menu.png">
 <img src ="./runs/cp311kbd1menu.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp311kbd1menu.cpp">
 </a --->

&nbsp;


---

<sup><sub><sup>*[File 20221116°1401 x311kbd1menu.md]* ⬞Ω</sup></sub></sup>
