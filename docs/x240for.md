﻿# Demonstrate For-Loop

The For-Loop is more quickly to understand in the code, than the While-Loop.
 It combines three control elements into one single line, the loop header.
 It is not so universal as the While-Loop.

Rule of thumb: Always use the For-Loop, if the number of looping is know
 right from the beginning.

## Python &nbsp; <sup><sub>[py240for.py](./../py/py240for.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

<a href="./runs/py240for.png">
 <img src ="./runs/py240for.png" width="654" height="308" data-dims="x1091y0514" alt="Run py240for.py">
 </a>

&nbsp;

<img src="./icos/20200630o0143.w3schools.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for W3Schools" id="id20220102o0452">
 &nbsp; About the <b>range</b> function see e.g.
 [w3schools](https://www.w3schools.com/python/ref_func_range.asp)
 <!--- [ref 20221216°1244] --->

&nbsp;

<img src="./icos/20201225o1757.realpython.v1.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for RealPython" id="id20220102o0454">
 &nbsp; About the <b>range</b> function also see e.g.
 [realpython.com/python-range](https://realpython.com/python-range/).
 <!--- [ref 20221216°1246] --->

&nbsp;


## Java &nbsp; <sup><sub>[jv240for.java](./../java/jv240for.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jv240for.png">
 <img src ="./runs/jv240for.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv240for.java">
 </a --->

&nbsp;


## C-Sharp &nbsp; <sup><sub>[cs240for.cs](./../cs/cs240for.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs240for.png">
 <img src ="./runs/cs240for.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs240for.cs">
 </a --->

&nbsp;


## JavaScript &nbsp; <sup><sub>[js240for.js](./../js/js240for.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js240for.png">
 <img src ="./runs/js240for.png" width="650" height="33" data-dims="x1084y0033" alt="Run js240for.js">
 </a --->

&nbsp;


## PHP &nbsp; <sup><sub>[ph240for.php](./../php/ph240for.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph240for.png">
 <img src ="./runs/ph240for.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph240for.php">
 </a --->

&nbsp;


## CPP &nbsp; <sup><sub>[cp240for.cpp](./../cpp/cp240for.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp240for.png">
 <img src ="./runs/cp240for.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp240for.cpp">
 </a --->

&nbsp;


---

<sup><sub><sup>*[File 20220826°1701 x240for.md]* ⬞Ω</sup></sub></sup>
