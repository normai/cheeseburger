﻿# Demonstrate Strings

What is interesting about Strings?
- Concatenate
- Substring
- Uppercase/lowercase
- Trim left and right
- Convert numbers to string
- Convert string to numbers
- String interpolation with various styles with various types

## Python &nbsp; <sup><sub>[py151string.py](./../py/py151string.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

<a href="./runs/py151string.png">
 <img src ="./runs/py151string.png" width="637" height="323" data-dims="x0980y0497" alt="Run py151string.py">
 </a>

&nbsp;

<img src="./icos/20201225o1757.realpython.v1.x0048y0048.png" align="left" style="margin-right:1.1em;" width="48" height="48" alt="Link icon for RealPython">
 RealPython article
 [Python String Formatting Best Practices](https://realpython.com/python-string-formatting/)
 lists four flavours of string interpolation
 &nbsp; <sup><sub><sup>*[ref 20221001°1223]*</sup></sub></sup></span>

&nbsp;

<img src="./icos/20200630o0143.w3schools.v2.x0048y0048.png" align="left" style="margin-right:1.1em;" width="48" height="48" alt="Link icon for W3Schools.com">
 W3Schools.com article
 [Python String format Method](https://www.w3schools.com/python/ref_string_format.asp)
 lists the formatting codes
 &nbsp; <sup><sub><sup>*[ref 20221001°1224]*</sup></sub></sup></span>

&nbsp;


## Java &nbsp; <sup><sub>[jv151string.java](./../java/jv151string.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jv151string.png">
 <img src ="./runs/jv151string.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv151string.java">
 </a --->


## C-Sharp &nbsp; <sup><sub>[cs151string.cs](./../cs/cs151string.cs)<csb></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs151string.png">
 <img src ="./runs/cs151string.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs151string.cs">
 </a --->


## JavaScript &nbsp; <sup><sub>[js151string.js](./../js/js151string.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js151string.png">
 <img src ="./runs/js151string.png" width="650" height="33" data-dims="x1084y0033" alt="Run js151string.js">
 </a --->


## PHP &nbsp; <sup><sub>[ph151string.php](./../php/ph151string.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph151string.png">
 <img src ="./runs/ph151string.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph151string.php">
 </a --->


## CPP &nbsp; <sup><sub>[cp151string.cpp](./../cpp/cp151string.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

The nasty matter with C++ is, that we have to deal with two different string
 types: (1) The C++ **`std::string`** class and (2) the C style **`char*`** type.
 Though the C style string shall die out, they just persist, e.g. in the Windows API.

<!--- a href="./runs/cp151string.png">
 <img src ="./runs/cp151string.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp151string.cpp">
 </a --->


## General

<img src="./icos/20220127o1733.wikisource.v2.x0048y0048.png" align="left" style="margin-right:1.1em;" width="48" height="48" alt="Link icon for W3Schools.com">
 By the way. Wikisource article
 [Das aesthetische Wiesel](https://de.wikisource.org/wiki/Das_aesthetische_Wiesel)
 tells the complete poem for the multi-line text sample used for the demo.
 &nbsp; <sup><sub><sup>*[ref 20221010°0912]*</sup></sub></sup></span>

&nbsp;


---

<sup><sub><sup>*[File 20221001°1301 x151string.md]* ⬞Ω</sup></sub></sup>
