﻿# Demonstrate Comparisons

Here we see comparisons combined with `and` and `or` while rolling dices.

## Python &nbsp; <sup><sub>[py286compars.py](./../py/py286compars.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

<a href="./runs/py286compars.png">
 <img src ="./runs/py286compars.png" width="650" height="254" data-dims="x1084y0424" alt="Run py286compars.py">
 </a>

&nbsp;


## Java &nbsp; <sup><sub>[jv286compars.java](./../java/jv286compars.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jv286compars.png">
 <img src ="./runs/jv286compars.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv286compars.java">
 </a --->

&nbsp;


## C-Sharp &nbsp; <sup><sub>[cs286compars.cs](./../cs/cs286compars.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs286compars.png">
 <img src ="./runs/cs286compars.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs286compars.cs">
 </a --->

&nbsp;


## JavaScript &nbsp; <sup><sub>[js286compars.js](./../js/js286compars.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js286compars.png">
 <img src ="./runs/js286compars.png" width="650" height="33" data-dims="x1084y0033" alt="Run js286compars.js">
 </a --->

&nbsp;


## PHP &nbsp; <sup><sub>[ph286compars.php](./../php/ph286compars.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph286compars.png">
 <img src ="./runs/ph286compars.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph286compars.php">
 </a --->

&nbsp;


## CPP &nbsp; <sup><sub>[cp286compars.cpp](./../cpp/cp286compars.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp286compars.png">
 <img src ="./runs/cp286compars.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp286compars.cpp">
 </a --->

&nbsp;


---

<sup><sub><sup>*[File 20220827°1201 x286compars.md]* ⬞Ω</sup></sub></sup>
