﻿# Demonstrate Character Type

One character once was one byte. Since the advent of Unicode, one character
 is two or more bytes. From characters strings are built.

In this 'character' topic, the different programming languages differ most.
 It is impossible to craft a sensible user story which is the same for all
 languages.

Status. From the most chaotic Python demo over the unfinished Java
 demo over the C# demo I try to approach some sensible user story
 common for all the languages. This process is not finished yet.

## Python &nbsp; <sup><sub>[py135char.py](./../py/py135char.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

Python has no `char` type. We investigate the bytes type instead.

<a href="./runs/py135char.png">
 <img src ="./runs/py135char.png" width="659" height="404" data-dims="x1199y0735*.55" alt="Run py135char.py">
 </a>

&nbsp;


## Java &nbsp; <sup><sub>[jv135char.java](./../java/jv135char.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

BTW. Java has no way to ask for the type of a primitive type variable.
 One uses a boxed type, so for an 'int' it tells 'Integer'. Only with
 much efford, one could write a function to get the primitive type.

<!--- a href="./runs/jv35char.png">
 <img src ="./runs/jv35char.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv35char.java">
 </a --->

&nbsp;


## C-Sharp &nbsp; <sup><sub>[cs135char.cs](./../cs/cs135char.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs35char.png">
 <img src ="./runs/cs35char.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs35char.cs">
 </a --->

&nbsp;


## JavaScript &nbsp; <sup><sub>[js135char.js](./../js/js135char.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js35char.png">
 <img src ="./runs/js35char.png" width="650" height="33" data-dims="x1084y0033" alt="Run js35char.js">
 </a --->

&nbsp;


## PHP &nbsp; <sup><sub>[ph135char.php](./../php/ph135char.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph35char.png">
 <img src ="./runs/ph35char.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph35char.php">
 </a --->

&nbsp;


## CPP &nbsp; <sup><sub>[cp135char.cpp](./../cpp/cp135char.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp35char.png">
 <img src ="./runs/cp35char.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp35char.cpp">
 </a --->

&nbsp;


---

<sup><sub><sup>*[File 20221012°0901 x135char.md]* ⬞Ω</sup></sub></sup>
