﻿# Demonstrate Create Statement

## Python &nbsp; <sup><sub>[py610create.py](./../py/py610create.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

<!--- a href="./runs/py610create.png">
 <img src ="./runs/py610create.png" width="650" height="33" data-dims="x1084y0033" alt="Run py610create.py">
 </a --->

&nbsp;


## Java &nbsp; <sup><sub>[jv610create.java](./../java/jv610create.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jv610create.png">
 <img src ="./runs/jv610create.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv610create.java">
 </a --->

&nbsp;


## C-Sharp &nbsp; <sup><sub>[cs610create.cs](./../cs/cs610create.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs610create.png">
 <img src ="./runs/cs610create.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs610create.cs">
 </a --->

&nbsp;


## JavaScript &nbsp; <sup><sub>[js610create.js](./../js/js610create.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js610create.png">
 <img src ="./runs/js610create.png" width="650" height="33" data-dims="x1084y0033" alt="Run js610create.js">
 </a --->

&nbsp;


## PHP &nbsp; <sup><sub>[ph610create.php](./../php/ph610create.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph610create.png">
 <img src ="./runs/ph610create.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph610create.php">
 </a --->

&nbsp;


## CPP &nbsp; <sup><sub>[cp610create.cpp](./../cpp/cp610create.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp610create.png">
 <img src ="./runs/cp610create.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp610create.cpp">
 </a --->

&nbsp;


---

<sup><sub><sup>*[File 20220831°1501]* ⬞Ω</sup></sub></sup>
