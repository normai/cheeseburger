﻿# Demonstrate Goto Statement

## Python &nbsp; <sup><sub>[py267goto.py](./../py/py267goto.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

<!--- a href="./runs/py267goto.png">
 <img src ="./runs/py267goto.png" width="650" height="33" data-dims="x1084y0033" alt="Run py267goto.py">
 </a --->

&nbsp;


## Java &nbsp; <sup><sub>[jv267goto.java](./../java/jv267goto.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jv267goto.png">
 <img src ="./runs/jv267goto.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv267goto.java">
 </a --->

&nbsp;


## C-Sharp &nbsp; <sup><sub>[cs267goto.cs](./../cs/cs267goto.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs267goto.png">
 <img src ="./runs/cs267goto.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs267goto.cs">
 </a --->

&nbsp;


## JavaScript &nbsp; <sup><sub>[js267goto.js](./../js/js267goto.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js267goto.png">
 <img src ="./runs/js267goto.png" width="650" height="33" data-dims="x1084y0033" alt="Run js267goto.js">
 </a --->

&nbsp;


## PHP &nbsp; <sup><sub>[ph267goto.php](./../php/ph267goto.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph267goto.png">
 <img src ="./runs/ph267goto.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph267goto.php">
 </a --->

See PHP manual chapter Control Structures —
 <a href="https://www.php.net/manual/en/control-structures.goto.php" id="">goto</a>

&nbsp;


## CPP &nbsp; <sup><sub>[cp267goto.cpp](./../cpp/cp267goto.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp267goto.png">
 <img src ="./runs/cp267goto.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp267goto.cpp">
 </a --->

&nbsp;


---

<sup><sub><sup>*[File 20220827°1001]* ⬞Ω</sup></sub></sup>
