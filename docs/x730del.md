﻿# Demonstrate The Unlucky Keyword `del` (Python only?)

Python has a keyword `del` to extinguish a formerly created object.
 Such thing works pretty different in other languages.

## Python &nbsp; <sup><sub>[py730del.py](./../py/py730del.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

Subject: Unlucky keyword del (showing partial versus full import)

<a href="./runs/py730del.png">
 <img src ="./runs/py730del.png" width="650" height=172" data-dims="x1084y0288" alt="Run py730del.py">
 </a>

&nbsp;


## Java &nbsp; <sup><sub>[jv730del.java](./../java/jv730del.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jv730del.png">
 <img src ="./runs/jv730del.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv730del.java">
 </a --->

&nbsp;


## C-Sharp &nbsp; <sup><sub>[cs730del.cs](./../cs/cs730del.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs730del.png">
 <img src ="./runs/cs730del.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs730del.cs">
 </a --->

&nbsp;


## JavaScript &nbsp; <sup><sub>[js730del.js](./../js/js730del.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js730del.png">
 <img src ="./runs/js730del.png" width="650" height="33" data-dims="x1084y0033" alt="Run js730del.js">
 </a --->

&nbsp;


## PHP &nbsp; <sup><sub>[ph730del.php](./../php/ph730del.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph730del.png">
 <img src ="./runs/ph730del.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph730del.php">
 </a --->

&nbsp;


## CPP &nbsp; <sup><sub>[cp730del.cpp](./../cpp/cp730del.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp730del.png">
 <img src ="./runs/cp730del.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp730del.cpp">
 </a --->

&nbsp;


---

<sup><sub><sup>*[File 20220830°1701 x730del.md]* ⬞Ω</sup></sub></sup>
