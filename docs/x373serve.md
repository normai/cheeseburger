﻿# Demonstrate Network Writing

The most popular kind of writing to a network in the internet is, that a
 server delivers pages to some browser …

## Python &nbsp; <sup><sub>[py373serve.py](./../py/py373serve.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

<!--- a href="./runs/py373serve.png">
 <img src ="./runs/py373serve.png" width="650" height="33" data-dims="x1084y0033" alt="Run py373serve.py">
 </a --->

&nbsp;


## Java &nbsp; <sup><sub>[jv373serve.java](./../java/jv373serve.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jv373serve.png">
 <img src ="./runs/jv373serve.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv373serve.java">
 </a --->

&nbsp;


## C-Sharp &nbsp; <sup><sub>[cs373serve.cs](./../cs/cs373serve.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs373serve.png">
 <img src ="./runs/cs373serve.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs373serve.cs">
 </a --->

&nbsp;


## JavaScript &nbsp; <sup><sub>[js373serve.js](./../js/js373serve.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js373serve.png">
 <img src ="./runs/js373serve.png" width="650" height="33" data-dims="x1084y0033" alt="Run js373serve.js">
 </a --->

&nbsp;


## PHP &nbsp; <sup><sub>[ph373serve.php](./../php/ph373serve.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph373serve.png">
 <img src ="./runs/ph373serve.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph373serve.php">
 </a --->

&nbsp;


## CPP &nbsp; <sup><sub>[cp373serve.cpp](./../cpp/cp373serve.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp373serve.png">
 <img src ="./runs/cp373serve.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp373serve.cpp">
 </a --->

&nbsp;


---

<sup><sub>*[File 20221223°1101 x373serve.md]* ⬞Ω</sub></sup>
