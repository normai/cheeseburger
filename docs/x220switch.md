﻿# Demonstrate Switch Statement

The Switch statement works like a `if/elseif/elseif/else` statement, just
 for this especially simple situation: The condition is bound to one single
 variable, which can be asked for the one or the other value.

## Python &nbsp; <sup><sub>[py220switch.py](./../py/py220switch.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

<a href="./runs/py220switch.png">
 <img src ="./runs/py220switch.png" width="644" height="194" data-dims="x0992y0299" alt="Run py220switch.py">
 </a>

Subject: Python originally had no `switch`, it had to be emulated. Only
 with Python 3.10 the `match` statement was introduced, which exacly
 resembles the `switch`.

<img src="./icos/20200805o1243.python.v2.x0048y0048.png" align="left" style="margin-right:1.1em;" width="48" height="48" alt="Link icon for Pyton.org">
 &nbsp; Python documentation page
 [4. More Control Flow Tools -- 4.6. match Statements](https://docs.python.org/3/tutorial/controlflow.html#match-statements)
 <!--- [ref 20221214°1556] --->

&nbsp;

<img src="./icos/20200521o1628.freecodecamp.v2.x0048y0048.png" align="left" style="margin-right:1.1em;" width="48" height="48" alt="Link icon for FreeCodeCamp">
 &nbsp; FreeCodeCamp article
 [Python Switch Statement – Switch Case Example](https://www.freecodecamp.org/news/python-switch-statement-switch-case-example/)
 <!--- [ref 20221214°1554] --->

&nbsp;

<img src="./icos/20220915o0913.datagy.v2.x0048y0048.png" align="left" style="margin-right:1.1em;" width="48" height="48" alt="Link icon for Datagy.io">
 &nbsp; Datagy.io tutorial
 [Python Switch (Match-Case) Statements: Complete Guide](https://datagy.io/python-switch-case/)
 <!--- [ref 20221214°1552] --->

&nbsp;

## Java &nbsp; <sup><sub>[jv220switch.java](./../java/jv220switch.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jv220switch.png">
 <img src ="./runs/jv220switch.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv220switch.java">
 </a --->


## C-Sharp &nbsp; <sup><sub>[cs220switch.cs](./../cs/cs220switch.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs220switch.png">
 <img src ="./runs/cs220switch.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs220switch.cs">
 </a --->


## JavaScript &nbsp; <sup><sub>[js220switch.js](./../js/js220switch.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js220switch.png">
 <img src ="./runs/js220switch.png" width="650" height="33" data-dims="x1084y0033" alt="Run js220switch.js">
 </a --->


## PHP &nbsp; <sup><sub>[ph220switch.php](./../php/ph220switch.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph220switch.png">
 <img src ="./runs/ph220switch.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph220switch.php">
 </a --->


## CPP &nbsp; <sup><sub>[cp220switch.cpp](./../cpp/cp220switch.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp220switch.png">
 <img src ="./runs/cp220switch.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp220switch.cpp">
 </a --->

&nbsp;


---

<sup><sub><sup>*[File 20220826°1401 x220switch.md]* ⬞Ω</sup></sub></sup>
