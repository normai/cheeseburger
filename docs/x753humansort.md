﻿# Demonstrate Sorting with the 'Human Sort Algorithm'

Sorting is a nice algorithm exercise task. There are many different
 sorting algorithms with different behaviour.

The 'Human Sort Algorithm' (or Selection Sort) is just one of many sorting algorithms.


## Python &nbsp; <sup><sub>[py753humansort.py](./../py/py753humansort.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

<a href="./runs/py753humansort.png">
 <img src ="./runs/py753humansort.png" width="650" height="347" data-dims="x1084y0579" alt="Run py753humansort.py">
 </a>

&nbsp;

<img src="./icos/20110902o1742.codeproject.v3.x0048y0048.png" align="left" style="margin-right:1.1em;" width="32" height="32" alt="Link to CodeProject">
 &nbsp; CodeProject article
 [A Fast New Sorting Routine - The Human Sort](https://www.codeproject.com/Articles/9926/A-Fast-New-Sorting-Routine-The-Human-Sort)
 seems to be describing the 'Selection Sort' algorithm.
 <!--- [ref 20221220°1212] --->

&nbsp;

<img src="./icos/20110921o1125.wikipedia.v1.x0048y0048.png" align="left" style="margin-right:1.1em;" width="32" height="32" alt="Link to Wikipedia">
 &nbsp; Wikipedia articles
 [Selection sort](https://en.wikipedia.org/wiki/Selection_sort)
 <!--- [ref 20221220°1216] --->
 (english) and
 [Selectionsort](https://de.wikipedia.org/wiki/Selectionsort)
 <!--- [ref 20221220°1217] --->
 (german)

&nbsp;

<img src="./icos/20221220o1223.xlinux-nist-gov--dads.v2.x0032y0032.png" align="left" style="margin-right:1.1em;" width="32" height="32" alt="Link to NIST/DADS">
 &nbsp; The NIST subdomain
 [Dictionary of Algorithms and Data Structures](https://xlinux.nist.gov/dads/)
 <!--- [ref 20221220°1224] --->
 describes many sorting algorithms, e.g. •
 [Bubble Sort](https://xlinux.nist.gov/dads/HTML/bubblesort.html)
 <!--- [ref 20221220°1225] --->
 and •
 [Selection Sort](https://xlinux.nist.gov/dads/HTML/selectionSort.html)
 <!--- [ref 20221220°1222] --->

&nbsp;


## Java &nbsp; <sup><sub>[jv753humansort.java](./../java/jv753humansort.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jv753humansort.png">
 <img src ="./runs/jv753humansort.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv753humansort.java">
 </a --->


## C-Sharp &nbsp; <sup><sub>[cs753humansort.cs](./../cs/cs753humansort.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs753humansort.png">
 <img src ="./runs/cs753humansort.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs753humansort.cs">
 </a --->


## JavaScript &nbsp; <sup><sub>[js753humansort.js](./../js/js753humansort.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js753humansort.png">
 <img src ="./runs/js753humansort.png" width="650" height="33" data-dims="x1084y0033" alt="Run js753humansort.js">
 </a --->


## PHP &nbsp; <sup><sub>[ph753humansort.php](./../php/ph753humansort.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph753humansort.png">
 <img src ="./runs/ph753humansort.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph753humansort.php">
 </a --->


## CPP &nbsp; <sup><sub>[cp753humansort.cpp](./../cpp/cp753humansort.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp753humansort.png">
 <img src ="./runs/cp753humansort.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp753humansort.cpp">
 </a --->


---

<sup><sub><sup>*[File 20220830°1801 x753humansort.md]* ⬞Ω</sup></sub></sup>
