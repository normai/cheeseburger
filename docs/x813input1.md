﻿# Demonstrate Window with Input Fields

## Python &nbsp; <sup><sub>[py813input1.py](./../py/py813input1.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

<!--- a href="./runs/py813input1.png">
 <img src ="./runs/py813input1.png" width="650" height="33" data-dims="x1084y0033" alt="Run py813input1.py">
 </a --->

&nbsp;


## Java &nbsp; <sup><sub>[jv813input1.java](./../java/jv813input1.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jv813input1.png">
 <img src ="./runs/jv813input1.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv813input1.java">
 </a --->

&nbsp;


## C-Sharp &nbsp; <sup><sub>[cs813input1.cs](./../cs/cs813input1.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs813input1.png">
 <img src ="./runs/cs813input1.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs813input1.cs">
 </a --->

&nbsp;


## JavaScript &nbsp; <sup><sub>[js813input1.js](./../js/js813input1.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js813input1.png">
 <img src ="./runs/js813input1.png" width="650" height="33" data-dims="x1084y0033" alt="Run js813input1.js">
 </a --->

&nbsp;


## PHP &nbsp; <sup><sub>[ph813input1.php](./../php/ph813input1.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph813input1.png">
 <img src ="./runs/ph813input1.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph813input1.php">
 </a --->

&nbsp;


## CPP &nbsp; <sup><sub>[cp813input1.cpp](./../cpp/cp813input1.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp813input1.png">
 <img src ="./runs/cp813input1.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp813input1.cpp">
 </a --->

&nbsp;


---

<sup><sub><sup>*[File 20220831°0901]* ⬞Ω</sup></sub></sup>
