﻿# Demonstrate File Attributes

All file systems decorate the files with attributes holding metatdata such
 as who can read, write, execute, delete, that file, whether it may be
 executed, whether it shall be hidden.

## Python &nbsp; <sup><sub>[py355attribs.py](./../py/py355attribs.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

<!--- a href="./runs/py355attribs.png">
 <img src ="./runs/py355attribs.png" width="650" height="33" data-dims="x1084y0033" alt="Run py355attribs.py">
 </a --->

&nbsp;


## Java &nbsp; <sup><sub>[jv355attribs.java](./../java/jv355attribs.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jv355attribs.png">
 <img src ="./runs/jv355attribs.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv355attribs.java">
 </a --->

&nbsp;


## C-Sharp &nbsp; <sup><sub>[cs355attribs.cs](./../cs/cs355attribs.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs355attribs.png">
 <img src ="./runs/cs355attribs.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs355attribs.cs">
 </a --->

&nbsp;


## JavaScript &nbsp; <sup><sub>[js355attribs.js](./../js/js355attribs.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js355attribs.png">
 <img src ="./runs/js355attribs.png" width="650" height="33" data-dims="x1084y000" alt="Run js355attribs.js">
 </a --->

&nbsp;


## PHP &nbsp; <sup><sub>[ph355attribs.php](./../php/ph355attribs.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph355attribs.png">
 <img src ="./runs/ph355attribs.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph355attribs.php">
 </a --->

&nbsp;


## CPP &nbsp; <sup><sub>[cp355attribs.cpp](./../cpp/cp355attribs.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp355attribs.png">
 <img src ="./runs/cp355attribs.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp355attribs.cpp">
 </a --->

&nbsp;


---

<sup><sub>*[File 20221223°0801 x355attribs.md]* ⬞Ω</sub></sup>
