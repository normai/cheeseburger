﻿# Demonstrate Feature …

The … Feature …

## Python &nbsp; <sup><sub>[py999template.py](./../py/py999template.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

<!--- a href="./runs/py999template.png">
 <img src ="./runs/py999template.png" width="650" height="33" data-dims="x1084y0033" alt="Run py999template.py">
 </a --->

&nbsp;


## Java &nbsp; <sup><sub>[jv999template.java](./../java/jv999template.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jv999template.png">
 <img src ="./runs/jv999template.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv999template.java">
 </a --->

&nbsp;


## C-Sharp &nbsp; <sup><sub>[cs999template.cs](./../cs/cs999template.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs999template.png">
 <img src ="./runs/cs999template.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs999template.cs">
 </a --->

&nbsp;


## JavaScript &nbsp; <sup><sub>[js999template.js](./../js/js999template.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js999template.png">
 <img src ="./runs/js999template.png" width="650" height="33" data-dims="x1084y0033" alt="Run js999template.js">
 </a --->

&nbsp;


## PHP &nbsp; <sup><sub>[ph999template.php](./../php/ph999template.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph999template.png">
 <img src ="./runs/ph999template.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph999template.php">
 </a --->

&nbsp;


## CPP &nbsp; <sup><sub>[cp999template.cpp](./../cpp/cp999template.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp999template.png">
 <img src ="./runs/cp999template.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp999template.cpp">
 </a --->

&nbsp;


---

<sup><sub>*[File 20220828°1601 x999template.md]* ⬞Ω</sub></sup>
