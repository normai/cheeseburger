﻿# Demonstrate Keyword `global` for Scoping

## Python &nbsp; <sup><sub>[py720global.py](./../py/py720global.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

<!--- a href="./runs/py720global.png">
 <img src ="./runs/py720global.png" width="650" height="33" data-dims="x1084y0033" alt="Run py720global.py">
 </a --->

&nbsp;


## Java &nbsp; <sup><sub>[jv720global.java](./../java/jv720global.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jv720global.png">
 <img src ="./runs/jv720global.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv720global.java">
 </a --->

&nbsp;


## C-Sharp &nbsp; <sup><sub>[cs720global.cs](./../cs/cs720global.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs720global.png">
 <img src ="./runs/cs720global.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs720global.cs">
 </a --->

&nbsp;


## JavaScript &nbsp; <sup><sub>[js720global.js](./../js/js720global.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js720global.png">
 <img src ="./runs/js720global.png" width="650" height="33" data-dims="x1084y0033" alt="Run js720global.js">
 </a --->

&nbsp;


## PHP &nbsp; <sup><sub>[ph720global.php](./../php/ph720global.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph720global.png">
 <img src ="./runs/ph720global.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph720global.php">
 </a --->

&nbsp;


## CPP &nbsp; <sup><sub>[cp720global.cpp](./../cpp/cp720global.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp720global.png">
 <img src ="./runs/cp720global.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp720global.cpp">
 </a --->

&nbsp;


---

<sup><sub><sup>*[File 20220830°1601]* ⬞Ω</sup></sub></sup>
