﻿# Demonstrate Keyboard Menu (without pressing Enter)

## Python &nbsp; <sup><sub>[py315kbd2menu.py](./../py/py315kbd2menu.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

<!--- a href="./runs/py315kbd2menu.png">
 <img src ="./runs/py315kbd2menu.png" width="650" height="33" data-dims="x1084y0033" alt="Run py315kbd2menu.py">
 </a --->

<img src="./imgs/20201106o0344.pypi.v2.x0032y0032.png" width="32" height="32" alt="Icon for PyPi">
 <img src="./imgs/20240921o1247.boppreh.v1.x0032y0032.png" width="32" height="32" alt="Icon for BoppreH">
 By the way, there also exists a **module `keyboard`**
 on PyPi page
 [pypi.org/project/keyboard/](https://pypi.org/project/keyboard/)
 <sup><sub><sup>*[ref 20240921°1232]*</sup></sub></sup>
 , with the GitHub project
 [github.com/boppreh/keyboard](https://github.com/boppreh/keyboard)
 <sup><sub><sup>*[ref 20240921°1242]*</sup></sub></sup>
 . Just this needs to be installed, it has friction, and it is unmaintained
 with the last release in 2020. Maintenance might continue some day.

&nbsp;


## Java &nbsp; <sup><sub>[jv315kbd2menu.java](./../java/jv315kbd2menu.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jv315kbd2menu.png">
 <img src ="./runs/jv315kbd2menu.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv315kbd2menu.java">
 </a --->

&nbsp;


## C-Sharp &nbsp; <sup><sub>[cs315kbd2menu.cs](./../cs/cs315kbd2menu.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs315kbd2menu.png">
 <img src ="./runs/cs315kbd2menu.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs315kbd2menu.cs">
 </a --->

<img src="./imgs/20110902o1742.codeproject.v3.x0048y0048.png" width="48" height="48" alt="Icon for CodeProject">
 CodeProject article
 [Building a Menu-Driven Console Application in C#](https://www.codeproject.com/Articles/5382189/Building-a-Menu-Driven-Console-Application-in-Csha)
 might provide additional ideas
 <sup><sub><sup>*[ref 20240522°1121]*</sup></sub></sup>

&nbsp;


## JavaScript &nbsp; <sup><sub>[js315kbd2menu.js](./../js/js315kbd2menu.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js315kbd2menu.png">
 <img src ="./runs/js315kbd2menu.png" width="650" height="33" data-dims="x1084y0033" alt="Run js315kbd2menu.js">
 </a --->

&nbsp;


## PHP &nbsp; <sup><sub>[ph315kbd2menu.php](./../php/ph315kbd2menu.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph315kbd2menu.png">
 <img src ="./runs/ph315kbd2menu.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph315kbd2menu.php">
 </a --->

&nbsp;


## CPP &nbsp; <sup><sub>[cp315kbd2menu.cpp](./../cpp/cp315kbd2menu.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp315kbd2menu.png">
 <img src ="./runs/cp315kbd2menu.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp315kbd2menu.cpp">
 </a --->

&nbsp;


---

<sup><sub><sup>*[file 20221116°1501]* ⬞Ω</sup></sub></sup>
