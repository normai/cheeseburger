﻿# Demonstrate Sublists

'Sublists' here means something like a 'ranged index'.

## Python &nbsp; <sup><sub>[py163sublist.py](./../py/py163sublist.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

<a href="./runs/py163sublist.png">
 <img src ="./runs/py163sublist.png" width="637" height="226" data-dims="x0980y0348" alt="Run py163sublist.py">
 </a>

&nbsp;


## Java &nbsp; <sup><sub>[jv163sublist.java](./../java/jv163sublist.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jv163sublist.png">
 <img src ="./runs/jv163sublist.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv163sublist.java">
 </a --->

&nbsp;


## C-Sharp &nbsp; <sup><sub>[cs163sublist.cs](./../cs/cs163sublist.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs163sublist.png">
 <img src ="./runs/cs163sublist.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs163sublist.cs">
 </a --->

&nbsp;


## JavaScript &nbsp; <sup><sub>[js163sublist.js](./../js/js163sublist.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js163sublist.png">
 <img src ="./runs/js163sublist.png" width="650" height="33" data-dims="x1084y0033" alt="Run js163sublist.js">
 </a --->

&nbsp;


## PHP &nbsp; <sup><sub>[ph163sublist.php](./../php/ph163sublist.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph163sublist.png">
 <img src ="./runs/ph163sublist.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph163sublist.php">
 </a --->

&nbsp;


## CPP &nbsp; <sup><sub>[cp163sublist.cpp](./../cpp/cp163sublist.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp163sublist.png">
 <img src ="./runs/cp163sublist.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp163sublist.cpp">
 </a --->

&nbsp;


---

<sup><sub><sup>*[File 20220828°1801 x163sublist.md]* ⬞Ω</sup></sub></sup>
