﻿# Demonstrate Sound Functions

To elicit sound from your machine, you have to ask the hardware. Sound
 is not a native part of any programming language. But of course the
 operating system or third-party drivers provide the means to either
 produce synthetic sound with the sound card, or to play recorded sounds
 from a file or a network stream.


## Python &nbsp; <sup><sub>[py771sound.py](./../py/py771sound.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

<a href="./runs/py771sound.png">
 <img src ="./runs/py771sound.png" width="650" height="182" data-dims="x1084y0304" alt="Run py771sound.py">
 </a>

&nbsp;

<img src="./icos/20201225o1757.realpython.v1.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for RealPython.com">
 &nbsp; RealPython article
 [Playing and Recording Sound in Python](https://realpython.com/playing-and-recording-sound-python/)
 lists 6 Python modules to play sound. Of those, only one comes with the standard installation,
 the Windows specific `windsound` module.
 <!--- [ref 20221001°1225] --->

&nbsp;

<img src="./icos/20201212o1653.pythonbasics.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for PythonBasics.org">
 &nbsp; PythonBasics article
 [Play sound in Python](https://pythonbasics.org/python-play-sound/)
 as well mentions `tkSnack` and native sound using e.g. ~`os.system(filename)`
 <!--- [ref 20221001°1226] --->

&nbsp;

<img src="./icos/20221228o0933.mp3cutter.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for MP3Cutter">
 &nbsp; MP3Cutter at
 [www.mp3cutter.com](https://www.mp3cutter.com/)
 was used to cut the Mystical-Sting Jingel from 12 to 5 seconds
 <!--- [ref 20221228°0932] --->

&nbsp;

## Java &nbsp; <sup><sub>[jv771sound.java](./../java/jv771sound.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jv771sound.png">
 <img src ="./runs/jv771sound.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv771sound.java">
 </a --->

&nbsp;


## C-Sharp &nbsp; <sup><sub>[cs771sound.cs](./../cs/cs771sound.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs771sound.png">
 <img src ="./runs/cs771sound.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs771sound.cs">
 </a --->

&nbsp;


## JavaScript &nbsp; <sup><sub>[js771sound.js](./../js/js771sound.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js771sound.png">
 <img src ="./runs/js771sound.png" width="650" height="33" data-dims="x1084y0033" alt="Run js771sound.js">
 </a --->

&nbsp;


## PHP &nbsp; <sup><sub>[ph771sound.php](./../php/ph771sound.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph771sound.png">
 <img src ="./runs/ph771sound.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph771sound.php">
 </a --->

&nbsp;


## CPP &nbsp; <sup><sub>[cp771sound.cpp](./../cpp/cp771sound.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp771sound.png">
 <img src ="./runs/cp771sound.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp771sound.cpp">
 </a --->

&nbsp;


---

## Credits

<img src="./icos/20221001o1713.serpentsoundstudios.v1.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for SerpentSoundStudios">
 &nbsp; Thanks to SerpentSoundStudios page
 [Royalty Free Jingles and Stings](https://www.serpentsoundstudios.com/royalty-free-music/jingles)
 for providing the Mystical String jingle under the CC BY 4.0
 <!--- [ref 20221001°1712] --->

&nbsp;


---

<sup><sub><sup>*[File 20221001°1501 x771sound.md]* ⬞Ω</sup></sub></sup>
