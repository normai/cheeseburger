﻿# Demonstrate Simplest Window

## Python &nbsp; <sup><sub>[py810window.py](./../py/py810window.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

<!--- a href="./runs/py810window.png">
 <img src ="./runs/py810window.png" width="650" height="33" data-dims="x1084y0033" alt="Run py810window.py">
 </a --->

&nbsp;

### References

Tutorials on working with Tkinter:

<img src="./icos/20200805o1243.python.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Python documentation">
 &nbsp; Python documentation
 [tkinter — Python interface to Tcl/Tk](https://docs.python.org/3/library/tkinter.html)
 …
 <!--- ref 20221210°1012 --->

&nbsp;

<img src="./icos/20201225o1757.realpython.v1.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for RealPython">
 &nbsp; RealPython tutorial
 [Python GUI Programming With Tkinter](https://realpython.com/python-gui-tkinter/)
 …
 <!--- ref 20221210°1014 --->

&nbsp;

<img src="./icos/20221210o1023.likegeeks.v3.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for LikeGeeks">
 &nbsp; LikeGeeks article
 [Python GUI examples (Tkinter Tutorial)](https://likegeeks.com/python-gui-examples-tkinter-tutorial/)
 …
 <!--- ref 20221210°1022 --->

&nbsp;

<img src="./icos/20210729o1033.pythonguides.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for PythonGuides">
 &nbsp; PythonGuides tutorial
 [Python Tkinter Animation](https://pythonguides.com/python-tkinter-animation/)
 …
 <!--- ref 20221210°1032 --->

&nbsp;

<img src="./icos/20200630o0223.tutorialsteacher.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for TutorialsTeacher">
 &nbsp; TutorialsTeacher tutorial
 [Create UI in Python-Tkinter](https://www.tutorialsteacher.com/python/create-gui-using-tkinter-python)
 …
 <!--- ref 20221210°1034--->

&nbsp;

<img src="./icos/20221210o1043.pythoncommandments.v3.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Python-Commandments">
 &nbsp; Python-Commandments article
 [tkinter basics](https://python-commandments.org/tkinter-basics/)
 …
 <!--- ref 20221210°1044 --->

&nbsp;

<img src="./icos/20180615o0435.githubmark1.v0.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for GitHub">
 <img src="./icos/20221210o1023.likegeeks.v3.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for LikeGeeks">
 &nbsp; GitHub project
 [ly15927086342/Tkinter-simple-demo](https://github.com/ly15927086342/Tkinter-simple-demo)
 <!--- ref 20221213°1512 --->
 provides the codes from LikeGeeks article
 [Python GUI examples (Tkinter Tutorial)](https://likegeeks.com/python-gui-examples-tkinter-tutorial/)
 …
 <!--- ref 20221210°1022 ico 20221210°1023 --->

&nbsp;

<img src="./icos/20221213o1523.dunebook.v1.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for DuneBook">
 &nbsp; DuneBook article
 [Top 20 Opensource Python Tkinter Projects](https://www.dunebook.com/top-20-opensource-python-tkinter-projects/)
 …
 <!--- ref 20221213°1522 --->

&nbsp;

<img src="./icos/20221213o1533.wikipython.v1.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for WikiPython">
 &nbsp; WikiPython page
 [Examples of All Widgets](https://www.wikipython.com/tkinter-ttk-tix/gui-demos/examples-of-all-widge/)
 …
 <!--- ref 20221213°1532 --->

&nbsp;

<img src="./icos/20210729o1033.pythonguides.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for PythonGuides">
 &nbsp; PythonGuides article
 [Python Tkinter TreeView – How to Use](https://pythonguides.com/python-tkinter-treeview/)
 …
 <!--- ref 20221213°1542 --->

&nbsp;

<img src="./icos/20221201o1343.pythontutorial.v1.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for PythonTutorial">
 &nbsp; PythonTutorial articl
 [Tkinter Treeview](https://www.pythontutorial.net/tkinter/tkinter-treeview/)
 …
 <!--- ref 20221213°1544 --->

&nbsp;


## Java &nbsp; <sup><sub>[jv810window.java](./../java/jv810window.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jv810window.png">
 <img src ="./runs/jv810window.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv810window.java">
 </a --->

&nbsp;


## C-Sharp &nbsp; <sup><sub>[cs810window.cs](./../cs/cs810window.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs810window.png">
 <img src ="./runs/cs810window.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs810window.cs">
 </a --->

&nbsp;


## JavaScript &nbsp; <sup><sub>[js810window.js](./../js/js810window.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js810window.png">
 <img src ="./runs/js810window.png" width="650" height="33" data-dims="x1084y0033" alt="Run js810window.js">
 </a --->

&nbsp;


## PHP &nbsp; <sup><sub>[ph810window.php](./../php/ph810window.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph810window.png">
 <img src ="./runs/ph810window.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph810window.php">
 </a --->

&nbsp;


## CPP &nbsp; <sup><sub>[cp810window.cpp](./../cpp/cp810window.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp810window.png">
 <img src ="./runs/cp810window.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp810window.cpp">
 </a --->

&nbsp;


---

<sup><sub><sup>*[File 20220830°1901]* ⬞Ω</sup></sub></sup>
