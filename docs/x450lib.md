﻿# Demonstrate Library File (Python)

Subject: Library file but showing itself when called directly

## Python &nbsp; <sup><sub>[py450lib.py](./../py/py450lib.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

<!--- a href="./runs/py450lib.png">
 <img src ="./runs/py450lib.png" width="650" height="33" data-dims="x1084y0033" alt="Run py450lib.py">
 </a --->

&nbsp;


## Java &nbsp; <sup><sub>[jv450lib.java](./../java/jv450lib.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jv450lib.png">
 <img src ="./runs/jv450lib.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv450lib.java">
 </a --->

&nbsp;


## C-Sharp &nbsp; <sup><sub>[cs450lib.cs](./../cs/cs450lib.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs450lib.png">
 <img src ="./runs/cs450lib.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs450lib.cs">
 </a --->

&nbsp;


## JavaScript &nbsp; <sup><sub>[js450lib.js](./../js/js450lib.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js450lib.png">
 <img src ="./runs/js450lib.png" width="650" height="33" data-dims="x1084y0033" alt="Run js450lib.js">
 </a --->

&nbsp;


## PHP &nbsp; <sup><sub>[ph450lib.php](./../php/ph450lib.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph450lib.png">
 <img src ="./runs/ph450lib.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph450lib.php">
 </a --->

&nbsp;


## CPP &nbsp; <sup><sub>[cp450lib.cpp](./../cpp/cp450lib.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp450lib.png">
 <img src ="./runs/cp450lib.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp450lib.cpp">
 </a --->

&nbsp;


---

<sup><sub><sup>*[File 20220828°1501]* ⬞Ω</sup></sub></sup>
