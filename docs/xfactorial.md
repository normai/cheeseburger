﻿# Demonstrate Factorial Calculation

Factorial calculation is a popular subject to demonstrate recursive functions.

## Python &nbsp; <sup><sub>[pyfactorial.py](./../py/pyfactorial.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

<!--- a href="./runs/pyfactorial.png">
 <img src ="./runs/pyfactorial.png" width="650" height="33" data-dims="x1084y0033" alt="Run pyfactorial.py">
 </a --->

&nbsp;

<img src="./icos/20220318o1503.pythonwife.v2.x0048y0048.png" align="left" width="32" height="32" alt="Link icon for PythonWife">
 &nbsp; PythonWife article
 [Recursion in Python](https://pythonwife.com/recursion-in-python/)
 <!---[ref 20230706°1516] -->
 👍

&nbsp;

<img src="./icos/20170815o0232.youtube2.v0.x0048y0048.png" align="left" width="32" height="32" alt="Link icon for YouTube">
 &nbsp; YouTube video(s)
 [Recursion - part 4 | Pythonwife.com](https://www.youtube.com/watch?v=6uDisC9_exg)
 <!---[ref 20230706°1523] -->
 or
 [Python Wife](https://www.youtube.com/@pythonwife875/videos)
 <!---[ref 20230706°1524] -->
 resp.

&nbsp;


## Java &nbsp; <sup><sub>[jvfactorial.java](./../java/jvfactorial.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jvfactorial.png">
 <img src ="./runs/jvfactorial.png" width="650" height="33" data-dims="x1084y0033" alt="Run jvfactorial.java">
 </a --->


## C-Sharp &nbsp; <sup><sub>[csfactorial.cs](./../cs/csfactorial.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/csfactorial.png">
 <img src ="./runs/csfactorial.png" width="650" height="33" data-dims="x1084y0033" alt="Run csfactorial.cs">
 </a --->


## JavaScript &nbsp; <sup><sub>[jsfactorial.js](./../js/jsfactorial.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/jsfactorial.png">
 <img src ="./runs/jsfactorial.png" width="650" height="33" data-dims="x1084y0033" alt="Run jsfactorial.js">
 </a --->


## PHP &nbsp; <sup><sub>[phfactorial.php](./../php/phfactorial.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/phfactorial.png">
 <img src ="./runs/phfactorial.png" width="650" height="33" data-dims="x1084y0033" alt="Run phfactorial.php">
 </a --->


## CPP &nbsp; <sup><sub>[cpfactorial.cpp](./../cpp/cpfactorial.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cpfactorial.png">
 <img src ="./runs/cpfactorial.png" width="650" height="33" data-dims="x1084y0033" alt="Run cpfactorial.cpp">
 </a --->


## General

<img src="./icos/20110921o1125.wikipedia.v1.x0032y0032.png" align="left" style="margin-right:1.1em;" width="32" height="32" alt="Link icon for Wikipedia">
 &nbsp; Wikipedia article
 [Factorial](https://en.wikipedia.org/wiki/Factorial)
 <!--- [ref 20221215°1222] --->

&nbsp;

<img src="./icos/20110921o1125.wikipedia.v1.x0032y0032.png" align="left" style="margin-right:1.1em;" width="32" height="32" alt="Link icon for Wikipedia">
 &nbsp; Wikipedia article
 [Fakultät (Mathematik)](https://de.wikipedia.org/wiki/Fakult%C3%A4t_%28Mathematik%29)
 <!--- [ref 20221215°1224] --->

&nbsp;


---

<sup><sub>*[File 20221215°1101 xfactorial.md]* ⬞Ω</sub></sup>
