﻿# Demonstrate Big Integers

Most programming languages have a built-in limit on how larege integers
 may grow. To exceed this limit, typically affords some extra means.

## Python &nbsp; <sup><sub>[py139bigint.py](./../py/py139bigint.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

Python natively supports arbitrarily large integers, limited only by
 memory availability.

<!--- a href="./runs/py139bigint.png">
 <img src ="./runs/py139bigint.png" width="650" height="33" data-dims="x1084y0033" alt="Run py139bigint.py">
 </a --->

&nbsp;

<img src="./icos/20210924o1933.delftstack.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for DelftStack">
 &nbsp; DelftStack article
 [Bigint in Python](https://www.delftstack.com/howto/python/bigint-in-python/)
 <!--- [ref 20221217°1212] --->

&nbsp;

<img src="./icos/20200805o1243.python.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Python.org">
 &nbsp; Python documentation page
 [math — Mathematical functions](https://docs.python.org/3/library/math.html)
 <!--- [ref 20221217°1214] --->

&nbsp;

<img src="./icos/20110921o1121.stackoverflow.v1.x0032y0032.gif" align="left" width="32" height="32" alt="Link icon for StackOverflow">
 &nbsp; StackOverflow thread
 [Handling very large numbers in Python](https://stackoverflow.com/questions/538551/handling-very-large-numbers-in-python)
 <!--- [ref 20221217°1216] --->

&nbsp;


## Java &nbsp; <sup><sub>[jv139bigint.java](./../java/jv139bigint.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

Java comes with a BigInteger class, extended from Number, for immutable arbitrary-precision integers.

<!--- a href="./runs/jv139bigint.png">
 <img src ="./runs/jv139bigint.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv139bigint.java">
 </a --->

&nbsp;

<img src="./icos/20220320o1415.oracle.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Oracle">
 &nbsp; .
 [Class BigInteger](https://docs.oracle.com/javase/10/docs/api/java/math/BigInteger.html)
 <!--- [ref 20221117°0922] --->

&nbsp;

<img src="./icos/20220320o1415.oracle.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Oracle">
 &nbsp; Oracle documentation
 [Class BigDecimal](https://docs.oracle.com/javase/10/docs/api/java/math/BigDecimal.html)
 <!--- [ref 20221117°0923] --->

&nbsp;

<img src="./icos/20220320o1415.oracle.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for StackOverflow">
 &nbsp; Oracle documentation
 [BigDecimal math operations](https://stackoverflow.com/questions/42413020/bigdecimal-math-operations)
 <!--- [ref 20221117°0925] --->

&nbsp;

<img src="./icos/20210728o1415.tabnine.v1.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for TabNine">
 &nbsp; TabNine article
 [How to usejava.math.BigIntegerconstructor](https://www.tabnine.com/code/java/methods/java.math.BigInteger/%3Cinit%3E)
 <!--- [ref 20221217°1222] --->

&nbsp;

<img src="./icos/20210728o1415.tabnine.v1.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for TabNine">
 &nbsp; TabNine article
 [How to usejava.math.BigDecimalconstructor](https://www.tabnine.com/code/java/methods/java.math.BigDecimal/%3Cinit%3E)
 <!--- [ref 20221117°0932] --->

&nbsp;

<img src="./icos/20221117o0943.codingcompiler.v1.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for CodingCompiler">
 &nbsp; CodingCompiler article
 [BigInteger in Java](https://codingcompiler.com/biginteger-in-java/)
 <!--- [ref 20221217°1224] --->

&nbsp;

<img src="./icos/20221117o0943.codingcompiler.v1.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for CodingCompiler">
 &nbsp; CodingCompiler article
 [BigDecimal in Java](https://codingcompiler.com/bigdecimal-in-java/)
 <!--- [ref 20221117°0942] --->

&nbsp;

<img src="./icos/20221215o1013.concretepage.v1.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for ConcretePage">
 &nbsp; ConcretePage article
 [Java BigInteger Tutorial with Example](https://www.concretepage.com/java/java-biginteger-tutorial-with-example)
 <!--- [ref 20221215°1012] --->

&nbsp;

<img src="./icos/20220905o0913.baeldung.v1.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Baeldung">
 &nbsp; Baeldung tutorial
 [Guide to Java BigInteger](https://www.baeldung.com/java-biginteger)
 <!--- [ref 20221215°1034] --->

&nbsp;


## C-Sharp &nbsp; <sup><sub>[cs139bigint.cs](./../cs/cs139bigint.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs139bigint.png">
 <img src ="./runs/cs139bigint.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs139bigint.cs">
 </a --->

&nbsp;

<img src="./icos/20170811o0621.microsoftgray.v5.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Microsoft">
 &nbsp; Microsoft Learn article
 [BigInteger Struct](https://learn.microsoft.com/en-us/dotnet/api/system.numerics.biginteger?view=net-7.0)
 <!--- [ref 20221215°1032] --->
 (.NET 7.0) or
 [BigInteger Struct](https://learn.microsoft.com/en-us/dotnet/api/system.numerics.biginteger?view=netframework-4.8.1)
 (.NET Framework 4.8.1)
 , depending on the compiler.
 <!--- [ref 20221215°1033] --->

&nbsp;

<img src="./icos/20170811o0621.microsoftgray.v5.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Microsoft">
 &nbsp; Microsoft Learn article
 [BigInteger Struct](https://learn.microsoft.com/en-us/dotnet/api/system.numerics.biginteger)
 …
 <!--- [ref 20221016°1832] --->

&nbsp;

<img src="./icos/20200630o0223.tutorialsteacher.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for TutorialsTeacher">
 &nbsp; TutorialsTeacher article
 [BigInteger Data Type in C#](https://www.tutorialsteacher.com/articles/biginteger-type-in-csharp)
 …
 <!--- [ref 20221016°1833] --->

&nbsp;


## JavaScript &nbsp; <sup><sub>[js139bigint.js](./../js/js139bigint.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js139bigint.png">
 <img src ="./runs/js139bigint.png" width="650" height="33" data-dims="x1084y0033" alt="Run js139bigint.js">
 </a --->

&nbsp;

<img src="./icos/20180511o0333.mozilla1logo.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Mozilla">
 &nbsp; Mozilla JavaScript documentation
 [BigInt](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/BigInt)
 <!--- [ref 20221215°1036] --->

&nbsp;


## PHP &nbsp; <sup><sub>[ph139bigint.php](./../php/ph139bigint.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph139bigint.png">
 <img src ="./runs/ph139bigint.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph139bigint.php">
 </a --->

&nbsp;

<img src="./icos/20201130o1043.php-net.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for PHP">
 &nbsp; PHP documentation
 [Mathematical Extensions](https://www.php.net/manual/en/refs.math.php)
 lists 5 extensions for mathematical purposes, 2 of them are for big numbers.
 <!--- [ref 20221216°0942] --->

&nbsp;

<img src="./icos/20201130o1043.php-net.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for PHP">
 &nbsp; PHP documentation
 [BCMath Arbitrary Precision Mathematics](https://www.php.net/manual/en/book.bc.php)
 is the extensions which is immediately available.
 As opposed to GMP, it has only 10 basic functions on board.
 <!--- [ref 20221216°0944] --->

&nbsp;

<img src="./icos/20201130o1043.php-net.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for PHP">
 &nbsp; PHP documentation
 [GNU Multiple Precision](https://www.php.net/manual/en/book.gmp.php)
 is the powerful big numbers tool, coming with 52 functions. Just this is
 not so easily available, it needs a specially compiled PHP version.
 <!--- [ref 20221216°0952] --->

&nbsp;


## CPP &nbsp; <sup><sub>[cp139bigint.cpp](./../cpp/cp139bigint.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp139bigint.png">
 <img src ="./runs/cp139bigint.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp139bigint.cpp">
 </a --->

&nbsp;

<img src="./icos/20110921o1121.stackoverflow.v1.x0032y0032.gif" align="left" width="32" height="32" alt="Link icon for StackOverflow">
 &nbsp; StackOverflow thread
 [BigInteger in C?](https://stackoverflow.com/questions/565150/biginteger-in-c)
 lists various libraries for handling big numbers in C.
 <!--- [ref 20221215°1038] --->

&nbsp;

<img src="./icos/20110921o1121.stackoverflow.v1.x0032y0032.gif" align="left" width="32" height="32" alt="Link icon for StackOverflow">
 &nbsp; StackOverflow thread
 [C++ Big Integer](https://stackoverflow.com/questions/4507121/c-big-integer)
 …
 <!--- [ref 20221017°1512] --->

&nbsp;

<img src="./icos/20110921o1121.stackoverflow.v1.x0032y0032.gif" align="left" width="32" height="32" alt="Link icon for StackOverflow">
 &nbsp; StackOverflow thread
 [How to implement big int in C++](https://stackoverflow.com/questions/269268/how-to-implement-big-int-in-c)
 …
 <!--- [ref 20221017°1513] --->

&nbsp;

<img src="./icos/20110921o1121.stackoverflow.v1.x0032y0032.gif" align="left" width="32" height="32" alt="Link icon for StackOverflow">
 &nbsp; StackOverflow thread
 [C++ handling very large integers](https://stackoverflow.com/questions/124332/c-handling-very-large-integers)
 …
 <!--- [ref 20221017°1514] --->

&nbsp;

<img src="./icos/20210510o1133.gmplib-org.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for GMPLib">
 &nbsp; Project
 [**GMP**](https://gmplib.org/)
 — The GNU Multiple Precision Arithmetic Library
 …
 <!--- [ref 20221017°1522] --->

&nbsp;

<img src="./icos/20221018o1215.mpfr.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for MPFR">
 &nbsp; Project
 [MPFR](https://www.mpfr.org/)
 — the GNU MPFR Library
 …
 <!--- [ref 20221018°1214] --->

&nbsp;


## General

<img src="./icos/20221215o1023.cpalgorithms.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for CP-Algorithms">
 &nbsp; CP-Algorithms article
 [Arbitrary-Precision Arithmetic](https://cp-algorithms.com/algebra/big-integer.html)
 <!--- [ref 20221215°1022] --->

&nbsp;

<img src="./icos/20211222o1813.wumbo.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Wumbo">
 &nbsp; Wumbo article
 [Math Operators](https://wumbo.net/operators/)
 <!--- [ref 20221215°1212] --->

&nbsp;

<img src="./icos/20110921o1125.wikipedia.v1.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Wikipedia">
 &nbsp; Wikipedia article
 [Age of the universe](https://en.wikipedia.org/wiki/Age_of_the_universe)
 …
 <!--- [ref 20221016°1852] --->

&nbsp;

<img src="./icos/20221016o1843.universetoday.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for UniverseToday">
 &nbsp; Universe Today article
 [How Many Atoms Are There in the Universe?](https://www.universetoday.com/36302/atoms-in-the-universe/)
 …
 <!--- [ref 20221016°1842] --->

&nbsp;


---

<sup><sub><sup>*[File 20221117°1001]* ⬞Ω</sup></sub></sup>
