﻿# Demonstrate Aggregation

## Python &nbsp; <sup><sub>[py540aggregate.py](./../py/py540aggregate.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

<!--- a href="./runs/py540aggregate.png">
 <img src ="./runs/py540aggregate.png" width="650" height="33" data-dims="x1084y0033" alt="Run py540aggregate.py">
 </a --->

&nbsp;


## Java &nbsp; <sup><sub>[jv540aggregate.java](./../java/jv540aggregate.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jv540aggregate.png">
 <img src ="./runs/jv540aggregate.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv540aggregate.java">
 </a --->

&nbsp;


## C-Sharp &nbsp; <sup><sub>[cs540aggregate.cs](./../cs/cs540aggregate.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs540aggregate.png">
 <img src ="./runs/cs540aggregate.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs540aggregate.cs">
 </a --->

&nbsp;


## JavaScript &nbsp; <sup><sub>[js540aggregate.js](./../js/js540aggregate.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js540aggregate.png">
 <img src ="./runs/js540aggregate.png" width="650" height="33" data-dims="x1084y0033" alt="Run js540aggregate.js">
 </a --->

&nbsp;


## PHP &nbsp; <sup><sub>[ph540aggregate.php](./../php/ph540aggregate.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph540aggregate.png">
 <img src ="./runs/ph540aggregate.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph540aggregate.php">
 </a --->

&nbsp;


## CPP &nbsp; <sup><sub>[cp540aggregate.cpp](./../cpp/cp540aggregate.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp540aggregate.png">
 <img src ="./runs/cp540aggregate.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp540aggregate.cpp">
 </a --->

&nbsp;


---

<sup><sub><sup>*[File 20220830°1201]* ⬞Ω</sup></sub></sup>
