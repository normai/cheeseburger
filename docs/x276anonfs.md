﻿# Demonstrate Anonymous Functions

## Python &nbsp; <sup><sub>[py276anonfs.py](./../py/py276anonfs.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

<!--- a href="./runs/py276anonfs.png">
 <img src ="./runs/py276anonfs.png" width="650" height="33" data-dims="x1084y0033" alt="Run py276anonfs.py">
 </a --->

&nbsp;


## Java &nbsp; <sup><sub>[jv276anonfs.java](./../java/jv276anonfs.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jv276anonfs.png">
 <img src ="./runs/jv276anonfs.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv276anonfs.java">
 </a --->

&nbsp;


## C-Sharp &nbsp; <sup><sub>[cs276anonfs.cs](./../cs/cs276anonfs.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs276anonfs.png">
 <img src ="./runs/cs276anonfs.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs276anonfs.cs">
 </a --->

&nbsp;


## JavaScript &nbsp; <sup><sub>[js276anonfs.js](./../js/js276anonfs.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js276anonfs.png">
 <img src ="./runs/js276anonfs.png" width="650" height="33" data-dims="x1084y0033" alt="Run js276anonfs.js">
 </a --->

&nbsp;


## PHP &nbsp; <sup><sub>[ph276anonfs.php](./../php/ph276anonfs.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph276anonfs.png">
 <img src ="./runs/ph276anonfs.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph276anonfs.php">
 </a --->

&nbsp;


## CPP &nbsp; <sup><sub>[cp276anonfs.cpp](./../cpp/cp276anonfs.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp276anonfs.png">
 <img src ="./runs/cp276anonfs.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp276anonfs.cpp">
 </a --->

&nbsp;


---

<sup><sub><sup>*[File 221213°1601]* ⬞Ω</sup></sub></sup>
