﻿# Demonstrate Lambda Expressions

Lambda expressions are an old concept for functional programming. They date
 back to Lisp in the 1960s, then got out of fashion. Now in the 21st century,
 due to multithreading and multiprocessors, lambdas came back. Programming
 languages which originally did not support lambdas, hurry to retrofit them.

## Python &nbsp; <sup><sub>[py278lambda.py](./../py/py278lambda.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

<a href="./runs/py278lambda.png">
 <img src ="./runs/py278lambda.png" width="650" height="184" data-dims="x1084y0308" alt="Run py278lambda.py">
 </a>

&nbsp;


## Java &nbsp; <sup><sub>[jv278lambda.java](./../java/jv278lambda.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jv278lambda.png">
 <img src ="./runs/jv278lambda.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv278lambda.java">
 </a --->

&nbsp;


## C-Sharp &nbsp; <sup><sub>[cs278lambda.cs](./../cs/cs278lambda.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs278lambda.png">
 <img src ="./runs/cs278lambda.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs278lambda.cs">
 </a --->

&nbsp;


## JavaScript &nbsp; <sup><sub>[js278lambda.js](./../js/js278lambda.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js278lambda.png">
 <img src ="./runs/js278lambda.png" width="650" height="33" data-dims="x1084y0033" alt="Run js278lambda.js">
 </a --->

&nbsp;


## PHP &nbsp; <sup><sub>[ph278lambda.php](./../php/ph278lambda.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph278lambda.png">
 <img src ="./runs/ph278lambda.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph278lambda.php">
 </a --->

&nbsp;


## CPP &nbsp; <sup><sub>[cp278lambda.cpp](./../cpp/cp278lambda.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp278lambda.png">
 <img src ="./runs/cp278lambda.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp278lambda.cpp">
 </a --->

&nbsp;


---

<sup><sub><sup>*[File 20221213°1701 x278lambda.md]* ⬞Ω</sup></sub></sup>
