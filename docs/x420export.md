﻿# Demonstrate Exporting Functions

If one file can import code from another file, that other file may have
 to satisfy some conditions, so it's code can be exported, means can be
 used from other files.

## Python &nbsp; <sup><sub>[py420export.py](./../py/py420export.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

<a href="./runs/py420export.png">
 <img src ="./runs/py420export.png" width="650" height="140 data-dims="x1084y0234" alt="Run py420export.py">
 </a>

&nbsp;


## Java &nbsp; <sup><sub>[jv420export.java](./../java/jv420export.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jv420export.png">
 <img src ="./runs/jv420export.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv420export.java">
 </a --->

&nbsp;


## C-Sharp &nbsp; <sup><sub>[cs420export.cs](./../cs/cs420export.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs420export.png">
 <img src ="./runs/cs420export.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs420export.cs">
 </a --->

&nbsp;


## JavaScript &nbsp; <sup><sub>[js420export.js](./../js/js420export.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js420export.png">
 <img src ="./runs/js420export.png" width="650" height="33" data-dims="x1084y0033" alt="Run js420export.js">
 </a --->

&nbsp;


## PHP &nbsp; <sup><sub>[ph420export.php](./../php/ph420export.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph420export.png">
 <img src ="./runs/ph420export.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph420export.php">
 </a --->

&nbsp;


## CPP &nbsp; <sup><sub>[cp420export.cpp](./../cpp/cp420export.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp420export.png">
 <img src ="./runs/cp420export.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp420export.cpp">
 </a --->

&nbsp;


---

<sup><sub><sup>*[File 20220828°1201 x420export.md]* ⬞Ω</sup></sub></sup>
