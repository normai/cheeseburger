﻿### Changelog

version **v0.2.0** *20240708°1515* — Little cleanup
- Reset ignore props
- Add \*.exe and \*.class files
- Little cleanup

chg 20221217°1111 — Try different frontpage layout
- Have new table for each category

chg 20221217°1012 — Bulk rename
- E.g. `cp131operas.cpp` to `cp121operas.cpp`, altogether 181 files

chg 20220922°1921 — Adjust IDs
- Streamline topics ID scheme
- Eliminate files x211ifs2 ids 20220826°13xx
- Establish ph211ifs1b.php (then renamed to ph815input2) id 20220826°1233

**v0.1.7** *v20220914°1217* — Intermediate
- Streamline file formatting
- Renumber demos, e.g. shift chapter 'Types' from 5 to 1.2 to make room for new chapter '5. Databases' <sup><sub><sup>*[chg 20220831°1311/°1411]*</sup></sub></sup>
- Chapter structure is pretty complete

**v0.1.5** *v20220724°1911* — Streamline files table

**v0.1.4** — Put project into Git repo &nbsp;
- Mirror/relocate project to `https://gitlab.com/normai/cheeseburger`
 <sup><sub><sup>*[repo 20220723°1411, version 20220723°1421]*</sup></sub></sup>

**v0.1.3** *v20220214°1931* — Tag/move
 - Tag `20220214o1931.pyahoj.v013`
 - Move from <del>`www.trekta.biz/svn/demospy/trunk/ahoj/`</del> &nbsp;<sup><sub><sup>*[repo 20141211°1211]*</sup></sub></sup>
 - to <del>`www.trekta.biz/svn/cheeseburgerdev/trunk/cheeseburger/ahoj/`</del> &nbsp;<sup><sub><sup>*[repo 20220214°1511]*</sup></sub></sup>

**v0.1.2** *v20211103°1132* — Little fixes

**v0.1.1** *v20211103°1112* — Initial Cheeseburger version
 - Tag `20211103o1112.ahoj.v011`

log 20210929°1811 — Start project as 'PyAhoj' &nbsp;<sup><sub><sup>*[folder 20210929°1821]*</sup></sub></sup>

<sup><sub><sup>*[File 20211103°1124]* ⬞Ω</sup></sub></sup>
