﻿# Demonstrate List Classes

Most programming languages offer List classes.

Lists may be handled quite like arrays on code level. The difference lies
 in the undergound: (1) An **array** is one continuous area in the memory,
 divided into equally-sized elements. (2) A **list** is an object where the
 single elements are located anywhere in the memory.

This difference provides for different runtime behavior of the two types.
 (1) The **array** is very fast with finding an indexed element. But it is
 extremely bad in changing it's size or inserting elements into the middle.
 (2) The **list** has no problem with changing it's size or inserting elements
 into the middle. But it is not so fast in finding one indexed member.

## Python <sup><sub>[py161list.py](./../py/py161list.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

<a href="./runs/py161list.png">
 <img src ="./runs/py161list.png" width="637" height="406" data-dims="x0980y0626" alt="Run py161list.py">
 </a>

&nbsp;

<img src="./icos/20200630o0143.w3schools.v2.x0048y0048.png" align="left" width="32" height="32" alt="Link icon for W3Schools">
 &nbsp; W3Schools article
 [Python Lists](https://www.w3schools.com/python/python_lists.asp)
 <!--- [ref 20221217°1256] --->

&nbsp;

<img src="./icos/20210924o1933.delftstack.v2.x0048y0048.png" align="left" width="48" height="48" alt="Link icon for DelftStack">
 DelftStack article
 [Create a List With a Specific Size in Python](https://www.delftstack.com/howto/python/how-to-create-a-list-with-a-specific-size-in-python/)
 <!--- [ref 20221214°1522] --->

&nbsp;

<img src="./icos/20221214o1533.moonbooks.v1.x0048y0048.png" align="left" width="48" height="48" alt="Link icon for Moonbook">
 Moonbooks article
 [How to create a list of numbers in python?](https://www.moonbooks.org/Articles/How-to-create-a-list-of-numbers-in-python-/)
 <!--- [ref 20221214°1532] --->

&nbsp;


## Java &nbsp; <sup><sub>[jv161list.java](./../java/jv161list.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jv161list.png">
 <img src ="./runs/jv161list.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv161list.java">
 </a --->

&nbsp;


## C-Sharp &nbsp; <sup><sub>[cs161list.cs](./../cs/cs161list.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs161list.png">
 <img src ="./runs/cs161list.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs161list.cs">
 </a --->


## JavaScript &nbsp; <sup><sub>[js161list.js](./../js/js161list.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js161list.png">
 <img src ="./runs/js161list.png" width="650" height="33" data-dims="x1084y0033" alt="Run js161list.js">
 </a --->

&nbsp;


## PHP &nbsp; <sup><sub>[ph161list.php](./../php/ph161list.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph161list.png">
 <img src ="./runs/ph161list.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph161list.php">
 </a --->

&nbsp;


## CPP &nbsp; <sup><sub>[cp161list.cpp](./../cpp/cp161list.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp161list.png">
 <img src ="./runs/cp161list.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp161list.cpp">
 </a --->

&nbsp;


---

<sup><sub><sup>*[File 20220828°1701 x161list.md]* ⬞Ω</sup></sub></sup>
