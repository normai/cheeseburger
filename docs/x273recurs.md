﻿# Demonstrate Recursion

Recursion occurs, when a function calls itself.

Recursion is a kind of looping. Any classic looping task can also be
 solved with recursion. Which of the two is more practical, depends on the
 nature of the task. Classic looping is easier to understand, recursion
 is sometimes more elegant with less code (but more memory consumption).

Proposal. One possible demo task were the Fibonacci sequence (see
 [Wikipedia links](#ref_20221230o1712) below and for code CppReference
 [link in x159date.md](./x159date.md#ref_20221229o1812)

## Python &nbsp; <sup><sub>[py273recurs.py](./../py/py273recurs.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

<!--- a href="./runs/py273recurs.png">
 <img src ="./runs/py273recurs.png" width="650" height="33" data-dims="x1084y0033" alt="Run py273recurs.py">
 </a --->

&nbsp;


## Java &nbsp; <sup><sub>[jv273recurs.java](./../java/jv273recurs.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jv273recurs.png">
 <img src ="./runs/jv273recurs.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv273recurs.java">
 </a --->

&nbsp;


## C-Sharp &nbsp; <sup><sub>[cs273recurs.cs](./../cs/cs273recurs.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs273recurs.png">
 <img src ="./runs/cs273recurs.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs273recurs.cs">
 </a --->

&nbsp;


## JavaScript &nbsp; <sup><sub>[js273recurs.js](./../js/js273recurs.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js273recurs.png">
 <img src ="./runs/js273recurs.png" width="650" height="33" data-dims="x1084y0033" alt="Run js273recurs.js">
 </a --->

&nbsp;


## PHP &nbsp; <sup><sub>[ph273recurs.php](./../php/ph273recurs.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph273recurs.png">
 <img src ="./runs/ph273recurs.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph273recurs.php">
 </a --->

&nbsp;


## CPP &nbsp; <sup><sub>[cp273recurs.cpp](./../cpp/cp273recurs.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp273recurs.png">
 <img src ="./runs/cp273recurs.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp273recurs.cpp">
 </a --->

&nbsp;


## General

<a name="ref_20221230o1712"></a>
<img src="./icos/20110921o1125.wikipedia.v1.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Wikipedia">
 &nbsp; Wikipedia articles
 [Fibonacci number](https://en.wikipedia.org/wiki/Fibonacci_number)
 (english) and
 <!--- [ref 20221230°1712] --->
 [Fibonacci-Folge](https://de.wikipedia.org/wiki/Fibonacci-Folge)
 (german) …
 <!--- [ref 20221230°1713] --->

&nbsp;

<img src="./icos/20110921o1125.wikipedia.v1.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Wikipedia">
 &nbsp; Wikipedia articles
 [Fibonacci prime](https://en.wikipedia.org/wiki/Fibonacci_prime)
 (english) and
 <!--- [ref 20221230°1714] --->
 [Fibonacci-Primzahl](https://de.wikipedia.org/wiki/Fibonacci-Primzahl)
 (german) …
 <!--- [ref 20221230°1715] --->

&nbsp;

<img src="./icos/20110921o1125.wikipedia.v1.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Wikipedia">
 &nbsp; Wikipedia articles
 [Fibonacci heap](https://en.wikipedia.org/wiki/Fibonacci_heap)
 (english) and
 <!--- [ref 20221230°1716] --->
 [Fibonacci-Heap](https://de.wikipedia.org/wiki/Fibonacci-Heap)
 (german) …
 <!--- [ref 20221230°1717] --->

&nbsp;


---

<sup><sub>*[File 20221222°1801 x273recurs.md]* ⬞Ω</sub></sup>
