﻿# Demonstrate Float Type

Floating point numbers seem simple at first sight, but tricky if inspected
 deeper. As opposed to integers, they have the following properties:

- They span from minus to plus infinity

- Their memory size is not related to number size but number precision

- They show rounding errors

## Python [<sup><sub>*py145float.py*</sub></sup>](./../py/py145float.py) &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

<a href="./runs/py145float.png">
 <img src ="./runs/py145float.png" width="651" height="211" data-dims="x1085y0353*.65" alt="Run py145float.py">
 </a>

&nbsp;

<img src="./icos/20221215o0923.itcodar.v2.x0048y0048.png" align="left" style="margin-right:1.1em;" width="48" height="48" alt="Link icon for ITCodar">
 &nbsp; ITCodar tutorial
 [Python Rounding Error With Float Numbers](https://www.itcodar.com/python/python-rounding-error-with-float-numbers.html)
 <!--- [ref 20221215°0922] --->

&nbsp;

<img src="./icos/20220320o1415.oracle.v2.x0048y0048.png" align="left" style="margin-right:1.1em;" width="48" height="48" alt="Link icon for Oracle">
 &nbsp; Oracle article
 [What Every Computer Scientist Should Know About Floating-Point Arithmetic](https://docs.oracle.com/cd/E19957-01/806-3568/ncg_goldberg.html)
 describes the circumstances very detailled
 <!--- [ref 20221215°0922] --->

&nbsp;


## Java &nbsp; <sup><sub>[jv145float.java](./../java/jv145float.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jv145float.png">
 <img src ="./runs/jv145float.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv145float.java">
 </a --->


## C-Sharp &nbsp; <sup><sub>[cs145float.cs](./../cs/cs145float.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs145float.png">
 <img src ="./runs/cs145float.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs145float.cs">
 </a --->


## JavaScript &nbsp; <sup><sub>[js145float.js](./../js/js145float.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js145float.png">
 <img src ="./runs/js145float.png" width="650" height="33" data-dims="x1084y0033" alt="Run js145float.js">
 </a --->

<img src="./icos/20200630o0143.w3schools.v2.x0048y0048.png" align="left" style="margin-right:1.1em;" width="48" height="48" alt="Link icon for W3Schools.com">
 &nbsp; W3Schools.com article
 [JavaScript Numbers](https://www.w3schools.com/js/js_numbers.asp)
 …
 <!--- [ref 20221216°1012] --->

&nbsp;


## PHP &nbsp; <sup><sub>[ph145float.php](./../php/ph145float.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph145float.png">
 <img src ="./runs/ph145float.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph145float.php">
 </a --->


## CPP &nbsp; <sup><sub>[cp145float.cpp](./../cpp/cp145float.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp145float.png">
 <img src ="./runs/cp145float.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp145float.cpp">
 </a --->

<img src="./icos/20221018o1215.mpfr.v2.x0048y0048.png" align="left" style="margin-right:1.1em;" width="48" height="48" alt="Link icon for MPFR">
 &nbsp; MPFR frontpage
 [www.mpfr.org](https://www.mpfr.org/)
 …
 <!--- [ref 20221018°1214] --->


---

<sup><sub><sup>*[File 20221012°1101]* ⬞Ω</sup></sub></sup>
