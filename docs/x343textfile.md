﻿# Demonstrate Create/Write/Read Textfile

Of course file processing is a strong requirement when dealing with data.
 Operations on the file system may be operating dependend.

## Python &nbsp; <sup><sub>[py343textfile.py](./../py/py343textfile.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

<a href="./runs/py343textfile.png">
 <img src ="./runs/py343textfile.png" width="650" height="352" data-dims="x1084y0587" alt="Run py343textfile.py">
 </a>

&nbsp;

<img src="./icos/20200805o1243.python.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Python.org">
 &nbsp; About <b>file handling</b> see e.g.
 [docs.python.org/3/library/functions.html#open](https://docs.python.org/3/library/functions.html#open)
 <!--- [ref 20221216°1242] --->

&nbsp;

<img src="./icos/20200805o1243.python.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for PythonSpot">
 &nbsp; PythonSpot tutorial
 [How to Read a File in Python](https://pythonspot.com/read-file/)
 …
 <!--- [ref 20220913°1022 ico 20220913°1023] --->

&nbsp;

<img src="./icos/20220915o0933.pynative.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for PyNative">
 &nbsp; PyNative tutorial
 [Read Specific Lines From a File in Python](https://pynative.com/python-read-specific-lines-from-a-file/)
 …
 <!--- [ref 20220913°1032 ico 20220913°1033] --->

&nbsp;


## Java &nbsp; <sup><sub>[jv343textfile.java](./../java/jv343textfile.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jv343textfile.png">
 <img src ="./runs/jv343textfile.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv343textfile.java">
 </a --->

&nbsp;


## C-Sharp &nbsp; <sup><sub>[cs343textfile.cs](./../cs/cs343textfile.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs343textfile.png">
 <img src ="./runs/cs343textfile.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs343textfile.cs">
 </a --->

&nbsp;


## JavaScript &nbsp; <sup><sub>[js343textfile.js](./../js/js343textfile.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js343textfile.png">
 <img src ="./runs/js343textfile.png" width="650" height="33" data-dims="x1084y0033" alt="Run js343textfile.js">
 </a --->

&nbsp;


## PHP &nbsp; <sup><sub>[ph343textfile.php](./../php/ph343textfile.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph343textfile.png">
 <img src ="./runs/ph343textfile.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph343textfile.php">
 </a --->

&nbsp;


## CPP &nbsp; <sup><sub>[cp343textfile.cpp](./../cpp/cp343textfile.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp343textfile.png">
 <img src ="./runs/cp343textfile.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp343textfile.cpp">
 </a --->

&nbsp;


---

<sup><sub><sup>*[File 20220829°1101 x343textfile.md]* ⬞Ω</sup></sub></sup>
