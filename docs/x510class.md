﻿# Demonstrate Classes

Subject: Class, simple

## Python &nbsp; <sup><sub>[py510class.py](./../py/py510class.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

<!--- a href="./runs/py510class.png">
 <img src ="./runs/py510class.png" width="650" height="33" data-dims="x1084y0033" alt="Run py510class.py">
 </a --->

&nbsp;


## Java &nbsp; <sup><sub>[jv510class.java](./../java/jv510class.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jv510class.png">
 <img src ="./runs/jv510class.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv510class.java">
 </a --->

&nbsp;


## C-Sharp &nbsp; <sup><sub>[cs510class.cs](./../cs/cs510class.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs510class.png">
 <img src ="./runs/cs510class.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs510class.cs">
 </a --->

&nbsp;


## JavaScript &nbsp; <sup><sub>[js510class.js](./../js/js510class.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js510class.png">
 <img src ="./runs/js510class.png" width="650" height="33" data-dims="x1084y0033" alt="Run js510class.js">
 </a --->

&nbsp;


## PHP &nbsp; <sup><sub>[ph510class.php](./../php/ph510class.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph510class.png">
 <img src ="./runs/ph510class.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph510class.php">
 </a --->

&nbsp;


## CPP &nbsp; <sup><sub>[cp510class.cpp](./../cpp/cp510class.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp510class.png">
 <img src ="./runs/cp510class.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp510class.cpp">
 </a --->

&nbsp;


---

<sup><sub><sup>*[File 20220829°1401]* ⬞Ω</sup></sub></sup>
