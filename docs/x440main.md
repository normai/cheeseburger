﻿# Demonstrate the `__main__` Variable (Python)

Subject: Show the `__main__` variable (Python)

## Python &nbsp; <sup><sub>[py440main.py](./../py/py440main.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

<!--- a href="./runs/py440main.png">
 <img src ="./runs/py440main.png" width="650" height="33" data-dims="x1084y0033" alt="Run py440main.py">
 </a --->

&nbsp;


## Java &nbsp; <sup><sub>[jv440main.java](./../java/jv440main.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jv440main.png">
 <img src ="./runs/jv440main.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv440main.java">
 </a --->

&nbsp;


## C-Sharp &nbsp; <sup><sub>[cs440main.cs](./../cs/cs440main.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs440main.png">
 <img src ="./runs/cs440main.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs440main.cs">
 </a --->

&nbsp;


## JavaScript &nbsp; <sup><sub>[js440main.js](./../js/js440main.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js440main.png">
 <img src ="./runs/js440main.png" width="650" height="33" data-dims="x1084y0033" alt="Run js440main.js">
 </a --->

&nbsp;


## PHP &nbsp; <sup><sub>[ph440main.php](./../php/ph440main.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph440main.png">
 <img src ="./runs/ph440main.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph440main.php">
 </a --->

&nbsp;


## CPP &nbsp; <sup><sub>[cp440main.cpp](./../cpp/cp440main.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp440main.png">
 <img src ="./runs/cp440main.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp440main.cpp">
 </a --->

&nbsp;


---

<sup><sub><sup>*[File 20220828°1401]* ⬞Ω</sup></sub></sup>
