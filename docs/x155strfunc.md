﻿# Demonstrate String Functions

String processing is a bread and butter task everywhere. So the programming
 languages offer a rich selection of functions to manipulate them.

## Python &nbsp; <sup><sub>[py155strfunc.py](./../py/py155strfunc.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

<a href="./runs/py155strfunc.png">
 <img src ="./runs/py155strfunc.png" width="637" height="213" data-dims="x0980y0329" alt="Run py155strfunc.py">
 </a>

&nbsp;


## Java &nbsp; <sup><sub>[jv155strfunc.java](./../java/jv155strfunc.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jv155strfunc.png">
 <img src ="./runs/jv155strfunc.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv155strfunc.java">
 </a --->

&nbsp;


## C-Sharp &nbsp; <sup><sub>[cs155strfunc.cs](./../cs/cs155strfunc.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs155strfunc.png">
 <img src ="./runs/cs155strfunc.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs155strfunc.cs">
 </a --->

&nbsp;


## JavaScript &nbsp; <sup><sub>[js155strfunc.js](./../js/js155strfunc.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js155strfunc.png">
 <img src ="./runs/js155strfunc.png" width="650" height="33" data-dims="x1084y0033" alt="Run js155strfunc.js">
 </a --->

&nbsp;


## PHP &nbsp; <sup><sub>[ph155strfunc.php](./../php/ph155strfunc.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph155strfunc.png">
 <img src ="./runs/ph155strfunc.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph155strfunc.php">
 </a --->

&nbsp;


## CPP &nbsp; <sup><sub>[cp155strfunc.cpp](./../cpp/cp155strfunc.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp155strfunc.png">
 <img src ="./runs/cp155strfunc.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp155strfunc.cpp">
 </a --->

&nbsp;


---

<sup><sub><sup>*[File 20221012°1701 x155strfunc.md]* ⬞Ω</sup></sub></sup>
