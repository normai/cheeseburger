﻿# Demonstrate Regular Expressions (RegEx)

## Python &nbsp; <sup><sub>[py726regex.py](./../py/py726regex.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

<!--- a href="./runs/py726regex.png">
 <img src ="./runs/py726regex.png" width="650" height="33" data-dims="x1084y0033" alt="Run py726regex.py">
 </a --->

&nbsp;


## Java &nbsp; <sup><sub>[jv726regex.java](./../java/jv726regex.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jv726regex.png">
 <img src ="./runs/jv726regex.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv726regex.java">
 </a --->

&nbsp;


## C-Sharp &nbsp; <sup><sub>[cs726regex.cs](./../cs/cs726regex.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs726regex.png">
 <img src ="./runs/cs726regex.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs726regex.cs">
 </a --->

&nbsp;


## JavaScript &nbsp; <sup><sub>[js726regex.js](./../js/js726regex.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js726regex.png">
 <img src ="./runs/js726regex.png" width="650" height="33" data-dims="x1084y0033" alt="Run js726regex.js">
 </a --->

&nbsp;


## PHP &nbsp; <sup><sub>[ph726regex.php](./../php/ph726regex.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph726regex.png">
 <img src ="./runs/ph726regex.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph726regex.php">
 </a --->

&nbsp;


## CPP &nbsp; <sup><sub>[cp726regex.cpp](./../cpp/cp726regex.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp726regex.png">
 <img src ="./runs/cp726regex.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp726regex.cpp">
 </a --->

&nbsp;


---

<sup><sub><sup>*[File 20221009°1101]* ⬞Ω</sup></sub></sup>
