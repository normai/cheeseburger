﻿# Demonstrate Polymorphism

## Python &nbsp; <sup><sub>[py550polymo.py](./../py/py550polymo.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

<!--- a href="./runs/py550polymo.png">
 <img src ="./runs/py550polymo.png" width="650" height="33" data-dims="x1084y0033" alt="Run py550polymo.py">
 </a --->

&nbsp;


## Java &nbsp; <sup><sub>[jv550polymo.java](./../java/jv550polymo.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jv550polymo.png">
 <img src ="./runs/jv550polymo.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv550polymo.java">
 </a --->

&nbsp;


## C-Sharp &nbsp; <sup><sub>[cs550polymo.cs](./../cs/cs550polymo.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs550polymo.png">
 <img src ="./runs/cs550polymo.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs550polymo.cs">
 </a --->

&nbsp;


## JavaScript &nbsp; <sup><sub>[js550polymo.js](./../js/js550polymo.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js550polymo.png">
 <img src ="./runs/js550polymo.png" width="650" height="33" data-dims="x1084y0033" alt="Run js550polymo.js">
 </a --->

&nbsp;


## PHP &nbsp; <sup><sub>[ph550polymo.php](./../php/ph550polymo.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph550polymo.png">
 <img src ="./runs/ph550polymo.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph550polymo.php">
 </a --->

&nbsp;


## CPP &nbsp; <sup><sub>[cp550polymo.cpp](./../cpp/cp550polymo.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp550polymo.png">
 <img src ="./runs/cp550polymo.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp550polymo.cpp">
 </a --->

&nbsp;


---

<sup><sub><sup>*[File 20220830°1301]* ⬞Ω</sup></sub></sup>
