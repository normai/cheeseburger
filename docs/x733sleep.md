﻿# Demonstrate Sleep Function

## Python &nbsp; <sup><sub>[py733sleep.py](./../py/py733sleep.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

<!--- a href="./runs/py733sleep.png">
 <img src ="./runs/py733sleep.png" width="650" height="33" data-dims="x1084y0033" alt="Run py733sleep.py">
 </a --->

&nbsp;


## Java &nbsp; <sup><sub>[jv733sleep.java](./../java/jv733sleep.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jv733sleep.png">
 <img src ="./runs/jv733sleep.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv733sleep.java">
 </a --->

&nbsp;


## C-Sharp &nbsp; <sup><sub>[cs733sleep.cs](./../cs/cs733sleep.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs733sleep.png">
 <img src ="./runs/cs733sleep.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs733sleep.cs">
 </a --->

&nbsp;


## JavaScript &nbsp; <sup><sub>[js733sleep.js](./../js/js733sleep.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js733sleep.png">
 <img src ="./runs/js733sleep.png" width="650" height="33" data-dims="x1084y0033" alt="Run js733sleep.js">
 </a --->

&nbsp;


## PHP &nbsp; <sup><sub>[ph733sleep.php](./../php/ph733sleep.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph733sleep.png">
 <img src ="./runs/ph733sleep.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph733sleep.php">
 </a --->

&nbsp;


## CPP &nbsp; <sup><sub>[cp733sleep.cpp](./../cpp/cp733sleep.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp733sleep.png">
 <img src ="./runs/cp733sleep.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp733sleep.cpp">
 </a --->

&nbsp;


---

<sup><sub><sup>*[File 20221213°1901]* ⬞Ω</sup></sub></sup>
