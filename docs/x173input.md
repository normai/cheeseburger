﻿# Demonstrate Input Functions

## Python &nbsp; <sup><sub>[py173input.py](./../py/py173input.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

<!--- a href="./runs/py173input.png">
 <img src ="./runs/py173input.png" width="650" height="33" data-dims="x1084y0033" alt="Run py173input.py">
 </a --->

&nbsp;


## Java &nbsp; <sup><sub>[jv173input.java](./../java/jv173input.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jv173input.png">
 <img src ="./runs/jv173input.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv173input.java">
 </a --->

&nbsp;


## C-Sharp &nbsp; <sup><sub>[cs173input.cs](./../cs/cs173input.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs173input.png">
 <img src ="./runs/cs173input.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs173input.cs">
 </a --->

&nbsp;


## JavaScript &nbsp; <sup><sub>[js173input.js](./../js/js173input.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js173input.png">
 <img src ="./runs/js173input.png" width="650" height="33" data-dims="x1084y0033" alt="Run js173input.js">
 </a --->

&nbsp;


## PHP &nbsp; <sup><sub>[ph173input.php](./../php/ph173input.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph173input.png">
 <img src ="./runs/ph173input.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph173input.php">
 </a --->

&nbsp;


## CPP &nbsp; <sup><sub>[cp173input.cpp](./../cpp/cp173input.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp173input.png">
 <img src ="./runs/cp173input.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp173input.cpp">
 </a --->


---

<sup><sub><sup>*[File 20220827°1401]* ⬞Ω</sup></sub></sup>
