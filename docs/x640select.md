﻿# Demonstrate Select Statement

## Python &nbsp; <sup><sub>[py640select.py](./../py/py640select.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

<!--- a href="./runs/py640select.png">
 <img src ="./runs/py640select.png" width="650" height="33" data-dims="x1084y0033" alt="Run py640select.py">
 </a --->

&nbsp;


## Java &nbsp; <sup><sub>[jv640select.java](./../java/jv640select.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jv640select.png">
 <img src ="./runs/jv640select.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv640select.java">
 </a --->

&nbsp;


## C-Sharp &nbsp; <sup><sub>[cs640select.cs](./../cs/cs640select.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs640select.png">
 <img src ="./runs/cs640select.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs640select.cs">
 </a --->

&nbsp;


## JavaScript &nbsp; <sup><sub>[js640select.js](./../js/js640select.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js640select.png">
 <img src ="./runs/js640select.png" width="650" height="33" data-dims="x1084y0033" alt="Run js640select.js">
 </a --->

&nbsp;


## PHP &nbsp; <sup><sub>[ph640select.php](./../php/ph640select.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph640select.png">
 <img src ="./runs/ph640select.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph640select.php">
 </a --->

&nbsp;


## CPP &nbsp; <sup><sub>[cp640select.cpp](./../cpp/cp640select.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp640select.png">
 <img src ="./runs/cp640select.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp640select.cpp">
 </a --->

&nbsp;


---

<sup><sub><sup>*[File 20220831°1801]* ⬞Ω</sup></sub></sup>
