﻿# Demonstrate Mode Flags

Subject: Show all the mode flags

## Python &nbsp; <sup><sub>[py350modes.py](./../py/py350modes.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

<!--- a href="./runs/py350modes.png">
 <img src ="./runs/py350modes.png" width="650" height="33" data-dims="x1084y0033" alt="Run py350modes.py">
 </a --->

&nbsp;


## Java &nbsp; <sup><sub>[jv350modes.java](./../java/jv350modes.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jv350modes.png">
 <img src ="./runs/jv350modes.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv350modes.java">
 </a --->

&nbsp;


## C-Sharp &nbsp; <sup><sub>[cs350modes.cs](./../cs/cs350modes.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs350modes.png">
 <img src ="./runs/cs350modes.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs350modes.cs">
 </a --->

&nbsp;


## JavaScript &nbsp; <sup><sub>[js350modes.js](./../js/js350modes.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js350modes.png">
 <img src ="./runs/js350modes.png" width="650" height="33" data-dims="x1084y0033" alt="Run js350modes.js">
 </a --->

&nbsp;


## PHP &nbsp; <sup><sub>[ph350modes.php](./../php/ph350modes.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph350modes.png">
 <img src ="./runs/ph350modes.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph350modes.php">
 </a --->

&nbsp;


## CPP &nbsp; <sup><sub>[cp350modes.cpp](./../cpp/cp350modes.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp350modes.png">
 <img src ="./runs/cp350modes.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp350modes.cpp">
 </a --->

&nbsp;


---

<sup><sub><sup>*[File 20220829°1301]* ⬞Ω</sup></sub></sup>
