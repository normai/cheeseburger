﻿# Documentation

Here come subjects which are too detailled for the frontpage
 and too general for the demo description files `x*.md`.


Subchapters: &nbsp; [Synopsis](#synopsis) &nbsp; [The Demos](#the_demos) &nbsp; [How to Run](#how_to_run)
 &nbsp; [What's Missing](#whats_missing) &nbsp; [Links](#links) &nbsp; [BTW](#by_the_way)  &nbsp; [Credits](#credits)

## Synopsis<a name="synopsis"></a>

Slogan : One demo per programming feature per programming language

Platforms : The source files are written in Windows under `python.exe` 3.7.8, `javac.exe` 11, `csc.exe` 4.0.1, etc.
 But the files shall work in other operating systems as well. If there is something operating system specific
 with a file, this is told explicitly.

License : The project as a whole runs under the BSD 3-Clause license.
 Some files run under other permissive licenses.

Copyright : © 2021 - 2024 Norbert C. Maier

Authors : Norbert C. Maier

Status : One third of about 500 listed demo files are ready to run.



## How to run the files? <a name="how_to_run"></a>

The easiest way is to run the codes from the console.
 Prerequisite is of course, that you have correctly set
 the pathes to the compilers and interpreters resp.

### How to run the Python files &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon" id="id20220124o0614">

The commandline may look like this:

```
G:\work\cheeseburger\py>python.exe d131types.py
(1) 123 + 1 = 124
(2) 2.34 + 1 = 3.34
(3) True + 1 = 2
(4) Ahoj + 1 = Ahoj1

G:\work\cheeseburger\py>
```

### How to run the Java files &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="48" height="48" alt="Java icon" id="id20220124o0612">

The commandline may look like this:

```
G:\work\cheeseburger\jv>javac.exe j131types.java

G:\work\cheeseburger\jv>java.exe j131types
Some Java types ..
(1) 127 + 1 = -128 (Byte.MAX_VALUE = 127)
(2) 32767 + 1 = -32768 (Short.MAX_VALUE = 32767)
 …
(7) !true = false
(8) Holla + 1 = Holla1

G:\work\cheeseburger\jv>
```

### How to run the C-Sharp files &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon" id="id20220124o0616">

The easiest way is to use the console offered from the Visual Studio start menu
 as '*Developer Command Prompt for VS*'

The commandline may look like this then:

```
G:\work\cheeseburger\cs>csc.exe cs131types.cs
Microsoft (R) Visual C# Compiler version 4.2.0-4.22281.5 (8d3180e5)
Copyright (C) Microsoft Corporation. All rights reserved.

G:\work\cheeseburger\cs>cs131types.exe
Some CSharp types ..
(1) 127 + 1 = 128 (Byte.MaxValue = 255)
(2) 32767 + 1 = -32768 (Int16.MaxValue = 32767)
 …
(7) !True = False
(8) Holla + 1 = Holla1

G:\work\cheeseburger\cs>
```

<img src="./icos/20210924o1933.delftstack.v2.x0048y0048.png" align="left" style="margin-right:1.1em;" width="48" height="48" alt="Link icon for DelftStack" id="id20220124o0422">
 &nbsp; Read the DelftStack article [Compile C# in Command Line](https://www.delftstack.com/howto/csharp/compile-csharp-command-line/)
 if you want set the pathes yourself, instead of using the '*Developer Command Prompt for VS*'

### How to run the JavaScript files &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript" id="id20220124o0622">

The JavaScript sequences are embedded into HTML files.
 So open that HTML file in a Browser and the scripts should run


### How to run the PHP files &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP" id="id20220124o0624">

The PHP files should run from the commandline, e.g.:

```
G:\work\,cheeseburger\php>php.exe ph111hello.php
﻿ <p>Hello PHP. The weather is fine, or not, anyway.</p>

G:\work\cheeseburger\php>
```

If you have a local webserver running, which serves PHP files,
 you can just open the PHP files in the browser.

### How to run the CPP files &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="CPP icon" id="id20220124o0618">

```
G:\work\cheeseburger\cpp>cl.exe cp111hello.cpp /EHsc
Microsoft (R) C/C++ Optimizing Compiler Version 19.32.31332 for x86
Copyright (C) Microsoft Corporation.  All rights reserved.

cp111hello.cpp
Microsoft (R) Incremental Linker Version 14.32.31332.0
Copyright (C) Microsoft Corporation.  All rights reserved.

/out:cp111hello.exe
cp111hello.obj

G:\work\cheeseburger\cpp>cp111hello.exe
Helo CPP!
G:\work\cheeseburger\cpp>
```

### Tricky demos:

Some demos afford extra measures to run, e.g. commandline options or
 library files. Here are they:

- `cs715assert.cs` — (1) Needs `Microsoft.VisualStudio.QualityTools.UnitTestFramework.dll`
 provided with the -r Parameter to compile and (2) needs the same file provided
 for runtime either by being copied into the executable folder or by being
 installed in the GAC.

- `jv117keywords.java` — Needs the `-enableassertions` or `-ea` option when executing

- `jv715assert.java` — Needs the `-enableassertions` or `-ea` option when executing



## What is missing? <a name="whats_missing"></a>

- Database operations (SQLite) *[e.g. `d841x.py`]*

- Import modules from other folders.
 See e.g. StackOverflow article
 [Importing files from different folder](https://stackoverflow.com/questions/4383571/importing-files-from-different-folder)
 &nbsp; <sup><sub><sup>*[ref 20211102°1736]*</sup></sub></sup></span>

- Provide a C demo for goto. See StackOverflow thread
 [Is there a goto statement in Java?](https://stackoverflow.com/questions/2545103/is-there-a-goto-statement-in-java).
 Why is it interesting for novices to see the `goto`?
 Because it deepens the insight how the source code is worked off sequentially line by line.
 This insight is wanted to understand branching, looping and function calling.

- Try to make the goto.py module work
 <sup><sub><sup>*[files 20211019°1011/1021]*</sup></sub></sup>. See Entrian Solution's
 [goto for Python](http://entrian.com/goto/) ,
 [pypi.org/project/goto-py](https://pypi.org/project/goto-py/),
 StackOverflow threads
 [Is there a label/goto in Python?](https://stackoverflow.com/questions/438844/is-there-a-label-goto-in-python), 
 and
 [Error in goto module [Python]](https://stackoverflow.com/questions/31797866/error-in-goto-module-python).
 Searching for 'goto.py' yields many pages and several implementations.

- Provide a Java demo for `switch`

## By the Way <a name="by_the_way"></a>

**Encoding**. The Python files are encoded as UTF-8-without-BOM.
 To see some error message, just convert one of the modules to ANSI,
 which is done easy with NotpadPlusPlus.
 For how to explicitly declare encoding, see Python documentation
 [PEP 263 -- Defining Python Source Code Encodings](https://www.python.org/dev/peps/pep-0263/)
 .

To run all Python scripts in bulk on Windows, use
 [./py/~runall.bat](./py/~runall.bat) .

## Links<a name="links"></a>

Here are one or more links to other demo collections.

<img src="./icos/20220910o1123.codemaze.v1.x0048y0048.png" align="left" style="margin-right:1.1em;" width="48" height="48" alt="Link icon for Code-Maze" id="id20220125o0532">
 &nbsp; [**Code-Maze**](https://code-maze.com/)<!-- ref 20220910°1127 -->
 publishes a C#/.NET demo suite on
 [github.com/CodeMazeBlog/CodeMazeGuides](https://github.com/CodeMazeBlog/CodeMazeGuides)<!-- ref 20220910°1125 -->
 under the MIT License.

&nbsp;

## Credits<a name="credits"></a>

Many thanks goes to the following people :

<img src="./icos/20201113o0203.openclipart.v1.x0048y0048.png" align="left" style="margin-right:1.1em;" width="48" height="48" alt="Link icon for OpenClipart.org" id="id20220102o0512">
 &nbsp; [**Gerald_G**](https://openclipart.org/artist/Gerald_G) for providing the master for the top right hamburger log
 on [openclipart.org/…/fast-food-lunchdinner-hamburger](https://openclipart.org/detail/9079/fast-food-lunchdinner-hamburger)
 under the
 [CC0 1.0 Universal (CC0 1.0) Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/)
 License

&nbsp;

<img src="./icos/20220219o2047.nedbatchelder.v2.x0048y0048.png" align="left" style="margin-right:1.1em;" width="48" height="48" alt="Link icon for Ned Batchelder">
 &nbsp; [**Ned Batchelder**](https://nedbatchelder.com/) for his blog article
 [Human sorting](https://nedbatchelder.com/blog/200712/human_sorting.html),
 from where [./py/py753humansort.py](./py/py753humansort.py) originates.

&nbsp;


---

<sup><sub><sup>*[File 20211103°1125]* ⬞Ω</sup></sub></sup>
