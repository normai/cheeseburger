﻿
   audio 20221001°1729 Mystical+Sting+-+320bit
   objecttype  : audio
   title       : Mystical Sting
   summary     : Short jingle
   file        :
   source      : https://www.serpentsoundstudios.com/royalty-free-music/jingles [ref 20221001°1712]
   ofilename   : Mystical+Sting+-+320bit.mp3
   authors     : Alexander Nakarada
   copyright   : © Alexander Nakarada
   license     : Creative Commons Attribution 4.0 International License
   duration    : 00:00:12
   container   :
   releasedate : ~2020
   album       : Jingles
   chain       : https://www.freeconvert.com/mp3-to-wav [ref 20221001°1622]
   chain       : https://www.mp3cutter.com/ [ref 20221228°0932]
   image       :
   processing  : v0 : converted from mp3 to wav [ref 20221001°1622]
   processing  : v1 : Cut from 12 to 4 seconds and saved as MOV [ref 20221228°0932]
   usage       : file 20221001°1511 py771sound.py
   tags        : sound, jingles, cc-by-40
   note        :
   ⬞Ω
