﻿# The most simple complete program with user input

A complete typical program fetches input from somewhere, e.g. from the user,
 processes the input into a result, then outputs the result.

## Python &nbsp; <sup><sub>[py111hello.py](./../py/py111hello.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

<a href="./runs/py111hello.png">
 <img src ="./runs/py111hello.png" width="643" height="159" data-dims="x1084y0266" data-factor="0.6" alt="Run py111hello.py">
 </a>

Here are some links on Python programming in general

<img src="./icos/20211102o1733.pythoncheatsheet.v1.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for PythonCheatsheet">
 &nbsp; The
 [**Python Cheatsheet**](https://www.pythoncheatsheet.org/)
 provides an overview on Python language elements.
 <!--- [ref 20211102°1732] --->

&nbsp;

<img src="./icos/20220218o1213.goalkicker.v2.x0032y0032.png" align="left" width="29" height="29" alt="Link icon for GoalKickers">
 &nbsp; GoalKickers book
 [**Python® Notes for Professionals book**](https://goalkicker.com/PythonBook/)
 can be downloaded under the CC BY-SA license
 <!--- [ref 20220218°1412] --->

&nbsp;


## Java &nbsp; <sup><sub>[jv111hello.java](./../java/jv111hello.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<a href="./runs/jv111hello.png">
 <img src ="./runs/jv111hello.png" width="650" height="193" data-dims="x1084yy0322" alt="Run jv111hello.java">
 </a>

Here are some links on Java programming in general

<img src="./icos/20220218o1213.goalkicker.v2.x0032y0032.png" align="left" width="29" height="29" alt="Link icon for GoalKickers">
 &nbsp; GoalKickers book
 [**Java® Notes for Professionals book**](https://goalkicker.com/JavaBook/)
 can be downloaded under the CC BY-SA license
 <!--- [ref 20220218°1312] --->

&nbsp;

<img src="./icos/20110921o1121.stackoverflow.v1.x0032y0032.gif" align="left" width="32" height="32" alt="Link icon for StackOverflow">
 &nbsp; StackOverflow thread
 [Compiling (javac) a UTF8 encoded Java source code with a BOM](https://stackoverflow.com/questions/9811382/compiling-javac-a-utf8-encoded-java-source-code-with-a-bom)
 concludes, that Java compiler cannot handle UTF-8-with-BOM.
 Thus the **Java source files come without BOM**.
 <!--- [ref 20220216°1742] --->

&nbsp;

<img src="./icos/20200316o1428.archiveorg.v1.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Archive.org">
 &nbsp; Archived Java documentation article
 [JDK-4508058 : UTF-8 encoding does not recognize initial BOM](https://web.archive.org/web/20161222001217/http://bugs.java.com/view_bug.do?bug_id=4508058)
 …
 <!--- [ref 20220216°1743] --->

&nbsp;


## C-Sharp &nbsp; <sup><sub>[cs111hello.cs](./../cs/cs111hello.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<a href="./runs/cs111hello.png">
 <img src ="./runs/cs111hello.png" width="650" height="229" data-dims="x1084y0382" alt="Run cs111hello.cs">
 </a>

&nbsp;


## JavaScript &nbsp; <sup><sub>[js111hello.js](./../js/js111hello.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<a href="./runs/js111hello_1.png">
 <img src ="./runs/js111hello_1.png" width="650" height="287" data-dims="x1084y0479" alt="Run js111hello.js">
 </a>

&nbsp;

<a href="./runs/js111hello_2.png">
 <img src ="./runs/js111hello_2.png" width="650" height="287" data-dims="x1084y0479" alt="Run js111hello.js">
 </a>

&nbsp;

<img src="./icos/20210630o1713.webhint.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for WebHint">
 &nbsp; WebHint article
 [**Correct Viewport**](https://webhint.io/docs/user-guide/hints/hint-meta-viewport/)
 provides information about the viewport attribute. (This page is linked
 from the Edge debugger.)
 <!--- [ref 20220825°1341 -><-] --->

&nbsp;

<img src="./icos/20200630o0143.w3schools.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for W3Schools.com">
 &nbsp; W3Schools.com article
 [JavaScript Popup Boxes](https://www.w3schools.com/js/js_popup.asp)
 describes the most simple JavaScript user input method. Note: Do not
 use this input method in productive software, it annoys the users.
 <!--- [ref 20220907°1244] --->

&nbsp;


## PHP &nbsp; <sup><sub>[ph111hello.php](./../php/ph111hello.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<a href="./runs/ph111hello_1.png">
 <img src ="./runs/ph111hello_1.png" width="650" height="287" data-dims="x1084y0479" alt="Run ph111hello.php">
 </a>

&nbsp;

<a href="./runs/ph111hello_2.png">
 <img src ="./runs/ph111hello_2.png" width="650" height="287" data-dims="x1084y0479" alt="Run ph111hello.php">
 </a>

&nbsp;


## CPP &nbsp; <sup><sub>[cp111hello.cpp](./../cpp/cp111hello.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<a href="./runs/cp111hello.png">
 <img src ="./runs/cp111hello.png" width="650" height="310" data-dims="x1084y0517" alt="Run cp111hello.cpp">
 </a>

&nbsp;

<img src="./icos/20200630o0143.w3schools.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon W3Schools.com">
 &nbsp; W3Schools.com article
 [C++ User Input Strings](https://www.w3schools.com/cpp/cpp_strings_input.asp)
 …
 <!--- [ref 20220907°1242] --->

&nbsp;


## General

Here is one or more links on general matters around programming languages

<img src="./icos/20220214o1313.fluentin3month.v1.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Fluent-in-3-month">
 &nbsp; Fluent-in-3-month article
 [How to Say “Hello” in 29 Different Languages](https://www.fluentin3months.com/international-greetings/)
 tells some alternatives to 'Hello'.
 <!--- [ref 20220214°1312] --->

&nbsp;


---

<sup><sub><sup>*[File 20220826°0901 x111hello.md]* ⬞Ω</sup></sub></sup>
