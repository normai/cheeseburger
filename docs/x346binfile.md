﻿# Demonstrate Create/Read/Write Binary file

## Python &nbsp; <sup><sub>[py346binfile.py](./../py/py346binfile.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

<!--- a href="./runs/py346binfile.png">
 <img src ="./runs/py346binfile.png" width="650" height="33" data-dims="x1084y0033" alt="Run py346binfile.py">
 </a --->

&nbsp;


## Java &nbsp; <sup><sub>[jv346binfile.java](./../java/jv346binfile.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jv346binfile.png">
 <img src ="./runs/jv346binfile.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv346binfile.java">
 </a --->

&nbsp;


## C-Sharp &nbsp; <sup><sub>[cs346binfile.cs](./../cs/cs346binfile.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs346binfile.png">
 <img src ="./runs/cs346binfile.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs346binfile.cs">
 </a --->

&nbsp;


## JavaScript &nbsp; <sup><sub>[js346binfile.js](./../js/js346binfile.js)</sub></sup>

<!--- a href="./runs/js346binfile.png">
 <img src ="./runs/js346binfile.png" width="650" height="33" data-dims="x1084y0033" alt="Run js346binfile.js">
 </a --->

&nbsp;


## PHP &nbsp; <sup><sub>[ph346binfile.php](./../php/ph346binfile.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph346binfile.png">
 <img src ="./runs/ph346binfile.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph346binfile.php">
 </a --->

&nbsp;


## CPP &nbsp; <sup><sub>[cp346binfile.cpp](./../cpp/cp346binfile.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp346binfile.png">
 <img src ="./runs/cp346binfile.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp346binfile.cpp">
 </a --->

&nbsp;


---

<sup><sub><sup>*[File 20220829°1201]* ⬞Ω</sup></sub></sup>
