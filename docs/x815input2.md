﻿# Demonstrate GUI with Input Field (fine language)

The functionality of this demo is (shall be) exactly the same as in demo
 x813input1, just the language features are a bit finer.

## Python &nbsp; <sup><sub>[py815input2.py](./../py/py815input2.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

Instead of plain variable assignment and defined event handlers (as in
 x813input1), there shall be used lambdas and method chaining.

<!--- a href="./runs/py815input2.png">
 <img src ="./runs/py815input2.png" width="650" height="33" data-dims="x1084y0033" alt="Run py815input2.py">
 </a --->

&nbsp;


## Java &nbsp; <sup><sub>[jv815input2.java](./../java/jv815input2.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jv815input2.png">
 <img src ="./runs/jv815input2.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv815input2.java">
 </a --->

&nbsp;


## C-Sharp &nbsp; <sup><sub>[cs815input2.cs](./../cs/cs815input2.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs815input2.png">
 <img src ="./runs/cs815input2.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs815input2.cs">
 </a --->

&nbsp;


## JavaScript &nbsp; <sup><sub>[js815input2.js](./../js/js815input2.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js815input2.png">
 <img src ="./runs/js815input2.png" width="650" height="33" data-dims="x1084y0033" alt="Run js815input2.js">
 </a --->

&nbsp;


## PHP &nbsp; <sup><sub>[ph815input2.php](./../php/ph815input2.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

Instead of plain PHP form (as in x813input1), the buttons are controlled
 with JavaScript.

<!--- a href="./runs/ph815input2.png">
 <img src ="./runs/ph815input2.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph815input2.php">
 </a --->

&nbsp;


## CPP &nbsp; <sup><sub>[cp815input2.cpp](./../cpp/cp815input2.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp815input2.png">
 <img src ="./runs/cp815input2.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp815input2.cpp">
 </a --->

&nbsp;


---

<sup><sub>*[File 20221216°1501 x815input2.md]* ⬞Ω</sub></sup>
