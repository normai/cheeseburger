﻿# Demonstrate simple program using variables, operators, strings, built-in function calls

To process input to some result, typically you need language features
 variables, operators, strings and function calls.

## Python &nbsp; <sup><sub>[py113aloha.py](./../py/py113aloha.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

<a href="./runs/py113aloha.png">
 <img src ="./runs/py113aloha.png" width="650" height="252" data-dims="x1084y0421" alt="Run py113aloha.py">
 </a>

&nbsp;


## Java &nbsp; <sup><sub>[jv113aloha.java](./../java/jv113aloha.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<a href="./runs/jv113aloha.png">
 <img src ="./runs/jv113aloha.png" width="650" height="287" data-dims="x1084y0479" alt="Run jv113aloha.java">
 </a>

&nbsp;


## C-Sharp &nbsp; <sup><sub>[cs113aloha.cs](./../cs/cs113aloha.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<a href="./runs/cs113aloha.png">
 <img src ="./runs/cs113aloha.png" width="650" height="309" data-dims="x1084y0516" alt="Run cs113aloha.cs">
 </a>

&nbsp;


## JavaScript &nbsp; <sup><sub>[js113aloha.js](./../js/js113aloha.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<a href="./runs/js113aloha.png">
 <img src ="./runs/js113aloha.png" width="650" height="287" data-dims="x1084y0479" alt="Run js113aloha.js">
 </a>

&nbsp;


## PHP &nbsp; <sup><sub>[ph113aloha.php](./../php/ph113aloha.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<a href="./runs/ph113aloha.png">
 <img src ="./runs/ph113aloha.png" width="650" height="287" data-dims="x1084y0479" alt="Run ph113aloha.php">
 </a>

&nbsp;


## CPP &nbsp; <sup><sub>[cp113aloha.cpp](./../cpp/cp113aloha.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<a href="./runs/cp113aloha.png">
 <img src ="./runs/cp113aloha.png" width="650" height="393" data-dims="x1084y0656" alt="Run cp113aloha.cpp">
 </a>

&nbsp;

<img src="./icos/20220825o1323.bloomfield.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Peter Bloomfield">
 &nbsp; Peter Bloomfield's article
 [Convert a number to a binary string (and back) in C++](https://peter.bloomfield.online/convert-a-number-to-a-binary-string-and-back-in-cpp/)
 …
 <!--- [ref 20220825°1322] --->

&nbsp;

<img src="./icos/20200206o1523.codespeedy.v1.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for CodeSpeedy">
 &nbsp; CodeSpeedy article
 [How to get the type of a variable in C++](https://www.codespeedy.com/get-the-type-of-a-variable-in-cpp/)
 …
 <!--- [ref 20220825°1326] --->

&nbsp;

<img src="./icos/20150308o0524.cplusplus.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for CPlusPlus">
 &nbsp; CPlusPlus article
 [function sprintf](https://cplusplus.com/reference/cstdio/sprintf/)
 …
 <!--- [ref 20220825°1327] --->

&nbsp;


---

<sup><sub><sup>*[File 20220825°1301 x113aloha.md]* ⬞Ω</sup></sub></sup>
