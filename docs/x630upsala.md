﻿# Demonstrate Update Statement

Note on the filename. This file was originalaly named `cp630update.cpp`.
 But when executing a file `cp630update.exe`, Windows wants the Admin
 password. That's why I renamed this series `*630upsala.*`.
 &nbsp; <sup>*[issue 20221227°1321 'cp630update.exe']*</sup>

&nbsp;

## Python &nbsp; <sup><sub>[py630upsala.py](./../py/py630upsala.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

<!--- a href="./runs/py630upsala.png">
 <img src ="./runs/py630upsala.png" width="650" height="33" data-dims="x1084y0033" alt="Run py630upsala.py">
 </a --->

&nbsp;


## Java &nbsp; <sup><sub>[jv630upsala.java](./../java/jv630upsala.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jv630upsala.png">
 <img src ="./runs/jv630upsala.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv630upsala.java">
 </a --->

&nbsp;


## C-Sharp &nbsp; <sup><sub>[cs630upsala.cs](./../cs/cs630upsala.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs630upsala.png">
 <img src ="./runs/cs630upsala.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs630upsala.cs">
 </a --->


## JavaScript &nbsp; <sup><sub>[js630upsala.js](./../js/js630upsala.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js630upsala.png">
 <img src ="./runs/js630upsala.png" width="650" height="33" data-dims="x1084y0033" alt="Run js630upsala.js">
 </a --->

&nbsp;


## PHP &nbsp; <sup><sub>[ph630upsala.php](./../php/ph630upsala.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph630upsala.png">
 <img src ="./runs/ph630upsala.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph630upsala.php">
 </a --->

&nbsp;


## CPP &nbsp; <sup><sub>[cp630upsala.cpp](./../cpp/cp630upsala.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp630upsala.png">
 <img src ="./runs/cp630upsala.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp630upsala.cpp">
 </a --->

&nbsp;


---

<sup><sub><sup>*[File 20220831°1701]* ⬞Ω</sup></sub></sup>
