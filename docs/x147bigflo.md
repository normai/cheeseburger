﻿# Demonstrate Big Floats = Arbitrary Precision Floating Point Numbers

When dealing with floats, the question is not how big it can grow (as is
 the matter with integers), the question is how precise they are.

Userstory: Calculate and output some popular non-terminating decimals.

The Python userstory is: Calculate the number Pi in various precisions.
 Just that is not satisfying, so for the other languages I used a more
 straight-on userstory.

*Todo. Do the Pi-Calculation with the Archimedes  method. Perhaps that
 one shows more nicely, how the precision grows with more iterations, as
 opposed to the Nilakantha method, where the decimals jump zigzag.*
 <sup>*[todo 20221227°1741]*</sup>


## Python &nbsp; <sup><sub>[py147bigflo.py](./../py/py147bigflo.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

<!--- a href="./runs/py147bigflo.png">
 <img src ="./runs/py147bigflo.png" width="650" height="33" data-dims="x1084y0033" alt="Run py147bigflo.py">
 </a --->

&nbsp;


## Java &nbsp; <sup><sub>[jv147bigflo.java](./../java/jv147bigflo.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jv147bigflo.png">
 <img src ="./runs/jv147bigflo.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv147bigflo.java">
 </a --->

&nbsp;

<img src="./icos/20220320o1415.oracle.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Oracle">
 &nbsp; Oracle documentation
 [Class BigDecimal](https://docs.oracle.com/javase/7/docs/api/java/math/BigDecimal.html)
 …
 <!--- [ref 20221227°1622] --->

&nbsp;

<img src="./icos/20220320o1415.oracle.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Oracle">
 &nbsp; Oracle article
 [What Every Computer Scientist Should Know About Floating-Point Arithmetic](https://docs.oracle.com/cd/E19957-01/806-3568/ncg_goldberg.html)
 …
 <!--- [ref 20221227°1624] --->

&nbsp;

<img src="./icos/20220320o1415.oracle.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Oracle">
 &nbsp; Oracle documentation
 [Enum RoundingMode](https://docs.oracle.com/javase/7/docs/api/java/math/RoundingMode.html)
 …
 <!--- [ref 20221227°1626] --->

&nbsp;

<img src="./icos/20220320o1415.oracle.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for StackOverflow">
 &nbsp; StackOverflow thread
 [Big float numbers weird results](https://stackoverflow.com/questions/16351687/big-float-numbers-weird-results)
 …
 <!--- [ref 20221227°1632] --->

&nbsp;

<img src="./icos/20220905o0913.baeldung.v1.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Baeldung">
 &nbsp; Baeldung article
 [BigDecimal and BigInteger in Java](https://www.baeldung.com/java-bigdecimal-biginteger)
 …
 <!--- [ref 20221227°1634] --->

&nbsp;

<img src="./icos/20221117o0943.codingcompiler.v1.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for CodingCompiler">
 &nbsp; CodingCompiler article
 [BigDecimal in Java](https://codingcompiler.com/bigdecimal-in-java/)
 …
 <!--- [ref 20221227°1636] --->

&nbsp;

<img src="./icos/20221227o1643.behindjava.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for BehindJava">
 &nbsp; BehindJava article
 [Float, Double and BigDecimal with Financial Calculation](https://www.behindjava.com/float-double-and-bigdecimal-with/)
 …
 <!--- [ref 20221227°1642] --->

&nbsp;

<img src="./icos/20221227o1653.nullbeans.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Nullbeans">
 &nbsp; Nullbeans article
 [Java BigDecimal, Dealing with high precision calculations](https://nullbeans.com/java-bigdecimal-dealing-with-high-precision-calculations/)
 …
 <!--- [ref 20221227°1652] --->

&nbsp;

<img src="./icos/20220320o1443.logicbig.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for LogicBig">
 &nbsp; LogicBig article
 [Java - How to set BigDecimal Precision?](https://www.logicbig.com/how-to/java-numbers/big-decimal-precision.html)
 …
 <!--- [ref 20221227°1712] --->

&nbsp;

<img src="./icos/20200319o0423.digitalocean.v1.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for DigitalOcean">
 &nbsp; DigitalOcean tutorial
 [Java Convert double to String](https://www.digitalocean.com/community/tutorials/java-convert-double-to-string)
 …
 <!--- [ref 20221227°1722] --->

&nbsp;

<img src="./icos/20200319o0423.digitalocean.v1.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for DigitalOcean">
 &nbsp; DigitalOcean tutorial
 [Java printf() - Print Formatted String to Console](https://www.digitalocean.com/community/tutorials/java-printf-method)
 …
 <!--- [ref 20221227°1724] --->

&nbsp;


## C-Sharp &nbsp; <sup><sub>[cs147bigflo.cs](./../cs/cs147bigflo.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

C# curiously provides a native BigInteger class, but no BigFloat class.
 They have a Decimal class with a precision of 28/29 decimals, but that
 is not arbitrary large.

<!--- a href="./runs/cs147bigflo.png">
 <img src ="./runs/cs147bigflo.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs147bigflo.cs">
 </a --->

&nbsp;

<img src="./icos/20110921o1121.stackoverflow.v1.x0032y0032.gif" align="left" width="32" height="32" alt="Link icon for ">StackOverflow
 &nbsp; StackOverflow thread
 [What is the equivalent of the Java BigDecimal class in C#?](https://stackoverflow.com/questions/2863388/what-is-the-equivalent-of-the-java-bigdecimal-class-in-c)
 …
 <!--- [ref 20221227°1812] --->

&nbsp;

<img src="./icos/20110921o1121.stackoverflow.v1.x0032y0032.gif" align="left" width="32" height="32" alt="Link icon for ">StackOverflow
 &nbsp; StackOverflow thread
 [Is there a BigFloat class in C#?](https://stackoverflow.com/questions/10359372/is-there-a-bigfloat-class-in-c)
 …
 <!--- [ref 20221227°1814] --->

&nbsp;

<img src="./icos/20110921o1121.stackoverflow.v1.x0032y0032.gif" align="left" width="32" height="32" alt="Link icon for ">StackOverflow
 &nbsp; StackOverflow thread
 [Is there support for arbitrary precision arithmetic in C#?](https://stackoverflow.com/questions/9728265/is-there-support-for-arbitrary-precision-arithmetic-in-c)
 …
 <!--- [ref 20221227°1816] --->

&nbsp;

<img src="./icos/20110921o1125.wikipedia.v1.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Wikipedia">
 &nbsp; Wikipedia article
 [List of arbitrary-precision arithmetic software](https://en.wikipedia.org/wiki/List_of_arbitrary-precision_arithmetic_software)
 shows no product for C# …
 <!--- [ref 20221227°1824] --->

&nbsp;

<img src="./icos/20170811o0621.microsoftgray.v5.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Microsoft">
 &nbsp; Microsoft reference
 [Floating-point numeric types (C# reference)](https://learn.microsoft.com/en-us/dotnet/csharp/language-reference/builtin-types/floating-point-numeric-types)
 …
 <!--- [ref 20221227°1832] --->

&nbsp;

<img src="./icos/20170811o0621.microsoftgray.v5.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Microsoft">
 &nbsp; Microsoft reference
 [Numerics in .NET](https://learn.microsoft.com/en-us/dotnet/standard/numerics)
 …
 <!--- [ref 20221227°1834] --->

&nbsp;

<img src="./icos/20170811o0621.microsoftgray.v5.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Microsoft">
 &nbsp; Microsoft reference
 [8 Types](https://learn.microsoft.com/en-us/dotnet/csharp/language-reference/language-specification/types#838-the-decimal-type)
 …
 <!--- [ref 20221227°1836] --->

&nbsp;

<img src="./icos/20170811o0621.microsoftgray.v5.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Microsoft">
 &nbsp; Microsoft reference
 [How to use integer and floating point numbers in C#](https://learn.microsoft.com/en-us/dotnet/csharp/tour-of-csharp/tutorials/numbers-in-csharp-local)
 …
 <!--- [ref 20221227°1838] --->

&nbsp;

<img src="./icos/20221227o1843.extremeoptimization.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for ExtremeOptimization">
 &nbsp; Extreme Optimization documentation
 [BigFloat Class](https://www.extremeoptimization.com/documentation/reference/extreme.mathematics.bigfloat.aspx)
 This is what we want, only it is a proprietary product
 with just a trial version for free …
 <!--- [ref 20221227°1842] --->

&nbsp;


## JavaScript &nbsp; <sup><sub>[js147bigflo.js](./../js/js147bigflo.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js147bigflo.png">
 <img src ="./runs/js147bigflo.png" width="650" height="33" data-dims="x1084y0033" alt="Run js147bigflo.js">
 </a --->

&nbsp;

<img src="./icos/20180511o0333.mozilla1logo.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Mozilla">
 &nbsp; Mozilla JavaScript documentation
 [**Number**](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number)
 …
 <!--- [ref 20221228°1012] --->

&nbsp;

<img src="./icos/20110921o1121.stackoverflow.v1.x0032y0032.gif" align="left" width="32" height="32" alt="Link icon for StackOverflow">
 &nbsp; StackOverflow thread
 [How to deal with floating point number precision in JavaScript?](https://stackoverflow.com/questions/1458633/how-to-deal-with-floating-point-number-precision-in-javascript)
 …
 <!--- [ref 20221228°1014] --->

&nbsp;

<img src="./icos/20110921o1121.stackoverflow.v1.x0032y0032.gif" align="left" width="32" height="32" alt="Link icon for StackOverflow">
 &nbsp; StackOverflow thread
 [How to deal with big numbers in javascript [duplicate]](https://stackoverflow.com/questions/4288821/how-to-deal-with-big-numbers-in-javascript)
 …
 <!--- [ref 20221228°1016] --->

&nbsp;

<img src="./icos/20220905o0933.stackabuse.v5.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for StackAbuse">
 &nbsp; StackAbuse article
 [How to Set Floating-Point Precision in JavaScript](https://stackabuse.com/how-to-set-floating-point-precision-in-javascript/)
 …
 <!--- [ref 20221228°1022] --->

&nbsp;

<img src="./icos/20221228o1033.matthewburfield.v1.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Matthew Burfield">
 &nbsp; Matthew Burfield's article
 [JavaScript deep dive - Floating point numbers and the (in)famous rounding errors](https://www.matthewburfield.com/javascript-deep-dive-floating-point-numbers/)
 …
 <!--- [ref 20221228°1032] --->

&nbsp;

<img src="./icos/20221228o1043.dailyjavascript.v0.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Daily-JavaScript">
<img src="./icos/20180615o0435.githubmark1.v0.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for GitHub">
 &nbsp; Daily-JavaScript article
 [bigfloat](http://daily-javascript.com/articles/bigfloat/)
 <!--- [ref 20221228°1042] --->
 describes GitHub project
 [charto/bigfloat](https://github.com/charto/bigfloat)
 <!--- [ref 20221228°1052] --->
 (last actualized 2018) …

&nbsp;

<img src="./icos/20221228o1113.mikmcl.v1.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for MikeMcl">
<img src="./icos/20180615o0435.githubmark1.v0.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for GitHub">
 &nbsp; GitHub project
 [MikeMcl/big.js](https://github.com/MikeMcl/big.js/)
 <!--- [ref 20221228°1112] --->
 looks of hight quality.
 The project is documented on
 [mikemcl.github.io/big.js](http://mikemcl.github.io/big.js/).
 <!--- [ref 20221228°1122] --->
 Mike writes even more arbitrary-precisions projects, see
 [github.com/MikeMcl](https://github.com/MikeMcl).
 <!--- [ref 20221228°1123] --->

&nbsp;


## PHP &nbsp; <sup><sub>[ph147bigflo.php](./../php/ph147bigflo.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph147bigflo.png">
 <img src ="./runs/ph147bigflo.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph147bigflo.php">
 </a --->

&nbsp;


## CPP &nbsp; <sup><sub>[cp147bigflo.cpp](./../cpp/cp147bigflo.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp147bigflo.png">
 <img src ="./runs/cp147bigflo.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp147bigflo.cpp">
 </a --->

&nbsp;

<img src="./icos/20221228o1413.ttmath.v1.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Matthew Burfield">
 &nbsp; TTMath project
 [www.ttmath.org](https://www.ttmath.org/)
 …
 <!--- [ref 20221228°1412] --->

&nbsp;


## General

### About Pi (π)

<img src="./icos/20110921o1125.wikipedia.v1.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Wikipedia">
 &nbsp; Wikipedia articles
 [**Pi**](https://en.wikipedia.org/wiki/Pi)
 <!--- [ref 20221216°1712] --->
 (english) and
 [**Kreiszahl**](https://de.wikipedia.org/wiki/Kreiszahl)
 (german) explain the great story.
 <!--- [ref 20221216°1714] --->

&nbsp;

<img src="./icos/20110921o1125.wikipedia.v1.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Wikipedia">
 &nbsp; Wikipedia articlee
 [Approximations of π](https://en.wikipedia.org/wiki/Approximations_of_%CF%80)
 <!--- [ref 20221228°1212] --->

&nbsp;

<img src="./icos/20210217o1536.wikihow.v1.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for WikiHow">
 &nbsp; WikiHow articles
 [**How to Calculate Pi**](https://www.wikihow.com/Calculate-Pi)
 <!--- [ref 20221216°1722] --->
 (english) and
 [**Pi berechnen**](https://de.wikihow.com/Pi-berechnen)
 (german) explain various ways how to calculate Pi.
 <!--- [ref 20221216°1724] --->

&nbsp;

<img src="./icos/20210217o1536.wikihow.v1.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for WikiHow">
 &nbsp; WikiHow article
 [How to Write a Python Program to Calculate Pi](https://www.wikihow.com/Write-a-Python-Program-to-Calculate-Pi)
 provides Python code for two methods (the code will not run 'as is').
 <!--- [ref 20221216°1726] --->

&nbsp;

<img src="./icos/20221216o1733.31415.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for 3.141592653589793238462643383279502884197169399375105820974944592.eu">
 &nbsp; 3.141592653589793238462643383279502884197169399375105820974944592.eu article
 [Kreiszahl Pi berechnen / Formeln + Algorithmen](https://3.141592653589793238462643383279502884197169399375105820974944592.eu/pi-berechnen-formeln-und-algorithmen/)
 provides all kind of information and discussion on Pi.
 <!--- [ref 20221216°1732] --->
 The frontpage
 [(3.141592653589793238462643383279502884197169399375105820974944592.eu]((https://3.141592653589793238462643383279502884197169399375105820974944592.eu)
 shows π with 500 decimals precision.
 <!--- [ref 20221216°1734] --->

&nbsp;


---

<sup><sub><sup>*[File 20221117°1101 x147bigflo.md]* ⬞Ω</sup></sub></sup>
