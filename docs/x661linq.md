﻿# Demonstrate Feature …

The … Feature …

## Python &nbsp; <sup><sub>[py661linq.py](./../py/py661linq.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

<!--- a href="./runs/py661linq.png">
 <img src ="./runs/py661linq.png" width="650" height="33" data-dims="x1084y0033" alt="Run py661linq.py">
 </a --->

&nbsp;


## Java &nbsp; <sup><sub>[jv661linq.java](./../java/jv661linq.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jv661linq.png">
 <img src ="./runs/jv661linq.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv661linq.java">
 </a --->

&nbsp;


## C-Sharp &nbsp; <sup><sub>[cs661linq.cs](./../cs/cs661linq.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs661linq.png">
 <img src ="./runs/cs661linq.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs661linq.cs">
 </a --->

&nbsp;


## JavaScript &nbsp; <sup><sub>[js661linq.js](./../js/js661linq.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js661linq.png">
 <img src ="./runs/js661linq.png" width="650" height="33" data-dims="x1084y0033" alt="Run js661linq.js">
 </a --->

&nbsp;


## PHP &nbsp; <sup><sub>[ph661linq.php](./../php/ph661linq.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph661linq.png">
 <img src ="./runs/ph661linq.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph661linq.php">
 </a --->

&nbsp;


## CPP &nbsp; <sup><sub>[cp661linq.cpp](./../cpp/cp661linq.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp661linq.png">
 <img src ="./runs/cp661linq.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp661linq.cpp">
 </a --->

&nbsp;


---

<sup><sub>*[File 20221223°1301 x661linq.md]* ⬞Ω</sub></sup>
