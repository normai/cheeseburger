﻿# Demonstrate Keywords

Keywords make the core specification of any programming language. The
 difference between keywords and operators is not always obvious.

Actually, a 'keyword demo' makes no sense. Nevertheless, I try to offer
 demos under the 'keyword' subject. But here we will see very different
 things in each language, not the usual 'equal output' everywhere.

## Python &nbsp; <sup><sub>[py117keywords.py](./../py/py117keywords.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

Python is the (only?) language, which has functions to list it's own keywords.

Such keyword listing feature is appropriate, because the Python language
 development is very dynamic, it adds keywords from version to version.
 You can write defensive code, e.g. with Python 3.11, you can care about
 your possible Python 3.7 users, so they do not just crash,
 but at least gracefully deny of offer a workaround.

<!--- a href="./runs/py117keywords.png">
 <img src ="./runs/py117keywords.png" width="650" height="33" data-dims="x1084y0033" alt="Run py117keywords.py">
 </a --->

&nbsp;

<img src="./icos/20200630o0143.w3schools.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for W3Schools.com">
 &nbsp; W3Schools.com article
 [Python Keyword](https://www.w3schools.com/python/python_ref_keywords.asp)
 lists 33 keywords, from which 31 are liked to a dedicated article.
 <!--- [ref 20221223°1512] --->

&nbsp;

<img src="./icos/20201225o1757.realpython.v1.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for RealPython">
 &nbsp; RealPython tutorial
 [Python Keywords: An Introduction](https://realpython.com/python-keywords/)
 …
 <!--- [ref 20221223°1514] --->

&nbsp;

<img src="./icos/20200805o1243.python.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Python.org">
 &nbsp; Python documentation
 [keyword — Testing for Python keywords](https://docs.python.org/3/library/keyword.html)
 …
 <!--- [ref 20221223°1516] --->

&nbsp;

<img src="./icos/20220719o1753.askpython.v1.x0048y0048.png" align="left" width="32" height="32" alt="Link icon for AskPython">
 &nbsp; AskPython article
 [Python Keywords](https://www.askpython.com/python/python-keywords)
 …
 <!--- [ref 20221223°1518] --->

&nbsp;

<img src="./icos/20110921o1121.stackoverflow.v1.x0048y0048.gif" align="left" width="32" height="32" alt="Link icon for StackOverflow">
 &nbsp; StackOverflow thread
 [What are "soft keywords"?](https://stackoverflow.com/questions/65800344/what-are-soft-keywords)
 sheds light on the difference between Python 'keywords' and 'soft keywords'.
 Synopsis: Soft keywords are context sensitive, they are allowed as identifiers
 in non-relevevant contexts.
 <!--- [ref 20221223°1522] --->

&nbsp;

<img src="./icos/20221223o1543.codeitbro.v1.x0048y0048.png" align="left" width="32" height="32" alt="Link icon for CodeItBro">
 &nbsp; CodeItBro tutorial
 [Python Reserved Words List With Definitions and Examples](https://www.codeitbro.com/python-reserved-words-list/)
 lists all Python keywords with a short code snippet for each.
 <!--- [ref 20221223°1542] --->

&nbsp;

<img src="./icos/20221223o1533.flixiple.v2.x0048y0048.png" align="left" width="32" height="32" alt="Link icon for Flexiple">
 &nbsp; Flexiple tutorial
 [Python Reserved Words List - Your Complete Guide](https://flexiple.com/python/python-reserved-words/)
 provides one code snippet per keyword.
 (Downside: In the output boxes, they consequently print `TRUE`
 and `FALSE` instead `True` and `False`.)
 <!--- [ref 20221223°1532] --->

&nbsp;


## Java &nbsp; <sup><sub>[jv117keywords.java](./../java/jv117keywords.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jv117keywords.png">
 <img src ="./runs/jv117keywords.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv117keywords.java">
 </a --->

&nbsp;

<img src="./icos/20200630o0143.w3schools.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for W3Schools.com">
 &nbsp; W3Schools.com article
 [Java Keywords](https://www.w3schools.com/java/java_ref_keywords.asp)
 …
 <!--- [ref 20221224°0912] --->

&nbsp;

<img src="./icos/20220320o1415.oracle.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for ">
 &nbsp; Oracle documentation
 [Java Language Keywords](https://docs.oracle.com/javase/tutorial/java/nutsandbolts/_keywords.html)
 …
 <!--- [ref 20221224°0914] --->

&nbsp;

<img src="./icos/20110921o1125.wikipedia.v1.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Wikipedia">
 &nbsp; Wikipedia article
 [List of Java keywords](https://en.wikipedia.org/wiki/List_of_Java_keywords)
 …
 <!--- [ref 20221224°0916] --->

&nbsp;

<img src="./icos/20221224o0923.dataflair.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for ">
 &nbsp; DataFlair tutorial
 [Java Keywords – List of 51 Keywords with Examples](https://data-flair.training/blogs/java-keywords/)
 …
 <!--- [ref 20221224°0922] --->

&nbsp;

<img src="./icos/20220214o1157.rollbar.v1.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Rollbar">
 &nbsp; Rollbar article
 [Java Exceptions Hierarchy Explained](https://rollbar.com/blog/java-exceptions-hierarchy-explained/)
 …
 <!--- [ref 20221224°0932] --->

&nbsp;


## C-Sharp &nbsp; <sup><sub>[cs117keywords.cs](./../cs/cs117keywords.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs117keywords.png">
 <img src ="./runs/cs117keywords.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs117keywords.cs">
 </a --->

&nbsp;

<img src="./icos/20170811o0621.microsoftgray.v5.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Microsoft">
 &nbsp; Microsoft Learn article
 [C# Keywords](https://learn.microsoft.com/en-us/dotnet/csharp/language-reference/keywords/)
 seems the only really complete article. It also explains
 the difference between *keywords* and *contextual keywords*
 , similar like in Python the hard and soft keywords.
 <!--- [ref 20221224°0942] --->

&nbsp;

<img src="./icos/20200629o1223.csharpcorner.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for CSharpCorner">
 &nbsp; C-Sharp Corner article
 [C# Keywords](https://www.c-sharpcorner.com/blogs/c-sharp-keywords)
 provides a nice table and does a categorization …
 <!--- [ref 20221224°0944] --->

&nbsp;

<img src="./icos/20220809o1656.w3schools-in.v1.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for W3Schools.in">
 &nbsp; W3Schools.in article
 [C# Keywords](https://www.w3schools.in/csharp/keywords)
 …
 <!--- [ref 20221224°0946] --->

&nbsp;

<img src="./icos/20220901o1013.w3schools-blog.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for W3Schools.blog">
 &nbsp; W3Schools.blog article
 [C Sharp Keywords](https://www.w3schools.blog/c-sharp-keywords)
 … (not soo exciting)
 <!--- [ref 20221224°0948] --->

&nbsp;

<img src="./icos/20200206o1533.javatpoint.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for JavaTPoint">
 &nbsp; Java-T-Point article
 [# Keywords](https://www.javatpoint.com/csharp-keywords)
 … (not soo exciting)
 <!--- [ref 20221224°0952] --->

&nbsp;

<img src="./icos/20200630o0223.tutorialsteacher.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for TutorialsTeacher">
 &nbsp; TutorialsTeacher article
 [C# Keywords](https://www.tutorialsteacher.com/csharp/csharp-keywords)
 provides a table and separates 11 contextual keywords …
 <!--- [ref 20221224°0954] --->

&nbsp;

<img src="./icos/20200611o1833.markheath.v1.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Mark Heath">
 &nbsp; Mark Heath' blog article
 [10 C# keywords you should be using](https://www.markheath.net/post/10-c-keywords-you-should-be-using)
 explains some not-so-often used keywords, even `goto`.
 <!--- [ref 20221224°1012] --->

&nbsp;

<img src="./icos/20170811o0621.microsoftgray.v5.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Microsoft">
 &nbsp; Microsoft Learn article
 [yield statement - provide the next element](https://learn.microsoft.com/en-us/dotnet/csharp/language-reference/statements/yield)
 explains the `yield` keyword, which is used for the example …
 <!--- [ref 20221227°0912] --->

&nbsp;

<img src="./icos/20110921o1121.stackoverflow.v1.x0032y0032.gif" align="left" width="32" height="32" alt="Link icon for StackOverflow">
 &nbsp; StackOverflow thread
 [Predefined type 'System.Runtime.CompilerServices.IsExternalInit' is not defined or imported &lt;duplicate&gt;](https://stackoverflow.com/questions/64749385/predefined-type-system-runtime-compilerservices-isexternalinit-is-not-defined)
 was wanted for the yield example to run …
 <!--- [ref 20221227°0918] --->

&nbsp;


## JavaScript &nbsp; <sup><sub>[js117keywords.js](./../js/js117keywords.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js117keywords.png">
 <img src ="./runs/js117keywords.png" width="650" height="33" data-dims="x1084y0033" alt="Run js117keywords.js">
 </a --->

&nbsp;

<img src="./icos/20180511o0333.mozilla1logo.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Mozilla">
 &nbsp; Mozilla reference
 [Lexical grammar](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Lexical_grammar)
 lists the keywords beside all the other language grammar
 <!--- [ref 20221226°0912] --->

&nbsp;

<img src="./icos/20200630o0143.w3schools.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for W3Schools.org">
 &nbsp; W3Schools.org article
 [JavaScript Reserved Words](https://www.w3schools.com/js/js_reserved.asp)
 lists the keywords in a simple manner.
 <!--- [ref 20221226°0914] --->

&nbsp;

<img src="./icos/20220809o1656.w3schools-in.v1.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for W3Schools.in">
 &nbsp; W3Schools.in article
 [What are JavaScript keywords?](https://www.w3schools.in/javascript/keywords/)
 …
 <!--- [ref 20221226°0916] --->

&nbsp;

<img src="./icos/20180511o0333.mozilla1logo.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Mozilla">
 &nbsp; Mozilla article
 [**await**](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/await)
 explains the `await` keyword, which is used as the example keyword …
 <!--- [ref 20221227°0932] --->

&nbsp;


## PHP &nbsp; <sup><sub>[ph117keywords.php](./../php/ph117keywords.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph117keywords.png">
 <img src ="./runs/ph117keywords.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph117keywords.php">
 </a --->

&nbsp;

<img src="./icos/20201130o1043.php-net.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for PHP manual">
 &nbsp; PHP manual article
 [List of Keywords](https://www.php.net/manual/en/reserved.keywords.php)
 <!--- [ref 20221226°0922] --->
 (english) or
 [Liste der Schlüsselwörter](https://www.php.net/manual/de/reserved.keywords.php)
 (deutsch). Curiously, they also list functions as keywords. …
 <!--- [ref 20221226°0924] --->

&nbsp;

<img src="./icos/20200630o0143.w3schools.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for W3Schools.org">
 &nbsp; W3Schools.org article
 [PHP Keywords](https://www.w3schools.com/php/php_ref_keywords.asp)
 …
 <!--- [ref 20221226°0926] --->

&nbsp;

<img src="./icos/20221226o0933.koderhq.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for KoderHq">
 &nbsp; KoderHq tutorial
 [PHP Keywords & Identifiers Tutorial](https://www.koderhq.com/tutorial/php/keywords-identifiers/)
 explains the identifier writing styles and it even mentions the difference
 between lvalues and rvalues …
 <!--- [ref 20221226°0932] --->

&nbsp;

<img src="./icos/20201130o1043.php-net.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for PHP">
 &nbsp; PHP manual article
 [Generator syntax](https://www.php.net/manual/en/language.generators.syntax.php)
 explains the use of keyword `yield`.
 <i>The demo is written after 'Example #1 A simple example of yielding values'.</i>
 <!--- [ref 20221227°0954] --->

&nbsp;


## CPP &nbsp; <sup><sub>[cp117keywords.cpp](./../cpp/cp117keywords.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp117keywords.png">
 <img src ="./runs/cp117keywords.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp117keywords.cpp">
 </a --->

&nbsp;

<img src="./icos/20200109o0113.cppreference.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for CppReference">
 &nbsp; CppReference
 [C++ keywords](https://en.cppreference.com/w/cpp/keyword)
 provides a complete table with the keywords linked to dedicated chapters.
 <!--- [ref 20221226°0942] --->

&nbsp;

<img src="./icos/20170811o0621.microsoftgray.v5.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Microsoft">
 &nbsp; Microsoft article
 [Keywords (C++)](https://learn.microsoft.com/en-us/cpp/cpp/keywords-cpp)
 provides a complete table with the keywords, linked to dedicated chapters,
 as well tables for the Microsoft specific keywords and other special cases.
 <!--- [ref 20221226°0944] --->

&nbsp;

<img src="./icos/20200911o1016.bogotobogo.v4.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for BogoToBogo">
 &nbsp; BogoToBogo tutorial
 [C++ Keywords](https://www.bogotobogo.com/cplusplus/cplusplus_keywords.php)
 <!--- [ref 20221226°0946] --->
 shows a table with the valid main function calls, then a list with
 the keywords with a short description for each, and most of them linked
 to dedicated sections with demo codes.
 The example code is inspired by the code in article
 [Type-Conversion Operators](https://www.bogotobogo.com/cplusplus/typecast.php)
 <!--- [ref 20221226°0947] --->

&nbsp;

<img src="./icos/20141115o1622.wikibooks.v1.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for WikiBooks">
 &nbsp; WikiBooks page
 [C++ Programming: Language Keywords](https://en.wikibooks.org/wiki/C%2B%2B_Programming/Programming_Languages/C%2B%2B/Code/Keywords)
 …
 <!--- [ref 20221226°0952] --->

&nbsp;

<img src="./icos/20200521o1628.freecodecamp.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for FreeCodeCamp">
 &nbsp; FreeCodeCamp article
 [C++ Keywords You Should Know](https://www.freecodecamp.org/news/cpp-keywords-you-should-know/)
 …
 <!--- [ref 20221226°0954] --->

&nbsp;

<img src="./icos/20221224o1003.programiz.v1.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Programiz">
 &nbsp; Programiz article
 [List of all Keywords in C Language](https://www.programiz.com/c-programming/list-all-keywords-c-language)
 lists the C keywords after categories and links each to a dedicated chapter.
 <!--- [ref 202212241002] --->

&nbsp;


---

<sup><sub>*[File 20221217°0901 x117keywords.md]* ⬞Ω</sub></sup>
