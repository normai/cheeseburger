﻿# Demonstrate For-Each-Loop

The For-Each-Loop is looping over a container object, and it loops as often
 as elements are in the container. With each loop, it provides the element
 in question as a variable to be used inside the loop body.

Technical language: You *iterate* over the elements of the container.

## Python &nbsp; <sup><sub>[py250foreach.py](./../py/py250foreach.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

<a href="./runs/py250foreach.png">
 <img src ="./runs/py250foreach.png" width="654" height="312" data-dims="x1091y0521" alt="Run py250foreach.py">
 </a>

&nbsp;


## Java &nbsp; <sup><sub>[jv250foreach.java](./../java/jv250foreach.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jv250foreach.png">
 <img src ="./runs/jv250foreach.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv250foreach.java">
 </a --->

&nbsp;


## C-Sharp &nbsp; <sup><sub>[cs250foreach.cs](./../cs/cs250foreach.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs250foreach.png">
 <img src ="./runs/cs250foreach.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs250foreach.cs">
 </a --->

&nbsp;


## JavaScript &nbsp; <sup><sub>[js250foreach.js](./../js/js250foreach.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js250foreach.png">
 <img src ="./runs/js250foreach.png" width="650" height="33" data-dims="x1084y0033" alt="Run js250foreach.js">
 </a --->

<img src="./icos/20200521o1628.freecodecamp.v2.x0048y0048.png" align="left" style="margin-right:1.1em;" width="48" height="48" alt="Link icon for FreeCodeCamp">
 &nbsp; FreeCodeCamp article
 [JavaScript forEach – How to Loop Through an Array in JS](https://www.freecodecamp.org/news/javascript-foreach-how-to-loop-through-an-array-in-js/)
 <!--- [ref 20221001°0923] --->

&nbsp;


## PHP &nbsp; <sup><sub>[ph250foreach.php](./../php/ph250foreach.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph250foreach.png">
 <img src ="./runs/ph250foreach.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph250foreach.php">
 </a --->

&nbsp;


## CPP &nbsp; <sup><sub>[cp250foreach.cpp](./../cpp/cp250foreach.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp250foreach.png">
 <img src ="./runs/cp250foreach.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp250foreach.cpp">
 </a --->

&nbsp;


---

<sup><sub><sup>*[File 20220826°1801 x250foreach.md]* ⬞Ω</sup></sub></sup>
