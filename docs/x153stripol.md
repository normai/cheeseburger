﻿# Demonstrate String Interpolation

Often we have some constant text block, where at runtime values shall be
 inserted dynamically. For this demand, all programming languages offer
 a special syntax called 'string interpolation'.

Note: Compare demo x335format.

## Python &nbsp; <sup><sub>[py153stripol.py](./../py/py153stripol.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

<a href="./runs/py153stripol.png">
 <img src ="./runs/py153stripol.png" width="637" height="213" data-dims="x0980y0329" alt="Run py153stripol.py">
 </a>

&nbsp;


## Java &nbsp; <sup><sub>[jv153stripol.java](./../java/jv153stripol.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jv153stripol.png">
 <img src ="./runs/jv153stripol.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv153stripol.java">
 </a --->

&nbsp;


## C-Sharp &nbsp; <sup><sub>[cs153stripol.cs](./../cs/cs153stripol.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs153stripol.png">
 <img src ="./runs/cs153stripol.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs153stripol.cs">
 </a --->

&nbsp;

<img src="./icos/20170811o0621.microsoftgray.v5.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Microsoft">
 &nbsp; Microsoft reference
 [String.Format Method](https://learn.microsoft.com/en-us/dotnet/api/system.string.format?view=net-7.0)
 …
 <!--- [ref 20221227°1932] --->

&nbsp;

<img src="./icos/20170811o0621.microsoftgray.v5.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Microsoft">
 &nbsp; Microsoft Learn article
 [String interpolation using `$`](https://learn.microsoft.com/en-us/dotnet/csharp/language-reference/tokens/interpolated)
 …
 <!--- [ref 20221227°1934] --->

&nbsp;


## JavaScript &nbsp; <sup><sub>[js153stripol.js](./../js/js153stripol.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js153stripol.png">
 <img src ="./runs/js153stripol.png" width="650" height="33" data-dims="x1084y0033" alt="Run js153stripol.js">
 </a --->

&nbsp;


## PHP &nbsp; <sup><sub>[ph153stripol.php](./../php/ph153stripol.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph153stripol.png">
 <img src ="./runs/ph153stripol.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph153stripol.php">
 </a --->

&nbsp;


## CPP &nbsp; <sup><sub>[cp153stripol.cpp](./../cpp/cp153stripol.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp153stripol.png">
 <img src ="./runs/cp153stripol.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp153stripol.cpp">
 </a --->

&nbsp;


---

<sup><sub><sup>*[File 20221012°1601 x153stripol.md]* ⬞Ω</sup></sub></sup>
