﻿# Demonstrate Print Functions

## Python &nbsp; <sup><sub>[py171print.py](./../py/py171print.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

<!--- a href="./runs/py171print.png">
 <img src ="./runs/py171print.png" width="650" height="33" data-dims="x1084y0033" alt="Run py171print.py">
 </a --->

&nbsp;


## Java &nbsp; <sup><sub>[jv171print.java](./../java/jv171print.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jv171print.png">
 <img src ="./runs/jv171print.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv171print.java">
 </a --->

&nbsp;


## C-Sharp &nbsp; <sup><sub>[cs171print.cs](./../cs/cs171print.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs171print.png">
 <img src ="./runs/cs171print.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs171print.cs">
 </a --->

&nbsp;


## JavaScript &nbsp; <sup><sub>[js171print.js](./../js/js171print.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js171print.png">
 <img src ="./runs/js171print.png" width="650" height="33" data-dims="x1084y0033" alt="Run js171print.js">
 </a --->

&nbsp;


## PHP &nbsp; <sup><sub>[ph171print.php](./../php/ph171print.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph171print.png">
 <img src ="./runs/ph171print.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph171print.php">
 </a --->

&nbsp;


## CPP &nbsp; <sup><sub>[cp171print.cpp](./../cpp/cp171print.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp171print.png">
 <img src ="./runs/cp171print.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp171print.cpp">
 </a --->

&nbsp;


---

<sup><sub><sup>*[File 20220827°1301]* ⬞Ω</sup></sub></sup>
