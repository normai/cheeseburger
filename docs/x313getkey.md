﻿# Demonstrate Ggetkey Function (Windows only)

## Python &nbsp; <sup><sub>[py313getkey.py](./../py/py313getkey.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

<!--- a href="./runs/py313getkey.png">
 <img src ="./runs/py313getkey.png" width="650" height="33" data-dims="x1084y0033" alt="Run py313getkey.py">
 </a --->

&nbsp;


## Java &nbsp; <sup><sub>[jv313getkey.java](./../java/jv313getkey.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jv313getkey.png">
 <img src ="./runs/jv313getkey.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv313getkey.java">
 </a --->

&nbsp;


## C-Sharp &nbsp; <sup><sub>[cs313getkey.cs](./../cs/cs313getkey.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs313getkey.png">
 <img src ="./runs/cs313getkey.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs313getkey.cs">
 </a --->

&nbsp;


## JavaScript &nbsp; <sup><sub>[js313getkey.js](./../js/js313getkey.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js313getkey.png">
 <img src ="./runs/js313getkey.png" width="650" height="33" data-dims="x1084y0033" alt="Run js313getkey.js">
 </a --->

&nbsp;


## PHP &nbsp; <sup><sub>[ph313getkey.php](./../php/ph313getkey.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph313getkey.png">
 <img src ="./runs/ph313getkey.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph313getkey.php">
 </a --->

&nbsp;


## CPP &nbsp; <sup><sub>[cp313getkey.cpp](./../cpp/cp313getkey.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp313getkey.png">
 <img src ="./runs/cp313getkey.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp313getkey.cpp">
 </a --->

&nbsp;


---

<sup><sub><sup>*[File 20220827°1501]* ⬞Ω</sup></sub></sup>
