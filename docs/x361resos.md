﻿# Demonstrate Resource Handling

Resources are objects you are provided by the operating system, they may
 reside on the network and you will perhaps share them with others.
 Resources are notably files, network connections, memory.

The crucial point with resources is, that you have to open them before you
 can use them, and if you are finished with them, you shall close them.
 Forgetting the close one is very unpolite, because you may lock out others
 from using it.

So many programming languages offer a 'using' clause which cares automatically
 for closing.

## Python &nbsp; <sup><sub>[py361resos.py](./../py/py361resos.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

<!--- a href="./runs/py361resos.png">
 <img src ="./runs/py361resos.png" width="650" height="33" data-dims="x1084y0033" alt="Run py361resos.py">
 </a --->

&nbsp;


## Java &nbsp; <sup><sub>[jv361resos.java](./../java/jv361resos.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jv361resos.png">
 <img src ="./runs/jv361resos.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv361resos.java">
 </a --->

&nbsp;


## C-Sharp &nbsp; <sup><sub>[cs361resos.cs](./../cs/cs361resos.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs361resos.png">
 <img src ="./runs/cs361resos.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs361resos.cs">
 </a --->

&nbsp;


## JavaScript &nbsp; <sup><sub>[js361resos.js](./../js/js361resos.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js361resos.png">
 <img src ="./runs/js361resos.png" width="650" height="33" data-dims="x1084y0033" alt="Run js361resos.js">
 </a --->

&nbsp;


## PHP &nbsp; <sup><sub>[ph361resos.php](./../php/ph361resos.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph361resos.png">
 <img src ="./runs/ph361resos.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph361resos.php">
 </a --->

&nbsp;


## CPP &nbsp; <sup><sub>[cp361resos.cpp](./../cpp/cp361resos.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp361resos.png">
 <img src ="./runs/cp361resos.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp361resos.cpp">
 </a --->

&nbsp;


---

<sup><sub>*[File 20221223°0901 x361resos.md]* ⬞Ω</sub></sup>
