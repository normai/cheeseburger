﻿# Demonstrate While Loops

While loops are the basic loops. They are universal, but the control of them
 happens distributed on various lines. This are three elements:
 (1) Some control variable must be initialized (2) Some control must happen
 to decide if the loop is executed or not and (3) Some variable must change
 so the loop can find an end.

Of course you can have an endless loop with a plain `while True`, and later
 exit the loop with a `break`. But that is not considered clean programming,
 such thing may be considered a workaround or even a *code smell*.

This is what the For-Loop later does, for special simple cases: It combines
 the three control elements into one single line, which is easily spottet.

## Python &nbsp; <sup><sub>[py230while.py](./../py/py230while.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

<a href="./runs/py230while.png">
 <img src ="./runs/py230while.png" width="644" height="176" data-dims="x0992y0271" alt="Run py230while.py">
 </a>

&nbsp;


## Java &nbsp; <sup><sub>[jv230while.java](./../java/jv230while.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jv230while.png">
 <img src ="./runs/jv230while.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv230while.java">
 </a --->

&nbsp;


## C-Sharp &nbsp; <sup><sub>[cs230while.cs](./../cs/cs230while.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs230while.png">
 <img src ="./runs/cs230while.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs230while.cs">
 </a --->

&nbsp;


## JavaScript &nbsp; <sup><sub>[js230while.js](./../js/js230while.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js230while.png">
 <img src ="./runs/js230while.png" width="650" height="33" data-dims="x1084y0033" alt="Run js230while.js">
 </a --->

&nbsp;


## PHP &nbsp; <sup><sub>[ph230while.php](./../php/ph230while.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph230while.png">
 <img src ="./runs/ph230while.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph230while.php">
 </a --->

&nbsp;


## CPP &nbsp; <sup><sub>[cp230while.cpp](./../cpp/cp230while.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp230while.png">
 <img src ="./runs/cp230while.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp230while.cpp">
 </a --->

&nbsp;


---

<sup><sub><sup>*[File 20220826°1501 x230while.md]* ⬞Ω</sup></sub></sup>
