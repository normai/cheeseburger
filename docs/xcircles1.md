﻿# Demonstrate How to Paint Circles

Drawing circles on the console ... involves the same algorithms as
 drawing circles on a GUI canvas, it is just more coarse-grained.

## Python &nbsp; <sup><sub>[pycircles1.py](./../py/pycircles1.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

<a href="./runs/pycircles1.png">
 <img src ="./runs/pycircles1.png" width="650" height="474" data-dims="x1084y0791" alt="Run pycircles1.py">
 </a>

## Java &nbsp; <sup><sub>[jvcircles1.java](./../java/jvcircles1.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jvcircles1.png">
 <img src ="./runs/jvcircles1.png" width="650" height="33" data-dims="x1084y0033" alt="Run jvcircles1.java">
 </a --->


## C-Sharp &nbsp; <sup><sub>[cscircles1.cs](./../cs/cscircles1.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cscircles1.png">
 <img src ="./runs/cscircles1.png" width="650" height="33" data-dims="x1084y0033" alt="Run cscircles1.cs">
 </a --->


## JavaScript &nbsp; <sup><sub>[jscircles1.js](./../js/jscircles1.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/jscircles1.png">
 <img src ="./runs/jscircles1.png" width="650" height="33" data-dims="x1084y0033" alt="Run jscircles1.js">
 </a --->


## PHP &nbsp; <sup><sub>[phcircles1.php](./../php/phcircles1.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/phcircles1.png">
 <img src ="./runs/phcircles1.png" width="650" height="33" data-dims="x1084y0033" alt="Run phcircles1.php">
 </a --->


## CPP &nbsp; <sup><sub>[cpcircles1.cpp](./../cpp/cpcircles1.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cpcircles1.png">
 <img src ="./runs/cpcircles1.png" width="650" height="33" data-dims="x1084y0033" alt="Run cpcircles1.cpp">
 </a --->


---

<sup><sub>*[File 20220928°1801 xcircles1.md]* ⬞Ω</sub></sup>
