﻿# Demonstrate Yield

The `yield` keyword allows to create generator functions, which then
 allow to apply For-Each loops on that object …

## Python &nbsp; <sup><sub>[py735yield.py](./../py/py735yield.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

<!--- a href="./runs/py735yield.png">
 <img src ="./runs/py735yield.png" width="650" height="33" data-dims="x1084y0033" alt="Run py735yield.py">
 </a --->

&nbsp;


## Java &nbsp; <sup><sub>[jv735yield.java](./../java/jv735yield.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jv735yield.png">
 <img src ="./runs/jv735yield.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv735yield.java">
 </a --->

&nbsp;


## C-Sharp &nbsp; <sup><sub>[cs735yield.cs](./../cs/cs735yield.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs735yield.png">
 <img src ="./runs/cs735yield.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs735yield.cs">
 </a --->

&nbsp;


## JavaScript &nbsp; <sup><sub>[js735yield.js](./../js/js735yield.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js735yield.png">
 <img src ="./runs/js735yield.png" width="650" height="33" data-dims="x1084y0033" alt="Run js735yield.js">
 </a --->

&nbsp;


## PHP &nbsp; <sup><sub>[ph735yield.php](./../php/ph735yield.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph735yield.png">
 <img src ="./runs/ph735yield.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph735yield.php">
 </a --->

&nbsp;

<img src="./icos/20201130o1043.php-net.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for PHP">
 &nbsp; PHP manual article
 [Generators](https://www.php.net/manual/en/language.generators.php)
 , the frontpage for the three following pages.
 <!--- [ref 20221227°0952] --->

&nbsp;

<img src="./icos/20201130o1043.php-net.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for PHP">
 &nbsp; PHP manual article
 [Generators overview](https://www.php.net/manual/en/language.generators.overview.php)
 …
 <!--- [ref 20221227°0953] --->

&nbsp;

<img src="./icos/20201130o1043.php-net.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for PHP">
 &nbsp; PHP manual article
 [Generator syntax](https://www.php.net/manual/en/language.generators.syntax.php)
 …
 <!--- [ref 20221227°0954] --->

&nbsp;

<img src="./icos/20201130o1043.php-net.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for PHP">
 &nbsp; PHP manual article
 [Comparing generators with Iterator objects](https://www.php.net/manual/en/language.generators.comparison.php)
 …
 <!--- [ref 20221227°0955] --->

&nbsp;


## CPP &nbsp; <sup><sub>[cp735yield.cpp](./../cpp/cp735yield.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp735yield.png">
 <img src ="./runs/cp735yield.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp735yield.cpp">
 </a --->

&nbsp;

<img src="./icos/20110921o1121.stackoverflow.v1.x0032y0032.gif" align="left" width="32" height="32" alt="Link icon for StackOverflow">
 &nbsp; StackOverflow thread
 [C++ Equivalent of C# Yield?](https://stackoverflow.com/questions/7213839/c-equivalent-of-c-sharp-yield)
 shows, that C++ has no `yield` or equivalent keyword. But it also shows
 alternative ways ways of how to achieve the equivalent effect.
 <!--- [ref 20221227°1222] --->

&nbsp;

<img src="./icos/20200109o0113.cppreference.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for CppReference">
 &nbsp; CppReference article
 [std::this_thread::yield](https://en.cppreference.com/w/cpp/thread/yield)
 explains the `yield` function, which C++ offers.
 But that does something very different from what the keyword `yield` does
 in C# or PHP.
 <!--- [ref 20221227°1224] --->

&nbsp;


---

<sup><sub>*[File 20221227°1001 x735yield.md]* ⬞Ω</sub></sup>
