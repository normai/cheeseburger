﻿# Demonstrate Comparison Operators

Comparison operations always yield a boolean. They are typically used
 with `if` and `while` statements.

## Python &nbsp; <sup><sub>[py123compare.py](./../py/py123compare.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

<a href="./runs/py123compare.png">
 <img src ="./runs/py123compare.png" width="644" height="640" data-dims="x0992y0986" alt="Run py123compare.py">
 </a>

&nbsp;


## Java &nbsp; <sup><sub>[jv123compare.java](./../java/jv123compare.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jv123compare.png">
 <img src ="./runs/jv123compare.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv123compare.java">
 </a --->

&nbsp;


## C-Sharp &nbsp; <sup><sub>[cs123compare.cs](./../cs/cs123compare.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs123compare.png">
 <img src ="./runs/cs123compare.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs123compare.cs">
 </a --->

&nbsp;


## JavaScript &nbsp; <sup><sub>[js123compare.js](./../js/js123compare.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js123compare.png">
 <img src ="./runs/js123compare.png" width="650" height="33" data-dims="x1084y0033" alt="Run js123compare.js">
 </a --->

&nbsp;


## PHP &nbsp; <sup><sub>[ph123compare.php](./../php/ph123compare.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph123compare.png">
 <img src ="./runs/ph123compare.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph123compare.php">
 </a --->

&nbsp;


## CPP &nbsp; <sup><sub>[cp123compare.cpp](./../cpp/cp123compare.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp123compare.png">
 <img src ="./runs/cp123compare.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp123compare.cpp">
 </a --->

&nbsp;


---

<sup><sub><sup>*[File 20220826°1001 x123compare.md]* ⬞Ω</sup></sub></sup>
