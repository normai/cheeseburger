﻿# Demonstrate Ternary Operator = Conditional Operator = ?:-Operator

The ternary operator looks like an If-Else structure on first sight. But
 the crucial difference is this:
- The If-Else is a full control stucture, which takes it's room in the program flow
- The ternary operator is just an expression, which can stand anywhere, where a value is wanted,
   e.g. inside the parameter parentheses of a function call

*Note/Todo. Other operators worth a dedicated demo are e.g.:
 • Member operator `.`
 • New/Del operator
 • Funktion operator `()`
 • Type operator*

**Userstory**: Throw a dice an tell whether it was more than three or not.

## Python &nbsp; <sup><sub>[py127ternary.py](./../py/py127ternary.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

<!--- a href="./runs/py127ternary.png">
 <img src ="./runs/py127ternary.png" width="650" height="33" data-dims="x1084y0033" alt="Run py127ternary.py">
 </a --->

&nbsp;

<img src="./icos/20200805o1243.python.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Python">
 &nbsp; Python documentation chapter
 [6.13. Conditional expressions](https://docs.python.org/3/reference/expressions.html#conditional-expressions)
 …
 <!--- [ref 20221227°1442] --->

&nbsp;

<img src="./icos/20220719o1753.askpython.v1.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for AskPython">
 &nbsp; AskPython article
 [Python ternary Operator Examples](https://www.askpython.com/python/python-ternary-operator)
 …
 <!--- [ref 20221227°1452] --->

&nbsp;

<img src="./icos/20221227o1513.pythongeeks.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for PythonGeeks">
 &nbsp; PythonGeeks article
 [Python Ternary Operator with Example](https://pythongeeks.org/python-ternary-operators/)
 …
 <!--- [ref 20221227°1512] --->

&nbsp;

<img src="./icos/20221227o1523.golinuxcloud.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for GoLinuxCloud">
 &nbsp; GoLinuxCloud article
 [Python Ternary Operator Explained [Easy Examples]](https://www.golinuxcloud.com/python-ternary-operator/)
 explains the operator and offers alternative ways to achieve the same effect …
 <!--- [ref 20221227°1522] --->

&nbsp;


## Java &nbsp; <sup><sub>[jv127ternary.java](./../java/jv127ternary.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jv127ternary.png">
 <img src ="./runs/jv127ternary.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv127ternary.java">
 </a --->

&nbsp;

<img src="./icos/20220320o1415.oracle.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Oracle">
 &nbsp; Oracle documentation
 [Equality, Relational, and Conditional Operators](https://docs.oracle.com/javase/tutorial/java/nutsandbolts/op2.html)
 …
 <!--- [ref 20221227°1532] --->

&nbsp;

<img src="./icos/20200630o0143.w3schools.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for W3Schools.com">
 &nbsp; W3Schools.com article
 [Java Short Hand If...Else (Ternary Operator)](https://www.w3schools.com/java/java_conditions_shorthand.asp)
 …
 <!--- [ref 20221227°1534] --->

&nbsp;


## C-Sharp &nbsp; <sup><sub>[cs127ternary.cs](./../cs/cs127ternary.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs127ternary.png">
 <img src ="./runs/cs127ternary.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs127ternary.cs">
 </a --->

&nbsp;

<img src="./icos/20170811o0621.microsoftgray.v5.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Microsoft">
 &nbsp; Microsoft Learn
 [?: operator - the ternary conditional operator](https://learn.microsoft.com/en-us/dotnet/csharp/language-reference/operators/conditional-operator)
 …
 <!--- [ref 20221227°1542] --->

&nbsp;


## JavaScript &nbsp; <sup><sub>[js127ternary.js](./../js/js127ternary.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js127ternary.png">
 <img src ="./runs/js127ternary.png" width="650" height="33" data-dims="x1084y0033" alt="Run js127ternary.js">
 </a --->

&nbsp;

<img src="./icos/20180511o0333.mozilla1logo.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Mozilla">
 &nbsp; Mozilla documentation
 [Conditional (ternary) operator](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Conditional_Operator)
 …
 <!--- [ref 20221227°1552] --->

&nbsp;


## PHP &nbsp; <sup><sub>[ph127ternary.php](./../php/ph127ternary.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph127ternary.png">
 <img src ="./runs/ph127ternary.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph127ternary.php">
 </a --->

&nbsp;

<img src="./icos/20201130o1043.php-net.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for PHP">
 &nbsp; PHP manual chapter
 [Comparison Operators](https://www.php.net/manual/en/language.operators.comparison.php)
 (pretty far down on that page) …
 <!--- [ref 20221227°1554] --->

&nbsp;


## CPP &nbsp; <sup><sub>[cp127ternary.cpp](./../cpp/cp127ternary.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp127ternary.png">
 <img src ="./runs/cp127ternary.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp127ternary.cpp">
 </a --->

&nbsp;

<img src="./icos/20200109o0113.cppreference.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for CppReference">
 &nbsp; CppReference page
 [Other operators](https://en.cppreference.com/w/cpp/language/operator_other)
 …
 <!--- [ref 20221227°1612] --->

&nbsp;

<img src="./icos/20200630o0143.w3schools.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for W3Schools.com">
 &nbsp; W3Schools.com article
 [C++ Short Hand If Else](https://www.w3schools.com/cpp/cpp_conditions_shorthand.asp)
 …
 <!--- [ref 20221227°1614] --->

&nbsp;


---

<sup><sub>*[File 20221002°0901 x127ternary.md]* ⬞Ω</sub></sup>
