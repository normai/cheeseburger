﻿# Demonstrates Variables

Variables provide named storage for short- or long-term storage of values.
 Variables typically change their value during their live time.

## Python &nbsp; <sup><sub>[py115varis.py](./../py/py115varis.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

<a href="./runs/py115varis.png">
 <img src ="./runs/py115varis.png" width="643" height="169" data-dims="x0990y0261" alt="Run py115varis.py">
 </a>

&nbsp;


## Java &nbsp; <sup><sub>[jv115varis.java](./../java/jv115varis.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jv115varis.png">
 <img src ="./runs/jv115varis.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv115varis.java">
 </a --->

&nbsp;


## C-Sharp &nbsp; <sup><sub>[cs115varis.cs](./../cs/cs115varis.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs115varis.png">
 <img src ="./runs/cs115varis.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs115varis.cs">
 </a --->

&nbsp;


## JavaScript &nbsp; <sup><sub>[js115varis.js](./../js/js115varis.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js115varis.png">
 <img src ="./runs/js115varis.png" width="650" height="33" data-dims="x1084y0033" alt="Run js115varis.js">
 </a --->

&nbsp;


## PHP &nbsp; <sup><sub>[ph115varis.php](./../php/ph115varis.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph115varis.png">
 <img src ="./runs/ph115varis.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph115varis.php">
 </a --->

&nbsp;


## CPP &nbsp; <sup><sub>[cp115varis.cpp](./../cpp/cp115varis.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp115varis.png">
 <img src ="./runs/cp115varis.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp115varis.cpp">
 </a --->

&nbsp;


---

<sup><sub><sup>*[File 20220825°1401 x115varis.md]* ⬞Ω</sup></sub></sup>
