﻿# Demonstrate Documentation Comments

Documentation comments are offered by most programming languages to
 to automatically extract a documentation over a project. …

## Python &nbsp; <sup><sub>[py767docoms.py](./../py/py767docoms.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

<!--- a href="./runs/py767docoms.png">
 <img src ="./runs/py767docoms.png" width="650" height="33" data-dims="x1084y0033" alt="Run py767docoms.py">
 </a --->

&nbsp;


## Java &nbsp; <sup><sub>[jv767docoms.java](./../java/jv767docoms.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jv767docoms.png">
 <img src ="./runs/jv767docoms.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv767docoms.java">
 </a --->

&nbsp;


## C-Sharp &nbsp; <sup><sub>[cs767docoms.cs](./../cs/cs767docoms.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs767docoms.png">
 <img src ="./runs/cs767docoms.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs767docoms.cs">
 </a --->

&nbsp;


## JavaScript &nbsp; <sup><sub>[js767docoms.js](./../js/js767docoms.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js767docoms.png">
 <img src ="./runs/js767docoms.png" width="650" height="33" data-dims="x1084y0033" alt="Run js767docoms.js">
 </a --->

&nbsp;


## PHP &nbsp; <sup><sub>[ph767docoms.php](./../php/ph767docoms.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph767docoms.png">
 <img src ="./runs/ph767docoms.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph767docoms.php">
 </a --->

&nbsp;


## CPP &nbsp; <sup><sub>[cp767docoms.cpp](./../cpp/cp767docoms.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp767docoms.png">
 <img src ="./runs/cp767docoms.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp767docoms.cpp">
 </a --->

&nbsp;


---

<sup><sub>*[File 20221227°1101 x767docoms.md]* ⬞Ω</sub></sup>
