﻿# Demonstrate Import Statement

Most languages have some mean to execute code not only from one file but
 from multiple files. One of the files has to be the start file, which
 uses code from the others.

The possibility to distribute code over multiple files is another
 important code structuring mean.

## Python &nbsp; <sup><sub>[py410import.py](./../py/py410import.py)</sub></sup>

<a href="./runs/py410import.png">
 <img src ="./runs/py410import.png" width="650" height="180" data-dims="x1084y0300" alt="Run py410import.py">
 </a>

&nbsp;


## Java &nbsp; <sup><sub>[jv410import.java](./../java/jv410import.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jv410import.png">
 <img src ="./runs/jv410import.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv410import.java">
 </a --->

&nbsp;


## C-Sharp &nbsp; <sup><sub>[cs410import.cs](./../cs/cs410import.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs410import.png">
 <img src ="./runs/cs410import.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs410import.cs">
 </a --->

&nbsp;


## JavaScript &nbsp; <sup><sub>[js410import.js](./../js/js410import.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js410import.png">
 <img src ="./runs/js410import.png" width="650" height="33" data-dims="x1084y0033" alt="Run js410import.js">
 </a --->

&nbsp;


## PHP &nbsp; <sup><sub>[ph410import.php](./../php/ph410import.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph410import.png">
 <img src ="./runs/ph410import.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph410import.php">
 </a --->

&nbsp;


## CPP &nbsp; <sup><sub>[cp410import.cpp](./../cpp/cp410import.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp410import.png">
 <img src ="./runs/cp410import.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp410import.cpp">
 </a --->

&nbsp;


---

<sup><sub><sup>*[File 20220828°1101 x410import.md]* ⬞Ω</sup></sub></sup>
