﻿# Demonstrate Read From Network

The most popular kind of reading from the network is surfing with a broswer,
 means retrieving a page from some remote server …

## Python &nbsp; <sup><sub>[py371surf.py](./../py/py371surf.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

<!--- a href="./runs/py371surf.png">
 <img src ="./runs/py371surf.png" width="650" height="33" data-dims="x1084y0033" alt="Run py371surf.py">
 </a --->

&nbsp;


## Java &nbsp; <sup><sub>[jv371surf.java](./../java/jv371surf.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jv371surf.png">
 <img src ="./runs/jv371surf.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv371surf.java">
 </a --->

&nbsp;


## C-Sharp &nbsp; <sup><sub>[cs371surf.cs](./../cs/cs371surf.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs371surf.png">
 <img src ="./runs/cs371surf.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs371surf.cs">
 </a --->

&nbsp;


## JavaScript &nbsp; <sup><sub>[js371surf.js](./../js/js371surf.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js371surf.png">
 <img src ="./runs/js371surf.png" width="650" height="33" data-dims="x1084y0033" alt="Run js371surf.js">
 </a --->

&nbsp;


## PHP &nbsp; <sup><sub>[ph371surf.php](./../php/ph371surf.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph371surf.png">
 <img src ="./runs/ph371surf.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph371surf.php">
 </a --->

&nbsp;


## CPP &nbsp; <sup><sub>[cp371surf.cpp](./../cpp/cp371surf.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp371surf.png">
 <img src ="./runs/cp371surf.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp371surf.cpp">
 </a --->

&nbsp;


---

<sup><sub>*[File 20221223°1001 x371surf.md]* ⬞Ω</sub></sup>
