# Demos for Random Functions

Generating random is hard for a computer. To make programmers live easier,
 the programming languages offer 'pseudo random numbers' as a starting point.
 They are good enough for many simple purposes.

But if the purposes demand higher standards, like e.g. cryptographic
 purposes do, we need to bring out bigger guns, and utilize more
 sophisticated functions from the specific programming language.

## Python &nbsp; <sup><sub>[py761random.py](./../py/py761random.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

<a href="./runs/py761random.png">
 <img src ="./runs/py761random.png" width="593" height="549" data-dims="x0989y0916" alt="Run py761random.py">
 </a>

&nbsp;

<img src="./icos/20200805o1243.python.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link to Python site">
 &nbsp; Python documentation page
 [random — Generate pseudo-random numbers](https://docs.python.org/3/library/random.html)
 <!--- [ref 20221214°1412 ico 20200805°1243] --->

&nbsp;

<img src="./icos/20220915o0913.datagy.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link to Datagy">
 &nbsp; Datagy tutorial
 [Generate Random Numbers in Python](https://datagy.io/python-random-number/)
 <!--- [ref 20221214°1414 ico 20220915°0913] --->

&nbsp;

## Java &nbsp; <sup><sub>[jv761random.java](./../java/jv761random.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jv761random.png">
 <img src ="./runs/jv761random.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv761random.java">
 </a --->

&nbsp;


## C-Sharp &nbsp; <sup><sub>[cs761random.cs](./../cs/cs761random.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs761random.png">
 <img src ="./runs/cs761random.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs761random.cs">
 </a --->

&nbsp;


## JavaScript &nbsp; <sup><sub>[js761random.js](./../js/js761random.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js761random.png">
 <img src ="./runs/js761random.png" width="650" height="33" data-dims="x1084y0033" alt="Run js761random.js">
 </a --->

&nbsp;


## PHP &nbsp; <sup><sub>[ph761random.php](./../php/ph761random.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph761random.png">
 <img src ="./runs/ph761random.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph761random.php">
 </a --->

&nbsp;


## CPP &nbsp; <sup><sub>[cp761random.cpp](./../cpp/cp761random.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp761random.png">
 <img src ="./runs/cp761random.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp761random.cpp">
 </a --->

&nbsp;


---

<sup><sub><sup>*[File 20220910°1301 x761random.md]* ⬞Ω</sup></sub></sup>
