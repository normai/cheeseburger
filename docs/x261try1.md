﻿# Demonstrate Simple Try Usage

The Try construct is helpful to catch runtime errors, e.g. due to
 missing resources.

## Python &nbsp; <sup><sub>[py261try1.py](./../py/py261try1.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

<a href="./runs/py261try1.png">
 <img src ="./runs/py261try1.png" width="654" height="236" data-dims="x1091y0394" alt="Run py261try1.py">
 </a>

&nbsp;


## Java &nbsp; <sup><sub>[jv261try1.java](./../java/jv261try1.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jv261try1.png">
 <img src ="./runs/jv261try1.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv261try1.java">
 </a --->

<img src="./icos/20220214o1157.rollbar.v1.x0048y0048.png" align="left" width="48" height="48" alt="Link icon for Rollbar">
 &nbsp; Rollbar article
 [Java Exceptions Hierarchy Explained](https://rollbar.com/blog/java-exceptions-hierarchy-explained/)
 …
 <!--- [ref 20221224°0932] --->

&nbsp;

## C-Sharp &nbsp; <sup><sub>[cs261try1.cs](./../cs/cs261try1.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs261try1.png">
 <img src ="./runs/cs261try1.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs261try1.cs">
 </a --->

&nbsp;


## JavaScript &nbsp; <sup><sub>[js261try1.js](./../js/js261try1.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js261try1.png">
 <img src ="./runs/js261try1.png" width="650" height="33" data-dims="x1084y0033" alt="Run js261try1.js">
 </a --->

&nbsp;


## PHP &nbsp; <sup><sub>[ph261try1.php](./../php/ph261try1.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph261try1.png">
 <img src ="./runs/ph261try1.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph261try1.php">
 </a --->

&nbsp;


## CPP &nbsp; <sup><sub>[cp261try1.cpp](./../cpp/cp261try1.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp261try1.png">
 <img src ="./runs/cp261try1.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp261try1.cpp">
 </a --->

&nbsp;


---

<sup><sub><sup>*[File 20220826°1901 x261try1.md]* ⬞Ω</sup></sub></sup>
