﻿# Demonstrate Integer Types

Integers are the most processor friendly data types. Integers are, with what
 the processors process everything, even strings and floats, indeed.

## Python &nbsp; <sup><sub>[py137int.py](./../py/py137int.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

In Python2 there was an int and a long type, the limits were told by
 a system constant `sys.maxint` and `-sys.maxint - 1` resp.

In Python3, there is no such limit. The `sys.maxint` constant does no
 more exist. Instead, one can possibly operate with `sys.maxsize`.

<a href="./runs/py137int.png">
 <img src ="./runs/py137int.png" width="637" height="226" data-dims="x0980y0348*.65" alt="Run py137int.py">
 </a>

&nbsp;

<img src="./icos/20210924o1933.delftstack.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for DelftStack">
 &nbsp; DelftStack article
 [Python sys.maxsize() Method](https://www.delftstack.com/api/python/python-sys-maxsize/)
 <!--- [ref 20221018°1312] --->

&nbsp;


## Java &nbsp; <sup><sub>[jv137int.java](./../java/jv137int.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

Java provides the classic byte/short/int/long types, but only in the
 signed flavour, not as unsigned.

<!--- a href="./runs/jv137int.png">
 <img src ="./runs/jv137int.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv137int.java">
 </a --->

&nbsp;


## C-Sharp &nbsp; <sup><sub>[cs137int.cs](./../cs/cs137int.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs137int.png">
 <img src ="./runs/cs137int.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs137int.cs">
 </a --->

<img src="./icos/20210303o1337.dotnetperls.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for DotNetPerls">
 &nbsp; DotNetPerls page
 [int.MaxValue, MinValue Examples](https://www.dotnetperls.com/int-maxvalue)
 …
 <!--- [ref 20221016°1242] --->

&nbsp;

<img src="./icos/20221016o1244.thedeveloperblog.v0.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for TheDeveloperBlog.com">
 &nbsp; TheDeveloperBlog.com article
 [C# Byte Type](https://thedeveloperblog.com/byte)
 <!--- [ref 20221016°1243] --->
 . The material seems taken from
 [www.dotnetperls.com/byte](https://www.dotnetperls.com/byte)
 <!--- [ref 20221016°1247] --->

&nbsp;

<img src="./icos/20210303o1337.dotnetperls.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for DotNetPerls">
 &nbsp; DotNetPerls article
 [Byte and sbyte Types](https://www.dotnetperls.com/byte)
 …
 <!--- [ref 20221016°1247] --->

&nbsp;

<img src="./icos/20211223o1813.udemy.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Udemy">
 &nbsp; Udemy article
 [C# Char: Everything You Need to Know about Characters in C#](https://blog.udemy.com/c-sharp-char/)
 …
 <!--- [ref 20221016°1252] --->

&nbsp;

<img src="./icos/20221016o1813.mkprog.v0.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for MKProg">
 &nbsp; MKProg Code Translation Project page
 [C# - Unsigned integers](http://ctp.mkprog.com/en/csharp/unsigned_integers/)
 …
 <!--- [ref 20221016°1812] --->

&nbsp;


## JavaScript &nbsp; <sup><sub>[js137int.js](./../js/js137int.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js137int.png">
 <img src ="./runs/js137int.png" width="650" height="33" data-dims="x1084y0033" alt="Run js137int.js">
 </a --->

&nbsp;


## PHP &nbsp; <sup><sub>[ph137int.php](./../php/ph137int.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph137int.png">
 <img src ="./runs/ph137int.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph137int.php">
 </a --->

&nbsp;


## CPP &nbsp; <sup><sub>[cp137int.cpp](./../cpp/cp137int.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp137int.png">
 <img src ="./runs/cp137int.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp137int.cpp">
 </a --->

&nbsp;

<img src="./icos/20221017o0923.nayuki.v1.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Nayuki">
 &nbsp; Nayuki article
 [**Summary of C/C++ integer rules**](https://www.nayuki.io/page/summary-of-c-cpp-integer-rules)
 👍 …
 <!--- [ref 20221017°0922 --->

&nbsp;

<img src="./icos/20200109o0113.cppreference.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for CppReference">
 &nbsp; CppReference article
 [**std::byte**](https://en.cppreference.com/w/cpp/types/byte)
 …
 <!--- [ref 20221017°1122] --->

&nbsp;

<img src="./icos/20110921o1121.stackoverflow.v1.x0032y0032.gif" align="left" width="32" height="32" alt="Link icon for StackOverflow">
 &nbsp; StackOverflow thread
 [How can I get the class name from a C++ object?](https://stackoverflow.com/questions/3649278/how-can-i-get-the-class-name-from-a-c-object)
 …
 <!--- [ref 20221017°1123] --->

&nbsp;

<img src="./icos/20220903o1533.techiedelight.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for TechieDelight">
 &nbsp; TechieDelight article
 [How to pad strings in C++](https://www.techiedelight.com/how-to-pad-strings-in-cpp/)
 about string output manipulation …
 <!--- [ref 20221017°1124] --->

&nbsp;

<img src="./icos/20221017o1133.thoughtsoncoding.v1.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Thoughts-On-Coding">
 &nbsp; Thoughts-On-Coding article
 [How to pad strings in C++](https://thoughts-on-coding.com/)
 not really for this demo here but for string formatting …
 <!--- [ref 20221017°1132] --->

&nbsp;

<img src="./icos/20110921o1121.stackoverflow.v1.x0032y0032.gif" align="left" width="32" height="32" alt="Link icon for StackOverflow">
 &nbsp; StackOverflow thread
 [What's the purpose of std::to_integer?](https://stackoverflow.com/questions/56719524/whats-the-purpose-of-stdto-integer)
 …
 <!--- [ref 20221227°1412] --->

&nbsp;

<img src="./icos/20200109o0113.cppreference.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for CppReference">
 &nbsp; CppReference article
 [std::byte](https://en.cppreference.com/w/cpp/types/byte)
 …
 <!--- [ref 20221227°1414] --->

&nbsp;

<img src="./icos/20221227o1423.namespacecpp.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Namespace-Cpp">
 &nbsp; Namespace-Cpp article
 [to_integer&lt;IntegralType&gt;()](https://namespace-cpp.de/std/doku.php/kennen/lib/to_integer)
 …
 <!--- [ref 20221227°1422] --->

&nbsp;


---

<sup><sub><sup>*[File 20221012°1001 x137int.md]* ⬞Ω</sup></sub></sup>
