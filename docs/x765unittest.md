﻿# Demonstrate UnitTest

## Python &nbsp; <sup><sub>[py765unittest.py](./../py/py765unittest.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

<!--- a href="./runs/py765unittest.png">
 <img src ="./runs/py765unittest.png" width="650" height="33" data-dims="x1084y0033" alt="Run py765unittest.py">
 </a --->

&nbsp;


## Java &nbsp; <sup><sub>[jv765unittest.java](./../java/jv765unittest.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jv765unittest.png">
 <img src ="./runs/jv765unittest.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv765unittest.java">
 </a --->

&nbsp;


## C-Sharp &nbsp; <sup><sub>[cs765unittest.cs](./../cs/cs765unittest.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs765unittest.png">
 <img src ="./runs/cs765unittest.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs765unittest.cs">
 </a --->

&nbsp;


## JavaScript &nbsp; <sup><sub>[js765unittest.js](./../js/js765unittest.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js765unittest.png">
 <img src ="./runs/js765unittest.png" width="650" height="33" data-dims="x1084y0033" alt="Run js765unittest.js">
 </a --->

&nbsp;


## PHP &nbsp; <sup><sub>[ph765unittest.php](./../php/ph765unittest.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph765unittest.png">
 <img src ="./runs/ph765unittest.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph765unittest.php">
 </a --->

&nbsp;


## CPP &nbsp; <sup><sub>[cp765unittest.cpp](./../cpp/cp765unittest.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp765unittest.png">
 <img src ="./runs/cp765unittest.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp765unittest.cpp">
 </a --->

As a tool, see e.g. Freedesktop.org project
 [cppunit test framework](https://www.freedesktop.org/wiki/Software/cppunit/) <!--- [ref 20230315°1032, dld 20230315°1033] --->


&nbsp;


---

<sup><sub><sup>*[File 20221001°1401]* ⬞Ω</sup></sub></sup>
