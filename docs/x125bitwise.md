﻿# Demonstrate Bitwise Operators

Bitwise operators may seem a bit miraculous for high level programming
 novices. They operate on the bit pattern of numbers, and the bit pattern
 of a number is mostly not visible nor interesting for a typical task.

The bit patterns of memory elements are interesting for low-level programming
 like operating systems, drivers or graphics.

## Python &nbsp; <sup><sub>[py125bitwise.py](./../py/py125bitwise.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

<a href="./runs/py125bitwise.png">
 <img src ="./runs/py125bitwise.png" width="644" height="476" data-dims="x0992y0733" alt="Run py125bitwise.py">
 </a>

&nbsp;


## Java &nbsp; <sup><sub>[jv125bitwise.java](./../java/jv125bitwise.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jv125bitwise.png">
 <img src ="./runs/jv125bitwise.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv125bitwise.java">
 </a --->

&nbsp;


## C-Sharp &nbsp; <sup><sub>[cs125bitwise.cs](./../cs/cs125bitwise.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs125bitwise.png">
 <img src ="./runs/cs125bitwise.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs125bitwise.cs">
 </a --->

&nbsp;


## JavaScript &nbsp; <sup><sub>[js125bitwise.js](./../js/js125bitwise.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js125bitwise.png">
 <img src ="./runs/js125bitwise.png" width="650" height="33" data-dims="x1084y0033" alt="Run js125bitwise.js">
 </a --->

&nbsp;


## PHP &nbsp; <sup><sub>[ph125bitwise.php](./../php/ph125bitwise.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph125bitwise.png">
 <img src ="./runs/ph125bitwise.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph125bitwise.php">
 </a --->

&nbsp;


## CPP &nbsp; <sup><sub>[cp125bitwise.cpp](./../cpp/cp125bitwise.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp125bitwise.png">
 <img src ="./runs/cp125bitwise.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp125bitwise.cpp">
 </a --->


---

<sup><sub><sup>*[File 20220826°1101 x125bitwise.md]* ⬞Ω</sup></sub></sup>
