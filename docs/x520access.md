﻿# Demonstrate Access Modifier

Subject: Access modifier (Python does not provide any)

## Python &nbsp; <sup><sub>[py520access.py](./../py/py520access.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

<!--- a href="./runs/py520access.png">
 <img src ="./runs/py520access.png" width="650" height="33" data-dims="x1084y0033" alt="Run py520access.py">
 </a --->

&nbsp;

<img src="./icos/20220318o1503.pythonwife.v2.x0048y0048.png" align="left" width="32" height="32" alt="Link icon for PythonWife">
 &nbsp; PythonWife article
 [Abstraction and Encapsulation in OOPS – Python](https://pythonwife.com/abstraction-and-encapsulation-in-oops-python/)
 <!---[ref 20230706°1514] -->
 👍

&nbsp;

## Java &nbsp; <sup><sub>[jv520access.java](./../java/jv520access.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jv520access.png">
 <img src ="./runs/jv520access.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv520access.java">
 </a --->

&nbsp;


## C-Sharp &nbsp; <sup><sub>[cs520access.cs](./../cs/cs520access.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs520access.png">
 <img src ="./runs/cs520access.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs520access.cs">
 </a --->

&nbsp;


## JavaScript &nbsp; <sup><sub>[js520access.js](./../js/js520access.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js520access.png">
 <img src ="./runs/js520access.png" width="650" height="33" data-dims="x1084y0033" alt="Run js520access.js">
 </a --->

&nbsp;


## PHP &nbsp; <sup><sub>[ph520access.php](./../php/ph520access.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph520access.png">
 <img src ="./runs/ph520access.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph520access.php">
 </a --->

&nbsp;


## CPP &nbsp; <sup><sub>[cp520access.cpp](./../cpp/cp520access.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp520access.png">
 <img src ="./runs/cp520access.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp520access.cpp">
 </a --->

&nbsp;


---

<sup><sub><sup>*[File 20220829°1501]* ⬞Ω</sup></sub></sup>
