﻿# Demonstrate Standard I/O (stdin, stdout, stderr) and redirect them

## Python &nbsp; <sup><sub>[py338stdio.py](./../py/py338stdio.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

<!--- a href="./runs/py338stdio.png">
 <img src ="./runs/py338stdio.png" width="650" height="33" data-dims="x1084y0033" alt="Run py338stdio.py">
 </a --->

&nbsp;


## Java &nbsp; <sup><sub>[jv338stdio.java](./../java/jv338stdio.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jv338stdio.png">
 <img src ="./runs/jv338stdio.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv338stdio.java">
 </a --->

&nbsp;


## C-Sharp &nbsp; <sup><sub>[cs338stdio.cs](./../cs/cs338stdio.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs338stdio.png">
 <img src ="./runs/cs338stdio.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs338stdio.cs">
 </a --->


## JavaScript &nbsp; <sup><sub>[js338stdio.js](./../js/js338stdio.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js338stdio.png">
 <img src ="./runs/js338stdio.png" width="650" height="33" data-dims="x1084y0033" alt="Run js338stdio.js">
 </a --->

&nbsp;


## PHP &nbsp; <sup><sub>[ph338stdio.php](./../php/ph338stdio.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph338stdio.png">
 <img src ="./runs/ph338stdio.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph338stdio.php">
 </a --->

&nbsp;


## CPP &nbsp; <sup><sub>[cp338stdio.cpp](./../cpp/cp338stdio.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp338stdio.png">
 <img src ="./runs/cp338stdio.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp338stdio.cpp">
 </a --->

&nbsp;


---

<sup><sub><sup>*[File 20220828°0901]* ⬞Ω</sup></sub></sup>
