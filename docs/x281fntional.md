﻿# Demonstrate Functional Programming

## Python &nbsp; <sup><sub>[py281fntional.py](./../py/py281fntional.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

<!--- a href="./runs/py281fntional.png">
 <img src ="./runs/py281fntional.png" width="650" height="33" data-dims="x1084y0033" alt="Run py281fntional.py">
 </a --->

&nbsp;


## Java &nbsp; <sup><sub>[jv281fntional.java](./../java/jv281fntional.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jv281fntional.png">
 <img src ="./runs/jv281fntional.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv281fntional.java">
 </a --->

&nbsp;


## C-Sharp &nbsp; <sup><sub>[cs281fntional.cs](./../cs/cs281fntional.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs281fntional.png">
 <img src ="./runs/cs281fntional.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs281fntional.cs">
 </a --->

&nbsp;

<img src="./icos/20221215o1533.akadia.v2.x0048y0048.png" align="left" style="margin-right:1.1em;" width="48" height="48" alt="Link icon for Akadia">
 &nbsp; Akadia tutorial
 [Delegates and Events in C# / .NET](https://www.akadia.com/services/dotnet_delegates_and_events.html)
 — Nice examples 👍
 <!--- [ref 20221215°1532] --->

&nbsp;


## JavaScript &nbsp; <sup><sub>[js281fntional.js](./../js/js281fntional.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js281fntional.png">
 <img src ="./runs/js281fntional.png" width="650" height="33" data-dims="x1084y0033" alt="Run js281fntional.js">
 </a --->

&nbsp;


## PHP &nbsp; <sup><sub>[ph281fntional.php](./../php/ph281fntional.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph281fntional.png">
 <img src ="./runs/ph281fntional.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph281fntional.php">
 </a --->

&nbsp;


## CPP &nbsp; <sup><sub>[cp281fntional.cpp](./../cpp/cp281fntional.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp281fntional.png">
 <img src ="./runs/cp281fntional.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp281fntional.cpp">
 </a --->

<img src="./icos/20110902o1742.codeproject.v3.x0048y0048.png" align="left" style="margin-right:1.1em;" width="48" height="48" alt="Link icon for CodeProject">
 &nbsp; CodeProject article
 [Asynchronous Multicast Delegates in C++](https://www.codeproject.com/Articles/1160934/Asynchronous-Multicast-Delegates-in-Cplusplus)
 provides C++ delegates as a little library *(not used in this demos)*.
 <!--- [ref 20221017°1113] --->

&nbsp;


---

<sup><sub><sup>*[File 20221213°1801]* ⬞Ω</sup></sub></sup>
