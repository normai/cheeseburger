﻿# Demonstrate Simple Date Types

The programming language provides the elementary data types like boolean,
 integers, floats, characters. From those simple types, later complex types
 are built.

## Python &nbsp; <sup><sub>[py131types.py](./../py/py131types.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

<a href="./runs/py131types.png">
 <img src ="./runs/py131types.png" width="643" height="210" data-dims="x0990y0324" alt="Run py131types.py">
 </a>

## Java &nbsp; <sup><sub>[jv131types.java](./../java/jv131types.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jv131types.png">
 <img src ="./runs/jv131types.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv131types.java">
 </a --->


## C-Sharp &nbsp; <sup><sub>[cs131types.cs](./../cs/cs131types.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs131types.png">
 <img src ="./runs/cs131types.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs131types.cs">
 </a --->


## JavaScript &nbsp; <sup><sub>[js131types.js](./../js/js131types.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js131types.png">
 <img src ="./runs/js131types.png" width="650" height="33" data-dims="x1084y0033" alt="Run js131types.js">
 </a --->


## PHP &nbsp; <sup><sub>[ph131types.php](./../php/ph131types.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph131types.png">
 <img src ="./runs/ph131types.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph131types.php">
 </a --->


## CPP &nbsp; <sup><sub>[cp131types.cpp](./../cpp/cp131types.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp131types.png">
 <img src ="./runs/cp131types.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp131types.cpp">
 </a --->

&nbsp;


---

<sup><sub><sup>*[File 20220825°1801 x131types.md]* ⬞Ω</sup></sub></sup>
