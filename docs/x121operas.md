﻿# Demonstrate Operators

Operators are language elements which combine two operands and calculate
 some result from them. A few operators work with only one operand
 *(the unary `not`)* or with three operands *(the 'ternary operator')*.

The most common operators are intuitively taken from what we know from
 mathematics: `+`, `-`, `*`, `/`. But some operators are not so intuitive,
 and consist of a word, e.g. `new`. So the dividing line is between
 key words and operators is not always clear.

*Todo.
So far, the operators are ordered as • arithmetic, • comparison and • bitwise.
Add a fourth category like 'others' with e.g. the ternary operator, the new
operator and others, which do not fit in one of the first three categories.*
<!--- [todo 20221001°1211] --->

## Python &nbsp; <sup><sub>[py121operas.py](./../py/py121operas.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

<a href="./runs/py121operas.png">
 <img src ="./runs/py121operas.png" width="644" height="343" data-dims="x0992y0529" alt="Run py121operas.py">
 </a>

&nbsp;


## Java &nbsp; <sup><sub>[jv121operas.java](./../java/jv121operas.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jv121operas.png">
 <img src ="./runs/jv121operas.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv121operas.java">
 </a --->

&nbsp;


## C-Sharp &nbsp; <sup><sub>[cs121operas.cs](./../cs/cs121operas.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs121operas.png">
 <img src ="./runs/cs121operas.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs121operas.cs">
 </a --->

&nbsp;


## JavaScript &nbsp; <sup><sub>[js121operas.js](./../js/js121operas.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js121operas.png">
 <img src ="./runs/js121operas.png" width="650" height="33" data-dims="x1084y0033" alt="Run js121operas.js">
 </a --->

<img src="./icos/20220304o0953.sebhastian.v2.x0048y0048.png" align="left" style="margin-right:1.1em;" width="48" height="48" alt="Link icon for Sebhastian" id="">
 Sebhastian article
 [JavaScript - how to square a number in three easy ways](https://sebhastian.com/javascript-square/)
 &nbsp; <sup><sub><sup>*[ref 20221001°0922 x121operas.md]*</sup></sub></sup></span>

&nbsp;


## PHP &nbsp; <sup><sub>[ph121operas.php](./../php/ph121operas.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph121operas.png">
 <img src ="./runs/ph121operas.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph121operas.php">
 </a --->

&nbsp;


## CPP &nbsp; <sup><sub>[cp121operas.cpp](./../cpp/cp121operas.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp121operas.png">
 <img src ="./runs/cp121operas.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp121operas.cpp">
 </a --->

&nbsp;


---

<sup><sub><sup>*[File 20220825°1501 x121operas.md]* ⬞Ω</sup></sub></sup>
