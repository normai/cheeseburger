﻿# Demonstrate Date and Time Types

The internal storage of datetime types is no problem, all programming
 languages have according types to do so. But input and output of thoses
 kind of data is a reoccurring source of nasty fiddling. Imagine only
 the myriad ways of representing dates and times exist internationally,
 not to mention the culturally different calendars.

**User story**: Define a passed, the current and a future date/time
 and print them in different formats. Calculate a long term time span,
 the time until next Halley's comet approach. Calculate a short term
 time span, the programs run time.

Note. Time zone handling is offered by most programming languages, but
 is not covered in this demos.

Todo. Possibly create dedicated demo to demonstrate time zone
 handling <sup><sub>*[todo 20221230°1611 'timezones']*</sub></sup>

&nbsp;


## Python &nbsp; [py159date.py](./../py/py159date.py) &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

<a href="./runs/py159date.png">
 <img src ="./runs/py159date.png" width="650" height="391" data-dims="x1084y0652" alt="Run py159date.py">
 </a>

&nbsp;

<img src="./icos/20200630o0143.w3schools.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for W3Schools">
 &nbsp; W3Schools.com article
 [Python Datetime](https://www.w3schools.com/python/python_datetime.asp)
 tells the basics …
 <!--- [ref 20221229°1112] --->

&nbsp;

<img src="./icos/20200805o1243.python.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Python">
 &nbsp; Python documentation
 [datetime — Basic date and time types](https://docs.python.org/3/library/datetime.html)
 tells the details …
 <!--- [ref 20221229°1114] --->

&nbsp;

<img src="./icos/20201107o0315.geekflare.v1.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for GeekFlare">
 &nbsp; GeekFlare tutorial
 [How to Calculate Time Difference in Python](https://geekflare.com/calculate-time-difference-in-python/)
 …
 <!--- [ref 20221229°1142] --->

&nbsp;


## Java &nbsp; [jv159date.java](./../java/jv159date.java) &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<a href="./runs/jv159date.png">
 <img src ="./runs/jv159date.png" width="650" height="411" data-dims="x1084y0685" alt="Run jv159date.java">
 </a>

&nbsp;

<img src="./icos/20200630o0143.w3schools.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for W3Schools.com">
 &nbsp; W3Schools.com article
 [Java Date and Time](https://www.w3schools.com/java/java_date.asp)
 …
 <!--- [ref 20221229°1212] --->

&nbsp;

<img src="./icos/20220320o1415.oracle.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Oracle">
 &nbsp; Oracle Java documentation
 <br>&nbsp;•&nbsp;
 [DateTime Overview](https://docs.oracle.com/javase/tutorial/datetime/iso/overview.html)
 …
 <!--- [ref 20221229°1222] --->
 <br>&nbsp;•&nbsp;
 [Class LocalDateTime](https://docs.oracle.com/javase/8/docs/api/java/time/LocalDateTime.html)
 is the main reference for this demo …
 <!--- [ref 20221229°1224] --->
 <br>&nbsp;•&nbsp;
 [Class DateTimeFormatter](https://docs.oracle.com/javase/8/docs/api/java/time/format/DateTimeFormatter.html)
 lists the string formatting codes, which are as used to parse user input …
 <!--- [ref 20221229°1226] --->

&nbsp;

<img src="./icos/20220905o0913.baeldung.v1.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Baeldung">
 &nbsp; Baeldung tutorials
 <br>&nbsp;•&nbsp;
 [Introduction to the Java 8 Date/Time AP](https://www.baeldung.com/java-8-date-time-intro)
 <!--- [ref 20221229°1232] --->
 <br>&nbsp;•&nbsp;
 [Difference Between Two Dates in Java](https://www.baeldung.com/java-date-difference)
 …
 <!--- [ref 20221229°1234] --->

&nbsp;

<img src="./icos/20200911o0947.dzone.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for DZone">
 &nbsp; DZone article
 [Mock Java Date/Time for Testing](https://dzone.com/articles/mock-java-datetime-for-testing)
 …
 <!--- [ref 20221229°1242] --->

&nbsp;

<img src="./icos/20110921o1121.stackoverflow.v1.x0032y0032.gif" align="left" width="32" height="32" alt="Link icon for StackOverflow">
 &nbsp; StackOverflow thread
 [Minimum date in Java](https://stackoverflow.com/questions/3838242/minimum-date-in-java)
 … tells, that `java.util.Date` is not the latest …
 <!--- [ref 20221230°0952] --->

&nbsp;

<img src="./icos/20110921o1121.stackoverflow.v1.x0032y0032.gif" align="left" width="32" height="32" alt="Link icon for StackOverflow">
 &nbsp; StackOverflow thread
 [Should I use java.util.Date or switch to java.time.LocalDate](https://stackoverflow.com/questions/28730136/should-i-use-java-util-date-or-switch-to-java-time-localdate)
 … discusses the difference between the old an new class in detail …
 <!--- [ref 20221230°0954] --->

&nbsp;

<img src="./icos/20221230o0943.dariawan.v1.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Dariawan">
 &nbsp; Dariawan tutorial
 [java.time.Duration Tutorial with Examples](https://www.dariawan.com/tutorials/java/java-time-duration-tutorial-examples/)
 explains how to get time differences …
 <!--- [ref 20221230°0942] --->

&nbsp;

<img src="./icos/20200106o0249.geeksforgeeks.v1.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for GeeksForGeeks">
 &nbsp; GeeksForGeeks tutorial
 [Java System.nanoTime() vs System.currentTimeMillis](https://www.geeksforgeeks.org/java-system-nanotime-vs-system-currenttimemillis/)
 … discusses the more lower level timing sources
 <!--- [ref 20221230°1012] --->

&nbsp;


## C-Sharp &nbsp; [cs159date.cs](./../cs/cs159date.cs) &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<a href="./runs/cs159date.png">
 <img src ="./runs/cs159date.png" width="650" height="391" data-dims="x1084y0652" alt="Run cs159date.cs">
 </a>

&nbsp;

<img src="./icos/20170811o0621.microsoftgray.v5.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Microsoft">
 &nbsp; Microsoft articles
 <br>&nbsp;•&nbsp;
 [DateTime Struct](https://learn.microsoft.com/en-us/dotnet/api/system.datetime?view=net-7.0)
 …
 <!--- [ref 20221229°1322] --->
 <br>&nbsp;•&nbsp;
 [Standard date and time format strings](https://learn.microsoft.com/en-us/dotnet/standard/base-types/standard-date-and-time-format-strings)
 …
 <!--- [ref 20221229°1324] --->
 <br>&nbsp;•&nbsp;
 [TimeSpan Struct](https://learn.microsoft.com/en-us/dotnet/api/system.timespan?view=net-7.0)
 … Note: Microseconds and nanoseconds exist in .NET 7, not yet in the Framework.
 <!--- [ref 20221230°1342] --->
 <br>&nbsp;•&nbsp;
 [DayOfWeek Enum](https://learn.microsoft.com/en-us/dotnet/api/system.dayofweek?view=net-7.0)
 … (Note. About an enumeration months, see StackOverflow thread.)
 <!--- [ref 20221230°1344] --->

&nbsp;

<img src="./icos/20200629o1223.csharpcorner.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for CSharpCorner">
 &nbsp; CSharpCorner tutorials and blog articles
 <br>&nbsp;•&nbsp;
 [DateTime In C#](https://www.c-sharpcorner.com/article/datetime-in-c-sharp/)
 it also shows cultures, timezones, formmatting, differences …
 <!--- [ref 20221229°1312] --->
 <br>&nbsp;•&nbsp;
 [DateTime Format In C#](https://www.c-sharpcorner.com/blogs/date-and-time-format-in-c-sharp-programming1)
 is listing the format specifiers …
 <!--- [ref 20221229°1313] --->
 <br>&nbsp;•&nbsp;
 [Working With C# DateTime](https://www.c-sharpcorner.com/uploadfile/mahesh/working-with-datetime-using-C-Sharp/)
 …
 <!--- [ref 20221229°1314] --->
 <br>&nbsp;•&nbsp;
 [Current Date And Time In C#](https://www.c-sharpcorner.com/Blogs/current-date-and-time-in-c-sharp)
 …
 <!--- [ref 20221229°1315] --->

&nbsp;

<img src="./icos/20221224o1033.makolyte.v1.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Makolyte">
 &nbsp; Makolyte article
 [C# – Get the current date and time](https://makolyte.com/csharp-get-the-current-date-and-time/)
 <!--- [ref 20221229°1332] --->
 …

&nbsp;

<img src="./icos/20110921o1121.stackoverflow.v1.x0032y0032.gif" align="left" width="32" height="32" alt="Link icon for StackOverflow">
 &nbsp; StackOverflow thread
 [Is there a predefined enumeration for Month in the .NET library?](https://stackoverflow.com/questions/899565/is-there-a-predefined-enumeration-for-month-in-the-net-library)
 mentions as a solution `CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(DateTime.Now.Month);` …
 <!--- [ref 20221230°1352] --->

&nbsp;


## JavaScript &nbsp; [js159date.js](./../js/js159date.js) &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<a href="./runs/js159date.png">
 <img src ="./runs/js159date.png" width="654" height="411" data-dims="x1090y0685" alt="Run js159date.js">
 </a>

&nbsp;

<img src="./icos/20180511o0333.mozilla1logo.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Mozilla">
 &nbsp; Mozilla articles
 <br>&nbsp;•&nbsp;
 [**Date**](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date)
 …
 <!--- [ref 20221229°1622] --->
 <br>&nbsp;•&nbsp;
 [**Date() constructor**](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date/Date)
 …
 <!--- [ref 20221230°1522] --->

&nbsp;

<img src="./icos/20190108o0356.javascriptinfo.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for JavaScript.info">
 &nbsp; JavaScript.info article
 [Date and time](https://javascript.info/date)
 <!--- [ref 20221229°1612] --->
 …

&nbsp;

<img src="./icos/20200630o0143.w3schools.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for W3Schools.com">
 &nbsp; W3Schools.com articles
 [JavaScript Date Objects](https://www.w3schools.com/js/js_dates.asp)
 …
 <!--- [ref 20221229°1632] --->
 <br>&nbsp;•&nbsp;
 [JavaScript Date Reference](https://www.w3schools.com/jsref/jsref_obj_date.asp)
 …
 <!--- [ref 20221230°1524] --->
 <br>&nbsp;•&nbsp;
 [JavaScript Date Formats](https://www.w3schools.com/js/js_date_formats.asp)
 …
 <!--- [ref 20221230°1526] --->

&nbsp;

<img src="./icos/20200521o1628.freecodecamp.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for FreeCodeCamp">
 &nbsp; FreeCodeCamp articles
 <br>&nbsp;•&nbsp;
 [JavaScript Date Format – How to Format a Date in JS](https://www.freecodecamp.org/news/javascript-date-format-how-to-format-a-date-in-js/)
 …
 <!--- [ref 20221230°1524] --->
 <br>&nbsp;•&nbsp;
 [How to Format Dates in JavaScript with One Line of Code](https://www.freecodecamp.org/news/how-to-format-dates-in-javascript/)
 …
 <!--- [ref 20221230°1526] --->


&nbsp;

<img src="./icos/20110921o1121.stackoverflow.v1.x0032y0032.gif" align="left" width="32" height="32" alt="Link icon for StackOverflow">
 &nbsp; StackOverflow threads
 <br>&nbsp;•&nbsp;
 [Minimum and maximum date](https://stackoverflow.com/questions/11526504/minimum-and-maximum-date)
 …
 <!--- [ref 20221230°1512] --->
 <br>&nbsp;•&nbsp;
 [How to calculate date difference in JavaScript? &#91;duplicate&#93;](https://stackoverflow.com/questions/7763327/how-to-calculate-date-difference-in-javascript)
 …
 <!--- [ref 20221230°1514] --->
 <br>&nbsp;•&nbsp;
 [How to calculate number of days between two dates?](https://stackoverflow.com/questions/542938/how-to-calculate-number-of-days-between-two-dates)
 …
 <!--- [ref 20221230°1516] --->

&nbsp;

<img src="./icos/20200302o0523.flaviocopes.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for FlavioCopes">
 &nbsp; FlavioCopes article
 [How to format a date in JavaScript](https://flaviocopes.com/how-to-format-date-javascript/)
 <!--- [ref 20221230°1542] --->
 …

&nbsp;


## PHP &nbsp; [ph159date.php](./../php/ph159date.php) &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<a href="./runs/ph159date.png">
 <img src ="./runs/ph159date.png" width="654" height="442" data-dims="x1090y0737" alt="Run ph159date.php">
 </a>

&nbsp;

<img src="./icos/20201130o1043.php-net.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for PHP manual">
 &nbsp; PHP manual pages
 <br>&nbsp;•&nbsp;
 [Date and Time](https://www.php.net/manual/en/book.datetime.php)
 …
 <!--- [ref 20221230°1622] --->
 <br>&nbsp;•&nbsp;
 [The DateTime class](https://www.php.net/manual/en/class.datetime.php)
 …
 <!--- [ref 20221229°1722] --->
 <br>&nbsp;•&nbsp;
 [The DateTimeImmutable class](https://www.php.net/manual/en/class.datetimeimmutable.php)
 <!--- [ref 20221230°1552] --->
 offers the same functionality as the DateTime class, except that it 
 returns a cloned object, if modifying functions are called …
 <br>&nbsp;•&nbsp;
 [Date/Time Functions](https://www.php.net/manual/en/ref.datetime.php)
 …
 <!--- [ref 20221229°1724] --->
 <br>&nbsp;•&nbsp;
 [**microtime**](https://www.php.net/manual/en/function.microtime.php)
 <!--- [ref 20221230°1554] --->
 . This function returns the current Unix timestamp in microseconds,
 it is not on all operating systems available.
 It is not shown in the present demo.
 <br>&nbsp;•&nbsp;
 [DateTime::format …](https://www.php.net/manual/en/datetime.format.php)
 …
 <!--- [ref 20221230°1556] --->
 <br>&nbsp;•&nbsp;
 [date function](https://www.php.net/manual/en/function.date.php)
 …
 <!--- [ref 20221230°1558] --->

&nbsp;

<img src="./icos/20200630o0143.w3schools.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for W3Schools">
 &nbsp; W3Schools.com articles
 <br>&nbsp;•&nbsp;
 [PHP Date and Time](https://www.w3schools.com/php/php_date.asp)
 …
 <!--- [ref 20221229°1712] --->
 <br>&nbsp;•&nbsp;
 [PHP time() Function](https://www.w3schools.com/PHP/func_date_time.asp)
 …
 <!--- [ref 20221229°1714] --->

&nbsp;

<img src="./icos/20180615o0256.csstricks.v1.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for CSS-Tricks">
 &nbsp; CSS-Tricks article
 [PHP Date and Time Recipes](https://css-tricks.com/php-date-and-time-recipes/)
 …
 <!--- [ref 20221229°1732] --->

&nbsp;

<a name="ref_20221230o1632"></a>
<img src="./icos/20110921o1121.stackoverflow.v1.x0032y0032.gif" align="left" width="32" height="32" alt="Link icon for StackOverflow">
 &nbsp; StackOverflow threads
- [Maximum time() | PHP](https://stackoverflow.com/questions/3953333/maximum-time-php) <!-- [ref 20221230°1632] -->
 … refers to [Wikipedia article](#ref_20221230o1634) 'Year 2038 problem' …
- <a name="ref_20221230o1636"></a>
 [DateTime with microseconds](https://stackoverflow.com/questions/33691428/datetime-with-microseconds) <!-- [ref 20221230°1636] -->
 shows a DateTime constructor with microseconds: `$d = new DateTime("15-07-2014 18:30:00.111111");`
 …
- [php DateTime with date without hours](https://stackoverflow.com/questions/36741877/php-datetime-with-date-without-hours) <!-- [ref 20221230°1642] -->
 stages the midnight-solution `$d = new \DateTime("midnight");`
 …
- [How to trim the time from the timestamp using php](https://stackoverflow.com/questions/11724399/how-to-trim-the-time-from-the-timestamp-using-php) <!-- [ref 20221230°1644] -->
 …

&nbsp;


## CPP &nbsp; [cp159date.cpp](./../cpp/cp159date.cpp) &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp159date.png">
 <img src ="./runs/cp159date.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp159date.cpp">
 </a --->

&nbsp;

<a name="ref_20221229o1812"></a>
<img src="./icos/20200109o0113.cppreference.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for CppReference">
 &nbsp; CppReference articles
- [Date and time utilities ](https://en.cppreference.com/w/cpp/chrono) <!-- [ref 20221229°1812] -->
  … by the way with code for a recursive Fibonacci numbers solution …
- [Standard library header &lt;ctime&gt;](https://en.cppreference.com/w/cpp/header/ctime) <!-- [ref 20221230°1754] -->
 …
- [Standard library header &lt;chrono&gt; *(C++11)*](https://en.cppreference.com/w/cpp/header/chrono) <!-- [ref 20221230°1752] -->
 …
- [std::chrono::system_clock](https://en.cppreference.com/w/cpp/chrono/system_clock) <!-- [ref 20221230°1742] -->
 … the only C++ clock that has the ability to map its time points to C-style time. …
- [std::chrono::steady_clock](https://en.cppreference.com/w/cpp/chrono/steady_clock) <!-- [ref 20221230°1744] -->
 …
- [std::chrono::high_resolution_clock](https://en.cppreference.com/w/cpp/chrono/high_resolution_clock) <!-- [ref 20221230°1746] -->
 …

&nbsp;

<img src="./icos/20150308o0524.cplusplus.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for CPlusPlus">
 &nbsp; CPlusPlus articles
- [function time](https://cplusplus.com/reference/ctime/time/) <!-- [ref 20221229°1822] -->
 (in &lt;ctime&gt;) …
- [function strftime ](https://cplusplus.com/reference/ctime/strftime/) <!-- [ref 20221229°1824] -->
 (in &lt;ctime&gt;) …

&nbsp;

<img src="./icos/20180615o0435.githubmark1.v0.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for GitHub">
 <img src="./icos/20221229o1843.howardhinnant.v1.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for HowardHinnant">
 &nbsp; GitHub project
 [HowardHinnant/date](https://github.com/HowardHinnant/date) <!--- [ref 20221229°1832] --->
 …

&nbsp;

<img src="./icos/20221230o1813.sanfoundry.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Sanfoundry">
 &nbsp; Sanfoundry tutorial
 [C++ Program to Display Current Date and Time](https://www.sanfoundry.com/cpp-program-print-current-date-time/) <!--- [ref 20221230°1812] --->
 …

&nbsp;

<img src="./icos/20110921o1121.stackoverflow.v1.x0032y0032.gif" align="left" width="32" height="32" alt="Link icon for StackOverflow">
 &nbsp; StackOverflow threads
 <br>&nbsp;•&nbsp;
 [How to get current time and date in C++?](https://stackoverflow.com/questions/997946/how-to-get-current-time-and-date-in-c) <!--- [ref 20221230°1732] --->
 …
 <br>&nbsp;•&nbsp;
 [Get current time in C++\\{C}](https://stackoverflow.com/questions/62259044/get-current-time-in-c-c) <!--- [ref 20221230°1734] --->
 …
 <br>&nbsp;•&nbsp;
 [Outputting Date and Time in C++ using std::chrono](https://stackoverflow.com/questions/17223096/outputting-date-and-time-in-c-using-stdchrono) <!--- [ref 20221230°1832] --->
 … discusses `&lt;chrono&gt;` versus `&lt;ctime&gt;` …
 <br>&nbsp;•&nbsp;
 [C++ date and time](https://stackoverflow.com/questions/12346260/c-date-and-time) <!--- [ref 20221230°1834] --->
 …
 <br>&nbsp;•&nbsp;
 [Library to work with historical (big) dates and time (eg, 11,043 BC)?](https://stackoverflow.com/questions/15916672/library-to-work-with-historical-big-dates-and-time-eg-11-043-bc) <!--- [ref 20230102°0922] --->
 …
 <br>&nbsp;•&nbsp;
 [Is there a standard date/time class in C++?](https://stackoverflow.com/questions/1650715/is-there-a-standard-date-time-class-in-c) <!--- [ref 20230102°0924] --->
 …
 <br>&nbsp;•&nbsp;
 [Date handling before 1970 c++](https://stackoverflow.com/questions/21859147/date-handling-before-1970-c) <!--- [ref 20230102°0926] --->
 …

&nbsp;

<img src="./icos/20210924o1933.delftstack.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for DelftStack">
 &nbsp; DelftStack articles:
 <br> •
 [Measure Execution Time of a Function in C++ STL](https://www.delftstack.com/howto/cpp/cpp-timing/) <!--- [ref 20230102°0912] --->
 …
 <br> •
 [Print System Time in C++](https://www.delftstack.com/howto/cpp/system-time-in-cpp/) <!--- [ref 20230102°0914] --->
 …

&nbsp;

<img src="./icos/20110902o1742.codeproject.v3.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for CodeProject">
 &nbsp; CodeProject article
 [Date and Time in C++](https://www.codeproject.com/Articles/3049/Date-and-Time-in-C) <!--- [ref 20230102°0932] --->
 …

&nbsp;

<img src="./icos/20200913o0133.modernescpp.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for ModernesCpp">
 &nbsp; ModernesCpp articles
 <br> •
 [Time Point](https://www.modernescpp.com/index.php/time-point) <!--- [ref 20230102°0952] --->
 …
 <br> •
 [Time Duration](https://www.modernescpp.com/index.php/time-duration) <!--- [ref 20230102°0953] --->
 …

&nbsp;

<img src="./icos/20221120o1013.appsloveworld.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for AppsLoveWorld">
 &nbsp; AppsLoveWorld article
 [[Solved] - How to calculate time differrences in C++ with TIME_T before the epoch? - C++](https://www.appsloveworld.com/cplus/100/156/how-to-calculate-time-differences-in-c-with-time-t-before-the-epoch) <!--- [ref 20230102°0942] --->
 …

&nbsp;

<img src="./icos/20200104o0143.reddit.v3.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Reddit">
 &nbsp; Reddit thread
 [Why is C++ so terrible when it comes to date/time manipulation?](https://www.reddit.com/r/cpp/comments/lj2fm1/why_is_c_so_terrible_when_it_comes_to_datetime/) <!--- [ref 20230102°0944] --->
 …

&nbsp;

<img src="./icos/20221230o1843.wikimass.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Sanfoundry">
 &nbsp; Wikimass section
 [C++ Tutorial](https://wikimass.com/cpp) <!--- [ref 20221230°1850] --->
 … just by the way …

&nbsp;


## General

<img src="./icos/20110921o1125.wikipedia.v1.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Wikipedia">
 &nbsp; Wikipedia article
 [List of future astronomical events](https://en.wikipedia.org/wiki/List_of_future_astronomical_events)
 …
 <!--- [ref 20221230°0924] --->

&nbsp;

<img src="./icos/20110921o1125.wikipedia.v1.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Wikipedia">
 &nbsp; Wikipedia article
 [Galileo Galilei](https://en.wikipedia.org/wiki/Galileo_Galilei)
 …
 <!--- [ref 20221230°0922] --->

&nbsp;

<img src="./icos/20110921o1125.wikipedia.v1.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Wikipedia">
 &nbsp; Wikipedia article
 [Halley's Comet](https://en.wikipedia.org/wiki/Halley%27s_Comet)
 …
 <!--- [ref 20221230°0926] --->

&nbsp;

<a name="ref_20221230o1634"></a>
<img src="./icos/20110921o1125.wikipedia.v1.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Wikipedia">
 &nbsp; Wikipedia article
 [Year 2038 problem](https://en.wikipedia.org/wiki/Year_2038_problem)
 … (refers to from [StackOverflow thread](#ref_20221230o1632) 'Maximum time() | PHP')
 <!--- [ref 20221230°1634] --->

&nbsp;


---

<sup><sub><sup>*[File 20221012°1801]* ⬞Ω</sup></sub></sup>
