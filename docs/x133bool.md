﻿# Demonstrate Boolean Type

The Boolean type can store either True or False. This means effectively it
 is exactly one bit. Well, but the memory consumption is nevertheless
 at least a byte, because a byte is the smallest unit, memory is addressed.

Sure you can access single bits inside a byte, but this is associated with
 much more effort than accesssing just a whole byte. See the demo for
 [Bitwise Operators](./x125bitwise.md).

## Python &nbsp; <sup><sub>[py133bool.py](./../py/py133bool.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

<a href="./runs/py133bool.png">
 <img src ="./runs/py133bool.png" width="643" height="467" data-dims="x0990y0719" alt="Run py133bool.py">
 </a>

&nbsp;


## Java &nbsp; <sup><sub>[jv133bool.java](./../java/jv133bool.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jv133bool.png">
 <img src ="./runs/jv133bool.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv133bool.java">
 </a --->

&nbsp;


## C-Sharp &nbsp; <sup><sub>[cs133bool.cs](./../cs/cs133bool.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs133bool.png">
 <img src ="./runs/cs133bool.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs133bool.cs">
 </a --->

&nbsp;


## JavaScript &nbsp; <sup><sub>[js133bool.js](./../js/js133bool.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js133bool.png">
 <img src ="./runs/js133bool.png" width="650" height="33" data-dims="x1084y0033" alt="Run js133bool.js">
 </a --->

&nbsp;


## PHP &nbsp; <sup><sub>[ph133bool.php](./../php/ph133bool.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph133bool.png">
 <img src ="./runs/ph133bool.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph133bool.php">
 </a --->

&nbsp;


## CPP &nbsp; <sup><sub>[cp133bool.cpp](./../cpp/cp133bool.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp133bool.png">
 <img src ="./runs/cp133bool.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp133bool.cpp">
 </a --->

&nbsp;


---

<sup><sub><sup>*[File 20221012°0801 x133bool.md]* ⬞Ω</sup></sub></sup>
