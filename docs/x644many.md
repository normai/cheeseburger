﻿# Demonstrate SQL Many-to-many Relation (m:n)

## Python &nbsp; <sup><sub>[py644many.py](./../py/py644many.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

<!--- a href="./runs/py644many.png">
 <img src ="./runs/py644many.png" width="650" height="33" data-dims="x1084y0033" alt="Run py644many.py">
 </a --->

&nbsp;


## Java &nbsp; <sup><sub>[jv644many.java](./../java/jv644many.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jv644many.png">
 <img src ="./runs/jv644many.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv644many.java">
 </a --->

&nbsp;


## C-Sharp &nbsp; <sup><sub>[cs644many.cs](./../cs/cs644many.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs644many.png">
 <img src ="./runs/cs644many.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs644many.cs">
 </a --->

&nbsp;


## JavaScript &nbsp; <sup><sub>[js644many.js](./../js/js644many.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js644many.png">
 <img src ="./runs/js644many.png" width="650" height="33" data-dims="x1084y0033" alt="Run js644many.js">
 </a --->

&nbsp;


## PHP &nbsp; <sup><sub>[ph644many.php](./../php/ph644many.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph644many.png">
 <img src ="./runs/ph644many.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph644many.php">
 </a --->

&nbsp;


## CPP &nbsp; <sup><sub>[cp644many.cpp](./../cpp/cp644many.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp644many.png">
 <img src ="./runs/cp644many.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp644many.cpp">
 </a --->

&nbsp;


---

<sup><sub><sup>*[File 20220831°2001]* ⬞Ω</sup></sub></sup>
