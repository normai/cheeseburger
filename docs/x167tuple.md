﻿# Demonstrate Tuple

A tuple is like a record as we know it from databases. It has a fixed
 number of elements of different types.

## Python &nbsp; <sup><sub>[py167tuple.py](./../py/py167tuple.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

<a href="./runs/py167tuple.png">
 <img src ="./runs/py167tuple.png" width="644" height="319" data-dims="x0992y0491" alt="Run py167tuple.py">
 </a>

In Python, tuples are immutable, means they cannot be changed after creation.

&nbsp;


## Java &nbsp; <sup><sub>[jv167tuple.java](./../java/jv167tuple.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jv167tuple.png">
 <img src ="./runs/jv167tuple.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv167tuple.java">
 </a --->

&nbsp;

<img src="./icos/20220905o0913.baeldung.v1.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Baeldung">
 &nbsp; Baeldung tutorial
 [Introduction to Javatuples](https://www.baeldung.com/java-tuples)
 …
 <!--- [ref 20220905°0912] --->

&nbsp;

<img src="./icos/20180615o0435.githubmark1.v0.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for GitHub">
 &nbsp; GitHub project
 [javatuples/javatuples](https://github.com/javatuples/javatuples)
 …
 <!--- [ref 20220905°0924] --->

&nbsp;

<img src="./icos/20220905o0933.stackabuse.v5.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for StackAbuse">
 &nbsp; StackAbuse thread
 [Definitive Guide to Java Pairs - Working with Tuples in Java](https://stackabuse.com/definitive-guide-to-java-pairs-working-with-tuples-in-java/)
 …
 <!--- [ref 20220905°0932] --->

&nbsp;

<img src="./icos/20220905o0943.core-cs-ksu-edu.v1.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Kansas State University">
 &nbsp; Kansas State University video
 [Java Tuples](https://core.cs.ksu.edu/2-cc210/14-collections/04-java-tuples/embed.html)
 also provides the slides from the lecture …
 <!--- [ref 20220905°0942] --->

&nbsp;

<img src="./icos/20210924o1933.delftstack.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for DelftStack">
 &nbsp; DelftStack tutorial
 [JavaTuples in Java](https://www.delftstack.com/howto/java/java-tuple/)
 …
 <!--- [ref 20220905°0952] --->

&nbsp;

<img src="./icos/20220811o1656.mvnrepository.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for MvnRepository">
 &nbsp; MVN Repository page
 [JAVATUPLES » 1.2](https://mvnrepository.com/artifact/org.javatuples/javatuples/1.2)
 <!--- [ref 20220905°0953] --->
 provides library `javatuples-1.2.jar` (65 507 bytes) for download
 , which is used for the demo.
 <!--- [dld 20220905°0953] --->

&nbsp;


## C-Sharp &nbsp; <sup><sub>[cs167tuple.cs](./../cs/cs167tuple.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs167tuple.png">
 <img src ="./runs/cs167tuple.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs167tuple.cs">
 </a --->

&nbsp;


## JavaScript &nbsp; <sup><sub>[js167tuple.js](./../js/js167tuple.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js167tuple.png">
 <img src ="./runs/js167tuple.png" width="650" height="33" data-dims="x1084y0033" alt="Run js167tuple.js">
 </a --->

&nbsp;


## PHP &nbsp; <sup><sub>[ph167tuple.php](./../php/ph167tuple.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph167tuple.png">
 <img src ="./runs/ph167tuple.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph167tuple.php">
 </a --->

&nbsp;


## CPP &nbsp; <sup><sub>[cp167tuple.cpp](./../cpp/cp167tuple.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp167tuple.png">
 <img src ="./runs/cp167tuple.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp167tuple.cpp">
 </a --->

&nbsp;


---

<sup><sub><sup>*[File 20220829°0901 x167tuple.md]* ⬞Ω</sup></sub></sup>
