﻿# Demonstrate Full Blown Exception

Though most programming language provide the try/catch keywords,
those keywords may work pretty different in detail, and be applied in quite
different situations.

Since this project likes to have equivalent demos over the different
 languages, we defined some canary situations and just applied those
 canaries equally for all languages. The canaries are:

- (1) Division by zero

- (2) Calculate square root of negative number

- (3) Array index out of bounds

- (4) Open non-existing file

- (5) All fine

*Todo: More sensible stories are wanted. Especially C++ examples, where
 the `throw` is done from some built-in library function. E.g.:*
 <!--- todo 20220913°1022 --->
- *Convert string to int with invalid input. E.g. In C++
    stoi() throw sstd::invalid_argument, see cp211ifs1.cpp*

&nbsp;


## Python &nbsp; <sup><sub>[py262try2.py](./../py/py262try2.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

<a href="./runs/py262try2.png">
 <img src ="./runs/py262try2.png" width="629" height="716" data-dims="x0787y0895" alt="Run py262try2.py">
 </a>

&nbsp;

<img src="./icos/20200630o0143.w3schools.v2.x0048y0048.png" align="left" width="48" height="48" alt="Link icon for W3Schools.com">
 &nbsp; W3Schools.com article
 [Python finally Keyword](https://www.w3schools.com/python/ref_keyword_finally.asp)
 …
 <!--- [ref 20220214°1154] --->

&nbsp;

<img src="./icos/20110921o1121.stackoverflow.v1.x0048y0048.gif" align="left" width="48" height="48" alt="Link icon for StackOverflow">
 &nbsp; StackOverflow thread
 [How do I determine what type of exception occurred?](https://stackoverflow.com/questions/9823936/python-how-do-i-know-what-type-of-exception-occurred)
 …
 <!--- [ref 20220214°1155] --->

&nbsp;

<img src="./icos/20220214o1157.rollbar.v1.x0048y0048.png" align="left" width="48" height="48" alt="Link icon for Rollbar">
 &nbsp; Rollbar tutorial
 [How to Throw Exceptions in Python](https://rollbar.com/blog/throwing-exceptions-in-python/)
 …
 <!--- [ref 20220214°1156] --->

&nbsp;

<img src="./icos/20220915o0913.datagy.v2.x0048y0048.png" align="left" width="48" height="48" alt="Link icon for Datagy.io">
 &nbsp; Datagy.io article
 [Python Square Root: How to Calculate a Square Root in Python](https://datagy.io/python-square-root/)
 …
 <!--- [ref 20220913°1012 ico 20220913°1013] --->

&nbsp;


## Java &nbsp; <sup><sub>[jv262try2.java](./../java/jv262try2.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jv262try2.png">
 <img src ="./runs/jv262try2.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv262try2.java">
 </a --->

&nbsp;


## C-Sharp &nbsp; <sup><sub>[cs262try2.cs](./../cs/cs262try2.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs262try2.png">
 <img src ="./runs/cs262try2.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs262try2.cs">
 </a --->

&nbsp;


## JavaScript &nbsp; <sup><sub>[js262try2.js](./../js/js262try2.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js262try2.png">
 <img src ="./runs/js262try2.png" width="650" height="33" data-dims="x1084y0033" alt="Run js262try2.js">
 </a --->

&nbsp;


## PHP &nbsp; <sup><sub>[ph262try2.php](./../php/ph262try2.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph262try2.png">
 <img src ="./runs/ph262try2.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph262try2.php">
 </a --->

&nbsp;


## CPP &nbsp; <sup><sub>[cp262try2.cpp](./../cpp/cp262try2.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp262try2.png">
 <img src ="./runs/cp262try2.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp262try2.cpp">
 </a --->

&nbsp;


---

<sup><sub><sup>*[File 20220827°0901 x262try2.md]* ⬞Ω</sup></sub></sup>
