﻿# Demonstrate Dictionaries

Dictionaries are a kind of list, but not indexed by a serial number, but
 by some key of any type. This kind of storage has many names, e.g.
 'associative array', ...

## Python &nbsp; <sup><sub>[py165dict.py](./../py/py165dict.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

<a href="./runs/py165dict.png">
 <img src ="./runs/py165dict.png" width="644" height="330" data-dims=x0992y508"" alt="Run py165dict.py">
 </a>

&nbsp;

<img src="./icos/20201225o1757.realpython.v1.x0048y0048.png" align="left" style="margin-right:1.1em;" width="48" height="48" alt="Link icon for RealPython">
 &nbsp; RealPython article
 [How to Iterate Through a Dictionary in Python](https://realpython.com/iterate-through-dictionary-python/)
 &nbsp; <sup><sub><sup>*[ref 20221001°1222]*</sup></sub></sup></span>

&nbsp;


## Java &nbsp; <sup><sub>[jv165dict.java](./../java/jv165dict.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jv165dict.png">
 <img src ="./runs/jv165dict.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv165dict.java">
 </a --->

&nbsp;


## C-Sharp &nbsp; <sup><sub>[cs165dict.cs](./../cs/cs165dict.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs165dict.png">
 <img src ="./runs/cs165dict.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs165dict.cs">
 </a --->

&nbsp;


## JavaScript &nbsp; <sup><sub>[js165dict.js](./../js/js165dict.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js165dict.png">
 <img src ="./runs/js165dict.png" width="650" height="33" data-dims="x1084y0033" alt="Run js165dict.js">
 </a --->

&nbsp;


## PHP &nbsp; <sup><sub>[ph165dict.php](./../php/ph165dict.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph165dict.png">
 <img src ="./runs/ph165dict.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph165dict.php">
 </a --->

&nbsp;


## CPP &nbsp; <sup><sub>[cp165dict.cpp](./../cpp/cp165dict.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp165dict.png">
 <img src ="./runs/cp165dict.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp165dict.cpp">
 </a --->

&nbsp;


---

<sup><sub><sup>*[File 20220828°1901 x165dict.md]* ⬞Ω</sup></sub></sup>
