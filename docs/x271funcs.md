﻿# Demonstrate Function Calls

Functions are a most important mean to structure the code and to provide
 named codes sequences for being used multiple times and from differnt
 places.

Function calling can be done with and without **parameters**, with and without **return** a value.

## Python &nbsp; <sup><sub>[py271funcs.py](./../py/py271funcs.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

<a href="./runs/py271funcs.png">
 <img src ="./runs/py271funcs.png" width="650" height="169" data-dims="x1084y0282*.6" alt="Run py271funcs.py">
 </a>

&nbsp;

<img src="./icos/20200805o1243.python.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Python.org">
 &nbsp; For <b>built-in</b> functions in general, see the Python documentation
 [python/functions](https://docs.python.org/3/library/functions.html)
 <!--- [ref 20221216°1242] --->

&nbsp;


## Java &nbsp; <sup><sub>[jv271funcs.java](./../java/jv271funcs.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jv271funcs.png">
 <img src ="./runs/jv271funcs.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv271funcs.java">
 </a --->

&nbsp;


## C-Sharp &nbsp; <sup><sub>[cs271funcs.cs](./../cs/cs271funcs.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs271funcs.png">
 <img src ="./runs/cs271funcs.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs271funcs.cs">
 </a --->

&nbsp;


## JavaScript &nbsp; <sup><sub>[js271funcs.js](./../js/js271funcs.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js271funcs.png">
 <img src ="./runs/js271funcs.png" width="650" height="33" data-dims="x1084y0033" alt="Run js271funcs.js">
 </a --->

&nbsp;


## PHP &nbsp; <sup><sub>[ph271funcs.php](./../php/ph271funcs.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph271funcs.png">
 <img src ="./runs/ph271funcs.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph271funcs.php">
 </a --->

&nbsp;


## CPP &nbsp; <sup><sub>[cp271funcs.cpp](./../cpp/cp271funcs.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp271funcs.png">
 <img src ="./runs/cp271funcs.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp271funcs.cpp">
 </a --->

&nbsp;

<a name="ref20221230o1817"></a>
<img src="./icos/20221230o1813.sanfoundry.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Sanfoundry">
 &nbsp; Sanfoundry tutorial
 [C++ Program to Find Factorial of a Number using Dynamic Programming](https://www.sanfoundry.com/cpp-program-find-factorial-number-dynamic-programming/) <!-- [ref 20221230°1817] -->
 … 'dynamic programming' is a not-so-often heard programming paradigm
 (see Wikipedias article [below](#ref20221230o1822))
 …

&nbsp;


## General

&nbsp;

<a name="ref20221230o1822"></a>
<img src="./icos/20110921o1125.wikipedia.v1.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Sanfoundry">
 &nbsp; Wikipedia article
 [Dynamic programming](https://en.wikipedia.org/wiki/Dynamic_programming) <!-- [ref 20221230°1822] -->
 … (also see Sanfoundry tutorial [above](#ref20221230o1817))
 …

&nbsp;



---

<sup><sub><sup>*[File 20220827°1101 x271funcs.md]* ⬞Ω</sup></sub></sup>
