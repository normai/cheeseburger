﻿# Demonstrate Assertions

Assertions are some way of testing code at runtime, typically used to
 ensure parameters and return values to comply with certain conditions. …

## Python &nbsp; <sup><sub>[py715assert.py](./../py/py715assert.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

<!--- a href="./runs/py715assert.png">
 <img src ="./runs/py715assert.png" width="650" height="33" data-dims="x1084y0033" alt="Run py715assert.py">
 </a --->

&nbsp;


## Java &nbsp; <sup><sub>[jv715assert.java](./../java/jv715assert.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jv715assert.png">
 <img src ="./runs/jv715assert.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv715assert.java">
 </a --->

&nbsp;


## C-Sharp &nbsp; <sup><sub>[cs715assert.cs](./../cs/cs715assert.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs715assert.png">
 <img src ="./runs/cs715assert.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs715assert.cs">
 </a --->

&nbsp;

<img src="./icos/20170811o0621.microsoftgray.v5.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Microsoft">
 &nbsp; Microsoft Learn article
 [Assert Class](https://learn.microsoft.com/en-us/dotnet/api/microsoft.visualstudio.testtools.unittesting.assert)
 …
 <!--- [ref 20221224°1022] --->

&nbsp;

<img src="./icos/20221224o1033.makolyte.v1.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Makolyte">
 &nbsp; Makolyte article
 [C# – Use Assert.ThrowsException instead of ExpectedException attribute](https://makolyte.com/csharp-use-assert-throwsexception-instead-of-expectsexception-attribute/)
 …
 <!--- [ref 20221224°1032] --->

&nbsp;

<img src="./icos/20170811o0621.microsoftgray.v5.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Microsoft">
 &nbsp; Microsoft Learn article
 [Utility Members](https://learn.microsoft.com/en-us/previous-versions/windows/embedded/ee437011%28v=msdn.10%29)
 about the no more maintained Utility class …
 <!--- [ref 20221224°1042] --->

&nbsp;

<img src="./icos/20170811o0621.microsoftgray.v5.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Microsoft">
 &nbsp; Microsoft article
 [Assert.ThrowsException Method](https://learn.microsoft.com/en-us/dotnet/api/microsoft.visualstudio.testtools.unittesting.assert.throwsexception?view=visualstudiosdk-2022)
 …
 <!--- [ref 20221224°1044] --->

&nbsp;

<img src="./icos/20170811o0621.microsoftgray.v5.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Microsoft">
 &nbsp; Microsoft article
 [Best practices for exceptions](https://learn.microsoft.com/en-us/dotnet/standard/exceptions/best-practices-for-exceptions)
 …
 <!--- [ref 20221224°1046] --->

&nbsp;

<img src="./icos/20170811o0621.microsoftgray.v5.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Microsoft">
 &nbsp; Microsoft article
 [Using Standard Exception Types](https://learn.microsoft.com/en-us/dotnet/standard/design-guidelines/using-standard-exception-types)
 …
 <!--- [ref 20221224°1048] --->

&nbsp;

<img src="./icos/20170811o0621.microsoftgray.v5.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Microsoft">
 &nbsp; Microsoft article
 [Code contracts (.NET Framework)](https://learn.microsoft.com/en-us/dotnet/framework/debug-trace-profile/code-contracts)
 …
 <!--- [ref 20221224°1052] --->

&nbsp;

<img src="./icos/20170811o0621.microsoftgray.v5.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Microsoft">
 &nbsp; Microsoft article
 [Nullable reference types](https://learn.microsoft.com/en-us/dotnet/csharp/nullable-references)
 …
 <!--- [ref 20221224°1054] --->

&nbsp;

<img src="./icos/20221224o1313.code4it.v1.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Code4IT">
 &nbsp; Code4IT article
 [MSTest Assert class - an overview](https://www.code4it.dev/blog/mstests-assert-overview)
 …
 <!--- [ref 20221224°1312] --->

&nbsp;

<img src="./icos/20221224o1323.codejourney.v1.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Code_Journey">
 &nbsp; Code_Journey article
 [Improve your tests with Assert Object Pattern](https://www.codejourney.net/improve-your-tests-with-assert-object-pattern/)
 …
 <!--- [ref 20221224°1322] --->

&nbsp;

Installing the Microsoft.VisualStudio.QualityTools.UnitTestFramework.dll assembly
 in the GAC goes like this with Admin rights:

<a href="./imgs/20221224o1042.vs-dev-prompt-admin--gactuil-install.png">
 <img src ="./imgs/20221224o1042.vs-dev-prompt-admin--gactuil-install.png" width="650" height="200" data-dims="x1084y0200" alt="Run cs715assert.cs">
 </a>

&nbsp;



## JavaScript &nbsp; <sup><sub>[js715assert.js](./../js/js715assert.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js715assert.png">
 <img src ="./runs/js715assert.png" width="650" height="33" data-dims="x1084y0033" alt="Run js715assert.js">
 </a --->

&nbsp;


## PHP &nbsp; <sup><sub>[ph715assert.php](./../php/ph715assert.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph715assert.png">
 <img src ="./runs/ph715assert.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph715assert.php">
 </a --->

&nbsp;


## CPP &nbsp; <sup><sub>[cp715assert.cpp](./../cpp/cp715assert.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp715assert.png">
 <img src ="./runs/cp715assert.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp715assert.cpp">
 </a --->

&nbsp;


---

<sup><sub>*[File 20221224°1201 x715assert.md]* ⬞Ω</sub></sup>
