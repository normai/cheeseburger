﻿# Demonstrate Interface

## Python &nbsp; <sup><sub>[py560iface.py](./../py/py560iface.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

<!--- a href="./runs/py560iface.png">
 <img src ="./runs/py560iface.png" width="650" height="33" data-dims="x1084y0033" alt="Run py560iface.py">
 </a --->

&nbsp;


## Java &nbsp; <sup><sub>[jv560iface.java](./../java/jv560iface.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jv560iface.png">
 <img src ="./runs/jv560iface.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv560iface.java">
 </a --->

&nbsp;


## C-Sharp &nbsp; <sup><sub>[cs560iface.cs](./../cs/cs560iface.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs560iface.png">
 <img src ="./runs/cs560iface.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs560iface.cs">
 </a --->

&nbsp;


## JavaScript &nbsp; <sup><sub>[js560iface.js](./../js/js560iface.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js560iface.png">
 <img src ="./runs/js560iface.png" width="650" height="33" data-dims="x1084y0033" alt="Run js560iface.js">
 </a --->

&nbsp;


## PHP &nbsp; <sup><sub>[ph560iface.php](./../php/ph560iface.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph560iface.png">
 <img src ="./runs/ph560iface.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph560iface.php">
 </a --->

&nbsp;


## CPP &nbsp; <sup><sub>[cp560iface.cpp](./../cpp/cp560iface.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp560iface.png">
 <img src ="./runs/cp560iface.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp560iface.cpp">
 </a --->

&nbsp;


---

<sup><sub><sup>*[File 20220830°1401]* ⬞Ω</sup></sub></sup>
