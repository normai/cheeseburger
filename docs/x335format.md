﻿# Demonstrate Format Functions

Compare
 [x153stripol.md](./x153stripol.md)

## Python &nbsp; <sup><sub>[py335format.py](./../py/py335format.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

<!--- a href="./runs/py335format.png">
 <img src ="./runs/py335format.png" width="650" height="33" data-dims="x1084y0033" alt="Run py335format.py">
 </a --->

<img src="./icos/20221215o0913.pylenin.v1.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Pylenin">
 &nbsp; Pylenin Mishra's tutorial
 [Understanding width and precision in Python string formatting](https://www.pylenin.com/blogs/python-width-precision/)  <!-- [ref 20221215°0912] -->
 — Learn how width and precision works in Python string formatting.

&nbsp;


## Java &nbsp; <sup><sub>[jv335format.java](./../java/jv335format.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jv335format.png">
 <img src ="./runs/jv335format.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv335format.java">
 </a --->

&nbsp;


## C-Sharp &nbsp; <sup><sub>[cs335format.cs](./../cs/cs335format.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs335format.png">
 <img src ="./runs/cs335format.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs335format.cs">
 </a --->

&nbsp;


## JavaScript &nbsp; <sup><sub>[js335format.js](./../js/js335format.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js335format.png">
 <img src ="./runs/js335format.png" width="650" height="33" data-dims="x1084y0033" alt="Run js335format.js">
 </a --->

<img src="./icos/20221215o1513.careerkarma.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for Carreer Karma">
 &nbsp; Carreer Karma tutorial
 [JavaScript String Interpolation: A Beginner’s Guide](https://careerkarma.com/blog/javascript-string-interpolation/) <!-- [ref 20221215°1512] -->
 — …

&nbsp;


## PHP &nbsp; <sup><sub>[ph335format.php](./../php/ph335format.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph335format.png">
 <img src ="./runs/ph335format.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph335format.php">
 </a --->

<img src="./icos/20200912o1235.riptutorial.v2.x0032y0032.png" align="left" width="32" height="32" alt="Link icon for RipTutorial">
 &nbsp; RipTutorial article
 [String Interpolation Example](https://riptutorial.com/php/example/22788/string-interpolation)
 — …
 <!--- [ref 20221215°1522] --->

&nbsp;


## CPP &nbsp; <sup><sub>[cp335format.cpp](./../cpp/cp335format.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp335format.png">
 <img src ="./runs/cp335format.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp335format.cpp">
 </a --->

&nbsp;


---

<sup><sub><sup>*[File 20220828°1001]* ⬞Ω</sup></sub></sup>
