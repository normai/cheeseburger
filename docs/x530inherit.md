﻿# Demonstrate Inheritance

## Python &nbsp; <sup><sub>[py530inherit.py](./../py/py530inherit.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

<!--- a href="./runs/py530inherit.png">
 <img src ="./runs/py530inherit.png" width="650" height="33" data-dims="x1084y0033" alt="Run py530inherit.py">
 </a --->

&nbsp;


## Java &nbsp; <sup><sub>[jv530inherit.java](./../java/jv530inherit.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jv530inherit.png">
 <img src ="./runs/jv530inherit.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv530inherit.java">
 </a --->

&nbsp;


## C-Sharp &nbsp; <sup><sub>[cs530inherit.cs](./../cs/cs530inherit.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs530inherit.png">
 <img src ="./runs/cs530inherit.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs530inherit.cs">
 </a --->

&nbsp;


## JavaScript &nbsp; <sup><sub>[js530inherit.js](./../js/js530inherit.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js530inherit.png">
 <img src ="./runs/js530inherit.png" width="650" height="33" data-dims="x1084y0033" alt="Run js530inherit.js">
 </a --->

&nbsp;


## PHP &nbsp; <sup><sub>[ph530inherit.php](./../php/ph530inherit.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph530inherit.png">
 <img src ="./runs/ph530inherit.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph530inherit.php">
 </a --->

&nbsp;


## CPP &nbsp; <sup><sub>[cp530inherit.cpp](./../cpp/cp530inherit.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp530inherit.png">
 <img src ="./runs/cp530inherit.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp530inherit.cpp">
 </a --->

&nbsp;


---

<sup><sub><sup>*[File 20220830°0901]* ⬞Ω</sup></sub></sup>
