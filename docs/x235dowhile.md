﻿# Demonstrate Do-While-Loop

The Do-While-Loop is also called a *foot-controlled* loop, it first executes,
 then asks, whether to execute again. As opposed to the (normal) *head-controlled* loop,
 which first asks whether to execute at all.

The Do-While-Loop is rarely used. You have to think a bit for finding
 a suited use case at all.

## Python &nbsp; <sup><sub>[py235dowhile.py](./../py/py235dowhile.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

<a href="./runs/py235dowhile.png">
 <img src ="./runs/py235dowhile.png" width="654" height="265" data-dims="x1091y442" alt="Run py235dowhile.py">
 </a>

Python has no `do/while` statement. So a foot-controlled loop has to be
 achieved with other means.

&nbsp;


## Java &nbsp; <sup><sub>[jv235dowhile.java](./../java/jv235dowhile.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jv235dowhile.png">
 <img src ="./runs/jv235dowhile.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv235dowhile.java">
 </a --->

&nbsp;


## C-Sharp &nbsp; <sup><sub>[cs235dowhile.cs](./../cs/cs235dowhile.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs235dowhile.png">
 <img src ="./runs/cs235dowhile.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs235dowhile.cs">
 </a --->

&nbsp;


## JavaScript &nbsp; <sup><sub>[js235dowhile.js](./../js/js235dowhile.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js235dowhile.png">
 <img src ="./runs/js235dowhile.png" width="650" height="33" data-dims="x1084y0033" alt="Run js235dowhile.js">
 </a --->

&nbsp;


## PHP &nbsp; <sup><sub>[ph235dowhile.php](./../php/ph235dowhile.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph235dowhile.png">
 <img src ="./runs/ph235dowhile.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph235dowhile.php">
 </a --->

&nbsp;


## CPP &nbsp; <sup><sub>[cp235dowhile.cpp](./../cpp/cp235dowhile.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp235dowhile.png">
 <img src ="./runs/cp235dowhile.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp235dowhile.cpp">
 </a --->

&nbsp;


---

<sup><sub><sup>*[File 20220826°1601 x235dowhile.md]* ⬞Ω</sup></sub></sup>
