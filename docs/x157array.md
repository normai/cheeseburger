# Demonstrate Arrays

Arrays are continuous areas in the memory, divided into equally-sized
elements, which are accessed by an index.

## Python &nbsp; <sup><sub>[py157array.py](./../py/py157array.py)</sub></sup> &nbsp; <img src="./imgs/20180619o2144.serpentnowed.v1.x0128y0085.png" width="57" height="38" alt="Python icon">

Python has no Array type, instead it has the very comfortable List class.

Oops, surely **Python has arrays**, just not built-in. There is a module
 `array` coming with the Python standard installation, offering some
 array features *(not yet shown in the demo)*.

<a href="./runs/py157array.png">
 <img src ="./runs/py157array.png" width="637" height="341" data-dims="x0980y0526" alt="Run py157array.py">
 </a>

&nbsp;

<img src="./icos/20200805o1243.python.v2.x0048y0048.png" align="left" width="32" height="32" alt="Link icon for Python.org">
 &nbsp; Python documentation page
 [array — Efficient arrays of numeric values](https://docs.python.org/3/library/array.html)
 <!--- [ref 20221214°1542] --->

&nbsp;

<img src="./icos/20200630o0143.w3schools.v2.x0048y0048.png" align="left" width="32" height="32" alt="Link icon for W3Schools">
 &nbsp; W3Schools article
 [Python Arrays](https://www.w3schools.com/python/python_arrays.asp)
 <!---[ref 20220901°1033] -->
 just tells about the List class, not what the `array` module can do.

&nbsp;

<img src="./icos/20220318o1503.pythonwife.v2.x0048y0048.png" align="left" width="32" height="32" alt="Link icon for PythonWife">
 &nbsp; PythonWife article
 [Arrays in Python](https://pythonwife.com/arrays-in-python/)
 <!---[ref 20230706°1515] -->
 👍

&nbsp;

## Java &nbsp; <sup><sub>[jv157array.java](./../java/jv157array.java)</sub></sup> &nbsp; <img src="./imgs/20180924o1117.tardigraduscut.v1.x0128y0128.png" width="51" height="51" alt="Java icon">

<!--- a href="./runs/jv157array.png">
 <img src ="./runs/jv157array.png" width="650" height="33" data-dims="x1084y0033" alt="Run jv157array.java">
 </a --->

&nbsp;

<img src="./icos/20200630o0143.w3schools.v2.x0048y0048.png" align="left" width="32" height="32" alt="Link icon for W3Schools pages">
 &nbsp; W3Schools article
 [Java ArrayList](https://www.w3schools.com/java/java_arraylist.asp)
 <!--- [ref 20221217°1232] --->

&nbsp;

<img src="./icos/20200630o0143.w3schools.v2.x0048y0048.png" align="left" width="32" height="32" alt="Link icon for W3Schools pages">
 &nbsp; W3Schools article
 [Java LinkedList](https://www.w3schools.com/java/java_linkedlist.asp)
 <!--- [ref 20221217°1234] --->

&nbsp;

## C-Sharp &nbsp; <sup><sub>[cs157array.cs](./../cs/cs157array.cs)</sub></sup> &nbsp; <img src="./imgs/20180504o0153.rooster.v2.x0128y0128.png" width="55" height="55" alt="C-Sharp icon">

<!--- a href="./runs/cs157array.png">
 <img src ="./runs/cs157array.png" width="650" height="33" data-dims="x1084y0033" alt="Run cs157array.cs">
 </a --->

&nbsp;

<img src="./icos/20200630o0143.w3schools.v2.x0048y0048.png" align="left" width="32" height="32" alt="Link icon for W3Schools pages">
 &nbsp; W3Schools article
 [C# Arrays](https://www.w3schools.com/cs/cs_arrays.php)
 <!--- [ref 20221217°1236] --->

&nbsp;

<img src="./icos/20170811o0621.microsoftgray.v5.x0048y0048.png" align="left" width="32" height="32" alt="Link icon for Microsoft pages">
 &nbsp; Microsoft documentation article
 [Arrays (C# Programming Guide)](https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/arrays/)
 <!--- [ref 20221217°1242] --->

&nbsp;

<img src="./icos/20110921o1121.stackoverflow.v1.x0048y0048.gif" align="left" width="32" height="32" alt="Link icon for StackOverflow pages">
 &nbsp; StackOverflow thread
 [C++ Vector like class in C#](https://stackoverflow.com/questions/18895727/c-vector-like-class-in-c-sharp)
 <!--- [ref 20221217°1246] --->

&nbsp;

<img src="./icos/20170811o0621.microsoftgray.v5.x0048y0048.png" align="left" width="32" height="32" alt="Link icon for Microsoft pages">
 &nbsp; Microsoft reference
 [List&lt;T&gt; Class](https://docs.microsoft.com/en-us/dotnet/api/system.collections.generic.list-1)
 <!--- [ref 20221217°1244] --->

&nbsp;

## JavaScript &nbsp; <sup><sub>[js157array.js](./../js/js157array.js)</sub></sup> &nbsp; <img src="./imgs/20180508o1733.bird-looking-over-its-shoulder.v1.x0128y0127.png" width="51" height="51" alt="Icon for JavaScript">

<!--- a href="./runs/js157array.png">
 <img src ="./runs/js157array.png" width="650" height="33" data-dims="x1084y0033" alt="Run js157array.js">
 </a --->

&nbsp;

<img src="./icos/20200630o0143.w3schools.v2.x0048y0048.png" align="left" width="32" height="32" alt="Link icon for W3Schools pages">
 &nbsp; W3Schools article
 [JavaScript Arrays](https://www.w3schools.com/js/js_arrays.asp)
 <!--- [ref 20221217°1248] --->

&nbsp;

<img src="./icos/20180511o0333.mozilla1logo.v2.x0048y0048.png" align="left" width="32" height="32" alt="Link icon for Mozilla pages">
 &nbsp; Mozilla article
 [JavaScript → Standard built-in objects → Array](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array)
 <!--- [ref 20221217°1252] --->

&nbsp;

## PHP &nbsp; <sup><sub>[ph157array.php](./../php/ph157array.php)</sub></sup> &nbsp; <img src="./imgs/20180718o1618.elephpant.v0.x0128y0088.png" width="57" height="39" alt="Icon for PHP">

<!--- a href="./runs/ph157array.png">
 <img src ="./runs/ph157array.png" width="650" height="33" data-dims="x1084y0033" alt="Run ph157array.php">
 </a --->

&nbsp;

<img src="./icos/20200630o0143.w3schools.v2.x0048y0048.png" align="left" width="32" height="32" alt="Link icon for W3Schools pages">
 &nbsp; W3Schools article
 [PHP Arrays](https://www.w3schools.com/php/php_arrays.asp)
 <!--- [ref 20221217°1238] --->

&nbsp;

## CPP &nbsp; <sup><sub>[cp157array.cpp](./../cpp/cp157array.cpp)</sub></sup> &nbsp; <img src="./imgs/20140423o0633.hydra.v2.x0180y0124.png" width="68" height="47" alt="Icon for CPP">

<!--- a href="./runs/cp157array.png">
 <img src ="./runs/cp157array.png" width="650" height="33" data-dims="x1084y0033" alt="Run cp157array.cpp">
 </a --->

&nbsp;

<img src="./icos/20200630o0143.w3schools.v2.x0048y0048.png" align="left" width="32" height="32" alt="Link icon for W3Schools pages">
 &nbsp; W3Schools article
 [C++ Arrays](https://www.w3schools.com/cpp/cpp_arrays.asp)
 <!--- [ref 20220901°1032] --->

&nbsp;

<img src="./icos/20150308o0524.cplusplus.x0048y0048.png" align="left" width="32" height="32" alt="Link icon for CPlusPlus.com">
 &nbsp; CPlusPlus.com article
 [std::vector](https://cplusplus.com/reference/vector/vector/)
 <!--- [ref 20221217°1254] --->

&nbsp;


## By The Way

<img src="./icos/20220901o1043.bundesland24.v2.x0048y0048.png" align="left" width="48" height="48" alt="Link icon for Bundesland24.de">
&nbsp; Bundesland24.de articles providing sample data:

- [Die 15 längsten Flüsse in Deutschland](https://bundesland24.de/fluesse/)
 <!--- [ref 20220901°1042] --->

- [Die 15 höchsten Berge von Deutschland](https://bundesland24.de/berge/)
 <!--- [ref 20220901°1044] --->

- [Die 15 größten Seen in Deutschlands](https://bundesland24.de/seen/)
 <!--- [ref 20220901°1047] --->

- [Die 15 größten Inseln Deutschlands](https://bundesland24.de/inseln/)
 <!--- [ref 20220901°1048] --->

&nbsp;

---

<sup><sub><sup>*[File 20220831°0801 x157array.md]* ⬞Ω</sup></sub></sup>
