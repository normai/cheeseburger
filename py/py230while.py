﻿#  file        : id 20220826°1511 — gitlab.com/normai/cheeseburger …/py/py230while.py
#  version     : • 20220910°1011 v0.1.7 Filling • 20220826°1511 v0.1.6 Stub
#  license     : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
#  subject     : Demonstrate while loop
#  summary     :
sPYTHONV = 'Python ' + __import__('platform').python_version()
sVERSION = 'v0.1.7'

from random import randint

print("*** Hailo, yah hai `py230while.py` %s — While Loop (%s) ***" % (sVERSION, sPYTHONV))  # "*** "हैलो, यह है"" does not print nice

# (1) Preparation — Roll dice first time
iDicenum = randint(1, 6)
iTimes = 1
print("Roll numbers until you yield the six. Your first number is " + str(iDicenum))

# (2) Continue as long no six is rolled
while iDicenum != 6:
   iDicenum = randint(1, 6)
   iTimes += 1
   print(" - Number " + str(iDicenum) + " is not six")

print("You took", iTimes, "rolls to yield the six.")

print("Alavida.")  # "अलविदा"" does not print nice
