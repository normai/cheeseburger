﻿#  file        : id 20221012°1011 — gitlab.com/normai/cheeseburger …/py/py137int.py
#  subject     : Demonstrate Integer Type
#  version     : • 20221018°1111 v0.1.8 Filling • 20221012°1011 v0.1.8 Stub
#  license     : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
#  status      :
#  todo        : Use example from CS/JS/PHP/CPP
#  ref         : https://www.pythonpool.com/python-max-int/ [ref 20221016°1212]
#  ref         : https://www.delftstack.com/howto/python/python-max-int/ [ref 20221016°1213]
#  ref         : https://stackoverflow.com/questions/50446353/is-there-something-sys-minint-in-python-similar-to-sys-maxint [ref 20221016°1214]
#  ref         : https://www.delftstack.com/api/python/python-sys-maxsize/ [ref 20221018°1312]
sPYTHONV = 'Python ' + __import__('platform').python_version()
sVERSION = 'v0.1.8'

import sys

print("*** Nǐ hǎo, zhè shì `py137int.py` %s — Demonstrate Integer Type (%s) ***" % (sVERSION, sPYTHONV))  # {"你好，这是"} does not print nice

i1 = 123_456
i2 = -123_456
i3 = 123_456_777_777_777_777_777_777_777_777_777_777_777_789
i4 = -123_456_777_777_777_777_777_777_777_777_777_777_777_789
print("(1) i1 =", i1, type(i1))
print("(2) i2 =", i2, type(i2))
print("(3) i3 =", i3, type(i3))
print("(4) i4 =", i4, type(i4))

print("(5) sys.maxsize =", sys.maxsize, type(sys.maxsize))

print("Zàijiàn.")  # "再见" [Zàijiàn] does not print nice
