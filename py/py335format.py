﻿# THIS FILE IS AN EMPTY STUB, IT IS YET TO BE FILLED WITH CODE

#  file        : id 20220828°1011 — gitlab.com/normai/cheeseburger …/py/py335format.py
#  version     : • 20xxxxxx°xxxx Filling • 20220828°1011 v0.1.6 Stub
#  license     : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
#  subject     : Demonstrate Python string formatting
#  summary     :
#  ref         : https://www.linisnil.com/articles/practical-guide-to-python-string-format-specifiers/ [ref 20220907°1022]
#  ref         : https://sahiljain444.medium.com/format-specifiers-in-python-601df860a6a4 [ref 20220907°1026]
#  ref         : https://realpython.com/python-string-formatting/  [ref 20220907°1032]
#  ref         : https://www.w3schools.com/python/ref_string_format.asp [ref 20220907°1033]
#  ref         : https://docs.python.org/3/library/string.html [ref 20220907°1034]
sPYTHONV = 'Python ' + __import__('platform').python_version()
sVERSION = 'v0.0.0'

print("*** Dobrý deň, toto je `py335format.py` %s — String formatting (%s) ***" % (sVERSION, sPYTHONV))





print("Dovidenia.")
