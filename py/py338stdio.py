﻿# file         : 20220828°091 py338stdio.py v20241103°0811
# summary      : Demonstrate some operations with the standard streams
# peculiarity  : Takes user input, needs at least one Enter press to finish
# status       : Just a quick start on the topic. Much more should be done, e.g.
#                 • Use sys.stdin.read()
#                 • Call script from batchfiles providing input files
#                 • Redirect standard streams to/from files
# license      : BSD-3Clause License | © 2022 - 2024 Norbert C. Maier
# encoding     : UTF-8-with-BOM
# ref          : https://en.wikipedia.org/wiki/Standard_streams [ref 20241102°2252]
# ref          : https://de.wikipedia.org/wiki/Standard-Datenstr%C3%B6me [ref 20241102°2253]
# ref          : https://www.geeksforgeeks.org/take-input-from-stdin-in-python/ [ref 20241102°2255]
# ref          : https://pytutorial.com/python-sysstdin-handling-standard-input/ [ref 20241102°2302]
# ref          : https://stackoverflow.com/questions/4675728/redirect-stdout-to-a-file-in-python [ref 20241102°2312]
# ref          : https://stackoverflow.com/questions/14245227/python-reset-stdout-to-normal-after-previously-redirecting-it-to-a-file [ref 20241102°2314]

import sys

print("*** Привет, это [Privet, eto] `py338stdio.py` ***")

sys.stdout.write('(1) Here is output via sys.stdout.write()\n')        # Use stdout

sys.stderr.write('(2) Here is a message via sys.stderr.write()\n')     # Use stdout

# Save original stdout to restore it easily
orgStdout = sys.stdout

print('(3) Redirect stdout to file, flavour one')
sys.stdout = open('tmp.txt', 'w')                                      # Redirect 1 stdout
print("Aloha sys.stdout")
sys.stdout = orgStdout
print("Did restore sys.stdout")

print('(4) Redirect stdout to file, flavour two')
with open('file.txt', 'w') as sys.stdout:                              # Redirect 2 stdout
    print('test')
sys.stdout = orgStdout

print('(5) Here comes reading via sys.stdin ..')
print('Input lines ending with Enter. Exit with Q or with an empty line.')
for sLine in sys.stdin:                                                # Use stdin
    if 'q' == sLine.strip().lower() or '' == sLine.strip():
        break
    print(f'Line : {sLine}')
print("Input finished.")

print("до свидания [Do svidaniya].")
