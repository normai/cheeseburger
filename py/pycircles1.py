﻿#  file        : id 20220928°1811 — gitlab.com/normai/cheeseburger … py/pycircles1.py
#  version     : • 20220928°1811 v0.1.8 Initial
#  license     : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
#  subject     : Draw ASCII Circles and other patterns
#  summary     :
#  reference   : https://www.onlinemath4all.com/how-to-determine-if-a-point-is-inside-or-outside-a-circle.html [ref 20220928°1822]
#  issue       : The for loop here counts from 0 to size -1. Compare the Java version,
#                which counts from 1 to size. Possibly refactore this Python version
#                to counting from 1 to size as well [issue 20221001°0911]
sPYTHONV = 'Python ' + __import__('platform').python_version()
sVERSION = 'v0.1.8'

def IsPointOnCirlce(r, x, y) :
   tolerance = r                                            # Empirical value for line width
   if abs(pow(x, 2) + pow(y, 2) - pow(r, 2)) < tolerance:
      return True
   else :
      return False

def IsPointOnDiagonal(x, y, size) :
   if x == size - y - 1 :                                   # Subtract 1 empirically
      return True
   else :
      return False

print("*** Dobrý deň, toto je `pycircles1.py` %s — Draw ASCII Circles (%s) ***" % (sVERSION, sPYTHONV))

sizes = [7, 17]
for size in sizes :                                            # Paint as many rectangles as sizes are given

   # Prepare convenient variables
   # Note how the integer division for the radius looks different in each language
   radius = size // 2 - 1                                      # Make size of circle slightly smaller than the rectangle
   shiftx = radius + 1                                         # Shift circle center from rectangle left to rectangle center
   shifty = radius + 1                                         # Shift circle center from rectangle top to rectangle center

   # Iterate over lines and columns
   for line in range(size) :
      for col in range(size) :
         if IsPointOnCirlce(radius, line - shiftx, col - shifty) :  # Is this point on the circle?
            print("@ ", end="")
         else :
            if IsPointOnDiagonal(line, col, size) :            # Is this point on the diagonal?
               print("# ", end="")
            else :                                             # All other cases
               print("· ", end="")
      print()                                                  # Goto next line

print("Dovidenia.")
