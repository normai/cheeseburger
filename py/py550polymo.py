﻿# THIS FILE IS AN EMPTY STUB, IT IS YET TO BE FILLED WITH CODE

#  file        : id 20220830°1311 — gitlab.com/normai/cheeseburger …/py/py550polymo.py
#  version     : • 20xxxxxx°xxxx Filling • 20220830°1311 v0.1.6 Stub
#  license     : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
#  subject     : Demonstrate polymorphism
#  reference   : https://www.codeproject.com/Articles/5314882/Python-vs-Cplusplus-Series-Polymorphism-and-Duck-T [ref 20211012°1122]
#  status      : empty
sPYTHONV = 'Python ' + __import__('platform').python_version()
sVERSION = 'v0.0.0'

print("*** Tere, see on `py550polymo.py` %s — Polymorphismus (%s) ***" % (sVERSION, sPYTHONV))





print("Hüvasti.")
