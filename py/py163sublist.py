﻿#  file        : id 20210929°1730 — gitlab.com/normai/cheeseburger …/py163sublist.py
#  version     : • 20220908°1351 v0.1.7 Filling • 20210929°1730 Stub
#  license     : BSD 3-Clause | © 2021 - 2024 Norbert C. Maier
#  subject     : This demonstrates sublists
sPYTHONV = 'Python ' + __import__('platform').python_version()
sVERSION = 'v0.1.7'

print("*** Ahoj `py163sublist.py` %s — (%s) ***" % (sVERSION, sPYTHONV))

l = ["Uno", "Due", "Tre", "Quattro", "Cinque", "Sei", "Sette"]
print("(1) l      =", l)
print("(2) l[:2]  =", l[:2])
print("(3) l[2:]  =", l[2:])
print("(4) l[:-2] =", l[:-2])
print("(5) l[2:5] =", l[2:5])

print("Nashledanou.")
