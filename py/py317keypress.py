﻿#!/usr/bin/env python
#  file        : 20220827°1611 (from 20201221°1011) — https://gitlab.com/normai/cheeseburger/-/blob/main/py/py317keypress.py
#  summary     : This demonstrates how to get key presses via Tkinter without pressing Enter
#  versions    : • 20240921°1221 v0.1.7 Code filled from file 20201221°1011 https://www.trekta.biz/svn/demospy/trunk/gizmos/keysprobe.py
#                • 20220827°1611 v0.1.6 Stub
#  license     : BSD 3-Clause License | © 2020 - 2024 Norbert C. Maier
#  encoding    : UTF-8-with-BOM
#  requires    : Python 3.6
#  subject     :

"""
   Demonstrate how to get key presses via Tkinter without pressing Enter
"""

import sys, time

sPYTHONV = 'Python ' + __import__('platform').python_version()
sVERSION = 'v0.1.7'

print("*** Bună, acesta este `py317keypress.py` %s — KeyPress via Tkinter (%s) ***" % (sVERSION, sPYTHONV))

if False :
   print('Your Python executable is :')
   print('        ' + sys.executable)


# [seq 20201221°1013]
try :
   import tkinter as tk                                                # No GUI, just get key strokes
except :
   e = sys.exc_info()[0]
   print("Module 'tkinter' seems not available ('" + str(e) + "')")
   input("Sorry, this script will not run -- Press Enter to exit.")
   sys.exit(123)


# [func 20201221°1015]
# Get a key without pressing Enter (after ref 20201221°0932)
# note : It responds as well to a modifier key like SHIFT as to any other keys.
# note : It always delivers lowercase chars, even if CAPSLOCK is on.
def keypress(event) :
    x = event.char
    x = x.lower() # indeed superfluous?
    if x == "a" :
        print("Blaha blaha blaha ..")
    elif x == "w" :
        print("Blaw blaw blaw ..")
    elif event.keysym == 'Escape' :
        print("You pressed ESC ..")
    else :
        print("You pressed '" + x + "'")
        time.sleep(1.1)
        root.destroy()


# [seq 20201221°1017]
###print('*** Welcome to keysprobe.py ***')
root = tk.Tk()
print("Use Keyboard : A = Msg, W = Msg, ESC = Msg, OtherKey = Exit")
root.bind_all('<Key>', keypress)
root.withdraw()                                                        # Don't show the tk window
root.mainloop()


# [seq 20201221°1018]
print ("Tk's mainloop is over. La revedere.")
time.sleep(2)


#------------------------------------------------------
# session 20201221°0911 'pygetkey'
# ref : https://stackoverflow.com/questions/24072790/detect-key-press-in-python [ref 20201221°0912]
# ref : https://pypi.org/project/keyboard/ [ref 20201221°0913]
# ref : https://github.com/boppreh/keyboard [ref 20201221°0916]
# ref : https://pythonhosted.org/pynput/ [ref 20201221°0921]
# ref : https://pypi.org/project/pynput/ [ref 20201221°0922]
# ref : https://github.com/moses-palmer/pynput [ref 20201221°0926]
# ref : https://pynput.readthedocs.io/en/latest/ [ref 20201221°0928]
# ref : https://www.daniweb.com/programming/software-development/threads/115282/get-key-press (this mentions TKinter) [ref 20201221°0932]
# ref : https://stackoverflow.com/questions/6501121/difference-between-exit-and-sys-exit-in-python [ref 20201221°0934]
# ref : https://stackoverflow.com/questions/56729764/python-3-7-asyncio-sleep-and-time-sleep [ref 20201221°0935]
# ref : https://www.tcl.tk/man/tcl8.4/TkCmd/keysyms.htm [ref 20201221°0936]
# ref : https://pythonexamples.org/ [ref 20201221°0944]
#------------------------------------------------------
