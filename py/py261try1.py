﻿#  file        : id 20210928°1541 — gitlab.com/normai/cheeseburger …/py/py261try1.py
#  version     : • 20220908°1911 v0.1.7 Tweak • 20210928°1541 v0.0.1 Initial filling
#  license     : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
#  subject     : Demonstrate Python Exception Handling
#  summary     : Present two snippets • plain except • except/else
#  ref         : E.g. www.w3schools.com/python/ref_keyword_try.asp [ref 20220214°1152]
#  ref         : E.g. www.w3schools.com/python/ref_keyword_except.asp [ref 20220214°1153]
sPYTHONV = 'Python ' + __import__('platform').python_version()
sVERSION = 'v0.1.7'

from random import randint
import math

print("*** Kon'nichiwa, desu `py261try1.py` %s — Try/Except (%s) ***" % (sVERSION, sPYTHONV))  # "こんにちは、です [Kon'nichiwa, desu]" does not print nice
print()

# (1) Try adding mismatching types
print("(1) Add 'Eins' and 2")
try:
    sum = 'Eins' + 2
except:
    print("    => Adding 'Eins' and 2 failed.")

# (2) Calculate square root, sometimes succeed, sometimes fail
randi = randint(-9, 9)
print("\n(2) Calculate sqare root of", randi)
try:
    root = math.sqrt(randi)
except:
    print("    => Sqare root of", randi, "calculation failed")
else:
    print("    => Sqare root of", randi, "is", root)

print()
print("Sayōnara.")  # "さようなら [Sayōnara]." does not print nice
