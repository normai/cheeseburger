﻿#  file        : id 20221012°1711 — gitlab.com/normai/cheeseburger …/py/py155strfunc.py
#  version     : • 20221013°1111 v0.1.8 Filling • 20221012°1711 Stub
#  license     : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
#  subject     : Demonstrate String Functions
#  userstory   :
#  summary     :
#  ref         : https://www.freecodecamp.org/news/how-to-substring-a-string-in-python/ [ref 20221009°0932]
#  todo        : Add • split function • a simple RegEx [todo 20221013°1715]
sPYTHONV = 'Python ' + __import__('platform').python_version()
sVERSION = 'v0.1.8'

print("*** Hallo, dit is `py155strfunc.py` %s — String Functions (%s) ***" % (sVERSION, sPYTHONV))

# () Fodder
sWord = "SilzuZankunKrei"

# (.) String Functions
# (.1) String length
print("(3.1) Length of '" + sWord + "' is " + str(len(sWord)))

# (.2) Character code to string
print("(3.2) chr() : " + chr(36) + chr(37) + chr(38) + chr(96) + chr(97) + chr(98) + chr(122) + chr(123) + chr(124) + chr(125) + chr(126) + chr(127) + ".")

# (.3) Substrings
print("(3.3) Substrings : " + sWord[0:5] + ' + ' + sWord[5:-4] + ' + ' + sWord[-4:])

# (.4) Upper/Lower
print("(3.4) Upper/Lower : " + sWord.lower() + ' / ' + sWord.upper())

# (.5) Trim
sBreezy = "    Breeze    "
print('(3.5) Trim "' + sBreezy + '" : "' + sBreezy.lstrip() + '", "' + sBreezy.rstrip() + '", "' + sBreezy.strip() + '"')

print("Tot ziens.")
