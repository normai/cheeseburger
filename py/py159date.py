﻿#  file        : id 20221012°1811 — gitlab.com/normai/cheeseburger …/py/py159date.py
#  version     : • 20221230°1321 v0.1.9 Filling • 20221012°1811 v0.1.6 Stub
#  license     : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
#  subject     : Demonstrate Date and Time Types
#  userstory   : Set and print dates in various formats and calculate time spans
#  issue       : The script runtime is about 16000 microseconds, but sometimes
#                 it shows 0 microseconds. How can this be? [issue 20221230°0911]
sPYTHONV = 'Python ' + __import__('platform').python_version()
sVERSION = 'v0.1.9'

import datetime

# Capture script start moment
now1 = datetime.datetime.now()                         # Put this on the very top

print("*** Tere, see on `py159date.py` %s — Date and Time Types (%s) ***" % (sVERSION, sPYTHONV))

# Print casual system information
print('(1) Minimum year =', datetime.MINYEAR, ', maximum year =', datetime.MAXYEAR)

# Provide one past and one future event
dt1 = datetime.datetime(1564, 2, 16)                           # Galileo birthday
dt3 = datetime.datetime(2061, 7, 29, 12, 34, 56, 78)           # Next Halley's Comet approach

# Output the three events
iCnt = 0
lst = [("Galileo´s birthday", dt1), ('Time right now', now1), ("Halley´s approach", dt3)]
for x in lst :
   dt = x[1]
   print('(2.' + str(iCnt + 1) + ')', x[0])
   print('    1 Plain  :', dt)
   print('    2 Parts  : y =', dt.year, 'm =', dt.month, 'd =', dt.day, 'h =', dt.hour, 'm =', dt.minute, 's =', dt.second, 'μs =', dt.microsecond)
   print('    3 Custom :', dt.strftime("%A %Y-%m-%d`%H:%M:%S %f"))
   iCnt += 1;

# Calculate large scale time span
now2 = datetime.datetime.now()
delta = dt3 - now2
print('(3) Wait for Halley :', delta)

# Calculate small scale time span
now3 = datetime.datetime.now()
delta = now3 - now1
print('(4) Script runtime  :', delta.microseconds, 'microseconds (Debug:', now3.microsecond, '-', now1.microsecond, ')')


print("Hüvasti.")
