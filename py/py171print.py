﻿# THIS FILE IS AN EMPTY STUB, IT IS YET TO BE FILLED WITH CODE

#  file        : id 20220827°1311 — gitlab.com/normai/cheeseburger …/py/py171print.py
#  version     : • 20xxxxxx°xxxx Filling • v0.1.6 20220827°1311 Stub
#  license     : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
#  subject     :
sPYTHONV = 'Python ' + __import__('platform').python_version()
sVERSION = 'v0.0.0'

print("*** Namastē, yō​ hō `py171print.py` %s — Print (%s) ***" % (sVERSION, sPYTHONV))  # "नमस्ते, यो ​​हो [Namastē, yō ​​hō]"





print("Namastē.")  # "नमस्ते [Namastē]."
