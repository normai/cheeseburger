﻿#  file        : id 20220219°2111 — gitlab.com/normai/cheeseburger … py/py753humansort.py — version 1
#  version     : • 20220908°2021 v0.1.7 Tweak • 20220219°211 v0.1.6 Filling
#  license     : CC BY-NC-SA 1.0 — creativecommons.org/licenses/by-nc-sa/1.0/
#  subject     : Natural sorting
#  summary     : Implement natural sorting in only few lines. Snipped found
#                 while researching on 'natural' versus 'ASCIIbetical' sorting.
#  copyright   : © 2007 Ned Batchelder and predecessors
#  authors     : Ned Batchelder, remixed by Norbert C. Maier
#  ref         : intellij-support.jetbrains.com/hc/en-us/community/posts/206156789-Class-files-sorting [ref 20220219°2044]
#  ref         : nedbatchelder.com/blog/200712/human_sorting.html [ref 20220219°2046] From here comes the snipped remixed below
#  todo        : Eliminate the True/False flag by providing the both versions in two
#                  separate functions and use them from the main procedure [todo 20220219°2121]
sPYTHONV = 'Python ' + __import__('platform').python_version()
sVERSION = 'v0.1.7'

import re

def tryint(s):
    """
    Return an int if possible, or `s` unchanged
    """

    try:
        return int(s)
    except ValueError:
        return s

def alphanum_key(s):
    """
    Turn a string into a list of string and number chunks
    >>> alphanum_key("z23a")
    ["z", 23, "a"]
    """

    if (True) :                                                        # Toggle
       print("Para =", type(s).__name__, s)                            # Debug
       #return [tryint(c) for c in re.split('([0-9]+)', s)]            # Original line
       return [tryint(c) for c in re.split('([0-9]+)', str(s))]        # Para may be integer! [fix 20220219°2115]
    else :
       return [tryint(c) if c.isdigit() else c for c in re.split('([0-9]+)', str(s))]  # Alternative by David Goodger 2007-Dec-11, modified by adding str() [fix 20220219°2115`02]

def human_sort(l):
    """
    Sort a list in the way that humans expect
    """

    l.sort(key = alphanum_key)


print("*** Kia ora, ko tenei `py753humansort.py` %s — Human Sort (%s) ***" % (sVERSION, sPYTHONV))

s = "z23x123a"
print("String to unpick: \"" + s + "\"")
l = alphanum_key(s)
print(l)
human_sort(l)
print(l)
l = [ 'xy', 'ab','a22b', 'a3b']
print("List to sort :", l)
human_sort(l)
print("Sorted list  :", l)

print("Tēnā koe.")
