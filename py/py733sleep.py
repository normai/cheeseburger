﻿# EMPTY STUB, YET TO BE FILLED WITH CODE

#  file        : id 20221213°1911 — gitlab.com/normai/cheeseburger …/py/py733sleep.py
#  version     : • 20xxxxxx°xxxx Filling • 20221213°1911 Stub
#  license     : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
#  subject     : Demonstrate a sleep function
#  userstory   :
sPYTHONV = 'Python ' + __import__('platform').python_version()
sVERSION = 'v0.0.0'

print("*** Šis ir `py733sleep.py` %s — Sleep function (%s) ***" % (sVERSION, sPYTHONV))





print("Uz redzēšanos.")
