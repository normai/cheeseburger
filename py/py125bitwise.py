﻿#  file        : id 20220826°1111 — gitlab.com/normai/cheeseburger …/py/py125bitwise.py
#  version     : • 20220908°1811 v0.1.7 Filling • 20220826°111 v0.1.6 Stub
#  license     : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
#  subject     : Demonstrate Python bitwise operators
#  summary     :
#  ref         : https://www.w3schools.com/python/gloss_python_bitwise_operators.asp [ref 20220907°1012]
#  ref         : https://realpython.com/python-bitwise-operators/ [ref 20220907°1013]
#  ref         : https://wiki.python.org/moin/BitwiseOperators [ref 20220907°1014]
#  ref         :
#  ref         :
sPYTHONV = 'Python ' + __import__('platform').python_version()
sVERSION = 'v0.1.7'

print("*** Hei, tämä on `py125bitwise.py` %s — Bitwise operators (%s) ***" % (sVERSION, sPYTHONV))

i1 = 43690
i2 = 3855

print("(1) Bitwise AND")
print("(1.1) i1       =   {0:17b} {0:7x} {0:7d}".format(i1))
print("(1.2) i2       =   {0:17b} {0:7x} {0:7d}".format(i2))
print("(1.3) i1 & i2  =   {0:17b} {0:7x} {0:7d}".format(i1 & i2))

print("(2) Bitwise OR")
print("(2.1) i1       =   {0:17b} {0:7x} {0:7d}".format(i1))
print("(2.2) i2       =   {0:17b} {0:7x} {0:7d}".format(i2))
print("(2.3) i1 | i2  =   {0:17b} {0:7x} {0:7d}".format(i1 | i2))

print("(3) Bitwise XOR")
print("(3.1) i1       =   {0:17b} {0:7x} {0:7d}".format(i1))
print("(3.2) i2       =   {0:17b} {0:7x} {0:7d}".format(i2))
print("(3.3) i1 ^ i2  =   {0:17b} {0:7x} {0:7d}".format(i1 ^ i2))

print("(4) Bitwise NOT")
print("(4.1) i1       =   {0:17b} {0:7x} {0:7d}".format(i1))
print("(4.2) ~i1      =   {0:17b} {0:7x} {0:7d}".format(~i1))

print("(5) Bit Shift Left")
print("(5.1) i1       =   {0:17b} {0:7x} {0:7d}".format(i1))
print("(5.2) i1 << 1  =   {0:17b} {0:7x} {0:7d}".format(i1 << 1))

print("(6) Bit Shift Right")
print("(6.1) i1       =   {0:17b} {0:7x} {0:7d}".format(i1))
print("(6.2) i1 >> 1  =   {0:17b} {0:7x} {0:7d}".format(i1 >> 1))

print("Näkemiin.")
