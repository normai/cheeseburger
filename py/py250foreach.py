﻿#  file        : id 20210928°1321 — gitlab.com/normai/cheeseburger … py/py250foreach.py
#  version     : • 2022xxxx°xx11 v0.1.8 Tweak • 20220908°1901 v0.1.7 Tweak • 20210928°1321 v0.1.6 Filling
#  license     : BSD 3-Clause | © 2021 - 2024 Norbert C. Maier
#  subject     : Demonstrates ForEach loops
#  userstory   : (1) For a list of grossprizes, calculate the netprize and the VAT for each
#                (2) For a dictionary with articles and their grossprizes, do the same
#  note        : Gross = Brutto, Net = Netto, VAT = Mehrwertsteuer (Value Added Tax)
#  ref         : https://realpython.com/iterate-through-dictionary-python/ [ref 20221001°1222]
#  ref         : https://realpython.com/python-string-formatting/ [ref 20221001°1223]
#  ref         : https://www.w3schools.com/python/ref_string_format.asp [ref 20221001°1224]
sPYTHONV = 'Python ' + __import__('platform').python_version()
sVERSION = 'v0.1.8'

VAT = 19                                                    # Upper case is convention as marker for constant

print("*** Bongiorno, sono `py250foreach.py` %s — ForEach (%s) ***" % (sVERSION, sPYTHONV))
print("Story: From Grossprize, calculate netprize and VAT amount for VAT = " + str(VAT) + " % ")

lst = [66.0, 75.0, 100.0, 119.0]                                           # List
dct = {"Shirt" : 66.0, "Shoes": 75.0, "Trousers": 100.0, "Jacket": 119.0}  # Dictionary

print("(1) ForEach loop over list")
print("The list :", lst)
for gprize in lst :
   fNet = gprize / (100 + VAT) * 100
   fVat = gprize / (100 + VAT) * VAT
   print(" • Brutto {0:>6.2f} ¥, Netto {1:>6.2f} ¥, VAT {2:>5.2f} ¥".format(gprize, fNet, fVat))

print("(2) ForEach loop over dictionary")
print("The dict :", dct)
for key in dct :
   fNet = dct[key] / (100 + VAT) * 100
   fVat = dct[key] / (100 + VAT) * VAT
   print(" • {0:<9}: Brutto {1:>6.2f} ¥, Netto {2:>6.2f} ¥, VAT {3:>5.2f} ¥".format(key, dct[key], fNet, fVat))

print("Arrivederci.")
