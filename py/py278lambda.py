﻿#  file        : id 20221213°1711 — gitlab.com/normai/cheeseburger …/py/py278lambda.py
#  subject     : Demonstrate lambda expressions
#  version     : • 20221214°1311 v0.1.8 Filling • 20221213°1711 Stub
#  license     : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
#  ref         : www.wikipython.com/other-concepts/anatomy-of-a-lambda/ [ref 20221213°1536]
sPYTHONV = 'Python ' + __import__('platform').python_version()
sVERSION = 'v0.1.8'

import random

print("*** Bongiorno, sono `py278lambda.py` %s — Lambda expressions (%s) ***" % (sVERSION, sPYTHONV))


# (1) Simplest lambda
rnd = random.random()                                  # 0 .. 0.999..
lamNum = lambda x : x * 3
print('(1) %f * 3 = %f' % (rnd, lamNum(rnd)))


# (2) Complex lambda
# Code inspired by www.wikipython.com/other-concepts/anatomy-of-a-lambda/ [ref 20221213°1536]
# Interest calculation formula:
#    future_value = principal * (1 + (annual_interest_rate / compound_frequency))
#                 ** (years_invested * compound_frequency)
amnt = 1000  # 1111.11                                 # Principal (EUR)
rate = .06                                             # Annual interest rate (6 %)
pdcy = 12.0                                            # Compounding periodicity (monthly)
years = random.randint(1, 12)                          # Duration, maximum is 9 years
future_val = lambda amnt, rate, pdcy, years :  \
            str(round(amnt * (1 + (rate / pdcy)) ** (years * years), 2))  \
             + ' EUR'  \
              if years <= 9 else "Sorry, maximum period is 9 years."
print('(2) Interest calculation')
print('   Baseline data : equity = %i EUR, interest = %.1f %% annual, duration = %.1f years' % (amnt, rate * 100, years))
print("   Resulting equity = "+ future_val(amnt, rate, pdcy, years))


print("Arrivederci.")
