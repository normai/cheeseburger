﻿# EMPTY STUB, YET TO BE FILLED WITH CODE

#  file        : id 20221222°1811 — gitlab.com/normai/cheeseburger …/py/py273recurs.py
#  version     : • 20xxxxxx°xxxx Filling • 20221222°1811 Stub
#  license     : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
#  subject     : Demonstrate recursion
#  status      :
sPYTHONV = 'Python ' + __import__('platform').python_version()
sVERSION = 'v0.0.0'

print("*** Yō `py273recurs.py` hō %s — Recursion (%s) ***" % (sVERSION, sPYTHONV))  # "यो 'py273recurs.py' हो [Yō 'py273recurs.py' hō]"





print("Namastē.")  # "नमस्ते [Namastē]."
