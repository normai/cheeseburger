﻿#  file        : id 20221012°1611 — gitlab.com/normai/cheeseburger …/py/py153stripol.py
#  version     : • 20221013°1011 v0.1.8 Filling • 20221012°1611 Stub
#  license     : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
#  subject     : Demonstrate String Interpolation
#  userstory   :
#  summary     :
#  ref         : https://www.codeleaks.io/python-string-interpolation/ [ref 20221009°0922]
sPYTHONV = 'Python ' + __import__('platform').python_version()
sVERSION = 'v0.1.8'

from string import Template

print("*** Hej, det er `py153stripol.py` %s — String Interpolation (%s) ***" % (sVERSION, sPYTHONV))

# Fodder
sHello = "Hello"
sJenny = 'Jenny'
x1 = ""

# () String interpolation four flavours
# (.1)
print(f"(1) F-String        : {sHello} {sJenny}")

# (.2)
print("(2) %%-Formatting    : %s %s" %(sHello, sJenny))                 # Note the percent sign being escaped with another percent sign

# (.3)
print("(3) str.format()    : {x} {y}".format(x = sHello, y = sJenny))

# (.4)
tmplt = Template("(4) Template string : $x $y")
print(tmplt.substitute(x = sHello, y = sJenny))

print("Farvel.")
