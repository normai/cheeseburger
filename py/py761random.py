﻿#  file        : gitlab.com/normai/cheeseburger …/py/py761random.py [id 20220910°1311]
#  subject     : Demonstrate Python Random Functions
#  versions    : • 20221214°1511 Filling • 20220910°1311 Stub
#  license     : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
#  status      : First quick implementation, paintDiagrams function needs finetuning
sPYTHONV = 'Python ' + __import__('platform').python_version()
sVERSION = 'v0.0.0'

import random

iRANDS = 100000                                                # Number of generated randoms for Uniform and Gauss
iRNDLIM = 100                                                  # Uniform/Gauss upper limit
iSEED = 123                                                    # Set 0 to run without seed


# function 20221214°1441
# Status : The formulas are quick'n'dirty, possibly need a review
# Note : Possibly extract into dedicated module to be used from everywhere
def paintDiagram(lstRaw) :

   iWIDTH = 80                                                 # Set width of the generated diagram
   iHEIGHT = 12                                                # Set height of the generated diagram
   bDEBUG = False                                              # Toggle this to hide or print debug messages

   lEvalWidth = [0 for _ in range(iWIDTH)]                     # Provide list to take the width condensed values
   iDbgCnt = 0

   # Fill counter list
   for fNum in lRandNums :
      iDbgCnt += 1
      iNdx = int(fNum / (iRNDLIM / iWIDTH))                    # Calculate evaluation slot from random number
      iNdx = iNdx if iNdx < iWIDTH else iWIDTH - 1             # Bad workaround
      lEvalWidth[iNdx] += 1
   if bDEBUG :
      print('     Dbg -- Distribution of ', sum(lEvalWidth), 'numbers:', lEvalWidth, 'max =', max(lEvalWidth))

   # Create list with scaled numbers from counter list
   lEvalScaled = [0 for _ in range(iWIDTH)]                    # Another list with zeros for each point of the width
   iNdx = 0
   for iNum in lEvalWidth :
      lEvalScaled[iNdx] = int(iNum * iHEIGHT / max(lEvalWidth))
      iNdx += 1
   if bDEBUG :
      print('     Dbg -- Condensed of ', sum(lEvalScaled), 'numbers:', lEvalScaled, 'max =', max(lEvalScaled))

   # Build the diagram
   lDiagram = [['∙' for _ in range(iWIDTH)] for _ in range(iHEIGHT)]  # Provide output matrix
   iCol = 0
   for iHight in lEvalScaled :
      lDiagram[iHEIGHT - iHight - 1][iCol] = '█'
      iCol += 1

   # Final output
   for line in lDiagram :
      for col in line :
         print(col, end='')
      print()


print("*** Привет, это `py761random.py` %s — Random functions (%s) ***" % (sVERSION, sPYTHONV))  # Привет, это [Privet, eto]

if iSEED > 0 :
   random.seed(iSEED)                                          # Make all program runs look the same

# (1) Roll dice
iDice = random.randint(1, 6)
print("(1) You rolled ", iDice)

# (2) Get basic float
fRand1 = random.random()
print("(2) Basic float =", fRand1)

# (3) Get float in range [0, 100), this is so including 0, excluding 100
fRand2 = random.random()
fCooked = fRand2 * 100
print("(3) Number [0..100) =", fCooked)

# (4) Uniform distribution
print('(4) Uniform distribution of %i numbers' % iRANDS)
lRandNums = []
for i in range(0, iRANDS) :
   fRnd = random.uniform(0, iRNDLIM)
   lRandNums.append(fRnd)
paintDiagram(lRandNums)

# (5) Gauss distribution
print('(5) Gauss distribution of %i numbers' % iRANDS)
lRandNums = []
for i in range(0, iRANDS) :
   fRnd = random.gauss(0.4, 0.2)
   lRandNums.append(fRnd * iRNDLIM)
paintDiagram(lRandNums)


print("до свидания.")  # до свидания [Do svidaniya]
