﻿# THIS FILE IS AN EMPTY STUB, IT IS YET TO BE FILLED WITH CODE

#  file        : id 20220829°1511 — gitlab.com/normai/cheeseburger …/py/py520access.py
#  version     : • 20xxxxxx°xxxx Filling • 20220829°1511 v0.1.6 Stub
#  license     : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
#  subject     : Python has no access modifiers, they are worked around by using the convention propending an underline.
sPYTHONV = 'Python ' + __import__('platform').python_version()
sVERSION = 'v0.0.0'

print("*** Hej, det er `py520access.py` %s — Access control (%s) ***" % (sVERSION, sPYTHONV))





print("Farvel.")
