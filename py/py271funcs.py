﻿#  file        : id 20210928°1411 — gitlab.com/normai/cheeseburger …/py/py271funcs.py
#  version     : • 20220908°1931 v0.1.7 Tweak • 20210928°1411 v0.1.6 Filling
#  license     : BSD 3-Clause | © 2021 - 2024 Norbert C. Maier
#  subject     : Demonstrate function calls within one module
sPYTHONV = 'Python ' + __import__('platform').python_version()
sVERSION = 'v0.1.7'

from random import randint

def roll_dice(num) :
   series = []
   for randi in range(num) :
      randi = randint(1, 6)
      series.append(randi)
   return series

def calc_average(series) :
   iSum = 0
   for i in series :
      iSum += i
   avg = iSum / len(series)
   return avg

print("*** Hello, ini `py271funcs.py` %s — Function calling (%s) ***" % (sVERSION, sPYTHONV))

series = roll_dice(12)

print("The series goes :" ,series)

iAvg = calc_average(series)

print("This are on average :" ,iAvg)

print("Selamat tinggal.")
