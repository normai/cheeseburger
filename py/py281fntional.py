﻿# EMPTY STUB, YET TO BE FILLED WITH CODE

#  file        : id 20221213°1811 — gitlab.com/normai/cheeseburger …/py/py281fntional.py
#  version     : • 20xxxxxx°xxxx Filling • 20221213°1811 Stub
#  license     : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
#  subject     : Demonstrate functional programming
#  userstory   :
#  summary     :
#  status      :
#  ref         :
sPYTHONV = 'Python ' + __import__('platform').python_version()
sVERSION = 'v0.0.0'

print("*** Kon'nichiwa, desu `py281fntional.py` %s — Functional programming (%s) ***" % (sVERSION, sPYTHONV))  # こんにちは、です [Kon'nichiwa, desu]





print("Sayōnara.")  # さようなら [Sayōnara]
