﻿#  file        : id 20220826°1711 — gitlab.com/normai/cheeseburger …/py/py240for.py
#  version     : • 20220919°1911 v0.1.8 Filling • 20220826°1711 v0.1.6 Stub
#  license     : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
#  subject     : Demonstrate for loop
#  ref         : https://www.w3schools.com/python/ref_func_range.asp [ref 20220919°1742]
sPYTHONV = 'Python ' + __import__('platform').python_version()
sVERSION = 'v0.1.8'

print("*** Halo, ini adalah `py240for.py` %s — For Loop (%s) ***" % (sVERSION, sPYTHONV))


# (1) Print squares for 3 through 10
print("(1) Two power 6 through 10")
for i in range(6, 11) :
   print("   -", i, pow(2, i))

print("(2) And the same just backward")
for i in range(10, 5, -1) :
   print("   -", i, pow(2, i))


print("Alavida.")  # "अलविदा" does not print nice
