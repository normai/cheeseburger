﻿#  file        : id 20210928°1411 — gitlab.com/normai/cheeseburger … /ppy041import.py y/py041import.py
#  version     : • 20220908°1951 v0.1.7 Tweak • 20210928°1411 v0.1.6 Filling
#  license     : BSD 3-Clause | © 2021 - 2024 Norbert C. Maier
#  subject     : Call functions or methods from another module
sPYTHONV = 'Python ' + __import__('platform').python_version()
sVERSION = 'v0.1.7'

import py420export

print("*** Pozdravljeni, tukaj `py410import.py` %s — Import statement (%s) ***" % (sVERSION, sPYTHONV))

series = py420export.roll_dice(12)

print("The series goes :", series)

iAvg = py420export.calc_average(series)

print("This is an average of :", iAvg)

print("Nasvidenje.")
