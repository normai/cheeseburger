﻿#  file        : id 20220214°1811 — gitlab.com/normai/cheeseburger …/py/py115varis.py
#  version     : • 20220908°0951 v0.1.7 Filling • 20220214°1811 Stub
#  license     : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
#  subject     : Demonstrate some variables
sPYTHONV = 'Python ' + __import__('platform').python_version()
sVERSION = 'v0.1.7'

print("*** Hello, this is `py113aloha.py` %s — Aloha (%s)***" % (sVERSION, sPYTHONV))

a = 123                                                # Integer
b = 2.34                                               # Float
c = 'Ahoj'                                             # String
d = c + ' ='
e = a + b
print(d, e)

print("Good bye.");
