﻿#  file        : id 20221001°1311 — gitlab.com/normai/cheeseburger … py/py151string.py
#  version     : • 20221009°1711 v0.1.8 Filling • 20221001°1311 Stub
#  license     : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
#  subject     : Demonstrate string syntax
#  ref         : https://www.w3schools.com/python/python_strings.asp [ref 20221009°0912]
#  ref         : https://www.w3schools.com/python/gloss_python_multi_line_strings.asp [ref 20221009°0913]
#  ref         : https://favtutor.com/blogs/string-to-float-python [ref 20221009°0942]
#  todo        : Split off a second demo for the string functions
sPYTHONV = 'Python ' + __import__('platform').python_version()
sVERSION = 'v0.1.8'

print("*** Pozdravljeni, tukaj `py151string.py` %s — Strings (%s) ***" % (sVERSION, sPYTHONV))

# =====================================================
# (1) String Syntax

# (1.1) Syntax — Quote types
print("(1.1) Double-quotes \" and single-quotes \' work equally")

# (1.2) Syntax — Mulitiline string options
# Note. Triple-single-quotes do work as well
sMulti = """Ein Wiesel
saß auf einem Kiesel
inmitten Bachgeriesel."""
print("(1.2)", sMulti)

# (1.3) Syntax — String concatenation
sHello = "Hello"
sJenny = 'Jenny'
sGreet = sHello + ' ' + sJenny
print("(1.3)", sGreet)

# (1.4) Syntax — String as Array
sWord = "SilzuZankunKrei"
print ('(1.4) String as Array: ', end='')
for c in sWord :
   print (c + '.', end='')
print()

# =====================================================
# (2) Conversions

# (2.1)
print('(2.1) Number to string: "' + str(1234) + '", "' + str(2.345) + '"')

# (2.2)
print('(2.2) String to number:')
nums = ["1234", "2.345", "Tree hundred twenty five"]
for sNum in nums :
   try :
      n = int(sNum)
   except :
      try :
         n = float(sNum)
      except :
         n = "Not a number"
   print('      - "' + sNum + '" => ' + str(n) + ' (' + str(type(n)) + ')')

print("Nasvidenje.")
