﻿#  file        : id 20221117°1011 — gitlab.com/normai/cheeseburger …/py/py139bigint.py
#  version     : • 20221216°0911 v0.1.8 Filling • 20221117°1011 Stub
#  license     : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
#  subject     : Demonstrate big integer types
#  status      : Too simple. Some more intelligent example(s) were nice.
sPYTHONV = 'Python ' + __import__('platform').python_version()
sVERSION = 'v0.1.8'

print("*** Hello, ez `py139bigint.py` %s — Big integer types (%s) ***" % (sVERSION, sPYTHONV))

# (1) Series One
i1 = 123_000_456
i2 = 123_000_000_000_000_456
i3 = 123_000_000_000_000_000_000_000_456
i4 = 123_000_000_000_000_000_000_000_000_000_000_456
print('1.1 —', i1, 'type =', str(type(i1)))
print('1.2 —', i2, 'type =', str(type(i2)))
print('1.3 —', i3, 'type =', str(type(i3)))
print('1.4 —', i4, 'type =', str(type(i4)))

# (2) Series Two
j1 = 10 ** 9 + 234
j2 = 10 ** 27 + 234
j3 = 10 ** 54 + 234
print('2.1 —', j1, 'type =', str(type(j1)))
print('2.2 —', j2, 'type =', str(type(j2)))
print('2.3 —', j3, 'type =', str(type(j3)))

print("Viszlát.")
