﻿#  file        : 20221001°1511 — gitlab.com/normai/cheeseburger …/py/py771sound.py
#  version     : 20221001°1511 v0.1.8 Initial
#  license     : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
#  subject     : Demonstrate sound (Windows specific)
#  userstory   : Play a jingle
#  credits     : Mystical+Sting+-+320bit.mp3 © Alexander Nakarada, licensed CC BY 4.0 (creativecommons.org/licenses/by/4.0/)
#                 the file was shortened from 12 to 6 seconds to remove trailing silence
sPYTHONV = 'Python ' + __import__('platform').python_version()
sVERSION = 'v0.1.8'

import winsound                                                # Windows specific!

print("*** Habari, hii ni `py771sound.py` %s — Sound (%s) ***" % (sVERSION, sPYTHONV))

print("(1) Beep")
winsound.Beep(800,250)

print("(2) Play jingle 5 seconds")
file = "./../docs/files/20221001o1729.Mystical_Sting_-_320bit.v1.mp3.wav"
winsound.PlaySound(file, winsound.SND_FILENAME)

print("(3) Beep again")
winsound.Beep(1100,250)

print("Kwaheri.")
