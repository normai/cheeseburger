﻿# file         : 20221215°1111 pyfactorial.py
# version      : 20221216°0921 Filling v0.1.8
# note         : Code after https://de.wikipedia.org/wiki/Fakult%C3%A4t_(Mathematik) [ref 20221215°1224]
sPYTHONV = 'Python ' + __import__('platform').python_version()
sVERSION = 'v0.1.8'

def fak(n: int) -> int:
    return 1 if n <= 1 else n * fak(n - 1)

print("*** Sveiki, tai `pyfactorial.py` %s — Factorial (%s) ***" % (sVERSION, sPYTHONV))

for i in range(-1, 21) :
   if i > 7 and i < 16 : continue
   print(i, fak(i))

print("Iki pasimatymo.")
