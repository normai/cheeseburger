﻿#  file        : id 20220828°1711 — gitlab.com/normai/cheeseburger …/py/py161list.py
#  version     : • 20220908°1251 v0.1.7 Filling • 20220828°1711 Stub
#  license     : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
#  subject     : Demonstrate Python List
#  ref         : https://www.w3schools.com/python/python_lists.asp []
#  ref         : https://docs.python.org/3/tutorial/datastructures.html []
sPYTHONV = 'Python ' + __import__('platform').python_version()
sVERSION = 'v0.1.7'

print("*** Nǐ hǎo `py161list.py` %s — (%s) ***" % (sVERSION, sPYTHONV))  # "你好 (Nǐ hǎo)"

# (1) Simple list
# (1.1) Create
islands = ["Rügen", "Usedom", "Fehmarn", "Sylt"]
islands.append("Föhr")
islands.append("Pellworm")

# (1.2) Print some infos
print("(1.2.1) type(islands) =", type(islands))
print("(1.2.2) len(islands) =", len(islands))
print("(1.2.3) islands =", islands)

# (1.3) Iterate
for s in islands :
   print("   -", s)

# (2) Sophisticated list
# (2.1) Create
mixture = ["Pink", True, 123, 2.34, ["Aha", "Oha", "Uhu"]]

# (2.2) Print some infos
print("(2.2.1) type(mixture) =", type(mixture))
print("(2.2.2) len(mixture) =", len(mixture))
print("(2.2.3) mixture =", mixture)

# (2.3) Iterate
for x in mixture :
   print("   -", x, "(type =", str(type(x)) + ")")

print("Zàijiàn.")  # "再见 [Zàijiàn]."
