﻿# THIS FILE IS AN EMPTY STUB, IT IS YET TO BE FILLED WITH CODE

#  file        : id 20220831°1611 — gitlab.com/normai/cheeseburger …/py/py620insert.py
#  version     : • 20xxxxxx°xxxx Filling • 20220831°1611 v0.1.6 Stub
#  license     : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
#  subject     : Demonstrate polymorphism
#  status      : empty
sPYTHONV = 'Python ' + __import__('platform').python_version()
sVERSION = 'v0.0.0'

print("*** Γεια σας, αυτό είναι το `py620insert.py` %s — SQL Insert statement (%s) ***" % (sVERSION, sPYTHONV))  # "Γεια σας, αυτό είναι το [Geia sas, aftó eínai to]"





print("Αντίο σας [Antio sas].")
