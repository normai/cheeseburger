﻿#  file        : id 20210928°1441 — gitlab.com/normai/cheeseburger …/ppy730del.py y/py730del.py
#  version     : • 20220908°2021 v0.1.7 Tweak • 20210928°1441 v0.1.6 Filling
#  license     : BSD 3-Clause | © 2021 - 2024 Norbert C. Maier
#  subject     : Demonstrate partial import versus complete import.
#  note        : To show the two different import flavours within one single
#                 module, I am using the unlucky 'del' to separate the two flavours.
#                 Rather demonstrate the two import flavours in two dedicated modules.
sPYTHONV = 'Python ' + __import__('platform').python_version()
sVERSION = 'v0.1.7'

print("*** Sveiki, tai `py730del.py` %s — Two 'import' flavours (%s) ***" % (sVERSION, sPYTHONV))

from random import randint                     # Import only specific method
iRand1 = randint(1, 6)
print("(1) Dice rolled : " + str(iRand1))

del randint                                    # Undo import, disrupt randint

import random                                  # Import complete module
iRand2 = random.randint(1, 6)
print("(2) Dice rolled : " + str(iRand2))

print("Iki pasimatymo.")
