﻿# THIS FILE IS AN EMPTY STUB, IT IS YET TO BE FILLED WITH CODE

#  file        : id 20220827°1011 — gitlab.com/normai/cheeseburger …/py/py267goto.py
#  version     : • 20xxxxxx°xxxx Filling • 20220827°1011 v0.1.6 Stub
#  license     : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
#  subject     : Demonstrate goto statement
#  summary     : In Python does no goto statement exist
sPYTHONV = 'Python ' + __import__('platform').python_version()
sVERSION = 'v0.0.0'

print("*** Sveiki, tai `py267goto.py` %s — There is no Goto in Python (%s) ***" % (sVERSION, sPYTHONV))





print("Iki pasimatymo.")
