﻿#  file        : id 20220216°1731 — gitlab.com/normai/cheeseburger …/py/py111hello.py
#  version     : • 20220908°0751 Filling • 20220216°1731 Stub
#  license     : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
#  subject     : Demonstrate a most simple Python program with user input
sPYTHONV = 'Python ' + __import__('platform').python_version()
sVERSION = 'v0.1.8'

print("*** This is `py111hello.py` %s — Hello (%s) ***" % (sVERSION, sPYTHONV))

x = input("Please input your name > ")
print("Good morning \"" + x + "\"!")

print("Bye.")
