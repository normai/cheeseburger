﻿# THIS FILE IS AN EMPTY STUB, IT IS YET TO BE FILLED WITH CODE

#  file        : id 20220831°1811 — gitlab.com/normai/cheeseburger …/py/py640select.py
#  version     : • 20xxxxxx°xxxx Filling • 20220831°1811 v0.1.6 Stub
#  license     : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
#  subject     : Demonstrate polymorphism
#  status      : empty
sPYTHONV = 'Python ' + __import__('platform').python_version()
sVERSION = 'v0.0.0'

print("*** Hailo, yah hai `py640select.py` %s — SQL Select statement %s) ***" % (sVERSION, sPYTHONV))  # "हैलो, यह है [Hailo, yah hai]"





print("Alavida.")  # "अलविदा [Alavida]."
