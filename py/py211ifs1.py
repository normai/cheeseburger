﻿#  file        : id 20210928°1511 — gitlab.com/normai/cheeseburger …/py/py211ifs1.py
#  version     : • 20220908°1821 v0.1.7 Tweak • 20210928°1511 v0.1.6 Filling
#  license     : BSD 3-Clause | © 2021 - 2024 Norbert C. Maier
#  subject     : Demonstrate Python branching
#  summary     : This demonstrates If/ElseIf/Else with four cases:
#                 • Unilateral branching
#                 • Bilateral branching
#                 • Multilateral branching
#                 • Wrong branching logic
#                The sequence goes like this (1) Create random integer (2) Let the user
#                overwrite this number (3) Do four different evaluations on that number
#  usage       : Command "python.exe py211ifs1.py"
#  ref         : E.g. www.pythonpool.com/python-check-if-string-is-integer/ [ref 20220212°1232]
sPYTHONV = 'Python ' + __import__('platform').python_version()
sVERSION = 'v0.1.7'

print("*** Hallo, hier ist `py211ifs1.py` %s — Branching (%s) ***" % (sVERSION, sPYTHONV))

# (A.1) Preparation — Get a random number rolled
from random import randint
randi = randint(1, 6)
print("You rolled number " + str(randi))

# (A.2) Optionally allow to manually choose the number
#  Toggle False/True to activate/deactivate the sequence
if False :
   s = input("Overwrite the rolled number with 1..6 : ")
   if s.isnumeric() :                                   # Does not recognize a minus sign
      randi = int(s)
      randi = 1 if randi < 1 else randi                 # Ternary operator since Python 2.5
      randi = 6 if randi > 6 else randi
   print("Your number now is '" + str(randi) + "'.")

# (B.1) Unilateral branching
print("(1) Unilateral branching")
if randi > 3 :
   print("(1.1) : Your number is bigger than three")

# (B.2) Two-way branching
print("(2) Bilateral branching")
if randi == 3 :
   print("(2.1) : The number is three")
else :
   print("(2.2) : The number is not three")

# (B.3) Multilateral branching — Only one branch will be executed
print("(3) Mulitlateral branching")
if randi > 5 :
   print("(3.1) : The number is six")
elif randi > 4 :
   print("(3.2) : The number is five")
elif randi > 3 :
   print("(3.3) : The number is four")
elif randi > 2 :
   print("(3.4) : The number is three")
else :
   print("(3.5) : The number is one or two")

# (B.4) A typical beginners mistake, looks very similar like above,
#  has multiple if instead elif statements, will not work as intended.
print("(4) Wrong logic")
if randi > 5 :
   print("(4.1) : The number is six")
if randi > 4 :
   print("(4.2) : The number is five or six")
if randi > 3 :
   print("(4.3) : The number is four to six")
if randi > 2 :
   print("(4.4) : The number is three to six")
else :
   print("(4.5) : The number is one or two")

print("Auf Wiedersehen.")
