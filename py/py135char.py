﻿#  file        : id 20221012°0911 — gitlab.com/normai/cheeseburger …/py/py135char.py
#  version     : • 20221016°1111 v0.1.8 Filling • 20221012°0911 v0.1.8 Stub
#  license     : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
#  subject     : Demonstrate Character Type
#  userstory   :
#  summary     :
#  ref         : https://realpython.com/python-strings/ [ref 20221015°0912]
#  ref         : https://www.w3schools.com/python/python_datatypes.asp [ref 20221015°0913]
#  ref         : https://pythonguides.com/python-string-to-byte-array/ [ref 20221015°0932]
#  ref         :
#  status      : Cases are collected. Cases need be sorted out now.
sPYTHONV = 'Python ' + __import__('platform').python_version()
sVERSION = 'v0.1.8'

import base64

print("*** Здравейте, това е `py135char.py` %s — Demonstrate Character Type (%s) ***" % (sVERSION, sPYTHONV))  # "Здравейте, това е [Zdraveĭte, tova e]"

# (1) Simple syntax and functions
# (1.1) Plain bytes
bytes_1 = b"Abc..xyz."  # b"Abc…xyz." yields error "SyntaxError: bytes can only contain ASCII literal characters."
bytes_1 = b"Abc\xe2\x80\xa6xyz."
print("(1.1) Plain bytes            :", bytes_1, " — ".rjust(21), str(type(bytes_1)))

# (1.2)
print("(1.2) Loop over the bytes    :", end=' ')
for x in bytes_1:
   print(x, end=' ')
print()

# (1.3)
print("(1.3) Print bytes as string  :", end=' ')
for x in bytes_1:
   print(chr(x), end=' ')
print()

# (1.4)
str_14 = bytes_1.decode('utf-8')
print("(1.4) Decode bytes to string :", str_14)

# (1.5)
str_15 = bytes_1.decode('utf-16')
print("(1.5) Decode bytes to string :", str_15)

# (2) Useing ord and chr
# (2.1) Get character code with ord()
str_21 = "Abcd…wxyz"
print("(2.1) ord(x)                 :", end=' ')
lst_21 = []
for x in str_21 :
   print(ord(x), end=' ')
   lst_21.append(ord(x))
print()

# (2.2) Get character from code with chr()
print("(2.2) chr(x)                 :", end=' ')
for x in lst_21 :
   print(chr(x), end=' ')
print()

# (3) Series after ref 20221015°0932 pythonguides.com/python-string-to-byte-array
# (3.1) Python string to byte array
sProbe1 = 'Abc..xyz.'
bytes2 = bytes(sProbe1, 'ascii')  # 'Abc…xyz.' yields error "UnicodeEncodeError: 'ascii' codec can't encode character '\u2026' in position 3: ordinal not in range(128)"
print("(3.1) ASCII bytes            :", bytes2)

# (3.2) Python string to byte array encoding
sProbe2 = 'Abc…xyz.'
bytes3 = sProbe2.encode()
print("(3.2) s.encode()             :", bytes3)

# (3.3) Python string to byte array UTF-8
bytes4a = bytes(sProbe2, 'utf-8')
print("(3.3) bytes('utf-8')         :", bytes4a, " — ".rjust(17), str(type(bytes4a)))

# (3.4) Python string to byte array UTF-16
by_y4 = bytes(sProbe2, 'utf-16')
print("(3.4) bytes('utf-16')        :", by_y4)

# (3.5)
s5 = "ab cd ef"
bytarr5 = bytearray.fromhex(s5)
print("(3.5) bytearray.fromhex(s5)  :", bytarr5)

# (3.6) Python base64 string to byte array
# Todo: Cleanup the mess and make the example sensible
# Note : The example from ref 20221015°0932 seems to be wrong.
# s61 = "Abc…xyz."                     # Raises below ValueError('string argument should contain only ASCII characters')
# s61 = "Abcxyz"                       # Yields "binascii.Error: Incorrect padding"
s61 = "Pythonguides"                   # Why is this better as "Abcxyz"?!
#s61 = "Abc…xyz" # "Aloha"
#s62 = base64.b64decode("Pythonguides")
#s62 = base64.b64decode("Pythonguides")
s62 = base64.b64decode(s61)
#print("(6) base64.b64decode(s61)   :", s62)
print("(3.6) base64.b64encode()     :", s62)

# (3.7) Python binary string to byte array
bytes_27 = bytearray("11000010110001001100011", "ascii")        # Todo: This makes no sense?!
print("(3.7) bytearray()            :", bytes_27)

# (3.8) Python JSON string to byte array
s_28 = '{"Name": "Kushi", "subjects": ["english", "maths"]}'
bytes_28 = bytearray(s_28, "ascii")        # Todo: This makes not much sense?!
print("(3.8) bytearray()            :", bytes_28)

# (3.9) Python string to a byte array without b
s_29 = b'Twenty nine'
bytes_29 = s_29.decode()
print("(3.9.1) b''                  :", s_29, " — ".rjust(25), str(type(s_29)))
print("(3.9.2) s.decode()           :", bytes_29, " — ".rjust(28), str(type(bytes_29)))

# (3.10) Python hex string to byte array
s_210 = "1A"
b_210 = bytearray.fromhex(s_210)
print("(3.10) bytearray.fromhex()   :", b_210, " — ".rjust(21), str(type(b_210)))

# (3.11) Python string to byte array ASCII
# Note: Compare with above example 2.2
by_211a = bytes(str('Abcxyz').encode("ascii"))
by_211b = bytes(str('Abc…xyz').encode("utf-8"))
by_211c = bytes(str('Abc…xyz').encode("utf-16"))
print("(3.11.1) bytes(str().encode(ascii)) :", by_211a, " — ".rjust(23), str(type(by_211a)))
print("(3.11.2) bytes(str().encode(utf-8)) :", by_211b, " — ".rjust(11), str(type(by_211b)))
print("(3.11.3) bytes(str().encode(utf-16)) :", by_211c, " — ".rjust(1), str(type(by_211c)))

# (3.12) Python 2.7 string to byte type -- This example looks not so interesting



print("Довиждане [Dovizhdane].")
