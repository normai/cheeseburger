﻿#  file        : id 20221116°1511 — gitlab.com/normai/cheeseburger/-/blob/main/py/py313getkey.py
#  slogan      : Demonstrate keyboard menu Windows specific without pressing Enter
#  versions    : • 20240921°1311 v0.1.9 Filling • 20221116°1511 v0.1.8 Stub
#  license     : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
#  userstory   :
#  summary     :

import msvcrt                                          # Windows specific getch(), returns a Byte type

sVERSION = 'v0.1.8'
sPYTHONV = 'Python ' + __import__('platform').python_version()

print("*** Hier ist `py315kbd2menu.py` %s — Keyboard menu w/o pressing Enter (%s) ***" % (sVERSION, sPYTHONV))

bContinue = True
while (bContinue) :

    print("----------------------------------------------------")
    print("Menu: a = Anton, b = Britney, c = Celine, x = Exit > ", end='', flush=True)  # If we use "end=''", then we need "flush=True"

    # Wait for the user pressing her selection [seq 20230824°1947]
    cKey = msvcrt.getch().decode().lower()             # https://www.pythonpool.com/python-bytes-to-string/ [ref 20220525°1112]
    print("'" + cKey + "'")                            # Selection echo

    if cKey == "a" :
        print("This is Anton speaking")
    elif cKey == "b" :
        print("This is Britney singing")
    elif cKey == "c" :
        print("This is Celine ringing")
    elif cKey == "x" :
        bContinue = False
    else :
        print('Invalid selection: "' + cKey + '"')

print("Auf Wiedersehen.")
