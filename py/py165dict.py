﻿#  file        : id 20220828°1911 — gitlab.com/normai/cheeseburger …/py/py165dict.py
#  version     : • 20220908°1451 v0.1.7 Filling • 20220828°1911 v0.1.6 Stub
#  license     : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
#  subject     : Demonstrate Python Dictionary
#  references  :
#                • https://www.w3schools.com/python/python_dictionaries.asp []
#                • https://www.reptilekingdoms.com/what-do-crocodiles-eat/ []
#                • https://de.wikipedia.org/wiki/Reptilien []
#                • https://en.wikipedia.org/wiki/Crocodilia []
#                • https://en.wikipedia.org/wiki/Nile_crocodile []
sPYTHONV = 'Python ' + __import__('platform').python_version()
sVERSION = 'v0.1.7'

print("*** Dette er `py165dict.py` %s — Dictionary  (%s) ***" % (sVERSION, sPYTHONV))

# (1) Create
dic = { 'kingdom' : 'Animalia'
      , 'class'   : 'Reptilia'
      , 'genus'   : 'Crocodylus'
      , 'species' : 'Nile crocodile'
      , 'name'    : 'Schnappi'
      , 2.34      : 'Green'                            # 'color'
      , 'legs'    : 4
      , 'food'    : 'Meat'
      }
print('Dict = ', dic)

# (2) Info
print('Dict size =', len(dic))

# (3) Iterate
print('Iterate over the key/value pairs:')
for x in dic :
   print(' -', str(x).ljust(8), ":", dic[x])

print("Farvel.")
