﻿# THIS FILE IS AN EMPTY STUB, IT IS YET TO BE FILLED WITH CODE

#  file        : id 20220830°1911 — gitlab.com/normai/cheeseburger …/py/py810window.py
#  version     : • 20xxxxxx°xxxx Filling • 20220830°1911 v0.1.6 Stub
#  license     : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
#  subject     : Demonstrate a plain window with a button
sPYTHONV = 'Python ' + __import__('platform').python_version()
sVERSION = 'v0.0.0'

print("*** Namastē, yō ​hō `py810window.py` %s — GUI Window (%s) ***" % (sVERSION, sPYTHONV))  # "नमस्ते, यो ​​हो [Namastē, yō ​​hō]"





print("Namastē.")  # "नमस्ते [Namastē]."
