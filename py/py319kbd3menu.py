﻿# THIS FILE IS AN EMPTY STUB, IT IS YET TO BE FILLED WITH CODE

#  file        : id 20221116°1611 — gitlab.com/normai/cheeseburger …/py/py319kbd3menu.py
#  version     : • 20xxxxxx°xxxx Filling • 20221116°1611 v0.1.8 Stub
#  license     : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
#  subject     : Demonstrate keyboard menu using a GUI function
#  userstory   :
#  summary     :
sPYTHONV = 'Python ' + __import__('platform').python_version()
sVERSION = 'v0.0.0'

print("*** Εδώ είναι το `py319kbd3menu.py` %s — Keyboard menu (GUI function) (%s) ***" % (sVERSION, sPYTHONV))  # Εδώ είναι το [Edó eínai to]





print("Αντίο σας [Antio sas].")  # "Αντίο σας [Antio sas]"
