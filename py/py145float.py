﻿#  file        : id 20221012°1111 — gitlab.com/normai/cheeseburger …/py/py145float.py
#  version     : • 20221215°0931 v0.1.8 Filling • 20221012°1111 Stub
#  license     : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
#  subject     : Demonstrate Float Type
#  status      : Too small. Shall be extended by showing some obvious rounding errors.
sPYTHONV = 'Python ' + __import__('platform').python_version()
sVERSION = 'v0.1.8'

import sys

print("*** Dobrý den, toto je `py145float.py` %s — Float types (%s) ***" % (sVERSION, sPYTHONV))


print('(1) Print some numbers:')
f1 = 123_456.789
f2 = -123_456.789
f3 = 123_456_777_777_777_777_777_777_777.789
f4 = -123_456_777_777_777_777_777_777_777.789
print("(1.1) f1 = %.2f %s" % (f1, str(type(f1))), f1)
print("(1.2) f2 = %.2f %s" % (f2, str(type(f2))), f2)
print("(1.3) f3 = %.2f %s" % (f3, str(type(f3))), f3)
print("(1.4) f4 = %.2f %s" % (f4, str(type(f4))), f4)


print("Nashledanou.")
