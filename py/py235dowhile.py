﻿#  file        : id 20210928°1531 — gitlab.com/normai/cheeseburger …/py/py235dowhile.py
#  version     : • 20221001°1111 v0.1.8 • 20220908°1851 v0.1.7 Tweak • 20210928°1531 v0.1.6 Filling
#  license     : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
#  subject     : Foot-controlled loop à la 'do while'.
#  summary     : Python has no do-while loop, so we have to mimic it
#  reference   : E.g. hashcodec.com/python-programming/do-while-loop [ref 20220212°1252] Not soo precise
sPYTHONV = 'Python ' + __import__('platform').python_version()
sVERSION = 'v0.1.8'

from random import randint

print("*** Hello, ez `py235dowhile.py` %s — Python Do/While loop (%s) ***" % (sVERSION, sPYTHONV))

# (1) Flavour one — Use an endless loop and break
print('Loop until you rolled a six (flavour one, using an endless loop and break)')
while True:
    randi = randint(1, 6)                                   # Numbers 1 through 6
    print(' • You rolled', randi)
    if randi == 6 :
        break

# (2) Flavour two — Use a continue-flag and set this False
print('Loop again until you rolled a six (flavour two, using a continue flag and setting that False)')
bContinue = True
while bContinue :
    randi = randint(1, 6)                                   # Numbers 1 through 6
    print(' • You rolled', randi)
    if randi == 6 :
        bContinue = False

print("Viszlát.")
