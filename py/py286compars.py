﻿#  file        : id 20210928°1331 — gitlab.com/normai/cheeseburger …/py/py286compars.py
#  version     : • 20220908°1941 v0.1.7 Tweak • 20210928°1331 v0.1.6 Filling
#  license     : BSD 3-Clause | © 2021 - 2024 Norbert C. Maier
#  subject     : Demonstrate some comparison features
sPYTHONV = 'Python ' + __import__('platform').python_version()
sVERSION = 'v0.1.7'

""" This demonstrates some comparison features.

 To gain a complete test coverage, you need 5 starts :
 1. Input string which cannot be converted to an integer
 2. A number too small to be a dice number
 3. A number too big to be a dice number
 4. Some correct dice number
 5. Input valid dice number but in the float notation (with a dot)

"""

import random

print("*** Kia ora, ko tenei `py286compars.py` %s — (%s) ***" % (sVERSION, sPYTHONV))

#s = input("(1) Please input an integer between 1 and 6 : ")
print("(1) Roll number between 1 and 6 (just this dice goes 0 to 9): ")
iEyes = random.randint(0, 9)
print("    Your number is", iEyes)


#try :
#   ###iEyes = int(s)                                           # Conversion may fail
#   fHalf = iEyes / 2
#   print("(2) Augen = " + str(iEyes) + " means half = " + str(fHalf))
#except Exception as e :
#   print("(2.1) Error : " + e.args[0])
#   iEyes = 1                                                   # Brute force repair
#   print("(2.2) Your dice eyes are set " + str(iEyes) + ".")

# Now iEyes is garanteed to be an integer
print()

# Validation flavour one
if iEyes < 1 :
   print("(2) Invalid dice eyes, smaller than one.")
elif iEyes > 6 :
   print("(2) Invalid dice eyes, bigger than six.")
else :
   print("(2) The dice eyes look fine : " + str(iEyes))

# Validation flavour two
if (iEyes < 1) or (iEyes > 6) :
   print("(3) Invalid dice eyes : " + str(iEyes))
else :
   print("(3) The dice eyes look fine : " + str(iEyes))

# Make the Boolean visible
b1 = iEyes <= 0                                        # Question: Invalid?
b2 = iEyes > 6
print("(4) b1 = " + str(b1) + " type(b1) " + str(type(b1)))
print("(5) b2 = " + str(b2) + " type(b2) " + str(type(b2)))

# Validation flavour two but with direct booleans
b3 = b1 or b2
if b3 :
   print("(6) Invalid dice eyes : " + str(iEyes))
else :
   print("(6) The dice eyes look fine : " + str(iEyes))

# Not
a = True
print("(7) a = " + str(a) + ", not a = " + str(not a))

print("Tēnā koe.")
