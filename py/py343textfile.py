﻿#  file        : id 20210928°1351 — gitlab.com/normai/cheeseburger …/ppy343textfile.py y/py343textfile.py
#  version     : • 20220908°2011 v0.1.7 Tweak • 20210928°1351 v0.1.6 Filling
#  license     : BSD 3-Clause | © 2021 - 2024 Norbert C. Maier
#  subject     : Demonstrate file create/read/write
sPYTHONV = 'Python ' + __import__('platform').python_version()
sVERSION = 'v0.1.7'

print("*** Привіт, це `py343textfile.py` %s — File create/write/read (%s) ***" % (sVERSION, sPYTHONV))  # Привіт, це [Pryvit, tse]

import sys
from pathlib import Path
import datetime

FILENAM = "py343textfile.OUTPUT.txt"

if not Path.is_file(Path(FILENAM)) :

   f = open(FILENAM, "x")
   f.write("Here comes a brand new file." + "\n")
   f.close()
   print('New file created: "' + FILENAM + '"')
   quit()

else :
   try :
      f = open(FILENAM, "a")
      f.write("Line appended at " + str(datetime.datetime.today()) + "\n")
      f.close()

      f = open(FILENAM, "r")
      text = f.read()
      print(text)
      f.close()

   except IOError :
      print("File access IOError.")
   except Exception as e :
      print("File access fail : \"" + e.args[0] + "\"")

print("до побачення.")  # до побачення [Do pobachennya]
