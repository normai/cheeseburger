﻿#  file        : id 20221217°0911 — gitlab.com/normai/cheeseburger …/py/py117keywords.py
#  version     : • 20221224°1211 v0.1.9 Filling • 20221217°0911 Stub
#  license     : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
#  subject     : Demonstrate keywords using the Python keyword list function
#  status      :
sPYTHONV = 'Python ' + __import__('platform').python_version()
sVERSION = 'v0.1.9'

import keyword
import platform                                        # Terse version

print("*** Ko tenei `py117keywords.py` %s — Keywords (%s) ***" % (sVERSION, sPYTHONV))
print('Python version =', platform.python_version())   # A verbos alternative 'sys.version'


# (1) Try using a keyword as variable name
#for = 4;                                              # "SyntaxError: invalid syntax"
print("(1.1) keyword.iskeyword('for') =", keyword.iskeyword('for'))
print("(1.2) keyword.iskeyword('For') =", keyword.iskeyword('For'))
For = 4;                                               # Fine
print('(1.3) For =', For)


# (2) List hard keywords
lKws = keyword.kwlist
print('(2.1) Number of hard keywords =', len(lKws))
print('(2.2) Keyword list =', lKws)


# (3) Soft keywords
lSKws = keyword.kwlist
print('(3) Number of soft keywords =', len(lKws))


print("Tēnā koe.")
