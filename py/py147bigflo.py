﻿#  file        : id 20221117°1111 — gitlab.com/normai/cheeseburger …/py/py147bigflo.py
#  version     : • 20221216°1821 v0.1.8 Filling • 20221117°1111 Stub
#  license     : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
#  subject     : Demonstrate big integer types
#  todo        : Implment simpler userstory with popular non-terminating decimals.
#  status      : Good enough for now. Questions on the algorithm remain.
sPYTHONV = 'Python ' + __import__('platform').python_version()
sVERSION = 'v0.1.8'

import decimal

def DemoBigFlo() :

   print("(1) Python floats:")
   f1 = 10.0 / 7.0
   f2 = 10.0 / 3.0
   f3 = 10.0 / 1.5
   print("(1.1) 10 / 7        = ", f1)
   print("(1.2) 10 / 3        = ", f2)
   print("(1.3) 10 / 1.5      = ", f3)
   print("(1.4) π-64          = ", 3.1415926535897932384626433832795028841971693993751058209749445923)  # Seventytwo decimals [ref 20221216°1734]
   print("(1.5) 22 / 7        = ", 22 / 7)
   print("(1.6) 355 / 113     = ", 355 / 113)
   print("(1.7) 62832 / 20000 = ", 62832 / 20000)
   print()

   print("(2) Python decimals with precision 56:");
   decimal.getcontext().prec = 56
   print("(2.1) 10 / 7        = ", decimal.Decimal(10.0) / decimal.Decimal(7.0))
   print("(2.2) 10 / 3        = ", decimal.Decimal(10.0) / decimal.Decimal(3.0))
   print("(2.3) 10 / 1.5      = ", decimal.Decimal(10.0) / decimal.Decimal(1.5))
   print("(2.4) π-64          = ", decimal.Decimal("3.1415926535897932384626433832795028841971693993751058209749445923"))  # Seventytwo decimals [ref 20221216°1734]
   print("(2.5) 22 / 7        = ", decimal.Decimal(22.0) / decimal.Decimal(7.0))
   print("(2.6) 355 / 113     = ", decimal.Decimal(355.0) / decimal.Decimal(113.0))
   print("(2.7) 62832 / 20000 = ", decimal.Decimal(62832.0) / decimal.Decimal(20000.0))


# Function 20221216°1811 Calculate Pi using the Nilakantha Series
# Code inspired by www.wikihow.com/Write-a-Python-Program-to-Calculate-Pi [ref 20221216°1726]
#  The code on WikiHow was written by 4 co-authors and published under an open source license.
#
# Findings on the runtime behaviour on a 64-bit Core i7 2 GHz machine:
# • 10 iterations          :   3 decimals precision, immediately
# • 100 iterations         :  ~5 decimals precision, immediately
# • 1000 iterations        : ~10 decimals precision, immediately
# • 10 000 iterations      : ~12 decimals precision, immediately
# • 100 000 iterations     : ~14 decimals precision, immediately
# • 1 000 000 iterations   : ~17 decimals precision, in ~2 seconds
# • 10 000 000 iterations  : ~19 decimals precision, in ~15 seconds
# • 100 000 000 iterations : ~22 decimals precision, in ~150 seconds
def PiByNilakantha(iPrec, iIters):

    decimal.getcontext().prec = iPrec
    dResult = decimal.Decimal(3.0)                             # Needs the pre-decimal digit, not nice

    iSign = 1                                                  # Sign
    for i in range(2, 2 * iIters + 1, 2):
        dResult += 4 / decimal.Decimal(i * (i + 1) * (i + 2) * iSign)
        iSign *= -1                                            # Flip sign
    return dResult

if __name__ == '__main__':

    print("*** Hailo, yah hai `py147bigflo.py` %s — Big floats (%s) ***" % (sVERSION, sPYTHONV))  # "हैलो, यह है [Hailo, yah hai]"

    DemoBigFlo()
    print()

    # Additional subject, shall be outsourced to dedicated file [todo 20221228°1231]
    print("(3) Additional — π by Nilakantha:")
    iPrecision = 32
    iIterations = 100  # 1000
    fResult = PiByNilakantha(iPrecision, iIterations)
    print('Result = ', fResult, '  • Prec =', iPrecision, '• Iters =', iIterations)

    print("Alavida.")                                              # "अलविदा [Alavida]"
