﻿#  file        : id 20221116°1411 — gitlab.com/normai/cheeseburger …/py/py311kbd1menu.py
#  version     : • 20221117°0912 v0.1.8 Filling • 20221116°1411 v0.1.8 Stub
#  license     : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
#  subject     : Demonstrate keyboard menu with pressing Enter
#  userstory   :
#  summary     :
#  status      : Good enough.
sPYTHONV = 'Python ' + __import__('platform').python_version()
sVERSION = 'v0.1.8'

print("*** Hei, tämä on `py311kbd1menu.py` %s — Keyboard menu (with Enter) (%s) ***" % (sVERSION, sPYTHONV))

bContinue = True
while (bContinue) :

   print("-----------------------------------------------")
   print("Menu: a = Anton, k = Kylie, s = Santa, x = Exit > ", end='')
   x = input()

   if x == "a" :
      print("This is Anton speaking")
   elif x == "k" :
      print("This is Kylie singing")
   elif x == "s" :
      print("This is Santa Claus ringing")
   elif x == "x" :
      bContinue = False
   else :
      print('Invalid selection: "' + x + '"')

print("Näkemiin.")
