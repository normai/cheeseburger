﻿#  file        : id 20210928°1421 — gitlab.com/normai/cheeseburger …/ppy042export.py y/py042export.py
#  version     : • 20220908°2001 v0.1.7 Tweak • 20210928°1421 v0.1.6 Filling
#  license     : BSD 3-Clause | © 2021 - 2024 Norbert C. Maier
#  subject     : Module with functions to be called from elsewhere
sPYTHONV = 'Python ' + __import__('platform').python_version()
sVERSION = 'v0.1.7'

from random import randint


def roll_dice(num) :
   series = []
   for randi in range(num) :
      randi = randint(1, 6)
      series.append(randi)
   return series


def calc_average(series) :
   iSum = 0
   for i in series :
      iSum += i
   avg = iSum / len(series)
   return avg


if __name__ == '__main__' :
   print("*** Hola, este es `py420export.py` %s — Export module (%s) ***" % (sVERSION, sPYTHONV))
   print("Adiós.")
