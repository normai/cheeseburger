﻿#  file        : id 20220214°1821 — gitlab.com/normai/cheeseburger …/py/py131types.py
#  version     : • 20220908°1051 v0.1.7 Filling • 20220214°1821 v0.1.6 Stub
#  license     : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
#  subject     : Demonstrate types
#  todo        : Add list, dictionary, tuple [todo 20220902°1111]
sPYTHONV = 'Python ' + __import__('platform').python_version()
sVERSION = 'v0.1.7'

print("*** Abhinandan `py131types.py` %s — Types (%s) ***" % (sVERSION, sPYTHONV))  # अभिनंदन [Abhinandan]

# Some declarations
i1 = 123                                               # Integer
f1 = 2.34                                              # Float
b1 = True                                              # Boolean
s1 = 'Ahoj'                                            # String

# Some operations
i2 = i1 + 1
f2 = f1 + 1
b2 = b1 + 1
s2 = s1 + str(1)

# Display the results
print("(1)", i1, "+ 1 =", i2)
print("(2)", f1, "+ 1 =", f2)
print("(3)", b1, "+ 1 =", b2)
print("(4)", s1, "+ 1 =", s2)

print("Alavida.")  # अलविदा [Alavida]
