﻿#  file        : id 20210928°1551 — gitlab.com/normai/cheeseburger …/py/py262try2.py
#  version     : • 20220915°1011 v0.1.8 Tweak • 20210928°1551 Initial filling
#  license     : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
#  subject     : Demonstrate try/except/finally
#  summary     : This is one single try block with a sequence of tasks. With
#                each pass, one more case succeeds to execute. The sequence is:
#                 • (1) Division (fails if divisor is zero)
#                 • (2) Calculate square root (fails if done with negative number)
#                 • (3) Calculate square root, manually test for negative number, use `raise`
#                 • (4) Array access (fails if index out of bounds)
#                 • (5) Open file (fails if file does not exist)
#                 • (6) Output all runs fine (only reached if non of above failed)
#                The catch clause is divided into a catch cascade for specific exceptions.
#  status      : Works but is too long
#  todo        : Simplify, possibly break down into multiple files
sPYTHONV = 'Python ' + __import__('platform').python_version()
sVERSION = 'v0.1.8'

from math import sqrt

print("*** Sveiki, šis ir `py262try2.py` %s — Try/Except/Finally/Raise (%s) ***" % (sVERSION, sPYTHONV))

for iCanary in [0, -2, 3, 4, 1] :

   print("\nCase with number", iCanary)
   try :
      #---------------------------------------------------
      # (1) Division by zero
      print("(1) Division")
      n = 1234 / iCanary
      print("(1.1)   => 1234 /", iCanary, "=", n)

      #---------------------------------------------------
      # (2) Calculate square root of negative number
      print("(2) Calculate sqare root")
      n = sqrt(iCanary)
      print("(2.1)   sqrt(" + str(iCanary) + ") = ", n)

      #---------------------------------------------------
      # (3) Calculate square root with testing for negativ, using raise
      print("(3) Calculate sqare root")
      if n < 0 :
         raise Exception("Attempt square root of negative %f" % n)             # Added 20221216°1251
      n = sqrt(iCanary)
      print("(3.1)   sqrt(" + str(iCanary) + ") = ", n)

      #---------------------------------------------------
      # (4) Array index out of bounds
      print("(4) Array index")
      l = ["Oins", "Zwoi", "Droi"]
      if iCanary > len(l) :                                                    # Missing the true boundary by one
         print("(4.1)   No array access because index is greater than array.")
      print("(4.2)   l[" + str(iCanary) + "] =", l[iCanary])

      #---------------------------------------------------
      # (5) Open non-existing file
      sFile = "./detjibbetnich.exe" if iCanary == 4 else "./py262try2.py"
      print("(5) Open file '" + sFile + "'")
      f = open(sFile, 'r')
      lins = f.read().splitlines()
      print("(5.1)   Line 1 = \"" + lins[0] + "\"")

      #---------------------------------------------------
      # (6) All fine
      print("(6) All are fine")
      print("(6.1)   No exception happend")

   except ZeroDivisionError as ex:
       print("(6.1) Exception thrown is '" + type(ex).__name__ + "': " + ex.args[0] + ".")

   except ValueError as ex:                                                    # Added 20221216°1252
       print("(6.2) ValueError exception '" + type(ex).__name__ + "': " + str(ex.args[0]) + ".")

   except FileNotFoundError as ex:
       print("(6.3) Exception thrown is '" + type(ex).__name__ + "': " + str(ex.args[0]) + ".")

   except Exception as ex:
       print("(6.4) Exception thrown is '" + type(ex).__name__ + "': " + ex.args[0] + ".")

   else:
       print("(7) All cases seem executed without any exception thrown.")

   finally:
       print("(8) Final action of the try/catch construct.")

print("Uz redzēšanos.")
