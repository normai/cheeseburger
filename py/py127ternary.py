﻿#  file        : id 20221002°0911 — gitlab.com/normai/cheeseburger …/py/py127ternary.py
#  version     : • 20221227°1611 v0.1.9 Filling • 20221002°0911 v0.1.8 Stub
#  license     : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
#  subject     : Demonstrate ternary operator
#  userstory   : Roll dice and tell whether it was greater than three or not
sPYTHONV = 'Python ' + __import__('platform').python_version()
sVERSION = 'v0.1.9'

import random

print("*** Hej, det här är `py127ternary.py` %s — Ternary operator (%s) ***" % (sVERSION, sPYTHONV))


# Preparation
iRand = random.randint(1, 6)
print("(1.1) You rolled", iRand)

# The demo line
print("(1.2) You rolled greater three: ", "Yes" if iRand > 3 else "No")


print("Adjö.")
