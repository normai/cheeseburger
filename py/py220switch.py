﻿#  file        : id 20210928°2111 — gitlab.com/normai/cheeseburger …/py/py220switch.py
#  version     : • 20220908°1841 v0.1.7 Tweak • 20210928°2111 v0.1.6 Filling
#  license     : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
#  subject     : Demonstrate a switch
#  summary     : Since Python has no switch keyword like many other programming
#                 languages, this functionality has to be gained in a if/elif/else structure.
#  usage       : On the console in this folder type "python.exe py220switch.py"
sPYTHONV = 'Python ' + __import__('platform').python_version()
sVERSION = 'v0.1.7'

from random import randint

print("*** Aloha, ʻo kēia `py220switch.py` %s — Python Switch mimiced (%s) ***" % (sVERSION, sPYTHONV))

print("Your dice is rolling …")
randi = randint(1, 7)                                       # Numbers 1 through 7
if randi == 1 : print("Look, a one")
elif randi == 2 : print("Whoops, a two")
elif randi == 3 : print("Uh-huh, a three")
elif randi == 4 : print("Holla, a four")
elif randi == 5 : print("Oh, a five")
elif randi == 6 : print("Hooray, a six")
else : print("Your dice seems broken, you rolled ", randi)

print("Aloha mai.")
