﻿#  file        : id 20210928°1341 — gitlab.com/normai/cheeseburger …/py/py121operas.py
#  version     : • 20220908°1611 v0.1.7 Filling • 20210928°1341 v0.1.6 Stub
#  license     : BSD 3-Clause | © 2021 - 2024 Norbert C. Maier
#  subject     : Demonstrate some Python operators
#  ref         : https://maoridictionary.co.nz/ [ref 20220806°1256]
#  ref         : https://www.dataquest.io/blog/python-ternary-operator/ [ref 20220909°0912]
sPYTHONV = 'Python ' + __import__('platform').python_version()
sVERSION = 'v0.1.7'

print("*** Hello, this is `py121operas.py` %s — Some Python operators (%s) ***" % (sVERSION, sPYTHONV))

# Assignment operators
v = w = x = y = z = 5                                           # Multi assignment
w += 2
x -= 2
y *= 2
z /= 2
print("(1.1) 'v = w = x = y = z = 5'             :", v)
print("(1.2) 'w += 2' identical with 'w = w + 2' :", w)
print("(1.3) 'x -= 2' identical with 'x = x - 2' :", x)
print("(1.4) 'y *= 2' identical with 'y = y * 2' :", y)
print("(1.5) 'z /= 2' identical with 'z = z / 2' :", z)
print()

# Remarkable operators
x1 = 100 / 30                                                   # Normal division, yielding a float
x2 = 100 // 30                                                  # Integer division, yielding an integer
x3 = 100 % 30                                                   # Modulo
x4 = 16 ** 3                                                    # Power of
x5 = 1234 if 1 > 2 else 5678                                    # Ternary operator
print("(2.1) 100 / 30 = " + str(x1) + " " + str(type(x1)))
print("(2.2) 100 // 30 = " + str(x2) + " " + str(type(x2)))
print("(2.3) 100 % 30 = " + str(x3) + " " + str(type(x3)))
print("(2.4) 16 ** 3 = " + str(x4) + " " + str(type(x4)))
print("(2.5) 1234 if 1 > 2 else 5678 = " + str(x5) + " " + str(type(x5)))
print()

# The format function introduces the member operator '.'
i1 = 1; i2 = 2.3; f1 = 3.456789
s = "(3) Take {0:.3f} or {1:.3f} or {2:.3f}.".format(i1, i2, f1)
print(s)

print("Good bye.")
