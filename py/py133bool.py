﻿#  file        : id 20221012°0811 — gitlab.com/normai/cheeseburger …/py/py133bool.py
#  version     : • 20221014°1111 v0.1.8 Filling • 20221012°0811 v0.1.8 Stub
#  license     : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
#  subject     : Demonstrate Boolean Type
#  userstory   :
#  summary     :
#  ref         : https://pynative.com/python-type-casting/#h-casting-integer-to-boolean-type [ref 20221014°0912]
#  ref         :
#  status      : Initial draft. Can be finetuned.
sPYTHONV = 'Python ' + __import__('platform').python_version()
sVERSION = 'v0.1.8'

print("*** Привіт, це `py133bool.py` %s — Boolean Type (%s) ***" % (sVERSION, sPYTHONV))  # "Привіт, це [Pryvit, tse]"

# (1) The naive test series
print('(1) Naive test series:')
bNone = None
bFalse = False
bTrue = True
iMinus = -1
iZero = 0
iOne = 1
iTwo = 2
fMinus = -0.1
fZero = 0.0
fOne = 0.1
fTwo = 1.23
print("(1.1)  bool(None) = ", bool(bNone))
print("(1.2)  False      = ", bFalse)
print("(1.3)  True       = ", bTrue)
print("(1.4)  bool(-1)   = ", bool(iMinus))
print("(1.5)  bool(0)    = ", bool(iZero))
print("(1.6)  bool(1)    = ", bool(iOne))
print("(1.7)  bool(2)    = ", bool(iTwo))
print("(1.8)  bool(-0.1) = ", bool(fMinus))
print("(1.9)  bool(0.0)  = ", bool(fZero))
print("(1.10) bool(0.1)  = ", bool(fOne))
print("(1.11) bool(1.23) = ", bool(fTwo))
print("(1.12) bool('')   = ", bool(''))                   # Empty strings evaluate to False
print("(1.13) bool('x')  = ", bool('x'))                  # Non-empty strings evaluate to True

# (2) Targeted conversion
print('(2) Targeted conversion:')
lInput = ['', 'Bla', 'Ja', 'No', 'True', "Yes"]
for x in lInput:
   b = x.lower() in ['1', 'j', 'ja', 't', 'true', 'y', 'yes']
   print(' -', "{:6}".format(x), b)

print("до побачення [Do pobachennya].")
