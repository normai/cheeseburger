﻿#  file        : id 20220831°0811 — gitlab.com/normai/cheeseburger …/py/py157array.py
#  version     : • 20220908°1151 v0.1.7 Filling • 20220831°0811 Stub
#  license     : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
#  subject     : Demonstrate Python 'arrays'
#  summary     : This demonstrates PHP 'arrays' in two servings:
#                 (1) List with single-elements-definition and for loop
#                 (2) List with bulk definition and for-each loop
sPYTHONV = 'Python ' + __import__('platform').python_version()
sVERSION = 'v0.1.7'

import sys                                                      # getsizeof()

print("*** Здравейте [Zdraveĭte] `py157array.py` %s — Array (%s) ***" % (sVERSION, sPYTHONV))  # "Здравейте [Zdraveĭte]"

# ==============================================================
# (1) Lower level style
# (1.1) Define array
mountains = []                                                  # List type
mountains.append("Zugspitze")
mountains.append("Schneefernerkopf")
mountains.append("Weather spike")
mountains.append("Middle Höllental Peak")

# (1.2) Retrieve size infos
print("(1.2.1) Mountains List memory consumption:"
      , mountains.__sizeof__(), " /"
       ,  sys.getsizeof(mountains), "bytes"                     # Including garbage collector overhead
       )
print("(1.2.2) Number of mountains: ", len(mountains))

# (1.3) Iterate over and output all elements
for i in range(0, len(mountains)) :
   print(" -", mountains[i])

# ==============================================================
# (2) Higher level style
# (2.1) Define array
rivers = ["Danube", "Elbe", "Main", "Rhine", "Weser"]           # List type

# (2.2) Retrieve size infos
print("(2.2.1) Rivers List memory consumption:"
      , rivers.__sizeof__(), " /"
       ,  sys.getsizeof(rivers), "bytes"
        )
print("(2.2.2) Number of rivers:", len(rivers))

# (2.3) Iterate over and output all elements
for s in rivers :
   print(" -", s)


print("Довиждане [Dovizhdane].")
