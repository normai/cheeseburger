﻿#  file        : id 20220829°0911 — gitlab.com/normai/cheeseburger …/py/py167tuple.py
#  version     : • 20220908°1511 v0.1.7 Filling • 20220829°0911 v0.1.6 Stub
#  license     : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
#  subject     : Demonstrate Tuples
#  summary     : A tuple is something like a record in a database table
#  ref         : https://www.w3schools.com/python/python_tuples.asp []
#  ref         : https://www.w3schools.com/python/python_tuples_loop.asp []
#  ref         : https://docs.python.org/3/c-api/tuple.html []
sPYTHONV = 'Python ' + __import__('platform').python_version()
sVERSION = 'v0.1.7'

print("*** Hallo, dit is `py167tuple.py` %s — Simple Tuple (%s) ***" % (sVERSION, sPYTHONV))

# (1) Create
tpl = ("Values", 1234, 2.345, True, ("Ding",))
print("(1) tpl = ", tpl)

# (2) Info
print("(2.1) Size =", len(tpl))
print("(2.2) Type =", type(tpl))

# (3) Iterate
print("(3) Iterate:")
for item in tpl:
   print("     -", str(item).ljust(12), type(item))

# (4) Pick by index
print("(4.1) Third item =", tpl[2])
print("(4.2) First of fifth =", tpl[4][0])

print("Tot ziens.")
