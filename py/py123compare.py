﻿#  file        : id 20220214°1831 — gitlab.com/normai/cheeseburger …/py/py123compare.py
#  version     : • 20220908°1711 v0.1.7 Filling • 20220214°1831 v0.1.6 Stub
#  license     : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
#  subject     : Demonstrate Python comparison operators
#  references :
#               • https://www.w3schools.com/python/python_operators.asp [ref 20220906°1312] Nice general overview
#               • https://www.w3schools.com/python/gloss_python_comparison_operators.asp [ref 20220906°1313] The page for this file
#               • https://docs.python.org/3/library/operator.html [ref 20220906°1314] Nice general background knowledge
#               • https://wiki.python.org/moin/BitwiseOperators [ref 20220906°1315] For the next demo py125bitwise.py
#               • https://davidamos.dev/the-right-way-to-compare-floats-in-python/ [ref 20220906°1322]
sPYTHONV = 'Python ' + __import__('platform').python_version()
sVERSION = 'v0.1.7'

import math

print("*** Ahoj `py123compare.py` %s — (%s) ***" % (sVERSION, sPYTHONV))

print('(1.1) "Ahoj" == "Aloha" :', "Ahoj" == "Aloha")
print('(1.2) 12345  == 23456   :', 12345  == 23456)
print('(1.3) 3.4567 == 4.5678  :', 3.4567 == 4.5678)
print('(1.4) True   == False   :', True   == False)
print()
print('(2.1) "Ahoj" != "Aloha" :', "Ahoj" != "Aloha")
print('(2.2) 12345  != 23456   :', 12345  != 23456)
print('(2.3) 3.4567 != 4.5678  :', 3.4567 != 4.5678)
print('(2.4) True   != False   :', True   != False)
print()
print('(3.1) "Ahoj" >  "Aloha" :', "Ahoj" > "Aloha")
print('(3.2) 12345  >  23456   :', 12345  > 23456)
print('(3.3) 3.4567 >  4.5678  :', 3.4567 > 4.5678)
print('(3.4) True   >  False   :', True   > False)
print()
print('(4.1) "Ahoj" >= "Aloha" :', "Ahoj" >= "Aloha")
print('(4.2) 12345  >= 23456   :', 12345  >= 23456)
print('(4.3) 3.4567 >= 4.5678  :', 3.4567 >= 4.5678)
print('(4.4) True   >= False   :', True   >= False)
print()
print('(5.1) "Ahoj" <  "Aloha" :', "Ahoj" < "Aloha")
print('(5.2) 12345  <  23456   :', 12345  < 23456)
print('(5.3) 3.4567 <  4.5678  :', 3.4567 < 4.5678)
print('(5.4) True   <  False   :', True   < False)
print()
print('(6.1) "Ahoj" >= "Aloha" :', "Ahoj" >= "Aloha")
print('(6.2) 12345  >= 23456   :', 12345  >= 23456)
print('(6.3) 3.4567 >= 4.5678  :', 3.4567 >= 4.5678)
print('(6.4) True   >= False   :', True   >= False)
print()
print('(7.1) 0.1 + 0.2 == 0.3  :', 0.1 + 0.2 == 0.3)
print('(7.2) math.isclose(0.1 + 0.2, 0.3) :', math.isclose(0.1 + 0.2, 0.3))

print("Hüvasti.")
