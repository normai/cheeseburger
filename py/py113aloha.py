﻿#  file        : id 20210928°1311 — gitlab.com/normai/cheeseburger …/py/py113aloha.py
#  version     : • 20220908°0851 v0.1.7 Filling • 20210928°1311 Stub
#  license     : BSD 3-Clause | © 2021 - 2024 Norbert C. Maier
#  subject     : Demonstrate basic program building elements
#  summary     : This demonstrates basic elements like • Variables • Operators • Strings • Built-in function calls
sPYTHONV = 'Python ' + __import__('platform').python_version()
sVERSION = 'v0.1.7'

# Built-in function call
print("*** ʻO kēia `py113aloha.py` %s — Aloha (%s) ***" % (sVERSION, sPYTHONV))

# Define some variables and use an operator
a = 123
b = 345
c = a + b

# Output with a bulky statement
print("(1) Sum of " + str(a) + " and " + str(b) + " is " + str(c))

# Output with a summarized statement
s = "(2) Sum of " + str(a) + " and " + str(b) + " is " + str(c)
print(s)

# Output numbers in various notations
sBin = bin(a * -1)
sHex = hex(a)
print ("(3) Bin : " + sBin + " Hex : " + sHex)

# Show difference between integers, floats and strings
print("(4) Get variable types:")
i = 12345
f = 12.345
s = "(5) A character chain"
print(" - i = " + str(i) + " " + str(type(i)))
print(" - f = " + str(f) + " " + str(type(f)))
print(" - s = \"" + str(s) + "\" " + str(type(s)))             # Escape sequence '\"'

print("Aloha mai.")
