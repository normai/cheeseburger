﻿<?php
   /**
    *  file    : id 20220826°0931 — gitlab.com/normai/cheeseburger … php/ph111hello.php
    *  version : • 20220908°0741 v0.1.7 Filling • 20220826°0931 Stub 20210901°0311
    *  chain   : file 20210831°1911 Monkeyform One
    *  license : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
    *  subject : Demonstrate a most simple PHP script with user input
    *  summary :
    *  usage   : View attached HTML page in browser from PHP-enabled localhost webserver
    */
   $sVERSION = "v0.1.7";

   echo("<p>*** This is 'ph111hello.php' $sVERSION — Monkeyform Zero ***</p>\n");
   echo("<pre>\n");

   $aktion = date("H:i:s");

   if (empty($_GET['sYourName']))
   {
      //var_dump($_GET);

      echo('
         <p>Please fill in this form.</p>
         <form action="ph111hello.php.html" method="get">

         <p>Your name:
            <input type="text" name="sYourName" size="50" value="'
            . ( isset($_GET['sYourName']) ? $_GET['sYourName'] : "") . '">
         </p>

         <p>
            <input type="submit" value="Send form">
         </p>

         </form>
      ');
   }
   else
   {
      // Any action, e.g. send email, post database entry
      echo("<p>Your name is \"" . $_GET['sYourName'] . "\"");


      echo("</pre>\n");
      echo("<p>Bye.</p>\n");
   }
?>
