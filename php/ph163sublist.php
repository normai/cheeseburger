﻿<?php
   /**
    *  file    : id 20220828°1831 — gitlab.com/normai/cheeseburger … php/ph163sublist.php
    *  version : • 20220908°1341 v0.1.7 Filling • 20220828°1831 v0.1.6 Stub
    *  license : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
    *  subject : Demonstrate array slicing
    *  ref     : https://www.php.net/manual/en/function.array-slice.php []
    */
   $sVERSION = "v0.1.7";

   echo("<p>*** Ahoj 'ph163sublist.php' $sVERSION — Sublists ***</p>\n");
   echo("<pre>\n");


   // (1) Create
   $words = array ("Uno", "Due", "Tre", "Quattro", "Cinque");
   array_push($words, "Sei");
   array_push($words, "Sette");
   echo("<p>Array = " . implode(" ", $words) . "</p>\n");

   // (2) Slice
   $slice = array_slice($words, 2, 4);

   // (3) Output
   echo("<p>Slice = ");
   foreach ($slice as $word)
   {
       echo(" " . $word);
   };
   echo("</p>\n");


   echo("</pre>\n");
   echo("<p>Nashledanou.</p>\n");
?>
