﻿<?php
/**
 *  file       : id 20221117°1131 — gitlab.com/normai/cheeseburger … php/ph147bigflo.php
 *  version    : • 20221228°1511 v0.1.8 Filling • 20221117°1131 v0.1.7 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate big float types
 *  userstory  :
 *  dependency : PHP module 'BCMath Arbitrary Precision Mathematics' which is usually compiled-in
 */
   $sVERSION = "v0.1.8";

   echo("<p>*** हैलो, यह है [Hailo, yah hai] `ph147bigflo.php` $sVERSION — Big floats ***</p>\n");
   echo("<pre>\n");


   echo("(1) Native PHP numbers :\n");
   $d1 = 10.0 / 7.0;
   $d2 = 10.0 / 3.0;
   $d3 = 10.0 / 1.5;
   echo("(1.1.1) 10 / 7.0      = $d1\n");
   echo("(1.1.2) 10 / 3.0      = $d2\n");
   echo("(1.1.3) 10 / 1.5      = $d3\n\n");

   // See e.g. https://en.wikipedia.org/wiki/Approximations_of_%CF%80 [ref 20221228°1212]
   $dPi65 = 3.14159265358979323846264338327950288419716939937510582097494459230;
   echo("(1.2.1) π-65-decimals = " . $dPi65 . "\n");
   echo("(1.2.2) 22 / 7        = " . 22 / 7 . "\n");
   echo("(1.2.3) 355 / 113     = " . 355 / 113 . "\n");
   echo("(1.2.4) 62832 / 20000 = " . 62832 / 20000 . "\n");
   echo("\n");


   echo("(2) BCMath Arbitrary Precision Mathematics :\n");
   $sBcm1 = bcdiv('10.0', '7.0', $scale = 64);
   $sBcm2 = bcdiv('10.0', '3.0', $scale = 64);
   $sBcm3 = bcdiv('10.0', '150', $scale = 64);
   echo("(2.1.1) 10 / 7.0      = $sBcm1\n");
   echo("(2.1.2) 10 / 3.0      = $sBcm2\n");
   echo("(2.1.3) 10 / 1.5      = $sBcm3\n\n");

   $sPi65 = "3.14159265358979323846264338327950288419716939937510582097494459230";
   echo("(2.2.1) π-65-decimals = " . $sPi65 . "\n");
   echo("(2.2.2) 22 / 7        = " . bcdiv('22', '7', $scale = 64) . "\n");
   echo("(2.2.3) 355 / 113     = " . bcdiv('355', '113', $scale = 64) . "\n");
   echo("(2.2.4) 62832 / 20000 = " . bcdiv('62832', '20000', $scale = 64) . "\n");


   echo("</pre>\n");
   echo("<p>अलविदा [Alavida].</p>\n");
?>
