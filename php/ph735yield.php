﻿<!-- EMPTY STUB, YET TO BE FILLED WITH CODE -->

<?php
/**
 *  file       : id 20221227°1031 — gitlab.com/normai/cheeseburger … php/ph735yield.php
 *  version    : • 20xxxxxx°xxxx v0.x.x Filling • 20221227°1031 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate the yield keyword
 *  status     :
 */
   $sVERSION = "v0.0.0";

   echo("<p>*** Це [Tse] 'ph735yield.php' $sVERSION — Yield ***</p>\n");
   echo("<pre>\n");



   echo("For a first simple example see file <a href=\"ph117keywords.php.html\">ph117keywords.php.html</a>\n");



   echo("</pre>\n");
   echo("<p>до побачення [Do pobachennya].</p>\n");
?>
