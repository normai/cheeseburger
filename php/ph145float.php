﻿<?php
/**
 *  file       : id 20221012°1131 — gitlab.com/normai/cheeseburger … php/ph145float.php
 *  version    : • 20221216°1115 v0.1.8 Filling • 20221012°1131 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate Float Type
 *  status     : Has room for enhancement. Should use string formatting for number output.
 */
    $sVERSION = "v0.1.8";

    echo("<p>*** Dobrý den, toto je 'ph145float.php' $sVERSION — Float Type ***</p>\n");
    echo("<pre>\n");


    echo("(1) Print some numbers:" . "\n");
    $f1 = 123_456.789;
    $f2 = -123_456.789;
    $f3 = 123_456_777_777_777_777_777_777_777.789;
    $f4 = -123_456_777_777_777_777_777_777_777.789;
    echo("(1.1) f1 = $f1 " . gettype($f1) . "\n");
    echo("(1.2) f2 = $f2 " . gettype($f2) . "\n");
    echo("(1.3) f3 = $f3 " . gettype($f3) . "\n");
    echo("(1.4) f4 = $f4 " . gettype($f4) . "\n");


    echo("</pre>\n");
    echo("<p>Nashledanou.</p>\n");
?>
