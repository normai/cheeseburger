﻿<?php
   /**
    *  file    : id 20221002°0931 — gitlab.com/normai/cheeseburger … php/ph127ternary.php
    *  version : • 20221227°1615 v0.1.9 Filling • 20221002°0931 v0.1.8 Stub
    *  license : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
    *  subject : Demonstrate ternary operator
    *  userstory : Roll dice and tell whether it was greater than three or not
    */
   $sVERSION = "v0.1.9";

   echo("<p>*** Hej, det här är `ph127ternary.php` $sVERSION — Ternary operator ***</p>\n");
   echo("<pre>\n");


   // Preparation — Roll dice
   $iRand = rand(1, 6);                                                // From 1 though 6
   echo("(1.1) You rolled " . $iRand. "\n");

   # The demo line
   echo("(1.2) You rolled greater three: " . ($iRand > 3 ? "Yes" : "No"));


   echo("</pre>\n");
   echo("<p>Adjö.</p>\n");
?>
