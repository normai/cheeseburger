﻿<?php
/**
 *  file       : id 20221001°1331 — gitlab.com/normai/cheeseburger … php/ph151string.php
 *  version    : • 20221009°1731 v0.1.8 Filling • 20221001°1331 v0.1.7 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate string syntax
 *  ref        : https://www.php.net/manual/en/language.types.string.php [ref 20221011°0912]
 *  ref        : https://stackoverflow.com/questions/17193597/strings-as-arrays-in-php [ref 20221011°0913]
 *  ref        : https://www.simplilearn.com/tutorials/php-tutorial/string-to-array-in-php [ref 20221011°0914]
 *  ref        : https://sebhastian.com/php-convert-string-to-number/ [ref 20221011°0922]
 *  ref        : https://stackoverflow.com/questions/40973648/php-check-if-a-string-contains-a-float [ref 20221011°0923]
 */
   $sVERSION = "v0.1.8";

   echo("<p>*** Pozdravljeni, tukaj 'ph151string.php' $sVERSION — Strings ***</p>\n");
   echo("<pre>\n");


   // ====================================================
   // (1) String Syntax

   // (1.1) Syntax — Quote types and newline
   echo("(1.1) Use double-quotes \" or single-quotes \' for strings, they behave different" . "\n");

   // (1.2) Syntax — Mulitiline string (Heredoc syntax and Nowdoc syntax) (See ref 20221011°0912)
   $sMulti = <<<HEREDOC
   Ein Wiesel
   saß auf einem Kiesel
   inmitten Bachgeriesel.
   HEREDOC;
   echo("(1.2) " . $sMulti . "\n");

   // (1.3) Syntax — String concatenation (is demonstrated again below)
   $sHello = "Hello";
   $sJenny = "Jenny";
   $sGreet = $sHello . ' ' . $sJenny;
   echo("(1.3) " . $sGreet . "\n");

   // (1.4) Syntax — String as array of chars
   $sWord = "SilzuZankunKrei";
   echo("(1.4) String as Array : ");
   foreach (str_split($sWord) as $c) {
      echo($c . ".");
   }
   echo("\n");

   echo("(1.5) String indices  : ");
   for ($i = 0; $i < strlen($sWord); $i++) {
      echo($sWord[$i] . ".");
   }
   echo("\n");

   // ==============================================
   // (2) Conversions

   echo("(2.1) Number to string: \"" . 1234 . "\", \"" . (string) 2.345 . "\"\n");

   echo("(2.2) String to number:" . "\n");                              // For more options see e.g. ref 20221011°0922
   $nums = ["1234", "2.345", "Tree hundred twenty five"];
   $o = null;
   foreach ($nums as $sNum) {
      if (is_numeric($sNum) && strpos($sNum, '.') === false) {
         $o = (int) $sNum;
      }
      else if (is_numeric($sNum)) {
         $o = (float) $sNum;
      }
      else {
         $o = "Not a number";
      }
      echo("      - \"" . $sNum . "\" => " . $o . " (" . gettype($o) . ")\n");
   }


   echo("</pre>\n");
   echo("<p>Nasvidenje.</p>\n");
?>
