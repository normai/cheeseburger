﻿<?php
/**
 *  file       : id 20221215°1131 — gitlab.com/normai/cheeseburger … php/phfactorial.php
 *  version    : • 20221216°0925 v0.1.8 Filling • 20221215°1131 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate factorial calculation
 *  status     : Algorithm is still too naive
 */
   $sVERSION = "v0.1.8";

   // Function 20221215°1421 Recursion naive
   function Factorial($iVal) {
      if ($iVal > 1) {
         $iVal = $iVal * Factorial($iVal - 1);
         return $iVal;
      }
      else {
         return 1;
      }
   }


   echo("<p>*** Sveiki, tai 'phfactorial.php' $sVERSION — Factorial ***</p>\n");
   echo("<pre>\n");


   for ($i = -1; $i < 24; $i++) {
      $iFact = Factorial($i);
      echo(" - " . $i . "! = " . $iFact . "\n");
   }


   echo("</pre>\n");
   echo("<p>Iki pasimatymo.</p>\n");
?>
