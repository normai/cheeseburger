﻿<!-- THIS FILE IS AN EMPTY STUB, IT IS YET TO BE FILLED WITH CODE -->

<?php
   /**
    *  file    : id 20220828°1031 — gitlab.com/normai/cheeseburger … php/ph335format.php
    *  version : • 20xxxxxx°xxxx v0.x.x Filling • 20220828°1031 v0.1.6 Stub
    *  license : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
    *  subject : Demonstrate PHP string formatting
    *  summary : Common tasks:
    *            • Convert number to decimal, hexadecimal, binary string
    *            • Fixed length string, padded right or padded left
    *            • Display floatingpoint numbers with fixed decimal places
    *            •
    *            •
    *  ref     : https://devdojo.com/devdojo/php-to-string-equivalent [ref 20220907°1213]
    *  ref     : https://stackoverflow.com/questions/10616334/how-to-display-the-binary-value-of-an-integer-in-php [ref 20220907°1214]
    *  ref     :
    *  ref     :
    *  ref     :
    */
   $sVERSION = "v0.0.0";

   echo("<p>*** Dobrý deň, toto je 'ph335format.php' $sVERSION — Format functions ***</p>\n");
   echo("<pre>\n");







   echo("</pre>\n");
   echo("<p>Dovidenia.</p>\n");
?>
