﻿<?php
/**
 *  file       : id 20221012°1831 — gitlab.com/normai/cheeseburger … php/ph159date.php
 *  version    : • 20221230°1651 v0.1.9 Filling • 20221012°1831 v0.1.8 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate Date and Time Types
 *  userstory  : Set and print dates in various formats and calculate time spans
 */
   $sVERSION = "v0.1.9";

   echo("<p>*** Tere, see on 'ph159date.php' $sVERSION — Date and Time types ***</p>\n");
   echo("<pre>\n");


   // () For calculation far below
   $dtStart = new DateTime();

   // (1) Show limits
   echo("(1) DateTime limits:\n");

   // What is the difference to DateTime? I see no difference  between
   // date() here and 'new DateTime' below. So skip this sequence
   if (False) {
      $dMin = date('Y-m-d H:i:s.u', PHP_INT_MIN);
      $dNul = date('Y-m-d H:i:s.u', 0);
      $dMax = date('Y-m-d H:i:s.u', PHP_INT_MAX);
      echo("   1a. Date min     = " . $dMin . "\n");
      echo("   2a. Date null    = " . $dNul . "\n");
      echo("   3a. Date min     = " . $dMin . "\n");
   }

   $dtMin = (new DateTime())->setTimestamp(PHP_INT_MIN);
   $dtNul = (new DateTime())->setTimestamp(0);
   $dtMax = (new DateTime())->setTimestamp(PHP_INT_MAX);
   echo("   1. DateTime min  = " . $dtMin->format('Y-m-d`H:i:s.u') . "\n");
   echo("   2. DateTime null = " . $dtNul->format('Y-m-d`H:i:s.u') . "\n");
   echo("   3. DateTime max  = " . $dtMax->format('Y-m-d`H:i:s.u') . "\n");

   // (2) Galileo birthday
   echo("(2) Galileo birthday:\n");
   $dtGal1 = new DateTime('1564-02-16');
   $dtGal2 = new DateTime("16-02-1564 12:34:56.123456789");            // See https://stackoverflow.com/questions/33691428/datetime-with-microseconds [ref 20221230°1636]
   echo("   1. RFC 2822/5322 = " . $dtGal1->format('r .u') . " — without time \n");
   echo("   2. RFC 2822/5322 = " . $dtGal2->format('r .u') . " — with time and μs\n");
   echo("   3. Custom        = " . $dtGal2->format('Y-m-d`H:i:s.u, D, M, e T') . "\n");

   // (3) Now
   echo("(3) Now:\n");
   $dtNow1 = new DateTime("midnight");                                 // See https://stackoverflow.com/questions/36741877/php-datetime-with-date-without-hours [ref 20221230°1642]
   $dtNow2 = new DateTime();
   echo("   1. RFC 2822/5322 = " . $dtNow1->format('r .u') . " — without time \n");
   echo("   2. RFC 2822/5322 = " . $dtNow2->format('r .u') . " — with time and μs\n");
   echo("   3. Custom        = " . $dtNow2->format('Y-m-d`H:i:s.u, D, M, e T') . "\n");

   // (4) Halley
   echo("(4) Next Halley approach:\n");
   $dtHal1 = new DateTime('2061-06-29');
   $dtHal2 = new DateTime("29-06-2061 12:34:56.123456789");            // See https://stackoverflow.com/questions/33691428/datetime-with-microseconds [ref 20221230°1636]
   echo("   1. RFC 2822/5322 = " . $dtHal1->format('r .u') . " — without time \n");
   echo("   2. RFC 2822/5322 = " . $dtHal2->format('r .u') . " — with time and μs\n");
   echo("   3. Custom        = " . $dtHal2->format('Y-m-d`H:i:s.u, D, M, e T') . "\n");

   // (5) Calculate large scale time span
   $wait = $dtNow2->diff($dtHal2);
   echo("(5) Wait for Halley : " . $wait->format('%R%a days')
                                  . " ≈ " . $wait->y . " y"
                                   . ", " . $wait->m . " m"
                                    . ", " . $wait->d . " d"
                                     . ", " . $wait->h . " h"
                                      . ", " . $wait->i . " m"
                                       . ", " . $wait->s . " s"
                                        . ", " . ($wait->f * 1000000) . " μs"
                                         . "\n");

   // (6) Calculate short scale time span
   $dtStop = new DateTime();
   $runti = $dtStart->diff($dtStop);
   echo("(6) Runtime duration : " . ($runti->f * 1000000) . " μs\n");


   echo("</pre>\n");
   echo("<p>Hüvasti.</p>\n");
?>
