﻿<?php
   /**
    *  file    : id 20220826°1431 — gitlab.com/normai/cheeseburger … php/ph220switch.php
    *  version : • 20221001°1031 v0.1.8 Filling • 20220826°1431 v0.1.6 Stub
    *  license : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
    *  subject : Demonstrate
    */
   $sVERSION = "v0.1.8";

   echo("<p>*** Aloha, ʻo kēia 'ph220switch.php' $sVERSION — PHP Switch ***</p>\n");
   echo("<pre>\n");
   echo("<pre>");

   echo("Your dice is rolling ...\n");
   $randi = rand(1, 7);                                             // Numbers 1 through 7
   switch ($randi)
   {
      case 1 : echo("Look, a one\n"); break;
      case 2 : echo("Whoops, a two\n"); break;
      case 3 : echo("Uh-huh, a three\n"); break;
      case 4 : echo("Holla, a four\n"); break;
      case 5 : echo("Oh, a five\n"); break;
      case 6 : echo("Hooray, a six\n"); break;
      default : echo("Your dice seems broken, you rolled " . $randi . "\n"); break;
   }


   echo("</pre>");
   echo("<p>Aloha mai.</p>\n");
?>
