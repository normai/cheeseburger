﻿   echo("<pre>\n");
<?php
   /**
    *  file    : id 20220826°1131 — gitlab.com/normai/cheeseburger … php/ph125bitwise.php
    *  version : • 20220908°1801 v0.1.7 Filling • 20220826°1131 v0.1.6 Stub
    *  license : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
    *  subject : Demonstrate PHP bitwise operators
    *  ref     : https://www.php.net/manual/en/language.operators.bitwise.php [ref 20220907°1212]
    */
   $sVERSION = "v0.1.7";

   $i1 = 43690;
   $i2 = 3855;

   echo("<p>*** Hei, tämä on 'ph125bitwise.php' $sVERSION — Bitwise operators ***</p>\n");
   echo("<pre>\n");


   echo("(1) Bitwise AND" . "\n");
   echo("(1.1) \$i1          =  "  . str_pad(decbin($i1), 64, " ", STR_PAD_LEFT)       . " " . str_pad(dechex($i1), 18, " ", STR_PAD_LEFT)       . " " . str_pad(strval($i1), 9, " ", STR_PAD_LEFT)       . "\n");
   echo("(1.2) \$i2          =  "  . str_pad(decbin($i2), 64, " ", STR_PAD_LEFT)       . " " . str_pad(dechex($i2), 18, " ", STR_PAD_LEFT)       . " " . str_pad(strval($i2), 9, " ", STR_PAD_LEFT)       . "\n");
   echo("(1.3) \$i1 & \$i2    =  " . str_pad(decbin($i1 & $i2), 64, " ", STR_PAD_LEFT) . " " . str_pad(dechex($i1 & $i2), 18, " ", STR_PAD_LEFT) . " " . str_pad(strval($i1 & $i2), 9, " ", STR_PAD_LEFT) . "\n");

   echo("(2) Bitwise OR" . "\n");
   echo("(2.1) \$i1          =  "  . str_pad(decbin($i1), 64, " ", STR_PAD_LEFT)       . " " . str_pad(dechex($i1), 18, " ", STR_PAD_LEFT)       . " " . str_pad(strval($i1), 9, " ", STR_PAD_LEFT)       . "\n");
   echo("(2.2) \$i2          =  "  . str_pad(decbin($i2), 64, " ", STR_PAD_LEFT)       . " " . str_pad(dechex($i2), 18, " ", STR_PAD_LEFT)       . " " . str_pad(strval($i2), 9, " ", STR_PAD_LEFT)       . "\n");
   echo("(2.3) \$i1 | \$i2    =  " . str_pad(decbin($i1 | $i2), 64, " ", STR_PAD_LEFT) . " " . str_pad(dechex($i1 | $i2), 18, " ", STR_PAD_LEFT) . " " . str_pad(strval($i1 | $i2), 9, " ", STR_PAD_LEFT) . "\n");

   echo("(3) Bitwise XOR" . "\n");
   echo("(3.1) \$i1          =  "  . str_pad(decbin($i1), 64, " ", STR_PAD_LEFT)       . " " . str_pad(dechex($i1), 18, " ", STR_PAD_LEFT)       . " " . str_pad(strval($i1), 9, " ", STR_PAD_LEFT)       . "\n");
   echo("(3.2) \$i2          =  "  . str_pad(decbin($i2), 64, " ", STR_PAD_LEFT)       . " " . str_pad(dechex($i2), 18, " ", STR_PAD_LEFT)       . " " . str_pad(strval($i2), 9, " ", STR_PAD_LEFT)       . "\n");
   echo("(3.3) \$i1 ^ \$i2    =  " . str_pad(decbin($i1 ^ $i2), 64, " ", STR_PAD_LEFT) . " " . str_pad(dechex($i1 ^ $i2), 18, " ", STR_PAD_LEFT) . " " . str_pad(strval($i1 ^ $i2), 9, " ", STR_PAD_LEFT) . "\n");

   echo("(4) Bitwise NOT" . "\n");
   echo("(4.1) \$i1          =  "  . str_pad(decbin($i1), 64, " ", STR_PAD_LEFT)       . " " . str_pad(dechex($i1), 18, " ", STR_PAD_LEFT)       . " " . str_pad(strval($i1), 9, " ", STR_PAD_LEFT)       . "\n");
   echo("(4.2) ~\$i1         =  "  . str_pad(decbin(~$i1), 64, " ", STR_PAD_LEFT)      . " " . str_pad(dechex(~$i1), 18, " ", STR_PAD_LEFT)      . " " . str_pad(strval(~$i1), 9, " ", STR_PAD_LEFT)       . "\n");

   echo("(5) Shift Left" . "\n");
   echo("(5.1) \$i1          =  "  . str_pad(decbin($i1), 64, " ", STR_PAD_LEFT)       . " " . str_pad(dechex($i1), 18, " ", STR_PAD_LEFT)       . " " . str_pad(strval($i1), 9, " ", STR_PAD_LEFT)       . "\n");
   echo("(5.2) \$i1 << 1     =  "  . str_pad(decbin($i1 << 1), 64, " ", STR_PAD_LEFT)  . " " . str_pad(dechex($i1 << 1), 18, " ", STR_PAD_LEFT)  . " " . str_pad(strval($i1 << 1), 9, " ", STR_PAD_LEFT)  . "\n");

   echo("(6) Shift Right(?!)" . "\n");
   echo("(6.1) \$i1          =  "  . str_pad(decbin($i1), 64, " ", STR_PAD_LEFT)       . " " . str_pad(dechex($i1), 18, " ", STR_PAD_LEFT)       . " " . str_pad(strval($i1), 9, " ", STR_PAD_LEFT)       . "\n");
   echo("(6.2) \$i1 >> 1     =  "  . str_pad(decbin($i1 >> 1), 64, " ", STR_PAD_LEFT)  . " " . str_pad(dechex($i1 >> 1), 18, " ", STR_PAD_LEFT)  . " " . str_pad(strval($i1 >> 1), 9, " ", STR_PAD_LEFT)  . "\n");


   echo("</pre>\n");
   echo("<p>Näkemiin.</p>\n");
?>
