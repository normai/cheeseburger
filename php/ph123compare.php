﻿<?php
   /**
    *  file    : id 20220826°1031 — gitlab.com/normai/cheeseburger … php/ph123compare.php
    *  version : • 20220908°1701 v0.1.7 Filling • 20220826°1031 v0.1.6 Stub
    *  license : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
    *  subject : Demonstrate PHP comparison operators
    *  summary : Some facts
    *             - Todo: Add demo for sameness comparison and for spaceship operator [todo 20220906°1413]
    *  ref     : https://www.php.net/manual/en/function.strcmp.php [ref 20220906°1412]
    *  ref     : https://www.w3schools.com/php/php_operators.asp [ref 20220906°1413]
    *  ref     : https://www.w3schools.com/php/func_string_strcmp.asp [ref 20220906°1414]
    *  ref     : https://w3schools.in/php/compare-strings [ref 20220906°1415]
    *  ref     :
    */
   $sVERSION = "v0.1.7";

   const THRESHOLD_1 = .000_000_000_1; // Problem — The tolerance has to be different, depending on the magnitude of the number!

   echo("<p>*** Tere, see on 'ph123compare.php' $sVERSION — Comparisons ***</p>\n");
   echo("<pre>\n");


   echo("(1.1)  \"Ahoj\" == \"Aloha\"         = " . ("Ahoj" == "Aloha") . "\n");
   echo("(1.2)  12345  == 23456           = " . (12345  == 23456) . "\n");
   echo("(1.3)  3.4567 == 4.5678          = " . (3.4567 == 4.5678) . "\n");
   echo("(1.4)  true   == false           = " . (true   == false) . "\n");
   echo("\n");
   echo("(2.1)  \"Ahoj\" != \"Aloha\"         = " . ("Ahoj" != "Aloha") . "\n");
   echo("(2.2)  12345  != 23456           = " . (12345  != 23456) . "\n");
   echo("(2.3)  3.4567 != 4.5678          = " . (3.4567 != 4.5678) . "\n");
   echo("(2.4)  true   != false           = " . (true   != false) . "\n");
   echo("\n");
   echo("(3.1)  strcmp(\"Ahoj\", \"Aloha\") = " . strcmp("Ahoj", "Aloha") . "\n");
   echo("(3.2)  12345  >  23456           = " . (12345  > 23456) . "\n");
   echo("(3.3)  3.4567 >  4.5678          = " . (3.4567 > 4.5678) . "\n");
   echo("(3.4)  true   >  false           = " . (true   > false) . "\n");
   echo("\n");
   echo("(4.1)  \"Ahoj\" >= \"Aloha\"         = " . ("Ahoj" >= "Aloha") . "\n");
   echo("(4.2)  12345  >= 23456           = " . (12345  >= 23456) . "\n");
   echo("(4.3)  3.4567 >= 4.5678          = " . (3.4567 >= 4.5678) . "\n");
   echo("(4.4)  true   >= false           = " . (true   >= false) . "\n");
   echo("\n");
   echo("(5.1)  \"Ahoj\" <  \"Aloha\"         = " . ("Ahoj" < "Aloha") . "\n");
   echo("(5.2)  12345  <  23456           = " . (12345  < 23456) . "\n");
   echo("(5.3)  3.4567 <  4.5678          = " . (3.4567 < 4.5678) . "\n");
   echo("(5.4)  true   <  false           = " . (true   < false) . "\n");
   echo("\n");
   echo("(6.1)  \"Ahoj\" >= \"Aloha\"         = " . ("Ahoj" >= "Aloha") . "\n");
   echo("(6.2)  12345  >= 23456           = " . (12345  >= 23456) . "\n");
   echo("(6.3)  3.4567 >= 4.5678          = " . (3.4567 >= 4.5678) . "\n");
   echo("(6.4)  true   >= false           = " . (true   >= false) . "\n");
   echo("\n");
   echo("(7.1)  0.1 + 0.2 == 0.3          = " . (0.1 + 0.2 == 0.3) . "\n");
   echo("(7.2)  abs((0.1 + 0.2) - 0.3) < THRESHOLD_1 = " . (abs((0.1 + 0.2) - 0.3) < THRESHOLD_1) . "\n");
   echo("\n");
   echo("(8.1)  strcmp(\"Ahoj\", \"Aloha\") = " . strcmp("Ahoj", "Aloha") . "\n");
   echo("(8.1)  strcmp(\"Aloha\", \"Ahoj\") = " . strcmp("Aloha", "Ahoj") . "\n");
   echo("(8.1)  strcmp(\"Ahoj\", \"Ahoj\")  = " . strcmp("Ahoj", "Ahoj") . "\n");


   echo("</pre>\n");
   echo("<p>Hüvasti.</p>\n");
?>
