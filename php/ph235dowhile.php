﻿<?php
   /**
    *  file    : id 20220826°1631 — gitlab.com/normai/cheeseburger … php/ph235dowhile.php
    *  version : • 20221001°1131 v0.1.8 Filling • 20220826°1631 v0.1.6 Stub
    *  license : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
    *  subject : Demonstrate
    */
   $sVERSION = "v0.1.8";

   echo("<p>*** Hello, ez 'ph235dowhile.php' $sVERSION — Do/While-Loop ***</p>\n");
   echo("<pre>\n");
   echo("<pre>");

   echo("Loop until you rolled a six :\n");
   $randi = null;                                           // Variable needs still be known behind the do block
   do
   {
      $randi = rand(1, 6);                                  // Numbers 1 through 6
      echo(" • You rolled " . $randi . "\n");
   }
   while ($randi != 6);


   echo("</pre>");
   echo("<p>Viszlát.</p>\n");
?>
