﻿<?php
   /**
    *  file    : id 20220928°1831 — gitlab.com/normai/cheeseburger … php/phcircles1.php
    *  version : • 20220928°18131 v0.1.8 Initial
    *  license : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
    *  subject : Draw ASCII Circles and other patterns
    *  usage   : View accompanied HTML page in browser from localhost webserver
    */
   $sVERSION = "v0.1.8";

   function IsPointOnCirlce($r, $x, $y) {
      $tolerance = $r;                                              // Empirical value for line width
      if (abs(pow($x, 2) + pow($y, 2) - pow($r, 2)) < $tolerance) {
         return true;
      }
      else {
         return false;
      }
   }

   function IsPointOnDiagonal($x, $y, $size) {
      if ($x == $size - $y + 1) {                                   // Add 1 empirically
         return true;
      }
      else {
         return false;
      }
   }

   echo("<p>*** Dobrý deň, toto je 'phcircles1.php' $sVERSION — Draw ASCII Circles ***</p>\n");
   echo("<pre>\n");


   $sizes = array(7, 17);
   foreach($sizes as $size) {                                          // Paint as many rectangles as sizes are given

      // Prepare convenient variables
      // Note how the integer division for the radius looks different in each language
      $radius = intdiv($size, 2) - 1;                                  // Make size of circle slightly smaller than the rectangle
      $shiftx = $radius + 2;                                           // Shift circle center from rectangle left to rectangle center
      $shifty = $radius + 2;                                           // Shift circle center from rectangle top to rectangle center

      // Iterate over lines and columns
      for ($line = 1; $line <= $size; $line++) {
         for ($col = 1; $col <= $size; $col++) {
            if (IsPointOnCirlce($radius, $line - $shiftx, $col - $shifty)) {  // Is this point on the circle?
               echo("@ ");
            }
            else {
               if (IsPointOnDiagonal($line, $col, $size)) {            // Is this point on the diagonal?
                  echo("# ");
               }
               else {                                                  // All other cases
                  echo(". ");
               }
            }
         }
         echo("\n");                                                   // Goto next line
      }
   }


   echo("</pre>\n");
   echo("<p>Dovidenia.</p>\n");
?>
