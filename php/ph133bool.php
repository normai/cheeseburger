﻿<?php
/**
 *  file       : id 20221012°0831 — gitlab.com/normai/cheeseburger … php/ph133bool.php
 *  version    : • 20221014°1131 v0.1.8 Filling • 20221012°0831 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate Boolean Type
 *  userstory  :
 *  summary    :
 */
   $sVERSION = "v0.1.8";

   echo("<p>*** Привіт, це [Pryvit, tse] 'ph133bool.php' $sVERSION — Boolean type ***</p>\n");
   echo("<pre>\n");


   // (1) The naive test series
   echo("(1) Naive test series\n");
   $bNone = null;
   $bFalse = false;
   $bTrue = true;
   $iMinus = -1;
   $iZero = 0;
   $iOne = 1;
   $iTwo = 2;
   $fMinus = -0.1;
   $fZero = 0.0;
   $fOne = 0.1;
   $fTwo = 1.23;
   echo("(1.1)  (bool) None = " . (bool) $bNone . "\n");
   echo("(1.2)  False       = " . $bFalse . "\n");
   echo("(1.3)  True        = " . $bTrue . "\n");
   echo("(1.4)  (bool) -1   = " . (bool) $iMinus . "\n");
   echo("(1.5)  (bool) 0    = " . (bool) $iZero . "\n");
   echo("(1.6)  (bool) 1    = " . (bool) $iOne . "\n");
   echo("(1.7)  (bool) 2    = " . (bool) $iTwo . "\n");
   echo("(1.8)  (bool) -0.1 = " . (bool) $fMinus . "\n");
   echo("(1.9)  (bool) 0.0  = " . (bool) $fZero . "\n");
   echo("(1.10) (bool) 0.1  = " . (bool) $fOne . "\n");
   echo("(1.11) (bool) 1.23 = " . (bool) $fTwo . "\n");
   echo("(1.12) (bool) ''   = " . (bool) "" . "\n");
   echo("(1.13) (bool) 'x'  = " . (bool) "x" . "\n");

   // (2) Targeted conversion
   echo("(2) Targeted conversion:" . "\n");
   $inputs = ["", "Bla", "Ja", "No", "True", "Yes"];
   $trues = ["1", "j", "ja", "t", "true", "y", "yes"];
   foreach($inputs as $x) {
      $b = in_array(trim(strtolower($x)), $trues, true);
      echo("    • " . $x . " -> " . $b . "\n");
   }


   echo("</pre>\n");
   echo("<p>до побачення [Do pobachennya].</p>\n");
?>
