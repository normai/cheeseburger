﻿<?php
   /**
    *  file    : id 20220825°1431 — gitlab.com/normai/cheeseburger …/php/ph115varis.php
    *  version : • 20220908°0942 v0.1.7 Filling • 20220825°1431 v0.1.6 Stub
    *  license : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
    *  summary :
    */
   $sVERSION = "v0.1.7";

   echo("<p>*** Hello, this is 'ph115varis.php' $sVERSION — ***</p>\n");
   echo("<pre>\n");

   $a = 123;                                                    // Integer
   $b = 2.34;                                                   // Float
   $c = "Ahoj";                                                 // String
   $d = $c . " = ";
   $e = $a + $b;
   echo("<p>" . $d . strval($e) . "</p>\n");                    // A cast '(string)' were also sufficient


   echo("</pre>\n");
   echo("<p>Good bye.</p>");
?>
