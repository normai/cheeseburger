﻿<?php
   /**
    *  file    : id 20220828°1931 — gitlab.com/normai/cheeseburger … php/ph165dict.php
    *  version : • 20220908°1441 v0.1.7 Filling • 20220828°1931 v0.1.6 Stub
    *  license : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
    *  subject : Demonstrate dictionary in PHP
    *  summary : PHP does not have a Dictionary class, but the associative array comes close
    *  ref     : https://www.php.net/manual/en/language.types.array.php []
    *  ref     : https://github.com/jcshoww/php-dictionary [ref 20220904°0912] -- An abstract dictionary class
    */
   $sVERSION = "v0.1.7";

   echo("<p>*** Hej, das ist 'ph165dict.php' $sVERSION — Dictionaries ***</p>\n");
   echo("<pre>\n");


   // (1) Create
   $being = array( "kingdom" => "Animals"
                 , "class"   => "Reptiles"
                 , "genus"   => "Crocodile"
                 , "species" => "Nile crocodile"
                 , "name"    => "Schnappi"
                 , "color"   => "Green"
                 , "legs"    => "Four"
                 , "food"    => "Meat"
                 );
   echo("<p>(1) Created = " . implode(" ", $being) . "</p>\n");

   // (2) Info
   echo("<p>(2) Dictionary size = " . count($being) . "</p>\n");

   // (3) Iterate
   echo("<p>(3) Iterate:</p>\n");
   foreach ($being as $key => $value)
   {
       echo("<p> - " . str_pad($key, 8, ".", STR_PAD_RIGHT) . " : " . $value . "</p>\n");
   }


   echo("</pre>\n");
   echo("<p>Farvel.</p>\n");
?>
