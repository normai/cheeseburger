﻿<?php
/**
 *  file       : id 20221012°1731 — gitlab.com/normai/cheeseburger … php/ph155strfunc.php
 *  version    : • 20221013°1131 v0.1.8 Filling • 20221012°1731 v0.1.7 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate String Functions
 *  userstory  :
 *  summary    :
 *  ref        : https://www.php.net/manual/en/function.chr.php [ref 20221011°0916]
 *  ref        : https://www.php.net/manual/en/function.ord.php [ref 20221011°0917]
 *  ref        : https://www.php.net/manual/en/function.substr.php [ref 20221011°0918]
 */
   $sVERSION = "v0.1.8";

   echo("<p>*** Hallo, dit is 'ph155strfunc.php' $sVERSION — String Functions ***</p>\n");
   echo("<pre>\n");


   // () Fodder
   $sWord = "SilzuZankunKrei";

   // (.) String Functions
   // (.1) String length
   echo("(3.1) Length of '" . $sWord . "' is " . strlen($sWord) . "\n");

   // (.2) Character code to string
   echo("(3.2) Code to char : " . chr(36) . chr(37) . chr(38) . chr(96) . chr(97) . chr(98) . chr(122) . chr(123) . chr(124) . chr(125) . '\u007e' . '\u007f' . ".\n");

   // (.3) Substrings
   echo("(3.3) Substrings : " . substr($sWord, 0, 5) . " + " . substr($sWord, 5, strlen($sWord) - 4) . " + " . substr($sWord, strlen($sWord) - 4) . "\n");

   // (.4) Upper/Lower
   echo("(3.4) Upper/Lower : " . strtolower($sWord) . " / " . strtoupper($sWord) . "\n");

   // (.5) Trim
   $sBreezy = "    Breeze    ";
   echo("(3.5) Trim \"" . $sBreezy . "\" : \"" . ltrim($sBreezy) . "\", \"" . rtrim($sBreezy) . "\", \"" . trim($sBreezy) . "\"\n");


   echo("</pre>\n");
   echo("<p>Tot ziens.</p>\n");
?>
