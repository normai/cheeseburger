﻿<?php
   /**
    *  file    : id 20220826°1731 — gitlab.com/normai/cheeseburger … php/ph240for.php
    *  version : • 20220919°1931 v0.1.8 Filling • 20220826°1731 v0.1.6 Stub
    *  license : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
    *  subject : Demonstrate the for loop
    *  ref     : https://www.w3schools.com/php/func_math_pow.asp []
    */
   $sVERSION = "v0.1.8";

   echo("<p>*** Halo, ini adalah 'ph240for.php' $sVERSION — For-Loop ***</p>\n");
   echo("<pre>\n");


   echo("(1) Two power 6 through 10\n");
   for ($i = 6; $i <= 10; $i++)
   {
      $iPow = pow(2, $i);
      echo("   - " . $i . " " . $iPow . "\n");
   }

   echo("(2) And the same backward\n");
   for ($i = 10; $i >= 6; $i--)
   {
      $iPow = pow(2, $i);
      echo("   - " . $i . " " . $iPow . "\n");
   }


   echo("</pre>\n");
   echo("<p>अलविदा [Alavida].</p>\n");
?>
