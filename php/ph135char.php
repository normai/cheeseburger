﻿<?php
/**
 *  file       : id 20221012°0931 — gitlab.com/normai/cheeseburger … php/ph135char.php
 *  version    : • 20221016°1131 v0.1.8 Filling • 20221012°0931 v0.1.7 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate Character Type
 *  userstory  :
 *  summary    :
 *  ref        : https://www.php.net/manual/en/ref.strings.php [ref 20221015°1722]
 *  ref        : https://www.php.net/manual/en/ref.mbstring.php [ref 20221015°1722] (👍)
 */
   $sVERSION = "v0.1.8";

   echo("<p>*** Здравейте, това е [Zdraveĭte, tova e] 'ph135char.php' $sVERSION — Character type ***</p>\n");
   echo("<pre>\n");


   // (A.1) Naive probe
   $sProbe = "Abcdé…xyz 👍";
   echo("(A.1) The probe                : \"" . $sProbe . "\", strlen = " . strlen($sProbe) . ", mb_strlen = " . mb_strlen($sProbe) . PHP_EOL);

   // (A.2)
   // (A.2.1)
   $chars = array();
   echo("(A.2.1) ForEach str_split()    :");
   foreach (str_split($sProbe) as $c) {
      echo(' ' . $c);
      array_push($chars, ord($c));
   }
   echo(PHP_EOL);

   // (A.2.2)
   echo("(A.2.2) ord()                  :");
   foreach ($chars as $ord) {
      echo(' ' . $ord);
   }
   echo(PHP_EOL);

   // (A.2.3)
   echo("(A.2.3) Reassemble by chr()    :");
   foreach ($chars as $c) {
      echo(' ' . chr($c));
   }
   echo(PHP_EOL);

   // (A.3)
   // (A.3.1)
   $mbchars = array();
   echo("(A.3.1) ForEach mb_str_split() :");
   foreach (mb_str_split($sProbe) as $c) {
      echo(' ' . $c);
      array_push($mbchars, mb_ord($c));
   }
   echo(PHP_EOL);

   // (A.3.2)
   echo("(A.3.2) mb_ord()               :");
   foreach ($mbchars as $mbc) {
      echo(' ' . $mbc);
   }
   echo(PHP_EOL);

   // (A.3.3)
   echo("(A.3.3) Reassemble by mb_chr() :");
   foreach ($mbchars as $mbc) {
      echo(' ' . mb_chr($mbc));
   }
   echo(PHP_EOL);


   echo("</pre>\n");
   echo("<p>Довиждане [Dovizhdane].</p>\n");
?>
