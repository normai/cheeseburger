﻿<?php
   /**
    *  file    : id 20220825°1331 — gitlab.com/normai/cheeseburger …/php/ph113aloha.php
    *  version : • 20220908°0841 v0.1.7 Filling • 20220825°1331 Stub
    *  license : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
    *  summary :
    */
   $sVERSION = "v0.1.7";

   echo("<p>*** ʻO kēia 'ph113aloha.php' $sVERSION — Variables, operators, strings ***</p>\n");
   echo("<pre>\n");

   // Define some variables and use an operator
   $a = 123;
   $b = 345;
   $c = $a + $b;

   // Output with a bulky statement
   echo("<p>(1) Sum of " . $a . " and " . $b . " is " . $c . "</p>\n");

   // Output with a summarized statement
   $s = "<p>(2) Sum of " . $a . " and " . $b . " is " . $c . "</p>\n";
   echo($s);

   // Output numbers in various notations
   $sBin = decbin($a);
   $sHex = dechex($a);
   echo("<p>(3) Bin : " . $sBin . ", Hex : " . $sHex . "</p>\n");

   // Show difference between integers, floats and strings
   echo("<p>(4) Get variable types:" . "</p>\n");
   $i = 12345;
   $f = 12.345;
   $s = "Chain of chars";
   echo("<p>  - i = " . $i . " : " . gettype($i) . "</p>\n");
   echo("<p>  - f = " . $f . " : " . gettype($f) . "</p>\n");
   echo("<p>  - s = \"" . $s . "\" : " . gettype($s) . "</p>\n");


   echo("</pre>\n");
   echo("<p>Aloha mai.</p>");
?>
