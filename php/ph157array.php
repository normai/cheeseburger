﻿<?php
   /**
    *  file    : id 20220831°0831 — gitlab.com/normai/cheeseburger … php/ph157array.php
    *  version : • 20220908°1141 v0.1.7 Filling • 20220831°0831 v0.1.6 Stub
    *  license : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
    *  subject : Demonstrate PHP arrays
    *  summary : This demonstrates PHP arrays in three servings:
    *             (1) Indexed array with single-elements-definition
    *             (2) Indexed array with bulk definition
    *             (3) Indexed array using the array_push function and the foreach structure
    */
   $sVERSION = "v0.1.7";

   echo("<p>*** Здравейте [Zdraveĭte] 'ph157array.php' $sVERSION — Arrays ***</p>\n");
   echo("<pre>\n");

   // =====================================================
   // (1) Archaic array, separate declaration plus bulky single-elements-definition
   // (1.1) Define some array
   $mountains = array();
   $mountains[0] = "Zugspitze";
   $mountains[1] = "Schneefernerkopf";
   $mountains[2] = "Weather spike";
   $mountains[3] = "Middle Höllental Peak";

   // (1.2) Retrieve size info
   echo("<p>(1.2.1) Mountains array memory consumption: "              // Memory consumption is hard to retrieve, e.g. by
        /* . Marshal.SizeOf(mountains) */ . "?" . " bytes</p>\n"       //  using memory_get_usage() before and after allocation
         );
   echo("<p>(1.2.2) Number of mountains: " . count($mountains) . "</p>\n");

   // (1.3) Iterate over and output all elements
   for ($i = 0; $i < count($mountains); $i++)
   {
      echo("<p> - " . $mountains[$i] . "</p>\n");
   }

   // =====================================================
   // (2) Archaic array, declaration and definition in one line
   // (2.1) Define some array
   $rivers = array("Danube", "Elbe", "Main", "Rhine", "Weser");

   // (2.2) Retrieve size info
   echo("<p>(2.2.1) Rivers array memory consumption: ? bytes</p>\n");  // This information is hard to retrieve
   echo("<p>(2.2.2) Number of rivers: " . count($rivers) . "</p>\n");

   // (2.3) Iterate over and output all elements
   for ($i = 0; $i < count($rivers); $i++)
   {
      echo("<p> - " . $rivers[$i] . "</p>\n");
   }

   // =====================================================
   // (3)
   // (3.1) Create array of lakes
   $lakes = array();
   array_push($lakes, "Bodensee");
   array_push($lakes, "Müritz");
   array_push($lakes, "Chiemsee");
   array_push($lakes, "Schweriner See");
   array_push($lakes, "Starnberger See");
   array_push($lakes, "Ammersee");

   // (3.2) Retrieve size info
   echo("<p>(3.2.1) Lakes ArrayList memory consumption: "
        . /* sizeof(lakes) */ "?" . " bytes</p>\n"
         );
   echo("<p>(3.2.2) Number of lakes: " . count($lakes) . "</p>\n");

   // (3.3) Iterate over and output all elements
   foreach( $lakes as $s)
   {
      echo("<p> - " . $s . "</p>\n");
   }


   echo("</pre>\n");
   echo("<p>Довиждане [Dovizhdane].</p>\n");
?>
