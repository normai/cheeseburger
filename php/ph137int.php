﻿<?php
/**
 *  file       : id 20221012°1031 — gitlab.com/normai/cheeseburger … php/ph137int.php
 *  version    : • 20221018°1131 v0.1.8 Filling • 20221012°1031 v0.1.7 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate Integer Type
 *  summary    :
 *  userstory  :
 *  ref        :
 *  status     :
 */
   $sVERSION = "v0.1.8";

   echo("<p>*** 你好，这是 [Nǐ hǎo, zhè shì] 'ph137int.php' $sVERSION — Integer types ***</p>\n");
   echo("<pre>\n");


   $i1 = 123;
   $i2 = -2147483648;
   $i3 = 2147483647;
   $i4 = $i3 + 1;
   $i5 = -9223372036854775808;
   $i6 = 9223372036854775807;
   $i7 = $i6 + 1;

   echo("(1) i1     = " . str_pad($i1, 24) . " " . gettype($i1) . "\n");
   echo("(2) i2     = " . str_pad($i2, 24) . " " . gettype($i2) . "\n");
   echo("(3) i3     = " . str_pad($i3, 24) . " " . gettype($i3) . "\n");
   echo("(4) i3 + 1 = " . str_pad($i4, 24) . " " . gettype($i4) . "\n");
   echo("(5) i5     = " . str_pad($i5, 24) . " " . gettype($i5) . "\n");
   echo("(6) i6     = " . str_pad($i6, 24) . " " . gettype($i6) . "\n");
   echo("(7) i6 + 1 = " . str_pad($i7, 24) . " " . gettype($i7) . "\n");


   echo("</pre>\n");
   echo("<p>再见 [Zàijiàn].</p>\n");
?>
