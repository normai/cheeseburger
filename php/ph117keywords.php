﻿<!-- EMPTY STUB, YET TO BE FILLED WITH CODE -->

<?php
/**
 *  file       : id 20221217°0931 — gitlab.com/normai/cheeseburger … php/ph117keywords.php
 *  version    : • 20221227°1215 v0.1.9 Filling • 20221217°0931 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate keywords, using yield as example
 *  status     :
 */
   $sVERSION = "v0.1.9";

   echo("<p>*** Ko tenei 'ph117keywords.php' $sVERSION — Keywords, e.g. yield ***</p>\n");
   echo("<pre>\n");


   // Function after https://www.php.net/manual/en/language.generators.syntax.php [ref 20221227°0954] [func 20221227°1211]
   function gen_one_to_three() {
      for ($i = 1; $i <= 3; $i++) {
         yield $i;                                     // Note that $i is preserved between yields
      }
   }

   $generator = gen_one_to_three();
   echo "Values retrieved via yield:\n";
   foreach ($generator as $value) {
      echo " - $value\n";
   }


   echo("</pre>\n");
   echo("<p>Tēnā koe.</p>\n");
?>
