﻿<?php
   /**
    *  file    : id 20220826°1531 — gitlab.com/normai/cheeseburger … php/ph230while.php
    *  version : • 20220910°1031 v0.1.7 Filling • 20220826°1531 v0.1.6 Stub
    *  license : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
    *  subject : Demonstrate PHP while loop
    *  summary :
    *  ref     : About random see ph761random.php
    *  ref     :
    */
   $sVERSION = "v0.1.7";

   echo("<p>*** हैलो, यह है  [Hailo, yah hai] 'ph230while.php' $sVERSION — While loop ***</p>\n");
   echo("<pre>\n");
   echo("<pre>");

   // (1) Roll dice first time
   echo("Roll the number six!\n");
   $iDicenum = rand(1, 6);
   $iTimes = 1;
   echo("Your first number is " . $iDicenum . "\n");

   // (2) Continue as long no six is rolled
   while ($iDicenum != 6)
   {
      $iDicenum = rand(1, 6);
      $iTimes += 1;
      echo(" - then " . $iDicenum . "\n");
   }

   echo("You rolled " . $iTimes . " times until number six.\n");


   echo("</pre>");
   echo("<p>अलविदा [Alavida].</p>\n");
?>
