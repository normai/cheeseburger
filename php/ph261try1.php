﻿<?php
   /**
    *  file    : id 20220826°1931 — gitlab.com/normai/cheeseburger … php/ph261try1.php
    *  version : • 20220910°1531 v0.1.7 Filling • 20220826°1931 v0.1.6 Stub
    *  license : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
    *  subject : Demonstrate PHP Exception Handling
    *  summary :
    *  ref     : https://www.w3schools.com/php/php_exception.asp [ref 20220910°1424]
    *  ref     : https://stackoverflow.com/questions/53348102/how-to-catch-php-fatal-error-uncaught-typeerror [ref 20220910°1425]
    *  ref     : https://www.w3schools.com/php/func_math_sqrt.asp [ref 20220910°1426]
    *  status  : Not settled yet.
    */
   $sVERSION = "v0.1.7";

   echo("<p>*** こんにちは、です [Kon'nichiwa, desu] 'ph261try1.php' $sVERSION — Exception Handling ***</p>\n");
   echo("<pre>\n");


   /*
   issue 20220910°1431
   symptom : Exception "Fatal error, Uncaught TypeError" does break the script.
   finding : On the suggestion of ref 20220910°1425, I outsourced the questionable
      line to a dedicated function 'concatenate', where parameter type hints
      shall prevent implicit conversions. They do prevent, but as well throw
      the exception in a way so the script does not continue.
   wanted : This exception shall not break the script, but propagate
      to the catch block, so the script continues running.
   status : Open
   */

   function concatenate(string $s, int $i)
   {
      return $s . " " . $i;
   }

   // (1) Try adding mismatching types -- NOT AN EXCEPTION
   echo("(1) Add 'Eins' and 2 — Demo not sensible (yet)\n");
/*
   try
   {
       $two = 3;
       ////$sum = 'Eins ' + $two;                                       // Exception "Fatal error, Uncaught TypeError"
       ///$sum = 'Eins ' . $two;                                           // Not an exception, implicit conversion int to string
       ///$sum = concatenate("Eins", $two);
       $sum = concatenate('Eins', 'Zwei');
       echo("    => " . $sum . "\n");
   }
   catch(Exception $ex)
   {
       echo("    => Operation failed: " . $ex->getMessage() . "\n");
   }
*/

   // (2) Calculate square root, sometimes succeed, sometimes fail
   $iRand = rand(-9, 9);                                 // Shall be from -9 to + 9
   echo("\n(2) Calculate sqare root of " . $iRand . "\n");
   try
   {
       if ($iRand < 0)
       {
          throw new Exception("Square root of negative number is NaN.");  // Workaround. Todo: Craft another example [todo 20220910°1421]
       }
       $fRoot = sqrt($iRand);
       echo("    => Sqare root of " . $iRand . " is " . $fRoot . "\n");
   }
   catch (Exception $ex)
   {
       echo("    => Calculation for " . $iRand . " failed. Exception \"" . $ex->getMessage() . "\"\n");
   }


   echo("</pre>\n");
   echo("<p>さようなら [Sayōnara].</p>\n");
?>
