﻿<!--
   This was formerly ph211ifs1a.php to demonstrate `if`.
   Now it is to demonstrate a simple GUI with user input field.
   Possibly throw out the dice functionality, whatever, just make sure
   to keep the user input facility and the butten, created with plain PHP.
-->

<?php
   /**
    *  file    : id 20220831°0931 — gitlab.com/normai/cheeseburger … php/ph813input1.php
    *  version : • 20221216°1611 v0.x.x Filling • 20220831°0931 Stub
    *  license : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
    *  subject : Demonstrate
    */
   $sVERSION = "v0.1.8";

   echo("<p>*** Witam, to jest 'ph813input1.php' $sVERSION — GUI with user input 1 ***</p>\n");
   echo("<pre>\n");


   // Preparation One — Get a random number rolled
   $bAllowManualOverwrite = True;                                       // Toggle this
   $bDbg = False;
   $randi = rand(1, 6);                                                 // From 1 though 6
   $iRandToShow = $randi;

   // (A.1) Preparation Two — Find out exact form status. This is not soo trivial.
   if ($bAllowManualOverwrite) {
      if ($bDbg) { echo("(A.1.1) Debug: " . str_replace(array("\r", "\n"), '', var_export($_GET, true)) . "\n"); }  // Alternative to var_dump()
      if (! isset($_GET['DiceOld'])) {
         if ($bDbg) { echo("(A.1.2) Status 1 = Fresh random\n"); }
      }
      else if ($_GET['DiceNew'] == "") {
         if ($bDbg) { echo("(A.1.3) Status 2 = Manual without number\n"); }
      }
      else {
         if ($bDbg) { echo("(A.1.3) Status 3 = Manual with number\n"); }
         $iRandToShow = $_GET['DiceOld'];
         $randi = $_GET['DiceNew'];
      }
      $_GET['DiceNew'] = "";
   }

   echo("(A.1.4) You rolled number <span style='font-size:1.7em; font-weight:bold;'>" . $iRandToShow. "</span>\n");

   // (A.2) Optionally allow to manually adjust the rolled number
   /* ------------------------------------------------- */
   if ($bAllowManualOverwrite) {
      if ($bDbg) { echo("(A.2.1) Debug: " .    str_replace(array("\r", "\n"), '', var_export($_GET, true)) . "\n"); }
      echo('Possibly overwrite the rolled number:</pre>
      <form action="ph211ifs1.php.html" method="get">
         <input type="hidden" name="DiceOld" value="' . $iRandToShow . '">
         <input type="text" name="DiceNew" size="5" value="' . $_GET['DiceNew'] . '">
         <input type="submit" value="Use this number">
         <input type="button" value="Reset URL" onclick="window.location=\'ph211ifs1.php.html\';">
      </form>
      ' . "\n<pre>");
      echo("Using number <span style='font-size:1.7em; font-weight:bold;'>" . $randi . "</span>\n");  // Debug output
   }
   /* ------------------------------------------------- */

   // (B.1) Unilateral branching
   echo("(B.1) Unilateral branching\n");
   if ($randi > 3) {
      echo("(B.1.1) : Your number is bigger than three\n");
   }

   // (B.2) Two-way branching
   echo("(B.2) Bilateral branching\n");
   if ($randi == 3) {
      echo("(B.2.1) : The number is three\n");
   }
   else {
      echo("(B.2.2) : The number is not three\n");
   }

   // (B.3) Multilateral branching — Only one branch will be executed
   echo("(B.3) Multilateral branching\n");
   if ($randi > 5) {
      echo("(B.3.1) : The number is six\n");
   }
   else if ($randi > 4) {
      echo("(B.3.2) : The number is five\n");
   }
   else if ($randi > 3) {
      echo("(B.3.3) : The number is four\n");
   }
   else if ($randi > 2) {
      echo("(B.3.4) : The number is three\n");
   }
   else {
      echo("(B.3.5) : The number is one or two\n");
   }

   // (B.4) A typical beginners mistake, looks very similar like above,
   //  has multiple if instead elif statements, will not work as intended.
   echo("(B.4) Wrong logic\n");
   if ($randi > 5) {
      echo("(B.4.1) : The number is six\n");
   }
   if ($randi > 4) {
      echo("(B.4.2) : The number is five or six\n");
   }
   if ($randi > 3) {
      echo("(B.4.3) : The number is four to six\n");
   }
   if ($randi > 2) {
      echo("(B.4.4) : The number is three to six\n");
   }
   else {
      echo("(B.4.5) : The number is one or two\n");
   }


   echo("</pre>\n");
   echo("<p>Do widzenia.</p>\n");
?>
