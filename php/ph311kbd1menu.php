﻿<?php
/**
 *  file       : id 20221116°1431 — gitlab.com/normai/cheeseburger … php/ph311kbd1menu.php
 *  version    : •  20221117°0916 v0.1.8 Filling • 20221116°1431 v0.1.7 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate keyboard menu with pressing Enter
 *  userstory  :
 *  summary    :
 *  status     : Good enough for the beginning.
 *  ref        : https://www.w3schools.com/php/php_forms.asp [ref 20221116°1912]
 *  ref        : file 20210816°0937 affenformular.php
 *  ref        : https://tutorial.eyehunts.com/html/html-input-text-box-field-value-size-width-multiline-example/ [ref 20221116°1922]
 *  ref        : https://www.freecodecamp.org/news/text-box-in-html-the-input-field-html-tag/ [ref 20221116°1932]
 *  ref        : 👍 https://stackoverflow.com/questions/28046360/send-value-in-textarea [ref 20221116°1934]
 *  ref        :
 */
$sVERSION = "v0.1.8";

$sWelcome = "*** Hei, tämä on 'ph311kbd1menu.php' $sVERSION — Keyboard menu (with Enter) ***\n";
$sMenuLine = "Menu: a = Anton, k = Kylie, s = Santa, x = Exit\n";

$aktion = date("H:i:s");                                               // Not (yet) used

// Avoid unset fields, assign them an empty string value
$_GET['continue'] = isset($_GET['continue']) ? $_GET['continue'] : TRUE;
$_GET['outpuz'] = isset($_GET['outpuz']) ? $_GET['outpuz'] : "";
$_GET['input'] = isset($_GET['input']) ? $_GET['input'] : "";

// On initial request
if (empty($_GET['outpuz']) == TRUE || $_GET['continue'] == FALSE) {
   $_GET['continue'] = TRUE;
   $_GET['outpuz'] = $sWelcome . $sMenuLine;
}
// On subsequent requests
else {
   $sKey = $_GET['input'];
   if ($sKey == "a") {
      $_GET['outpuz'] .= "This is Anton speaking\n";
   }
   else if ($sKey == "k") {
      $_GET['outpuz'] .= "This is Kylie singing\n";
   }
   else if ($sKey == "s") {
      $_GET['outpuz'] .= "This is Santa Claus ringing\n";
   }
   else if ($sKey == "x") {
      $_GET['continue'] = FALSE;
      $_GET['outpuz'] .= "Näkemiin.\n";
   }
   else {
      $_GET['outpuz'] .= 'Invalid selection: "' . $sKey . "\"\n";
   }
   $_GET['input'] = "";
}

// Render the HTML
echo('
   <form action="ph311kbd1menu.php" method="get">

   <p style="vertical-align: top;">Output:
   <textarea name="outpuz" cols="81" rows="12">' . $_GET['outpuz'] . '</textarea>
   </p>
   
   <p>
   ' . ($_GET['continue'] ? ('Input: &nbsp;&nbsp; <input type="text" name="input" value="' . $_GET['input'] . '" autofocus>') : "") . '
   <input type="hidden" size="3" name="continue" id="continue" value="' . $_GET['continue'] . '">
   <input type="hidden" size="3" name="aktion" id="aktion" value="' . $aktion . '">
   <input type="submit" value="Enter" ' . ($_GET['continue'] ? "" : " autofocus") . '>
   </p>

   </form>
');
