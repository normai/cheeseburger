﻿<?php
   /**
    *  file    : id 20220828°1731 — gitlab.com/normai/cheeseburger … php/ph161list.php
    *  version : • 20220908°1241 v0.1.7 Filling • 20220828°1731 v0.1.6 Stub
    *  license : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
    *  subject : Demonstrate 'list' in PHP
    *  note    : PHP has no on-board container classes
    *  ref     : https://www.w3schools.com/php/php_arrays.asp []
    *  ref     : Suggestions how to create an own List class, you may find in StackOverflow thread
    *             https://stackoverflow.com/questions/42200769/how-to-create-list-of-objects-in-php []
    *  ref     : https://code-boxx.com/convert-array-string-php/ []
    */
   $sVERSION = "v0.1.7";

   echo("<p>*** 你好 (Nǐ hǎo) 'ph161list.php' $sVERSION — Lists ***</p>\n");
   echo("<pre>\n");


   // (1) Simple 'list'
   // (1.1) Create
   $islands = array ( "Rügen"                                        // Array
                     , "Usedom"
                      , "Fehmarn"
                       );
   array_push($islands, "Sylt");

   // (1.2) Print some infos
   echo("<p>(1.2.1) Type = " . gettype($islands) . "</p>\n");
   echo("<p>(1.2.2) Length = " . count($islands) . "</p>\n");
   echo("<p>(1.2.3) Value = " . implode(" ", $islands) . "</p>\n");

   // (2.3) Iterate
   for ($i = 0; $i < count($islands); $i++)
   {
       echo("<p>&nbsp;&nbsp;&nbsp;   - " . $islands[$i] . " (type =" . gettype($islands[$i]) . ")</p>\n");
   }

   // (1) Complex 'list'
   // (1.1) Create
   $mixture = array("Pink", true, 123, 2.34, array("Aha", "Oha", "Uhu"));

   // (1.2) Print some infos
   echo("<p>(2.2.1) Type = " . gettype($mixture) . "</p>\n");
   echo("<p>(2.2.2) Length = " . count($mixture) . "</p>\n");
   //echo("<p>(2.2.3) Value = " . implode($mixture) . "</p>\n");      // Not so easy since one element is an array itself // todo: Solve issue [todo 20220902°1151]

   // (2.3) Iterate
   foreach ($mixture as $value)
   {
       $value = gettype($value) === "array" ? implode(" ", $value) : $value;
       echo("<p>&nbsp;&nbsp;&nbsp;   - " . $value . " ( type = " . gettype($value) . ")</p>\n");
   };


   echo("</pre>\n");
   echo("<p>再见 [Zàijiàn].</p>\n");
?>
