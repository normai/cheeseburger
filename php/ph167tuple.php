﻿<?php
   /**
    *  file    : id 20220829°0931 — gitlab.com/normai/cheeseburger … php/ph167tuple.php
    *  version : • 20220908°1501 v0.1.7 Filling • 20220829°0931 v0.1.6 Stub
    *  license : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
    *  subject : Demonstrate
    *  summary : Facts
    *             • There are no Tuples in PHP
    *             • Some pages show classes to provide special tuple features
    *             • Usually you just use an array instead a dedicated tuple
    *             • Unpacking is also provided (not shown yet)
    *             • 
    *  ref     : https://stackoverflow.com/questions/22083760/are-there-tuples-in-php [ref 20220905°1911]
    *  ref     : https://eddmann.com/posts/tuples-in-php/ [ref 20220905°1912]
    *  ref     : https://coderwall.com/p/bah4oq/tuples-in-php [ref 20220905°1916]
    *  ref     : https://sasablagojevic.com/php-tuples [ref 20220905°1922] -- This one proposes dedicated Tuple classes, implementing ArrayAccess, Countable .. trying to achieve 'multiple return values' and 'explicit and strict code'
    *  ref     : https://www.php.net/manual/en/class.arrayaccess.php [ref 20220905°1926]
    *  ref     : https://alanstorm.com/php_array_access/ [ref 20220905°1947]
    *  status  : This tries to implement the Tuple class from ref 20220905°1922.
    *            Just that article seems not complete, e.g. there are no examples
    *            on how to use the three classes. And in class Tuple it looks like
    *            the constructor is missing. So the points:
    *             • I succeeded in making a class implementing ArrayAccess working
    *             • It is not clear, what I missed by ignoring the other two classes
    *             • It is not sure, what is the advantage as opposed to a plain const array
    *             • The advantage is, that the array/tuple signature is hard coded in the class
    *             •
    */
   $sVERSION = "v0.1.7";


    // () [class 20220905°1931] Code after ref 20220905°1922
    // Note. The original code had no constructor. And that makes no sense
    //  for me. Especially because in class ContentSlice`s constructor exists
    //  this line: "parent::__construct($items, $count);".
    //  So I gave up so far to get classes ContentSlice and ContentRepository
    //  running and suffice with having class Tuple working.
    ////class Tuple implements ArrayAccess, Countable
    class TupleDing5 implements ArrayAccess, Countable
    {
        ////private $values;                                           // Original line
        private $values = array();                                     // Fix after ref 20220905°1926

        // () Experimentally added
        public function __construct(string $s1, int $i1, float $d1, bool $b1, string $s2 )
        {
            $this->values[] = $s1;
            $this->values[] = $i1;
            $this->values[] = $d1;
            $this->values[] = $b1;
            $this->values[] = $s2;

            //var_dump($this->values);
        }

        // Array Access
        #[\ReturnTypeWillChange]                                       // Todo: refacture so the #[\ReturnTypeWillChange] is no more necessary [todo 20220905°1937]
        public final function offsetExists($offset)
        {
            return isset($this->values[$offset]);
        }

        #[\ReturnTypeWillChange]                                       // Todo: refacture so the #[\ReturnTypeWillChange] is no more necessary [todo 20220905°1937`02]
        public final function offsetGet($offset)
        {
            return isset($this->values[$offset]) ? $this->values[$offset] : null;
        }

        #[\ReturnTypeWillChange]                                       // Todo: refacture so the #[\ReturnTypeWillChange] is no more necessary [todo 20220905°1937`03]
        public final function offsetSet($offset, $value) {}

        #[\ReturnTypeWillChange]                                       // Todo: refacture so the #[\ReturnTypeWillChange] is no more necessary [todo 20220905°1937`04]
        public final function offsetUnset($offset) {}

        // Countable
        public function count() : int
        {
            return count($this->values);
        }
    }

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Tow more classes, unused so far.
    // Todo. To continue exerimenting, the constructor of TupleDing5 must be
    // reset. Or better provide the original code class Tuple. [todo 20220905°1948]

    // () [class 20220905°1933] Code after ref 20220905°1922
    class ContentSlice extends TupleDing5 // Tuple
    {
        /**
         *  ContentSlice constructor.
         *  @param array<int, Content> $items
         *  @param int $count
         */
        public function __construct(array $items, int $count)
        {
            parent::__construct($items, $count);
        }
    }

    // () [class 20220905°1935] Code after ref 20220905°1922
    class ContentRepository 
    {
        public function getSlice(int $page, int $perPage, array $filters = [], array $sorts = []) : TupleDing5 // Tuple
        {
            // Something, something
            ////$items = [Content(), Content(), Content()];        // Original line
            $items = ["Oans", "Zwo", "Drei"];                      // Experiment
            return new ContentSlice($items, 15);
        }
    }
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


    echo("<p>*** Hallo, dit is 'ph167tuple.php' $sVERSION — Tuples ***</p>\n");
    echo("<pre>\n");


    // (1) Create
    $tupl = new TupleDing5("Sparrow", 1234, 2.345, True, "Ding");

    echo("<p>(1) Tuple = </p>\n&nbsp;&nbsp;&nbsp;");
    print_r($tupl);

    // (2) Info
    echo("<p>(2.1) Type = " . gettype($tupl) . "</p>");
    echo("<p>(2.2) Size = " . $tupl->count() . "</p>");

    // (3) Iterate
    echo("<p>(3) Iterate:</p>");
    for ($i = 0; $i < $tupl->count(); $i++)
    {
        echo("<p>&nbsp;&nbsp;&nbsp;- " . $tupl[$i] . "</p>\n");
    }

    // (4) Pick by index
    echo("<p>(4.1) Third item = " . $tupl[2] . "</p>");        // strval() not necessary
    echo("<p>(4.2) (First of) fifth = " . $tupl->offsetGet(4) . "</p>");


    echo("</pre>\n");
    echo("<p>Tot ziens.</p>\n");
?>
