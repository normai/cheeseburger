﻿<?php
/**
 *  file       : id 20221117°1031 — gitlab.com/normai/cheeseburger … php/ph139bigint.php
 *  version    : • 20221216°0916 v0.1.8 Filling • 20221117°1031 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate big integer types
 *  note       : This is using the PHP built-in BC Math Functions
 *  status     :
 */
   $sVERSION = "v0.1.8";

   echo("<p>*** Hello, ez 'ph139bigint.php' $sVERSION — Big integers ***</p>\n");
   echo("<pre>\n");


   $sBig = "2";
   for ($i = 2; $i < 11; $i++) {
      $sBig = bcpow($sBig, "2");
      echo(' - ' . $i . ' ' . $sBig . "\n");
   }


   echo("</pre>\n");
   echo("<p>Viszlát.</p>\n");
?>
