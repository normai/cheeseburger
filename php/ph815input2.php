﻿<!--
   This was originally ph211ifs1b.php to demonstrate `if`.
   Now it is to demonstrate a simple GUI with user input field.
   Possibly throw out the dice functionality, whatever, just make sure
   to keep the user input facility and the butten, controlled with JavaScript.
-->

<?php
   // Todo. This file can be eliminated, after the JavaScript form button
   //  handling is transferred to some dedicated demo [todo 20220922°1931]

   /**
    *  file    : id 20221216°1531 [formerly 20220826°1233] — gitlab.com/normai/cheeseburger … php/ph815input2.php
    *  version : • 20220922°1952 v0.1.8 Filling, using JavaScript for the send button
    *             • 20220826°1233 v0.1.6 Stub
    *  license : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
    *  subject : Demonstrate branching with If/ElseIf/Else
    *  summary : This demonstrates If/ElseIf/Else with four cases:
    *             • Unilateral branching
    *             • Bilateral branching
    *             • Multilateral branching
    *             • Wrong branching logic
    *            The sequence goes like this (1) Create random integer (2) Let the user
    *            overwrite this number (3) Do four different evaluations on that number
    *  usage   : (1) Open accompanying HTML file in browser from a PHP capable
    *             server or (2) run from commandline "php.exe ph815input2.php"
    *  status  : Almost as wanted. It hangs with F5 after manual overwrite (see todo 20220922°1941)
    */
   $sVERSION = "v0.1.8";

   echo("<p>*** Γεια σας, αυτό είναι το <cite>[Geia sas, aftó eínai to]</cite> 'ph815input2.php' $sVERSION — If/ElseIf/Else ***</p>\n");
   echo("<pre>\n");


   // (A.1) Preparation One — Get a random number rolled
   $bAllowManualOverwrite = True;                                       // Toggle this
   $bDbg = False;
   $randi = rand(1, 6);                                                 // From 1 though 6
   $iRandToShow = $randi;

   // (A.2) Preparation Two — Find out exact form status. This is not soo trivial.
   if ($bAllowManualOverwrite) {
      if ($bDbg) { echo("(A.2.1) Debug: " . str_replace(array("\r", "\n"), '', var_export($_GET, true)) . "\n"); }
      if (! isset($_GET['DiceOld'])) {
         if ($bDbg) { echo("(A.2.2) Status 1 = Fresh random\n"); }
      }
      else if ($_GET['DiceNew'] == "") {
         if ($bDbg) { echo("(A.2.3) Status 2 = Manual without number\n"); }
      }
      else {
         if ($bDbg) { echo("(A.2.4) Status 3 = Manual with number\n"); }
         $iRandToShow = $_GET['DiceOld'];
         $randi = $_GET['DiceNew'];
      }
      $_GET['DiceNew'] = "";
   }
   echo("(A.2.5) You rolled number <span style='font-size:1.7em; font-weight:bold;'>" . $iRandToShow. "</span>\n");

   // (A.3) Facility to allow to manually adjust the rolled number
   /* ------------------------------------------------- */
   if ($bAllowManualOverwrite) {
      if ($bDbg) { echo("(A.3.1) Debug: " . str_replace(array("\r", "\n"), '', var_export($_GET, true)) . "\n"); }
      echo('Possibly overwrite the rolled number:</pre>
      <form action="ph815input2.php.html" method="get" name="RollForm">
         <input type="hidden" name="DiceOld" value="' . $iRandToShow . '">
         <input type="text" name="DiceNew" size="5" value="' . (isset($_GET['DiceNew']) ? $_GET['DiceNew'] : "") . '">
         <!-- input type="submit" value="Send Number" --><!-- It was a too complicated logic, doing it with the pure form controls, so I retreated to JavaScript -->
         <input type="button" value="Use this number" onclick="window.location=\'ph815input2.php.html?\'
             + \'DiceNew=\' + document.forms[\'RollForm\'][\'DiceNew\'].value
             + \'&DiceOld=\' + document.forms[\'RollForm\'][\'DiceOld\'].value">
         <input type="button" value="Reset URL" onclick="window.location=\'ph815input2.php.html\';">
      </form>
      ' . "\n<pre>");
      echo("Using number <span style='font-size:1.7em; font-weight:bold;'>" . $randi . "</span>\n");
   }
   /* ------------------------------------------------- */

   // (B.1) Unilateral branching
   echo("(1) Unilateral branching\n");
   if ($randi > 3) {
      echo("(1.1) : Your number is bigger than three\n");
   }

   // (B.2) Two-way branching
   echo("(2) Bilateral branching\n");
   if ($randi == 3) {
      echo("(2.1) : The number is three\n");
   }
   else {
      echo("(2.2) : The number is not three\n");
   }

   // (B.3) Multilateral branching — Only one branch will be executed
   echo("(3) Multilateral branching\n");
   if ($randi > 5) {
      echo("(3.1) : The number is six\n");
   }
   else if ($randi > 4) {
      echo("(3.2) : The number is five\n");
   }
   else if ($randi > 3) {
      echo("(3.3) : The number is four\n");
   }
   else if ($randi > 2) {
      echo("(3.4) : The number is three\n");
   }
   else {
      echo("(3.5) : The number is one or two\n");
   }

   // (B.4) A typical beginners mistake, looks very similar like above,
   //  has multiple if instead elif statements, will not work as intended.
   echo("(4) Wrong logic\n");
   if ($randi > 5) {
      echo("(4.1) : The number is six\n");
   }
   if ($randi > 4) {
      echo("(4.2) : The number is five or six\n");
   }
   if ($randi > 3) {
      echo("(4.3) : The number is four to six\n");
   }
   if ($randi > 2) {
      echo("(4.4) : The number is three to six\n");
   }
   else {
      echo("(4.5) : The number is one or two\n");
   }


   echo("</pre>\n");
   echo("<p>Αντίο σας <cite>[Antio sas]</cite>.</p>\n");
?>
