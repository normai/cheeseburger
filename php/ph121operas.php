﻿<?php
   /**
    *  file    : id 20220825°1531 — gitlab.com/normai/cheeseburger …/php/ph121operas.php
    *  version : • 20220908°1601 v0.1.7 Filling • 20220825°1531 v0.1.6 Stub
    *  license : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
    *  subject : Demonstrate some PHP operators
    *  ref     : https://riptutorial.com/php/example/22788/string-interpolation [ref 20220825°1446]
    *  ref     : https://www.php.net/manual/en/language.types.string.php [ref 20220909°0922] The four PHP string styles
    */
   $sVERSION = "v0.1.7";

   echo(<<<'ENDMARK'
   <!DOCTYPE html>
   <html lang="en">
    <head>
     <meta charset="utf-8">
     <title>ph121operas.php</title>
     <meta name="viewport" content="width=device-width">
     <style>
        body { font-family: Monospace, Monospace; }
        p { margin:0 0 0 0; }
     </style>
    </head>
    <body>
   ENDMARK . PHP_EOL);

   echo("<p>*** Hello, this is 'ph121operas.php' $sVERSION v0.1.7 — Some operators ***</p>\n");
   echo("<pre>\n");

   // Assignment operators
   $v = $w = $x = $y = $z = 5;
   $w += 2;
   $x -= 2;
   $y *= 2;
   $z /= 2;
   echo("(1.1) 'v = w = x = y = z = 5'             : " . strval($v) . "\n");
   echo("(1.2) 'w += 2' identical with 'w = w + 2' : " . strval($w) . PHP_EOL);
   echo("(1.3) 'x -= 2' identical with 'x = x - 2' : " . strval($x) . PHP_EOL);
   echo("(1.4) 'y *= 2' identical with 'y = y * 2' : " . strval($y) . PHP_EOL);
   echo("(1.5) 'z /= 2' identical with 'z = z / 2' : " . strval($z) . PHP_EOL);
   echo(PHP_EOL);

   // Some remarkable operators
   $x1 = 100 / 30;                                              // Division
   $x2 = intdiv(100, 30);                                       // Integer division
   $x3 = 100 % 30;                                              // Modulo or remainder
   $x4 = pow(16, 3);                                            // Power
   $x5 = 1 > 2 ? 1234 : 5678;                                   // Ternary operator
   echo("(2.1) 100 / 30            = " . strval($x1) . " " . gettype($x1) . PHP_EOL);
   echo("(2.2) 100 / 30            = " . strval($x2) . " " . gettype($x2) . PHP_EOL);
   echo("(2.3) 100 % 30            = " . strval($x3) . " " . gettype($x3) . PHP_EOL);
   echo("(2.4) 16 ** 3             = " . strval($x4) . " " . gettype($x4) . PHP_EOL);
   echo("(2.4) 1 > 2 ? 1234 : 5678 = " . strval($x5) . " " . gettype($x5) . PHP_EOL);
   echo(PHP_EOL);

   // The format function introduces the member operator '.'
   $n1 = 1;
   $n2 = 2.3;
   $n3 = 3.456789;
   $s = "(3) Take {$n1} or {$n2} or {$n3}.";                    // String interpolation, also available without curly braces
   echo("" . $s . PHP_EOL);

   echo("</pre>" . PHP_EOL);
   echo("<p>Good bye.</p>" . PHP_EOL);


   $s = <<<'ENDMARK'
    </body>
   </html>
   ENDMARK;
   echo($s);
?>
