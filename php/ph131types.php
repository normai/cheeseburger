﻿<?php
   /**
    *  file    : id 20220825°1831 — gitlab.com/normai/cheeseburger … php/ph131types.php
    *  version : • 20220908°1041 v0.1.7 Filling • 20220825°1831 v0.1.6 Stub
    *  license : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
    *  summary : Demonstrate some types
    *  ref     : https://www.php.net/manual/en/language.types.integer.php [ref 20220825°1932]
    *  ref     : https://www.php.net/manual/en/language.types.float.php [ref 20220825°1933]
    *  ref     : https://www.php.net/manual/en/language.types.string.php []
    */
   $sVERSION = "v0.1.7";

   echo("<p>*** अभिनंदन (abhinandan) 'ph131types.php' $sVERSION — Types ***</p>\n");
   echo("<pre>\n");


   // Some declarations
   $iByte1 = 127;                                      // byte
   $iShort1 = 32767;                                   // short
   $iInt1 = 2147483647;                                // int
   $iLong1 = PHP_INT_MAX;                              // long e.g. Int64.MaxValue
   $f1 = 1.23;                                         // float ─ The 'f' postfix prevents error 'lossy conversion'
   $d1 = float_max();                                  // double ─ double.MaxValue;
   $b1 = True;                                         // bool
   $c1 = 'x';                                          // char
   $s1 = "Holla";                                      // string

   // Some operations
   $iByte2 = $iByte1 + 1;                              //
   $iShort2 = $iShort1 + 1;                            //
   $iInt2 = $iInt1 + 1;
   $iLong2 = $iLong1 + 1;
   $f2 = $f1 + 1;
   $d2 = $d1 + 1;
   $b2 = ! $b1;                                        // The plus operator is not possible with boolean?
   $c2 = $c1 . chr(1);                                 //
   $s2 = $s1 . chr(1);                                 //

   // Display the results
   echo("<p>(1) " . $iByte1 . " + 1 = " . $iByte2 . " (PHP_INT_MAX = " . PHP_INT_MAX . ")</p>\n");
   echo("<p>(2) " . $iShort1 . " + 1 = " . $iShort2 . " (PHP_INT_MAX = " . PHP_INT_MAX . ")</p>\n");
   echo("<p>(3) " . $iInt1 . " + 1 = " . $iInt2 . "</p>\n");
   echo("<p>(4) " . $iLong1 . " + 1 = " . $iLong2 . "</p>\n");
   echo("<p>(5) " . $f1 . " + 1 = " . $f2 . "</p>\n");
   echo("<p>(6) " . $d1 . " + 1 = " . $d2 . " (float_max() = " . float_max() . ")</p>\n");
   echo("<p>(7) !" . $b1 . " = \"" . $b2 . "\"</p>\n");  // Todo : Possibly supplement variations on this [todo 20220825°1941]
   echo("<p>(8) " . $s1 . " + 1 = " . $s2 . "</p>\n");


   echo("</pre>\n");
   echo("<p>अलविदा (alavida).</p>\n");

   // function 20220825°1921
   // Source from PHP manual, contribution by magicaltux on 2010-06-01`07:02
   //    https://www.php.net/manual/en/language.types.float.php#98216 [ref 20220825°193312]
   // See also PHP manual, contribution by Bob on 2009-05-07`08:34
   //    https://www.php.net/manual/en/language.types.float.php#90753 [ref 20220825°193313]
   function float_max($mul = 2, $affine = 1) {
      $max = 1; $omax = 0;
      while((string)$max != 'INF') { $omax = $max; $max *= $mul; }

      for($i = 0; $i < $affine; $i++) {
         $pmax = 1; $max = $omax;
         while((string)$max != 'INF') {
            $omax = $max;
            $max += $pmax;
            $pmax *= $mul;
         }
      }
      return $omax;
   }
?>
