﻿<?php
/**
 *  file       : id 20221012°1631 — gitlab.com/normai/cheeseburger … php/ph153stripol.php
 *  version    : • 20221013°1031 v0.1.8 Filling • 20221012°1631 v0.1.7 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate String Interpolation
 *  userstory  :
 *  summary    :
 *  ref        : https://riptutorial.com/php/example/22788/string-interpolation [ref 20221011°0915]
 */
   $sVERSION = "v0.1.8";

   echo("<p>*** Hej, det er 'ph153stripol.php' $sVERSION — String interpolation ***</p>\n");
   echo("<pre>\n");


   // () Provide fodder
   $iX = 234;
   $sHello = "Hello";
   $sJenny = "Jenny";

   // (x) String interpolation (more details see ref 20221011°0915)
   // (x.1)
   echo('(1) Concatenation : ' . $sHello . ' ' . $iX . ' ' . $sJenny . ".\n");

   // (x.2)
   echo("(2) Interpolation : $sHello $iX $sJenny.\n");
 
   // (x.3)
   echo("(3) Curly braces  : {$sHello} {$iX} {$sJenny}.\n");


   echo("</pre>\n");
   echo("<p>Farvel.</p>\n");
?>
