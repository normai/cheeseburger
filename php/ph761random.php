﻿<!-- THIS FILE IS AN EMPTY STUB, IT IS YET TO BE FILLED WITH CODE -->

<?php
   /**
    *  file    : id 20220910°1331 — gitlab.com/normai/cheeseburger … php/ph761random.php
    *  version : • 20xxxxxx°xxxx v0.x.x Filling • 20220910°1331 v0.1.6 Stub
    *  license : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
    *  subject : Demonstrate PHP Random Functions
    *  ref     : https://www.w3schools.com/PHP/func_math_rand.asp [ref 20220910°1142]
    *  ref     : https://www.php.net/manual/en/function.rand.php [ref 20220910°1143]
    *  ref     : https://www.php.net/manual/en/function.random-int.php [ref 20220910°1144]
    *  ref     : https://stackoverflow.com/questions/3140235/php-random-number [ref 20220910°1145]
    *  ref     : https://www.php.net/manual/en/function.random-bytes.php [ref 20220910°1146]
    *  ref     : https://www.positioniseverything.net/php-random-array [ref 20220910°1147]
    *  usage   : E.g. ph230while.php
    */
   $sVERSION = "v0.0.0";

   echo("<p>*** Привет, это [Privet, eto] 'ph761random.php' $sVERSION — Random Functions ***</p>\n");
   echo("<pre>\n");






   echo("</pre>\n");
   echo("<p>до свидания [Do svidaniya].</p>\n");
?>
