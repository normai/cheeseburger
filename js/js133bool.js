﻿/**
 *  file       : id 20221012°0825 — gitlab.com/normai/cheeseburger …/js/js133bool.js
 *  version    : • 20221014°1125 v0.1.8 Filling • v0.1.8 20221012°0825 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate Boolean Type
 *  userstory  :
 *  summary    :
 *  ref        : https://www.freecodecamp.org/news/how-to-convert-value-to-boolean-javascript/ [ref 20221014°0922] Nice compilation
 *  ref        : https://www.30secondsofcode.org/articles/s/javascript-truthy-falsy-values [ref 20221014°0942] 'JavaScript Truthy and Falsy Values Cheatsheet'
 *  ref        :
 */
let sVERSION = "v0.1.8"

document.write(`<p>*** Привіт, це [Pryvit, tse] 'js133bool.js' ${sVERSION} — Boolean Type ***</p>\n`);
document.write("<pre>\n");

// (1) The naive test series
document.write("(1) Naive test series\n");
let bNone = null;
let bFalse = false;
let bTrue = true;
let iMinus = -1;
let iZero = 0;
let iOne = 1;
let iTwo = 2;
let fMinus = -0.1;
let fZero = 0.0;
let fOne = 0.1;
let fTwo = 1.23;
document.write("(1.1)  Boolean(None) = " + Boolean(bNone) + "\n");
document.write("(1.2)  False         = " + bFalse + "\n");
document.write("(1.3)  True          = " + bTrue + "\n");
document.write("(1.4)  Boolean(-1)   = " + Boolean(iMinus) + "\n");
document.write("(1.5)  Boolean(0)    = " + Boolean(iZero) + "\n");
document.write("(1.6)  Boolean(1)    = " + Boolean(iOne) + "\n");
document.write("(1.7)  Boolean(2)    = " + Boolean(iTwo) + "\n");
document.write("(1.8)  Boolean(-0.1) = " + Boolean(fMinus) + "\n");
document.write("(1.9)  Boolean(0.0)  = " + Boolean(fZero) + "\n");
document.write("(1.10) Boolean(0.1)  = " + Boolean(fOne) + "\n");
document.write("(1.11) Boolean(1.23) = " + Boolean(fTwo) + "\n");
document.write("(1.12) Boolean('')   = " + Boolean("") + "\n");
document.write("(1.13) Boolean('x')  = " + Boolean("x") + "\n");

// (2) Targeted conversion
document.write("(2) Targeted conversion:" + "\n");
inputs = ["", "Bla", "Ja", "No", "True", "Yes"];
trues = ["1", "j", "ja", "t", "true", "y", "yes"];
inputs.forEach(x => {
   let b = trues.some((t) => {                                          // ES6
      return t === x.toLowerCase().trim();
   });
   document.write("    • " + x + " -> " + b + "\n");
});

document.write("</pre>\n");
document.write("<p>до побачення [Do pobachennya].</p>\n");
