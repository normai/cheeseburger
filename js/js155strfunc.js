﻿/**
 *  file       : id 20221012°1725 — gitlab.com/normai/cheeseburger …/js/js155strfunc.js
 *  version    : • 20221013°1125 v0.1.8 Filling • v0.1.8 20221012°1725 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate String Functions
 *  userstory  :
 *  summary    :
 */
let sVERSION = "v0.1.8"

document.write(`<p>*** Hallo, dit is 'js155strfunc.js' ${sVERSION} — String Functions ***</p>\n`);
document.write("<pre>\n");

// () Fodder
let sWord = "SilzuZankunKrei";

// (.) String Functions
// (.1) String length
document.write("(3.1) Length of '" + sWord + "' is " + sWord.length + "\n");

// (.2) Character code to string
document.write("(3.2) Code to char : " + String.fromCharCode(36) + String.fromCharCode(37, 38, 96, 97, 98, 122, 123, 124, 125) + '\u007e' + '\u007f' + ".\n");

// (.3) Substrings
document.write("(3.3) Substrings : " + sWord.substring(0, 5) + " + " + sWord.substring(5, sWord.length - 4) + " + " + sWord.substring(sWord.length - 4) + "\n");

// (.4) Upper/Lower
document.write("(3.4) Upper/Lower : " + sWord.toLowerCase() + " / " + sWord.toUpperCase() + "\n");

// (.5) Trim
let sBreezy = "    Breeze    ";
document.write("(3.5) Trim \"" + sBreezy + "\" : \"" + sBreezy.trimStart() + "\", \"" + sBreezy.trimEnd() + "\", \"" + sBreezy.trim() + "\"\n");



document.write("</pre>\n");
document.write("<p>Tot ziens.</p>\n");
