﻿/**
 *  file       : id 20220825°1325 — gitlab.com/normai/cheeseburger …/js/js113aloha.js
 *  version    : • 20220908°0821 v0.1.7 Filling • 20220825°1325 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : First variables, operators, strings
 */
let sVERSION = "v0.1.7"

document.write(`<p>*** ʻO kēia 'js113aloha.js' ${sVERSION} — Variables, operators, strings ***</p>`);
document.write("<pre>\n");


// Define some variables and use an operator
a = 123;
b = 345;
c = a + b;

// Output with a bulky statement
document.write("<p>(1) Sum of " + a + " and " + b + " is " + c + "</p>");

// Output with a summarized statement
s = "<p>(2) Sum of " + a + " and " + b + " is " + c + "</p>";
document.write(s);

// Output numbers in various notations
sBin = a.toString(2);
sHex = a.toString(16);
document.write("<p>(3) Bin : " + sBin + ", Hex : " + sHex + "</p>");

// Show difference between integers, floats and strings
document.write("<p>(4) Get variable types:" + "</p>");
i = 12345;
f = 12.345;
s = "Chain of chars";
document.write("<p>  - i = " + i + " : " + typeof i + "</p>");
document.write("<p>  - f = " + f + " : " + typeof f + "</p>");
document.write("<p>  - s = \"" + s + "\" : " + typeof s + "</p>");


document.write("</pre>\n");
document.write("<p>Aloha mai.</p>");
