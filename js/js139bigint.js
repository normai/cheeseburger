﻿/**
 *  file       : id 20221117°1025 — gitlab.com/normai/cheeseburger …/js/js139bigint.js
 *  version    : • 20221216°0914 v0.1.8 Filling • 20221117°1025 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate big integer types
 *  status     : Too simple, does not calculate precise enough!
 *  todo       : Use BigInt function
 *  ref        : developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/BigInt [ref 20221215°1036]
 */
let sVERSION = "v0.1.8"

document.write(`<p>*** Hello, ez 'js139bigint.js' ${sVERSION} — Big integers ***</p>\n`);
document.write(`<p>Todo: Use the JavaScript BigInt function, the number type is not precise enough.</p>\n`);
document.write("<pre>\n");


// (1) Prelude
let x = 0.1 + 0.2;
document.write(`(1) 0.1 + 0.2 = ${x}\n\n`);

// (2)
document.write(`(2) Numbers :\n`);
let iBig = 2;
for (let i = 2; i < 11; i++) {
   iBig = iBig ** 2;
   document.write(` - ${i} ${iBig}\n`);
}


document.write("</pre>\n");
document.write("<p>Viszlát.</p>\n");
