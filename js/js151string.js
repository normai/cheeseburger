﻿/**
 *  file       : id 20221001°1325 — gitlab.com/normai/cheeseburger … js/js151string.js
 *  version    : • 20221009°1725 v0.1.8 Filling • v0.1.8 20221001°1325 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate string syntax
 *  ref        : https://stackoverflow.com/questions/1155678/what-is-the-javascript-string-newline-character [ref 20221010°1212]
 *  ref        : https://stackoverflow.com/questions/27678052/usage-of-the-backtick-character-in-javascript [ref 20221010°1213]
 *  ref        : https://attacomsian.com/blog/javascript-convert-string-to-arra [ref 20221010°1222] Four options to convert string to array
 *  todo       : Since parseInt/parseFloat do not throw an exception, we have to test
 *                for NaN to recognize invalid string input [todo 20221010°1221 'Check for NaN']
 *  ref        : https://www.freecodecamp.org/news/string-to-number-in-javascript-convert-a-string-to-an-int-in-js/ [ref 20221010°1242]
 *  ref        : https://www.freecodecamp.org/news/how-to-convert-a-string-to-a-number-in-javascript/ [ref 20221010°1243]
 *  ref        : https://www.freecodecamp.org/news/javascript-typeof-how-to-check-the-type-of-a-variable-or-object-in-js/ [ref 20221010°1244]
 *  notes      : Backtick since ES6 for templates. There is no Heredoc syntax in JavaScript [note 20221010°1217]
 */
let sVERSION = "v0.1.8"

document.write(`<p>*** Pozdravljeni, tukaj 'js151string.js' ${sVERSION} — Strings ***</p>\n`);
document.write("<pre>\n");

// ====================================================
// (1) String Syntax

// (1.1) Syntax — Quote types and newline
document.write("(1.1) Use double-quotes \" or single-quotes \' for strings" + "\n");

// (1.2) Syntax — Mulitiline string                                     // See ref 20221010°1212
let sMulti = "Ein Wiesel\n\
saß auf einem Kiesel\n\
inmitten Bachgeriesel.";
document.write("(1.2) " + sMulti + "\n");

// (1.3) Syntax — String concatenation (is demonstrated again below)
let sHello = "Hello";
let sJenny = "Jenny";
let sGreet = sHello + ' ' + sJenny;
document.write("(1.3) " + sGreet + "\n");

// (1.4) Syntax — String as array of chars
let sWord = "SilzuZankunKrei";
const chars = Array.from(sWord);                                        // See ref 20221010°1222 for four options to do so
document.write("(1.4) String as Array: ");
chars.forEach (c => {
   document.write(c + ".");
});
document.write("\n");

// ==============================================
// (2) Conversions

document.write("(2.1) Number to string: \"" + 1234 + "\", \"" + (2.345).toString() + "\"\n");

document.write("(2.2) String to number:" + "\n");                       // See refs 20221010°1242, 20221010°1243. See todo 20221010°1221 'Check for NaN'.
let nums = ["1234", "2.345", "Tree hundred twenty five"];
let o = null;
nums.forEach (sNum => {
   try {
      o = parseInt(sNum, 10);                                           // Does not throw (see todo 20221010°1221)
   }
   catch (e1) {
      try {
         o = parseFloat(sNum);                                          // Does not throw (see todo 20221010°1221)
      }
      catch (e2) {
         document.write("      Two exceptions thrown: " + e1 + " and " + e2 + "\n");
         o = "Not a number";
      }
   }
   document.write("      - \"" + sNum + "\" => " + o + " (" + typeof o + ")\n");
});

document.write("</pre>\n");
document.write("<p>Nasvidenje.</p>\n");
