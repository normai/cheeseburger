﻿/**
 *  file       : id 20221116°1425 — gitlab.com/normai/cheeseburger …/js/js311kbd1menu.js
 *  version    : • 20221117°0915 v0.1.8 Filling • 20221116°1425 v0.1.8 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate keyboard menu with pressing Enter
 *  userstory  :
 *  summary    :
 *  status     : Good enough for the beginning.
 *  note       : Having a keyboard menu on a website rarely makes sense.
 *               More sense it would make if this JavaScript is run in Node.js.
 *               For a 'web terminal' see https://github.com/normai/purpleterms
 *  ref        : https://www.bitdegree.org/learn/javascript-prompt [ref 20221116°1842]
 *  ref        : https://developer.mozilla.org/en-US/docs/Web/API/Document/write [ref 20221116°1852]
 */
let sVERSION = "v0.1.8"

var bIsFirstMenuAction = true;
////document.write(sWelcome);

var bContinue = true;
setTimeout(function(){ action(); }, 100);

function action() {

   // As a workaround, repeat the initial welcome notification,
   //  because on the first action, it is curiously lost.
   // Todo : Find out why/how this happens. Try to get rid of this workaround.
   if (bIsFirstMenuAction) {
      document.write(`<p>*** Hei, tämä on 'js311kbd1menu.js' ${sVERSION} — Keyboard menu (with pressing Enter) ***</p>\n`);
      document.write("<pre>\n");
      bIsFirstMenuAction = false
   }

   sMenu = ("Menu: a = Anton, k = Kylie, s = Santa, x = Exit\n");
   sKey = prompt(sMenu);

   switch (sKey)
   {
       case "a" :
          document.write("<pre>\nThis is Anton speaking</pre>\n");
          break;
       case "k" :
          document.write("<pre>\nThis is Kylie singing</pre>\n");
          break;
       case "s" :
          document.write("<pre>\nThis is Santa Claus ringing</pre>\n");
          break;
       case "x" :
          bContinue = false;
          document.write("</pre>\n");
          document.write("<p>Näkemiin.</p>\n");
          break;
       default :
          document.write("<pre>\nInvalid selection: \"" + sKey + "\"</pre>\n");
   }

   if (bContinue) {
      setTimeout(function(){ action(); }, 100);
   }
};
