﻿/**
 *  file       : id 20220826°1925 — gitlab.com/normai/cheeseburger …/js/js261try1.js
 *  version    : • 20220910°1525 v0.1.7 Filling • v0.1.6 20220826°1925 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate JavaScript Exception Handling
 *  ref        : https://www.w3schools.com/jsref/jsref_try_catch.asp [ref 20220910°1423]
 *  ref        : https://www.w3schools.com/jsref/jsref_sqrt.asp [ref 20220910°1422]
 *  status     : Must be tweaked.
 */
let sVERSION = "v0.1.7"

document.write(`<p>*** こんにちは、です [Kon'nichiwa, desu] 'js261try1.js' ${sVERSION} — Exception Handling ***</p>\n`);
document.write("<pre>\n");


// (1) Try adding mismatching types -- NOT AN EXCEPTION
document.write("(1) Add 'Eins' and 2\n")
try
{
    sum = 'Eins ' + 2                                           // Not an exception, JavaScript implicitly converts number to string
    document.write("    => " + sum + "\n")
}
catch(ex)
{
    document.write("    => Adding 'Eins' and 2 failed.\n")
}

// (2) Calculate square root, sometimes succeed, sometimes fail
let iRand = Math.floor(Math.random() * 19) - 9;                                 // Shall be from -9 to + 9
document.write("\n(2) Calculate sqare root of " + iRand);
try
{
    if (iRand < 0)
    {
       throw new ArithmeticException("The root of a negative number is NaN, not a number.");  // Workaround. Todo: Craft another example [todo 20220910°1421]
    }
    fRoot = Math.sqrt(iRand);
    document.write("    => Sqare root of " + iRand + " is " + fRoot + "\n");
}
catch (ex)
{
    document.write("    => Sqare root of " + iRand + " calculation failed: " + ex + "\n");
}

document.write("</pre>\n");
document.write("<p>さようなら [Sayōnara].</p>");
