﻿/**
 *  file       : id 20220828°1925 — gitlab.com/normai/cheeseburger …/js/js165dict.js
 *  version    : • 20220908°1421 v0.1.7 Filling • v0.1.6 20220828°1925 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate ...
 *  ref        : https://stackoverflow.com/questions/1248302/how-to-get-the-size-of-a-javascript-object []
 *  ref        : https://maxschmitt.me/posts/padding-strings-javascript/ []
 *  ref        : https://usefulangle.com/post/120/javascript-string-padding []
 */
let sVERSION = "v0.1.7"

document.write(`<p>*** Hej, das ist 'js165dict.js' ${sVERSION} — Dictionaries ***</p>\n`);
document.write("<pre>\n");

// (1) Create
const being = { "kingdom" : "Animals"
              , "class"   : "Reptiles"
              , "genus"   : "Crocodile"
              , "species" : "Nile crocodile"
              , "name"    : "Schnappi"
              , "color"   : "Green"
              , "legs"    : "Four"
              , "food"    : "Meat"
              }
document.write("<p>(1) Created = " + being.toString() + "</p>\n");

// (2) Info
document.write("<p>(2) Dictionary size = " + being.length + "</p>\n");

// (3) Iterate
document.write("<p>(3) Iterate:</p>\n");
Object.entries(being).forEach(([key, value]) => {
    document.write(`<p> - ${key.padEnd(8, '.')} : ${value}</p>\n`);
})

document.write("</pre>\n");
document.write("<p>Farvel.</p>\n");
