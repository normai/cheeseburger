﻿/**
 *  file       : id 20220826°1425 — gitlab.com/normai/cheeseburger …/js/js220switch.js
 *  version    : • 20221001°1025 v0.1.8 Filling • v0.1.6 20220826°1425 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate ...
 */
let sVERSION = "v0.1.8"

document.write(`<p>*** Aloha, ʻo kēia 'js220switch.js' ${sVERSION} — JavaScript Switch ***</p>`);
document.write("<pre>");

document.write("Your dice is rolling ...\n");
randi = Math.floor(Math.random() * 7) + 1;                          // Numbers 1 through 7
switch (randi)
{
   case 1 : document.write("Look, a one\n"); break;
   case 2 : document.write("Whoops, a two\n"); break;
   case 3 : document.write("Uh-huh, a three\n"); break;
   case 4 : document.write("Holla, a four\n"); break;
   case 5 : document.write("Oh, a five\n"); break;
   case 6 : document.write("Hooray, a six\n"); break;
   default : document.write("Your dice seems broken, you rolled " + randi + "\n"); break;
}

document.write("</pre>\n");
document.write("<p>Aloha mai.</p>");
