﻿/**
 *  file       : id 20220826°1025 — gitlab.com/normai/cheeseburger …/js/js123compare.js
 *  version    : • 20220908°1641 v0.1.7 Filling • v0.1.6 20220826°1025 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate JavaScript comparison operators
 *  summary    : Somne facts
 *               • As compared with other programming languages, it adds the type-strict
 *                  operators, e.g '===', making a difference between equality and sameness
 *               • Todo: Add examples or dedicated demo to show difference between equality and sameness [todo 20220906°1411]
 *  ref        : https://www.w3schools.com/js/js_operators.asp [ref 20220906°1352] General operator overview
 *  ref        : https://www.freecodecamp.org/news/javascript-string-comparison-how-to-compare-strings-in-js/ [ref 20220906°1353] String comparison
 *  ref        : https://developer.mozilla.org/en-US/docs/Web/JavaScript/Equality_comparisons_and_sameness [ref 20220906°1354]
 *  ref        : https://dev.to/alldanielscott/how-to-compare-numbers-correctly-in-javascript-1l4i [ref 20220906°1355]
 *  ref        : https://jsfiddle.net/r0begv7a/3/ [ref 20220906°1356]
 *  ref        :
 */
let sVERSION = "v0.1.7"

const THRESHOLD_1 = .000_000_000_1; // Problem — The tolerance has to be different, depending on the magnitude of the number!

document.write(`<p>*** Tere, see on 'js123compare.js' ${sVERSION} — ***</p>`);
document.write("<pre>\n");

document.write("(1.1)  \"Ahoj\" == \"Aloha\"         = " + ("Ahoj" == "Aloha") + "\n");
document.write("(1.2)  12345  == 23456           = " + (12345  == 23456) + "\n");
document.write("(1.3)  3.4567 == 4.5678          = " + (3.4567 == 4.5678) + "\n");
document.write("(1.4)  true   == false           = " + (true   == false) + "\n");
document.write("\n");
document.write("(2.1)  \"Ahoj\" != \"Aloha\"         = " + ("Ahoj" != "Aloha") + "\n");
document.write("(2.2)  12345  != 23456           = " + (12345  != 23456) + "\n");
document.write("(2.3)  3.4567 != 4.5678          = " + (3.4567 != 4.5678) + "\n");
document.write("(2.4)  true   != false           = " + (true   != false) + "\n");
document.write("\n");
document.write("(3.1)  \"Ahoj\".localeCompare(\"Aloha\") = " + "Ahoj".localeCompare("Aloha") + "\n");
document.write("(3.2)  12345  >  23456           = " + (12345  > 23456) + "\n");
document.write("(3.3)  3.4567 >  4.5678          = " + (3.4567 > 4.5678) + "\n");
document.write("(3.4)  true   >  false           = " + (true   > false) + "\n");
document.write("\n");
document.write("(4.1)  \"Ahoj\" >= \"Aloha\"         = " + ("Ahoj" >= "Aloha") + "\n");
document.write("(4.2)  12345  >= 23456           = " + (12345  >= 23456) + "\n");
document.write("(4.3)  3.4567 >= 4.5678          = " + (3.4567 >= 4.5678) + "\n");
document.write("(4.4)  true   >= false           = " + (true   >= false) + "\n");
document.write("\n");
document.write("(5.1)  \"Ahoj\" <  \"Aloha\"         = " + ("Ahoj" < "Aloha") + "\n");
document.write("(5.2)  12345  <  23456           = " + (12345  < 23456) + "\n");
document.write("(5.3)  3.4567 <  4.5678          = " + (3.4567 < 4.5678) + "\n");
document.write("(5.4)  true   <  false           = " + (true   < false) + "\n");
document.write("\n");
document.write("(6.1)  \"Ahoj\" >= \"Aloha\"         = " + ("Ahoj" >= "Aloha") + "\n");
document.write("(6.2)  12345  >= 23456           = " + (12345  >= 23456) + "\n");
document.write("(6.3)  3.4567 >= 4.5678          = " + (3.4567 >= 4.5678) + "\n");
document.write("(6.4)  true   >= false           = " + (true   >= false) + "\n");
document.write("\n");
document.write("(7.1)  0.1 + 0.2 == 0.3          = " + (0.1 + 0.2 == 0.3) + "\n");
document.write("(7.2)  Math.abs((0.1 + 0.2) - 0.3) < THRESHOLD_1 = " + (Math.abs((0.1 + 0.2) - 0.3) < THRESHOLD_1) + "\n");
document.write("\n");
document.write("(8.1)  \"Ahoj\".localeCompare(\"Aloha\") = " + "Ahoj".localeCompare("Aloha") + "\n");
document.write("(8.1)  \"Aloha\".localeCompare(\"Ahoj\") = " + "Aloha".localeCompare("Ahoj") + "\n");
document.write("(8.1)  \"Ahoj\".localeCompare(\"Ahoj\")  = " + "Ahoj".localeCompare("Ahoj") + "\n");

document.write("</pre>\n");
document.write("<p>Hüvasti.</p>");
