﻿/**
 *  file       : id 20220215°0921 — gitlab.com/normai/cheeseburger … js/js131types.js
 *  version    : • 20220908°1021 v0.1.7 Filling • v0.1.6 20220215°0921 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate some types
 *  summary    :
 */
let sVERSION = "v0.1.7"

document.write(`<p>*** अभिनंदन [abhinandan] 'js131types.js' ${sVERSION} — Types ***</p>`);
document.write("<pre>\n");

// Some declarations
iByte1 = 127;                                          // byte
iShort1 = 32767;                                       // short
iInt1 = 2147483647;                                    // int
iLong1 = Number.MAX_VALUE; // Int64.MaxValue           // long
f1 = 1.23;                                             // float -- The 'f' postfix prevents error 'lossy conversion'
d1 = Number.MAX_VALUE // double.MaxValue;              // double
b1 = true;                                             // bool
c1 = 'x';                                              // char
s1 = "Holla";                                          // string

// Some operations
iByte2 = iByte1 + 1;                                   //
iShort2 = iShort1 + 1;                                 //
iInt2 = iInt1 + 1;
iLong2 = iLong1 + 1;
f2 = f1 + 1;
d2 = d1 + 1;
b2 = !b1;                                              // The plus operator is not possible with boolean?
c2 = c1 + 1;                                           //
s2 = s1 + 1;                                           //

// Display the results
document.write("<p>(1) " + iByte1 + " + 1 = " + iByte2 + " (Number.MAX_VALUE = " + Number.MAX_VALUE + ")</p>\n");
document.write("<p>(2) " + iShort1 + " + 1 = " + iShort2 + " (Number.MAX_VALUE = " + Number.MAX_VALUE + ")</p>\n");
document.write("<p>(3) " + iInt1 + " + 1 = " + iInt2 + "</p>\n");
document.write("<p>(4) " + iLong1 + " + 1 = " + iLong2 + "</p>\n");
document.write("<p>(5) " + f1 + " + 1 = " + f2 + "</p>\n");
document.write("<p>(6) " + d1 + " + 1 = " + d2 + "</p>\n");
document.write("<p>(7) !" + b1 + " = " + b2 + "</p>\n");
document.write("<p>(8) " + s1 + " + 1 = " + s2 + "</p>\n");

document.write("</pre>\n");
document.write("<p>अलविदा [Alavida].</p>\n");
