﻿/**
 *  file       : id 20220829°0925 — gitlab.com/normai/cheeseburger …/js/js167tuple.js
 *  version    : • 20220905°1851 v0.1.7 Filling • v0.1.6 20220829°0925 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate JavaScript tuples
 *  summary    : Some facts
 *                • The array-like syntax with octothorp prefix is not yet available in the wild
 *                • The polyfill I didn't like, so I choose the class from ... ref 20220905°1826
 *                • Unpacking also possible (not yet shown) -- Todo: Demonstrate unpacking []
 *                •
 *  ref        : https://www.w3schools.com/typescript/typescript_tuples.php [ref 20220905°1811] -- This is not about JavaScript but has nice tuple explanations
 *  ref        : https://betterprogramming.pub/tuples-in-javascript-57ede9b1c9d2 [ref 20220905°1812] -- Tuples were syntactic sugar on arrays
 *  ref        : https://www.becomebetterprogrammer.com/javascript-tuples/ [ref 20220905°1816]
 *  ref        : https://rickbutton.github.io/record-tuple-playground [ref 20220905°1822]
 *  ref        : https://ntgard.medium.com/tuples-in-javascript-cd33321e5277 [ref 20220905°1823]
 *  ref        : https://fjolt.com/article/javascript-records-and-tuples [ref 20220905°1832]
 *  ref        : https://www.delftstack.com/howto/javascript/javascript-tuples-explained-with-method/ [ref 20220905°1826]
 */
let sVERSION = "v0.1.7"

// () Provide Tuple class [class 20220905°1841]
// Code after ref 20220905°1826 https://www.delftstack.com/howto/javascript/javascript-tuples-explained-with-method/
class MyBestTupleExplanation extends Array {
    constructor(...items) {
        super(...items);
        Object.freeze(this);
    }
}

document.write(`<p>*** Hallo, dit is 'js167tuple.js' ${sVERSION} — Tuples ***</p>`);
document.write("<pre>\n");

// (1) Create
let tpNested = new MyBestTupleExplanation("Ding");
let tupl = new MyBestTupleExplanation("Values", 1234, 2.345, true, tpNested);
document.write("<p>(1) tupl = " + tupl + "</p>");

// (2) Info
document.write("<p>(2.1) Size = " + tupl.length + "</p>");
document.write("<p>(2.2) Type = " + typeof tupl + "</p>");

// (3) Iterate
document.write("<p>(3) Iterate:" + "</p>");
for (let i = 0; i < tupl.length; i++)
{
   document.write("<p> - " + tupl[i].toString().padEnd(12, '.') + " " + typeof tupl[i] + "</p>");
}

// (4) Pick by index
document.write("<p>(4.1) Third item = " + tupl[2] + "</p>");
document.write("<p>(4.2) First of fifth = " + tupl[4][0] + "</p>");

document.write("</pre>\n");
document.write("<p>Tot ziens.</p>");
