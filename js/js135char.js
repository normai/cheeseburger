﻿/**
 *  file       : id 20221012°0925 — gitlab.com/normai/cheeseburger …/js/js135char.js
 *  version    : • 20221016°1125 v0.1.8 Filling • v0.1.8 20221012°0925 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate Character Type
 *  summary    : Involved functions:
 *                • Bracket notation s[i]
 *                • s.charAt(i)
 *                • s.charCodeAt(i)
 *                • s.codePointAt(i)
 *                • String.fromCharCode(i) — Not yet used below
 *                • String.fromCodePoint(i)
 *                • s.length — Tells the byte size, not the UTF-8 char size
 *  userstory  :
 *  ref        : https://betterprogramming.pub/how-to-iterate-through-strings-in-javascript-65c51bb3ace5 [ref 20221015°1152] (👍)
 *  ref        : https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/charAt [ref 20221015°1712]
 *  ref        : https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/charCodeAt [ref 20221015°1713]
 *  ref        : https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/codePointAt [ref 20221015°1714]
 *  ref        : https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/fromCharCode [ref 20221015°1715]
 *  ref        : https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/fromCodePoint [ref 20221015°1716]
 */
let sVERSION = "v0.1.8"

document.write(`<p>*** Здравейте, това е [Zdraveĭte, tova e] js135char.js ${sVERSION} — Character Type ***</p>\n`);
document.write("<pre>\n");

let s1 = "Aloha … é 👍";

// (A.1)
// Note: The string has 11 UTF-8 chars, but length tells 12
document.write("(A.1) Length of               \"" + s1 + "\" is " + s1.length + "\n");

// (B)
// (B.1) For loop and character access bracket notation
// Note. Separating the chars in the loop e.g. by a blank will destroy the Thumb-up-sign
document.write("(B.1) Bracket notation       : ");
for (let i = 0; i < s1.length; i++) {
   document.write(s1[i]);
}
document.write("\n");

// (B.2) For/of loop
document.write("(B.2) For/Of loop            :");
for (let c of s1) {
   document.write(' ' + c);
}
document.write("\n");

// (B.3) ForEach loop
document.write("(B.3) ForEach loop           :");
[...s1].forEach(c => {
   document.write(' ' + c);
});
document.write("\n");

// (B.4) charCodeAt() — The returned character code value lies between 0 and 65535
cps = [];
document.write("(B.4) charCodeAt()           :");
for (let i = 0; i < s1.length; i++) {
   document.write(' ' + s1.charCodeAt(i));
   cps.push(s1.charCodeAt(i));
}
document.write("\n");

// (B.5) String.fromCodePoint()
document.write("(B.5) fromCodePoint()        :");
cps.forEach(iCode => {
   document.write(' ' + String.fromCodePoint(iCode));
});
document.write("\n");

// (B.6) String.fromCodePoint()
document.write('(B.6) fromCodePoint(0x1f44d) : ' + String.fromCodePoint(0x1f44d) + "\n");

document.write("</pre>\n");
document.write("<p>Довиждане [Dovizhdane].</p>\n");
