﻿/**
 *  file       : id 20221012°1825 — gitlab.com/normai/cheeseburger …/js/js159date.js
 *  version    : • 20221230°1421 v0.1.9 Filling • 20221012°1825 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate Date and Time Types
 *  userstory  : Set and print dates in various formats and calculate time spans
 */
let sVERSION = "v0.1.9"

document.write(`<p>*** Tere, see on 'js159date.js' ${sVERSION} — Date and Time Types ***</p>\n`);
document.write("<pre>\n");

let dStart = new Date();                          // For the calculation far below

// Limits
document.write("(1) DateTime limits:\n");
let dMin = new Date(-8640000000000000);           // Number from https://stackoverflow.com/questions/11526504/minimum-and-maximum-date
let dMax = new Date(8640000000000000);
document.write("   1. Date Min = " + dMin.toLocaleString("en", {"year": "numeric", "era": "short"}) + "\n");
document.write("   2. Date Max = " + dMax.toLocaleString("en", {"year": "numeric", "era": "short"}) + "\n");


// Galileo
document.write("(2) Galileo birthday:\n");
let dGal1 = new Date(1564, 1, 16);                     // Month index is zero-based, 1 = February!
let dGal2 = new Date(1564, 1, 16, 13, 14, 15);
let dGal3 = new Date(1564, 1, 16, 13, 14, 15, 123);
let sCustom = `${dGal3.getFullYear()}-${dGal3.getMonth()}-${dGal3.getDate()}\`${dGal3.getHours()}:${dGal3.getMinutes()}:${dGal3.getSeconds()}.${dGal3.getMilliseconds()}`;
document.write("   1. D1 = " + dGal1 + " — date only\n");
document.write("   2. D2 = " + dGal2 + " — with seconds\n");
document.write("   3. D3 = " + dGal3 + " — with millisec\n");
document.write("   4. Custom = " + sCustom + "\n");

// Now
document.write("(3) Now:\n");
let dNow = new Date();
let sNow = `${dNow.getFullYear()}-${dNow.getMonth()}-${dNow.getDate()}\`${dNow.getHours()}:${dNow.getMinutes()}:${dNow.getSeconds()}.${dNow.getMilliseconds()}`;
document.write("   1. Native = " + dNow + "\n");
document.write("   2. Custom = " + sCustom + "\n");

// Halley
document.write("(4) Next Halley approach:\n");
let dHal = new Date(2061, 6, 29);                            // Month 6 = July
let sHal = `${dHal.getFullYear()}-${dHal.getMonth()}-${dHal.getDate()}\`${dHal.getHours()}:${dHal.getMinutes()}:${dHal.getSeconds()}.${dHal.getMilliseconds()}`;
document.write("   1. Native = " + dHal + "\n");
document.write("   2. Custom = " + sHal + "\n");

// Calculate large scale time span
let msHal = dHal - dNow;
let iMscPar = msHal % 1000;
let iSecTot = Math.floor(msHal / 1000);
let iSecPar = iSecTot % 60;
let iMinTot = Math.floor(iSecTot / 60);
let iMinPar = iMinTot % 60;
let iHouTot = Math.floor(iMinTot / 60);
let iHouPar = iHouTot % 24;
let iDayTot = Math.floor(iHouTot / 24);
let iDayPar = iDayTot % 365;
let iYears = Math.floor(iDayTot / 365);
document.write(`(5) Wait for Halley : ${msHal} ms ≈ ${iYears} years, ${iDayPar} days, ${iHouPar}:${iMinPar}:${iSecPar}.${iMscPar}\n`);

// Calculate short scale time span
let dStop = new Date();
let dura = dStop - dStart;
document.write(`(6) Runtime duration : ${dura} ms\n`);

document.write("</pre>\n");
document.write("<p>Hüvasti.</p>\n");
