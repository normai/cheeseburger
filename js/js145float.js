﻿/**
 *  file       : id 20221012°1125 — gitlab.com/normai/cheeseburger …/js/js145float.js
 *  version    : • 20221216°1113 v0.1.8 Filling • 20221012°1125 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate Float Type
 *  status     : Has room for enhancement. Should use string formatting for number output.
 */
let sVERSION = "v0.1.8"

document.write(`<p>*** Dobrý den, toto je 'js139bigint.js' ${sVERSION} — Float Types ***</p>\n`);
document.write("<pre>\n");


document.write("(1) Print some numbers:\n");
let f1 = 123_456.789;
let f2 = -123_456.789;
let f3 = 123_456_777_777_777_777_777_777_777.789;
let f4 = -123_456_777_777_777_777_777_777_777.789;
document.write(`(1.1) f1 = ${f1} ${typeof f1}\n`);
document.write(`(1.2) f2 = ${f2} ${typeof f2}\n`);
document.write(`(1.3) f3 = ${f3} ${typeof f3}\n`);
document.write(`(1.4) f4 = ${f4} ${typeof f4}\n`);


document.write("</pre>\n");
document.write("<p>Nashledanou.</p>\n");
