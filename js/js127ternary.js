﻿/**
 *  file       : id 20221002°0925 — gitlab.com/normai/cheeseburger …/js/js127ternary.js
 *  version    : • 20221227°1614 v0.1.8 Filling • v0.1.8 20221002°0925 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate ternary operator
 *  userstory  : Roll dice and tell whether it was greater than three or not
 */
let sVERSION = "v0.1.8";

document.write(`<p>*** Hej, det här är \`js127ternary.js\` ${sVERSION} — Ternary operator ***</p>\n`);
document.write("<pre>\n");


// Preparation — Roll dice
let iRand = Math.floor(Math.random() * (6)) + 1;                       // From 1 though 6
document.write("(1.1) You rolled " + iRand.toString() + "\n");

// The demo line
document.write("(1.2) You rolled greater three : " + (iRand > 3 ? "Yes" : "No"));


document.write("</pre>\n");
document.write("<p>Adjö.</p>\n");
