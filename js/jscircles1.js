﻿/**
 *  file       : id 20220928°1825 — gitlab.com/normai/cheeseburger …/js/jscircles1.js
 *  version    : • 20220928°1825 v0.1.8 Initial filling
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Draw ASCII Circles and other patterns
 *  ref        : https://sebhastian.com/javascript-square/ [ref 20221001°0922 ico 20220304°0953]
 *  ref        : https://www.freecodecamp.org/news/javascript-foreach-how-to-loop-through-an-array-in-js/ [ref 20221001°0923 ico 20210216°1153]
 */
let sVERSION = "v0.0.0"

document.write(`<p>*** Dobrý deň, toto je 'circles1.js' ${sVERSION} — Draw ASCII Circles ***</p>\n`);
document.write("<pre>\n");

function IsPointOnCirlce(r, x, y) {
   tolerance = r;                                                   // Empirical value for line width
   if (Math.abs(Math.pow(x, 2) + Math.pow(y, 2) - Math.pow(r, 2)) < tolerance) {
      return true;
   }
   else {
      return false;
   }
}

function IsPointOnDiagonal(x, y, size) {
   if (x == size - y + 1) {                                         // Add 1 empirically
      return true;
   }
   else {
      return false;
   }
}

const sizes = [7, 17];
sizes.forEach(function(size) {                                      // Paint as many rectangles as sizes are given

   // Prepare convenient variables
   // Note how the integer division for the radius looks different in each language
   let radius = Math.floor((size / 2) - 1);                         // Make size of circle slightly smaller than the rectangle
   let shiftx = radius + 2;                                         // Shift circle center from rectangle left to rectangle center
   let shifty = radius + 2;                                         // Shift circle center from rectangle top to rectangle center

   // Iterate over lines and columns
   for (let line = 1; line <= size; line++) {
      for (let col = 1; col <= size; col++) {
         if (IsPointOnCirlce(radius, line - shiftx, col - shifty)) {  // Is this point on the circle?
            document.write("@ ");
         }
         else {
            if (IsPointOnDiagonal(line, col, size)) {               // Is this point on the diagonal?
               document.write("# ");
            }
            else {                                                  // All other cases
               document.write(". ");
            }
         }
      }
      document.write("\n");                                         // Goto next line
   }
});

document.write("</pre>\n");
document.write("<p>Dovidenia.</p>\n");
