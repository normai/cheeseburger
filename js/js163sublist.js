﻿/**
 *  file       : id 20220828°1825 — gitlab.com/normai/cheeseburger …/js/js163sublist.js
 *  version    : • 20220908°1321 v0.1.7 Filling • v0.1.6 20220828°1825 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate array slicing
 *  summary    :
 *  reference  : https://www.w3schools.com/jsref/jsref_slice_array.asp []
 */
let sVERSION = "v0.1.7"

document.write(`<p>*** Ahoj 'js163sublist.js' ${sVERSION} — Sublists ***</p>`);
document.write("<pre>\n");

// (1) Create
const words = ["Uno", "Due", "Tre", "Quattro", "Cinque"];
words.push("Sei");
words.push("Sette");
document.write("<p>Array = " + words + "</p>\n");

// (2) Slice
const slice = words.slice(2, 6);

// (3) Output
document.write("<p>Slice = ");
slice.forEach( function(word)
{
   document.write(" " + word);
});
document.write("</p>\n");


document.write("</pre>\n");
document.write("<p>Nashledanou.</p>");
