﻿/**
 *  file       : id 20221215°1125 — gitlab.com/normai/cheeseburger …/js/jsfactorial.js
 *  version    : • 20221216°0924 v0.1.8 Filling • 20221215°1125 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate factorial calculation
 *  status     : Algorithm is still too naive
 */
let sVERSION = "v0.1.8";

// Function 20221215°1411 Recursion naive
function Factorial(iVal) {
   if (iVal > 1) {
      iVal = iVal * Factorial(iVal - 1);
      return iVal;
   }
   else {
      return 1;
   }
}

document.write(`<p>*** Sveiki, tai 'jsfactorial.js' ${sVERSION} — Factorial ***</p>\n`);
document.write("<pre>\n");

for (i = -1; i < 24; i++) {
   iFact = Factorial(i);
   document.write(" - " + i + "! = " + iFact + "\n");
}

document.write("</pre>\n");
document.write("<p>Iki pasimatymo.</p>\n");
