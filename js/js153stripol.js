﻿/**
 *  file       : id 20221012°1625 — gitlab.com/normai/cheeseburger …/js/js153stripol.js
 *  version    : • 20221013°1025 v0.1.8 Filling • v0.1.8 20221012°1625 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate String Interpolation
 *  userstory  :
 *  summary    :
 *  ref        : https://www.codeleaks.io/javascript-string-interpolation/ [ref 20221010°1232]
 */
let sVERSION = "v0.1.8"

document.write(`<p>*** Hej, det er 'js153stripol.js' ${sVERSION} — String Interpolation ***</p>\n`);
document.write("<pre>\n");

// () Provide fodder
let iX = 234;
let sHello = "Hello";
let sJenny = "Jenny";

// (x) String interpolation
// (x.1) String concatenation
document.write("(2.1) Concatenation       : " + sHello + " " + iX + " " + sJenny + ".\n");

// (x.2) String template literal
document.write(`(2.2) Template Literal    : ${sHello} ${iX} ${sJenny}.\n`);

document.write("</pre>\n");
document.write("<p>Farvel.</p>\n");
