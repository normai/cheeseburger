﻿/**
 *  file       : id 20221012°1025 — gitlab.com/normai/cheeseburger …/js/js137int.js
 *  version    : • 20221018°1125 v0.1.8 Filling • v0.1.8 20221012°1025 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate JavaScript Integers
 *  summary    :
 *  userstory  :
 *  ref        : https://www.w3schools.com/js/js_numbers.asp [ref 20221017°0912]
 *  ref        :
 *  status     :
 */
let sVERSION = "v0.1.8"

document.write(`<p>*** 你好，这是 [Nǐ hǎo, zhè shì] 'js137int.js' ${sVERSION} — JavaScript Integers ***</p>\n`);
document.write("<pre>\n");

let i1 = 123;
let i2 = -2147483648;
let i3 = 2147483647;
let i4 = i3 + 1;
let i5 = -9223372036854775808;
let i6 = 9223372036854775807;
let i7 = i6 + 1;

document.write("(1) i1     = " + ('' + i1).padEnd(24) + " " + typeof i1 + "\n");
document.write("(2) i2     = " + ('' + i2).padEnd(24) + " " + typeof i2 + "\n");
document.write("(3) i3     = " + ('' + i3).padEnd(24) + " " + typeof i3 + "\n");
document.write("(4) i3 + 1 = " + ('' + i4).padEnd(24) + " " + typeof i4 + "\n");
document.write("(5) i5     = " + ('' + i5).padEnd(24) + " " + typeof i5 + "\n");
document.write("(6) i6     = " + ('' + i6).padEnd(24) + " " + typeof i6 + "\n");
document.write("(7) i6 + 1 = " + ('' + i7).padEnd(24) + " " + typeof i7 + "\n");

document.write("</pre>\n");
document.write("<p>再见 [Zàijiàn]./p>\n");
