﻿/**
 *  file       : id 20221117°1125 — gitlab.com/normai/cheeseburger …/js/js147bigflo.js
 *  version    : • 20221228°1151 v0.1.9 Filling • 20221117°1125 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate big integer types
 *  userstory  : Calculate and output some popular non-terminating decimals.
 *  dependecy  : Library src="./libs/big.js"
 */
let sVERSION = "v0.1.9"

document.write(`<p>*** हैलो, यह है [Hailo, yah hai] 'js147bigflo.js' ${sVERSION} — Big Floats ***</p>\n`);
document.write("<pre>\n");


document.write("(1) Native JS numbers :\n");
let d1 = 10.0 / 7.0;
let d2 = 10.0 / 3.0;
let d3 = 10.0 / 1.5;
document.write(`(1.1) 10 / 7.0 = ${d1}\n`);
document.write(`(1.2) 10 / 3.0 = ${d2}\n`);
document.write(`(1.3) 10 / 1.5 = ${d3}\n`);

document.write("\n(2) Big.js numbers :\n");
Big.strict = true;
Big.DP = 64;

let bTen = new Big('10.0')
let big1 = bTen.div(new Big('7.0'))
let big2 = bTen.div(new Big('3.0'))
let big3 = bTen.div(new Big('1.5'))
document.write(`(2.1) 10 / 7.0 = ${big1}\n`);
document.write(`(2.2) 10 / 3.0 = ${big2}\n`);
document.write(`(2.3) 10 / 1.5 = ${big3}\n`);


document.write("</pre>\n");
document.write("<p>अलविदा [Alavida].</p>\n");
