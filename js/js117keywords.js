﻿/**
 *  file       : id 20221217°0925 — gitlab.com/normai/cheeseburger …/js/js117keywords.js
 *  version    : • 20221227°0941 v0.1.9 Filling • 20221217°0925 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate keywords, using the await keyword as identifier plus as keyword
 *  status     :
 */
let sVERSION = "v0.1.9"

document.write(`<p>*** Ko tenei 'js117keywords.js' ${sVERSION} — Keywords ***</p>\n`);
document.write("<pre>\n");


function resolveAfterSomeTime(x) {
   return new Promise((resolve) => {
      setTimeout(() => {
         resolve(x);
      }, 1400);                                        // Milliseconds to wait to resolve
   });
}

async function f1() {
   const x = await resolveAfterSomeTime(4567);         // Conditionally reserved word 'await'
   document.write(`Async : ${x}\n`);
}

let await = 123;
document.write(`Variablename await, value = ${await}\n`);
f1();


document.write("</pre>\n");
document.write("<p>Tēnā koe.</p>\n");
