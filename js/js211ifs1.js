﻿/**
 *  file       : id 20220826°1225 — gitlab.com/normai/cheeseburger …/js/js211ifs1.js
 *  version    : • 20220922°1825 v0.1.8 Filling • v0.1.6 20220826°1225 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate branching with if, else if, else
 *  summary    : This demonstrates If/ElseIf/Else with four cases:
 *                • Unilateral branching
 *                • Bilateral branching
 *                • Multilateral branching
 *                • Wrong branching logic
 *               The sequence goes like this (1) Create random integer (2) Let the user
 *               overwrite this number (3) Do four different evaluations on that number
 *  usage      : Open accompanying HTML file in browser
 *  ref        :
 */
let sVERSION = "v0.1.8"

document.write(`<p>*** Hallo, hier ist 'js211ifs1.js' ${sVERSION} — If/ElseIf/Else ***</p>\n`);
document.write("<pre>\n");

// (A.1) Preparation — Get a random number rolled
let randi = Math.floor(Math.random() * (6)) + 1;                           // From 1 though 6
document.write("You rolled number " + randi.toString() + "\n");

// (A.2) Optionally allow to manually choose the number
if (false) {                                                               // Optionally toggle this
   document.write("Overwrite rolled number\n");
   let s = prompt("Overwrite rolled number " + randi.toString());
   let n = parseFloat(s);
   if (! Number.isNaN(n)) {                                                // ECMAScript-6
      n = n < 1 ? 1 : n;
      n = n > 6 ? 6 : n;
      randi = n;
   }
   else {
      document.write("Your input was not a number: \"" + s + "\"\n");
   }
   document.write("Your number now is '" + randi.toString() + "'.\n");
}

// (B.1) Unilateral branching
document.write("(1) Unilateral branching\n");
if (randi > 3) {
   document.write("(1.1) : Your number is bigger than three\n");
}

// (B.2) Two-way branching
document.write("(2) Bilateral branching\n");
if (randi == 3) {
   document.write("(2.1) : The number is three\n");
}
else {
   document.write("(2.2) : The number is not three\n");
}

// (B.3) Multilateral branching — Only one branch will be executed
document.write("(3) Multilateral branching\n");
if (randi > 5) {
   document.write("(3.1) : The number is six\n");
}
else if (randi > 4) {
   document.write("(3.2) : The number is five\n");
}
else if (randi > 3) {
   document.write("(3.3) : The number is four\n");
}
else if (randi > 2) {
   document.write("(3.4) : The number is three\n");
}
else {
   document.write("(3.5) : The number is one or two\n");
}

// (B.4) A typical beginners mistake, looks very similar like above,
//  has multiple if instead elif statements, will not work as intended.
document.write("(4) Wrong logic\n");
if (randi > 5) {
   document.write("(4.1) : The number is six\n");
}
if (randi > 4) {
   document.write("(4.2) : The number is five or six\n");
}
if (randi > 3) {
   document.write("(4.3) : The number is four to six\n");
}
if (randi > 2) {
   document.write("(4.4) : The number is three to six\n");
}
else {
   document.write("(4.5) : The number is one or two\n");
}

document.write("</pre>\n");
document.write("<p>Auf Wiedersehen.</p>\n");
