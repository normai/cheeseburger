﻿/**
 *  file       : id 20220826°1625 — gitlab.com/normai/cheeseburger …/js/js235dowhile.js
 *  version    : • 20221001°1125 v0.1.8 Filling • v0.1.6 20220826°1625 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate ...
 */
let sVERSION = "v0.1.8"

document.write(`<p>*** Hello, ez 'js235dowhile.js' ${sVERSION} — Do/while-Loop ***</p>`);
document.write("<pre>");

document.write("Loop until you rolled a six :\n");
let randi = null;                                           // Variable needs still be known behind the do block
do
{
   randi = Math.floor(Math.random() * 6) + 1;               // Numbers 1 through 6
   document.write(" • You rolled " + randi + "\n");
}
while (randi != 6);

document.write("</pre>\n");
document.write("<p>Viszlát.</p>");
