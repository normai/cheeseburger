﻿/**
 *  file       : id 20220825°1525 — gitlab.com/normai/cheeseburger …/js/js121operas.js
 *  version    : • 20220908°1541 v0.1.7 Filling • v0.1.6 20220825°1525 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  summary    : This demonstrates some operators
 *  ref        : https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Template_literals [ref 20220825°1444]
 */
let sVERSION = "v0.1.7"

document.write(`<p>*** Hello, this is 'js14operas.js' ${sVERSION} — Some operators ***</p>\n`);  // Buongiorno
document.write("<pre>\n");

// Assignment operators
v = w = x = y = z = 5;
w += 2;
x -= 2;
y *= 2;
z /= 2;
document.write("(1.1) 'v = w = x = y = z = 5'             : " + v + "\n");
document.write("(1.2) 'x += 2' identical with 'x = x + 2' : " + w + "\n");
document.write("(1.3) 'x -= 2' identical with 'x = x - 2' : " + x + "\n");
document.write("(1.4) 'y *= 2' identical with 'y = y * 2' : " + y + "\n");
document.write("(1.5) 'y /= 2' identical with 'y = y / 2' : " + z + "\n");
document.write();

// Some remarkable operators
x1 = 100 / 30;                                                  // Division
x2 = Math.floor(100 / 30);                                      // Integer division, in Python "//"
x3 = 100 % 30;                                                  // Modulo or remainder
x4 = Math.pow(16, 3);                                           // Power, in Python operator "16 ** 3"
x5 = 1 > 2 ? 1234 : 5678;                                       // Ternary operator
document.write("(2.1) 100 / 30            = " + x1 + " " + typeof x1 + "\n");
document.write("(2.2) 100 / 30            = " + x2 + " " + typeof x2 + "\n");
document.write("(2.3) 100 % 30            = " + x3 + " " + typeof x3 + "\n");
document.write("(2.4) 16 ** 3             = " + x4 + " " + typeof x4 + "\n");
document.write("(2.5) 1 > 2 ? 1234 : 5678 = " + x5 + " " + typeof x5 + "\n");
document.write();

// The format function introduces the member operator '.'
i1 = 1;
i2 = 2;
f1 = 3.4;
s = `(7) Take ${i1} or ${i2} or ${f1}.`;                        // Template literals since ES6
document.write("(3)" + s + "\n");

document.write("</pre>\n");
document.write("<p>Good bye.</p>");
