﻿/**
 *  file       : id 20220826°1125 — gitlab.com/normai/cheeseburger …/js/js125bitwise.js
 *  version    : • 20220908°1741 v0.1.7 Filling • v0.1.6 20220826°1125 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate JavaScript Bitwise-Operators
 *  summary    :
 *  ref        : https://www.w3schools.com/js/js_bitwise.asp [ref 20220907°1112] Overview
 *  ref        : https://medium.com/@soni.dumitru/javascript-bitwise-operations-190001bf1fc [ref 20220907°1113] Tutorial
 *  ref        : https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators [ref 20220907°1114] On operators in general
 *  ref        : https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Bitwise_AND [ref 20220907°1115] Other pages for the other operators
 *  ref        : https://www.delftstack.com/howto/javascript/convert-number-to-binary-in-javascript/ [ref 20220907°1142] (Also for file 20220828°1025 js335format.js)
 */
let sVERSION = "v0.1.7"

i1 = 43690;
i2 = 3855;

document.write(`<p>*** Hei, tämä on 'js125bitwise.js' ${sVERSION} — ***</p>`);
document.write("<pre>");

document.write("(1) Bitwise AND" + "\n");
document.write("(1.1) i1          =  " + i1.toString(2).padStart(18)         + " " + i1.toString(16).padStart(9)         + " " + `${i1}`.padStart(9)        + "\n");
document.write("(1.2) i2          =  " + i2.toString(2).padStart(18)         + " " + i2.toString(16).padStart(9)         + " " + `${i2}`.padStart(9)        + "\n");
document.write("(1.3) i1 & i2     =  " + (i1 & i2).toString(2).padStart(18)  + " " + (i1 & i2).toString(16).padStart(9)  + " " + `${(i1 & i2)}`.padStart(9) + "\n");

document.write("(2) Bitwise OR" + "\n");
document.write("(2.1) i1          =  " + i1.toString(2).padStart(18)         + " " + i1.toString(16).padStart(9)         + " " + `${i1}`.padStart(9)        + "\n");
document.write("(2.2) i2          =  " + i2.toString(2).padStart(18)         + " " + i2.toString(16).padStart(9)         + " " + `${i2}`.padStart(9)        + "\n");
document.write("(2.3) i1 | i2     =  " + (i1 | i2).toString(2).padStart(18)  + " " + (i1 | i2).toString(16).padStart(9)  + " " + `${(i1 | i2)}`.padStart(9) + "\n");

document.write("(3) Bitwise XOR" + "\n");
document.write("(3.1) i1          =  " + i1.toString(2).padStart(18)         + " " + i1.toString(16).padStart(9)         + " " + `${i1}`.padStart(9)        + "\n");
document.write("(3.2) i2          =  " + i2.toString(2).padStart(18)         + " " + i2.toString(16).padStart(9)         + " " + `${i2}`.padStart(9)        + "\n");
document.write("(3.3) i1 ^ i2     =  " + (i1 ^ i2).toString(2).padStart(18)  + " " + (i1 ^ i2).toString(16).padStart(9)  + " " + `${(i1 ^ i2)}`.padStart(9) + "\n");

// Todo: Check — This yields not the memory bit pattern we see in the other programming languages [todo 20220907°1153]
document.write("(4) Bitwise NOT" + "\n");
document.write("(4.1) i1          =  " + i1.toString(2).padStart(18)         + " " + i1.toString(16).padStart(9)         + " " + `${i1}`.padStart(9)        + "\n");
document.write("(4.2) ~i1         =  " + (~i1).toString(2).padStart(18)      + " " + (~i1).toString(16).padStart(9)      + " " + `${(~i1)}`.padStart(9) + "\n");

document.write("(5) Shift Left" + "\n");
document.write("(5.1) i1          =  " + i1.toString(2).padStart(18)         + " " + i1.toString(16).padStart(9)         + " " + `${i1}`.padStart(9)        + "\n");
document.write("(5.2) i1 << 1     =  " + (i1 << 1).toString(2).padStart(18)  + " " + (i1 << 1).toString(16).padStart(9)  + " " + `${(i1 << 1)}`.padStart(9) + "\n");

// Todo: Check — Why does this '>>' not fill the leftmost bit with a 1? [todo 20220907°1151]
document.write("(6) Shift Right(?!)" + "\n");
document.write("(6.1) i1          =  " + i1.toString(2).padStart(18)         + " " + i1.toString(16).padStart(9)         + " " + `${i1}`.padStart(9)        + "\n");
document.write("(6.2) i1 >> 1     =  " + (i1 >> 1).toString(2).padStart(18)  + " " + (i1 >> 1).toString(16).padStart(9)  + " " + `${(i1 >> 1)}`.padStart(9) + "\n");

document.write("(7) Zero fill right shift" + "\n");
document.write("(7.1) i1          =  " + i1.toString(2).padStart(18)         + " " + i1.toString(16).padStart(9)         + " " + `${i1}`.padStart(9)        + "\n");
document.write("(7.2) i1 >>> 1    =  " + (i1 >>> 1).toString(2).padStart(18) + " " + (i1 >>> 1).toString(16).padStart(9) + " " + `${(i1 >>> 1)}`.padStart(9) + "\n");

document.write("</pre>");
document.write("<p>Näkemiin.</p>");
