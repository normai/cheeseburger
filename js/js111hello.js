﻿/**
 *  file       : id 20220826°0925 — gitlab.com/normai/cheeseburger …/js/js111hello.js
 *  version    : • 20220908°0721 v0.1.7 Tweak • v0.1.6 20220826°0925 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate a most simple JavaScript program with user input
 */
let sVERSION = "v0.1.7"

document.write(`<p>*** This is 'js111hello.js' ${sVERSION} — Hello ***</p>\n`);
document.write("<pre>\n");


let sInput = window.prompt("Please enter your name:")
document.write("<p>Good morning \"" + sInput + "\"!</p>\n");


document.write("</pre>\n");
document.write("<p>Bye.</p>\n");
