﻿/**
 *  file       : id 20220826°1725 — gitlab.com/normai/cheeseburger …/js/js240for.js
 *  version    : • 20220919°1925 v0.1.8 Filling • v0.1.6 20220826°1725 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate the for loop
 *  ref        : https://www.w3schools.com/jsref/jsref_pow.asp []
 */
let sVERSION = "v0.1.8"

document.write(`<p>*** Halo, ini adalah 'js240for.js' ${sVERSION} — For loop ***</p>\n`);
document.write("<pre>\n");

document.write("(1) Two power 6 through 10\n");
for (let i = 6; i <= 10; i++)
{
   let iPow = Math.pow(2, i);
   document.write("   - " + i + " " + iPow + "\n");
}

document.write("(2) And the same backward\n");
for (let i = 10; i >= 6; i--)
{
   let iPow = Math.pow(2, i);
   document.write("   - " + i + " " + iPow + "\n");
}

document.write("</pre>\n");
document.write("<p>अलविदा [Alavida].</p>\n");