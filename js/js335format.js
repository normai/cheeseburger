﻿// THIS FILE IS AN EMPTY STUB, IT IS YET TO BE FILLED WITH CODE

/**
 *  file       : id 20220828°1025 — gitlab.com/normai/cheeseburger …/js/js335format.js
 *  version    : • 20xxxxxx°xxxx v0.0.0 Filling • 20220828°1025 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate JavaScript string formatting
 *  summary    :
 *  ref        : https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Text_formatting [ref 20220907°1116]
 *  ref        : https://www.freecodecamp.org/news/javascript-string-format-how-to-format-strings-in-js/ [ref 20220907°1117]
 *  ref        : https://sebhastian.com/javascript-format-string/ [ref 20220907°1122]
 *  ref        : https://stackoverflow.com/questions/610406/javascript-equivalent-to-printf-string-format [ref 20220907°1125]
 *  ref        : http://www.mredkj.com/javascript/numberFormat.html [ref 20220907°1132]
 *  ref        : https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number/toFixed [ref 20220907°1136]
 *  ref        : https://www.delftstack.com/howto/javascript/convert-number-to-binary-in-javascript/ [ref 20220907°1142]
 *  ref        :
 */
let sVERSION = "v0.0.0"

document.write(`<p>*** Dobrý deň, toto je 'js335format.js' ${sVERSION} — Format .. ***</p>`);
document.write("<pre>\n");





document.write("</pre>\n");
document.write("<p>Dovidenia.</p>");
