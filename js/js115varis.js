﻿/**
 *  file       : id 20220825°1425 — gitlab.com/normai/cheeseburger … js/js115varis.html
 *  version    : • 20220908°0921 v0.1.7 Filling • v0.1.6 20220825°1425 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate ...
 *  summary    :
 */
let sVERSION = "v0.1.7"

document.write(`<p>*** Hello, this is 'js115varis.js' ${sVERSION} — ***</p>`);
document.write("<pre>\n");

a = 123;                                               // Integer
b = 2.34;                                              // Float
c = "Ahoj";                                            // String
d = c + " = ";
e = a + b;
document.write("<p>" + d + e + "</p>");

document.write("</pre>\n");
document.write("<p>Good bye.</p>");
