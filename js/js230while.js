﻿/**
 *  file       : id 20220826°1525 — gitlab.com/normai/cheeseburger …/js/js230while.js
 *  version    : • 20220910°1025 v0.1.7 Filling • v0.1.6 20220826°1525 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate ...
 *  summary    :
 *  ref        : About random see js761random.js
 *  ref        :
 */
let sVERSION = "v0.1.7"

document.write(`<p>*** हैलो, यह है  [Hailo, yah hai] 'js230while.js' ${sVERSION} —  While  loop***</p>\n`)
document.write("<pre>");
document.write("Roll the number six!\n");

// (1) Roll dice first time
iDicenum = Math.floor(Math.random() * 6) + 1;
document.write("Your first number is " + iDicenum + "\n");

// (2) Continue as long no six is rolled
iTimes = 1;
while (iDicenum != 6)
{
   iDicenum = Math.floor(Math.random() * 6) + 1;
   iTimes += 1;
   document.write(" - then " + iDicenum + "\n");
}

document.write("You rolled " + iTimes + " times until number six.\n");

document.write("</pre>\n");
document.write("<p>अलविदा  [Alavida].</p>\n");
