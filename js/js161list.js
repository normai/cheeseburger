﻿/**
 *  file       : id 20220828°1725 — gitlab.com/normai/cheeseburger …/js/js161list.js
 *  version    : • 20220908°1221 v0.1.7 Filling • v0.1.6 20220828°1725 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate a list in JavaScript
 *  summary    : This demonstrate
 *  note       : JavaScript has no List class, we use an array instead
 *  ref        : https://www.w3schools.com/js/js_arrays.asp []
 *  ref        : https://www.delftstack.com/howto/javascript/list-of-objects-in-javascript/ []
 */
let sVERSION = "v0.1.7"

document.write(`<p>*** 你好 (Nǐ hǎo) 'js161list.js' ${sVERSION} — ***</p>`);
document.write("<pre>\n");

// (1) Simple 'list'
// (1.1) Create
let islands = [ "Rügen"                                        // This is an array
               , "Usedom"
                , "Fehmarn"
                 ];
islands.push("Sylt");

// (1.2) Print some infos
document.write("<p>(1.2.1) Type =" + typeof islands + "</p>");
document.write("<p>(1.2.2) Length =" + islands.length + "</p>");
document.write("<p>(1.2.3) Value =" + islands + "</p>");

// (2.3) Iterate
for (i = 0; i < islands.length; i++)
{
    document.write("<p>&nbsp;&nbsp;&nbsp;   - ", islands[i], " (type =", typeof islands[i] + ")</p>");
}


// (1) Complex 'list'
// (1.1) Create
let mixture = ["Pink", true, 123, 2.34, ["Aha", "Oha", "Uhu"]];

// (1.2) Print some infos
document.write("<p>(2.2.1) Type =" + typeof mixture + "</p>");
document.write("<p>(2.2.2) Length =" + mixture.length + "</p>");
document.write("<p>(2.2.3) Value =" + mixture + "</p>");

// (2.3) Iterate
mixture.forEach(function(x)
{
    document.write("<p>&nbsp;&nbsp;&nbsp;   - ", x, " (type = ", typeof x + ")</p>");
});

document.write("</pre>\n");
document.write("<p>再见 [Zàijiàn].</p>");
