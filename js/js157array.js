﻿/**
 *  file       : id 20220831°0825 — gitlab.com/normai/cheeseburger …/js/js157array.js
 *  version    : • 20220908°1121 v0.1.7 Filling • v0.1.6 20220831°0825 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate JavaScript arrays
 *  summary    : This demonstrates JavaScript arrays in three servings:
 *                (1) Plain array with single-elements-definition
 *                (2) Plain array with bulk definition
 *                (3) Plain array using the push method and the forEach method
 *  todo       : Cleanup or setup that 'size info' stuff [todo 20220902°1131]
 */
let sVERSION = "v0.1.7"

document.write(`<p>*** Здравейте [Zdraveĭte] 'js157array.js' ${sVERSION} — ***</p>\n`);
document.write("<pre>\n");

// =====================================================
// (1) Archaic array, separate declaration plus bulky single-elements-definition
// (1.1) Define some array
const mountains = [];                                          // Alternatively use "= new Array();"
mountains[0] = "Zugspitze";
mountains[1] = "Schneefernerkopf";
mountains[2] = "Weather spike";
mountains[3] = "Middle Höllental Peak";                        // BTW. "Hochwanner" is missing

// (1.2) Retrieve size info
document.write("<p>(1.2.1) Mountains array memory consumption: "  // Memory consumption is hard to retrieve,
               /* + Marshal.SizeOf(mountains) */ + "?" + " bytes"  // could only be found by e.g. browser tools
                + "</p>\n");
let iNumberOfMountains = mountains.length;
document.write("<p>(1.2.2) Number of mountains: " + iNumberOfMountains + "</p>\n");

// (1.3) Iterate over and output all elements
for (let i = 0; i < iNumberOfMountains; i++)
{
   document.write("<p> - " + mountains[i] + "</p>\n");
}

// =====================================================
// (2) Archaic array, declaration and definition in one line
// (2.1) Define some array
const rivers = ["Danube", "Elbe", "Main", "Rhine", "Weser"];

// (2.2) Retrieve size info
document.write("<p>(2.2.1) Rivers array memory consumption: ? bytes</p>\n");  // This information is hard to retrieve
let iNumberOfRivers = rivers.length;
document.write("<p>(2.2.2) Number of rivers: " + iNumberOfRivers + "</p>\n");

// (2.3) Iterate over and output all elements
for (let i = 0; i < rivers.length; i++)
{
   document.write("<p> - " + rivers[i] + "</p>\n");
}

// =====================================================
// (3)
// (3.1) Create array of lakes
const lakes = new Array();
lakes.push("Bodensee");
lakes.push("Müritz");
lakes.push("Chiemsee");
lakes.push("Schweriner See");
lakes.push("Starnberger See");
lakes.push("Ammersee");

// (3.2) Retrieve size info
document.write("<p>(3.2.1) Lakes ArrayList memory consumption: "
               /* + sizeof(lakes) */ + "?" + " bytes</p>\n"
                );
iNumberOfLakes = lakes.length;
document.write("<p>(3.2.2) Number of lakes: " + iNumberOfLakes + "</p>\n");

// (3.3) Iterate over and output all elements
lakes.forEach( function(s)                                     // Compatibility: forEach() is ECMAScript5 (ES5)
{
   document.write("<p> - " + s + "</p>\n");
});

document.write("</pre>\n");
document.write("<p>Довиждане [Dovizhdane].</p>\n");
