/**
 *  file       : id 20221001°1315 — gitlab.com/normai/cheeseburger … java/jv151string.java
 *  version    : • 20221009°1715 v0.1.8 Filling • 20221001°1315 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate string syntax
 *  summary    :
 *  ref        : https://www.baeldung.com/java-multiline-string [ref 20221010°0922]
 *  ref        : https://www.digitalocean.com/community/tutorials/java-string-array-to-string [ref 20221010°0923]
 *  todo       : Supplement OS-line-break
 */

import java.util.ArrayList;

class jv151string
{
   private static final String sVERSION = "v0.1.8";
   private static final String sJAVAVER = "(Javac " + System.getProperty("java.runtime.version") + ")";

   public static void main(String[] args)
   {
      System.out.println(String.format("*** Pozdravljeni, tukaj `jv151string.java` %s -- Strings %s ***", sVERSION, sJAVAVER));

      // ====================================================
      // (1) String Syntax

      // (1.1) Syntax — Quote types (and newline)
      System.out.print("(1.1) Use double-quotes \" for strings, single-quotes \' for chars" + System.lineSeparator());

      // (1.2) Syntax — Mulitiline string options — 'Text Block' available since Java 15
      String sMulti = """
Ein Wiesel
saß auf einem Kiesel
inmitten Bachgeriesel.""";
      System.out.println("(1.2) " + sMulti);

      // (1.3) Syntax — String concatenation (is demonstrated again below)
      String sHello = "Hello";
      String sJenny = "Jenny";
      String sGreet = sHello + ' ' + sJenny;
      System.out.println("(1.3) " + sGreet);

      // (1.4) Syntax — String as array of chars
      String sWord = "SilzuZankunKrei";
      char[] chars = sWord.toCharArray();
      System.out.print ("(1.4) String as Array: ");
      for (char c : chars ) {
         System.out.print(Character.toString(c) + ".");                // Character.toString(() seems superflouos
      }
      System.out.println();

      // ==============================================
      // (2) Conversions

      // ref : https://attacomsian.com/blog/java-convert-integer-to-string [ref 20221010°0944]
      System.out.println("(2.1) Number to string: \"" + Integer.toString(1234) + "\", \"" + Double.toString(2.345) + "\"");

      System.out.println("(2.2) String to number:");
      ArrayList<String> nums = new ArrayList<String>();
      nums.add("1234");
      nums.add("2.345");
      nums.add("Tree hundred twenty five");
      Object o;
      for (String sNum : nums) {
         try {
            o = Integer.valueOf(sNum);
         }
         catch (Exception e1) {
            try {
               o = Double.valueOf(sNum);
            }
            catch (Exception e2) {
               o = "Not a number";
            }
         }
         System.out.println("      - \"" + sNum + "\" => " + o + " (" + o.getClass().getSimpleName() + ")");
      }

      System.out.println("Nasvidenje.");
   }
}
