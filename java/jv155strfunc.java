/**
 *  file       : id 20221012°1715 — gitlab.com/normai/cheeseburger …/java/jv155strfunc.java
 *  version    : • 20221013°1115 v0.1.8 Filling • 20221012°1715 v0.1.7 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate String Functions
 *  userstory  :
 *  summary    :
 *  ref        : https://www.baeldung.com/java-trim-alternatives [ref 20221010°0943] Note: Java has only trim(), no leftTrim() or rightTrim()
 */

class jv155strfunc
{
   private static final String sVERSION = "v0.1.8";
   private static final String sJAVAVER = "(Javac " + System.getProperty("java.runtime.version") + ")";

   public static void main(String[] args)
   {
      System.out.println(String.format("*** Hallo, dit is `jv155strfunc.java` %s -- String Functions %s ***", sVERSION, sJAVAVER));

      // () Fodder
      String sWord = "SilzuZankunKrei";

      // (.) String Functions
      // (.1) String length
      System.out.println("(3.1) Length of '" + sWord + "' is " + sWord.length());

      // (.2) Character code to string
      System.out.println("(3.2) Code to char : " + (char) 36 + (char) 37 + (char) 38 + (char) 96 + (char) 97 + (char) 98 + (char) 122 + (char) 123 + (char) 124 + (char) 125 + (char) 126 + (char) 127 + ".");

      // (.3) Substrings
      System.out.println("(3.3) Substrings : " + sWord.substring(0, 5) + " + " + sWord.substring(5, sWord.length() - 4) + " + " + sWord.substring(sWord.length() - 4));

      // (.4) Upper/Lower
      System.out.println("(3.4) Upper/Lower : " + sWord.toLowerCase() + " / " + sWord.toUpperCase());

      // (.5) Trim
      String sBreezy = "    Breeze    ";
      System.out.println("(3.5) Trim \"" + sBreezy + "\" : \"" + sBreezy.replaceAll("^\\s+", "") + "\", \"" + sBreezy.replaceAll("^\\s+", "") + "\", \"" + sBreezy.trim() + "\"");

      System.out.println("Tot ziens.");
   }
}
