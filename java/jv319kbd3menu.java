// THIS FILE IS AN EMPTY STUB, IT IS YET TO BE FILLED WITH CODE

/**
 *  file       : id 20221116°1615 — gitlab.com/normai/cheeseburger …/java/jv319kbd3menu.java
 *  version    : • 20xxxxxx°xxxx v0.x.x Filling • 20221116°1615 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate keyboard menu using a GUI function
 *  userstory  :
 *  summary    :
 */

class jv319kbd3menu
{
   private static final String sVERSION = "v0.0.0";
   private static final String sJAVAVER = "(Javac " + System.getProperty("java.runtime.version") + ")";

   public static void main(String[] args)
   {
      System.out.println(String.format("*** Afto einai to `jv319kbd3menu.java` %s -- Keyboard menu (GUI func) %s ***", sVERSION, sJAVAVER));  // "Αυτό είναι το [Aftó eínai to]"





      System.out.println("Antio sas.");  // "Αντίο σας [Antio sas]."
   }
}
