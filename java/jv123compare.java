/**
 *  file       : id 20220215°1011 — gitlab.com/normai/cheeseburger …/java/jv123compare.java
 *  version    : • 20220908°1651 v0.1.7 Filling • 20220215°1011 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate comparison operators
 *  summary    :
 *  ref        : https://www.w3schools.com/java/java_operators.asp [ref 20220906°1332] General operators overview
 *  ref        : https://howtodoinjava.com/java-examples/correctly-compare-float-double/ [ref 20220906°1333]
 *  ref        :
 */

import java.lang.Math;

class jv123compare
{
   private static final String sVERSION = "v0.1.7";
   private static final String sJAVAVER = "(Javac " + System.getProperty("java.runtime.version") + ")";

   public static void main(String[] args)
   {
      // Problem with threshold solution: The threshold has to be different, depending on the magnitude of the number!
      final double THRESHOLD_0 = .000_1;                        // The value used in ref 20220906°1333
      final double THRESHOLD_1 = .000_000_000_000_000_1;        // Smallest value working on my machine (Java 11 on Win 64), for this example
      final double THRESHOLD_2 = .000_000_000_000_000_01;       // Fails on on my machine, is too small, for this example

      System.out.println(String.format("*** Tere, see on `jv123compare.java` %s -- Compare %s ***", sVERSION, sJAVAVER));

      System.out.println("(1.1)  \"Ahoj\" == \"Aloha\"         = " + ("Ahoj" == "Aloha"));
      System.out.println("(1.2)  12345  == 23456           = " + (12345  == 23456));
      System.out.println("(1.3)  3.4567 == 4.5678          = " + (3.4567 == 4.5678));
      System.out.println("(1.4)  true   == false           = " + (true   == false));
      System.out.println();
      System.out.println("(2.1)  \"Ahoj\" != \"Aloha\"         = " + ("Ahoj" != "Aloha"));
      System.out.println("(2.2)  12345  != 23456           = " + (12345  != 23456));
      System.out.println("(2.3)  3.4567 != 4.5678          = " + (3.4567 != 4.5678));
      System.out.println("(2.4)  true   != false           = " + (true   != false));
      System.out.println();
      System.out.println("(3.1)  \"Ahoj\".compareTo(\"Aloha\") = " + "Ahoj".compareTo("Aloha"));
      System.out.println("(3.2)  12345  >  23456           = " + (12345  > 23456));
      System.out.println("(3.3)  3.4567 >  4.5678          = " + (3.4567 > 4.5678));
//    System.out.println("(3.4)  true   >  false           = " + (true   > false));
      System.out.println();
//    System.out.println("(4.1)  \"Ahoj\" >= \"Aloha\"         = " + ("Ahoj" >= "Aloha"));
      System.out.println("(4.2)  12345  >= 23456           = " + (12345  >= 23456));
      System.out.println("(4.3)  3.4567 >= 4.5678          = " + (3.4567 >= 4.5678));
//    System.out.println("(4.4)  true   >= false           = " + (true   >= false));
      System.out.println();
//    System.out.println("(5.1)  \"Ahoj\" <  \"Aloha\"         = " + ("Ahoj" < "Aloha"));
      System.out.println("(5.2)  12345  <  23456           = " + (12345  < 23456));
      System.out.println("(5.3)  3.4567 <  4.5678          = " + (3.4567 < 4.5678));
//    System.out.println("(5.4)  true   <  false           = " + (true   < false));
      System.out.println();
//    System.out.println("(6.1)  \"Ahoj\" >= \"Aloha\"         = " + ("Ahoj" >= "Aloha"));
      System.out.println("(6.2)  12345  >= 23456           = " + (12345  >= 23456));
      System.out.println("(6.3)  3.4567 >= 4.5678          = " + (3.4567 >= 4.5678));
//    System.out.println("(6.4)  true   >= false           = " + (true   >= false));
      System.out.println();
      System.out.println("(7.1)  0.1 + 0.2 == 0.3          = " + (0.1 + 0.2 == 0.3));
      System.out.println("(7.2)  Math.abs((0.1 + 0.2) - 0.3) < THRESHOLD_1 = " + (Math.abs((0.1 + 0.2) - 0.3) < THRESHOLD_1));
      System.out.println();
      System.out.println("(8.1)  \"Ahoj\".compareTo(\"Aloha\") = " + "Ahoj".compareTo("Aloha"));
      System.out.println("(8.1)  \"Aloha\".compareTo(\"Ahoj\") = " + "Aloha".compareTo("Ahoj"));
      System.out.println("(8.1)  \"Ahoj\".compareTo(\"Ahoj\")  = " + "Ahoj".compareTo("Ahoj"));


      System.out.println("Hüvasti.");
   }
}
