/**
 *  file       : id 20221012°1115 — gitlab.com/normai/cheeseburger …/java/jv145float.java
 *  version    : • 20221216°1111 v0.1.8 Filling • 20221012°1115 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate Float Type
 *  status     : Has room for enhancement. Should use string formatting for number output.
 */

class jv145float
{
   private static final String sVERSION = "v0.1.8";
   private static final String sJAVAVER = "(Javac " + System.getProperty("java.runtime.version") + ")";


   public static void main(String[] args)
   {
      System.out.println(String.format("*** Dobry den, toto je `jv145float.java` %s -- Float Type %s ***", sVERSION, sJAVAVER));  // "Dobrý den, toto je"


      System.out.println("(1) Print some numbers:");
      double f1 = 123_456.789;
      double f2 = -123_456.789;
      double f3 = 123_456_777_777_777_777_777_777_777.789;
      double f4 = -123_456_777_777_777_777_777_777_777.789;
      System.out.println(String.format("(1.1) f1 = %.2f %s", f1, ((Object) f1).getClass().getSimpleName()));
      System.out.println(String.format("(1.2) f2 = %.2f %s", f2, ((Object) f2).getClass().getSimpleName()));
      System.out.println(String.format("(1.3) f3 = %.2f %s", f3, ((Object) f3).getClass().getSimpleName()));
      System.out.println(String.format("(1.4) f4 = %.2f %s", f4, ((Object) f4).getClass().getSimpleName()));


      System.out.println("Nashledanou.");
   }
}
