/**
 *  file       : id 20221002°0915 — gitlab.com/normai/cheeseburger …/java/jv127ternary.java
 *  version    : • 20221227°1612 v0.1.9 Filling • 20221002°0915 v0.1.8 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate ternary operator
 *  userstory  : Roll dice and tell whether it was greater than three or not
 */

import java.util.Random;

class jv127ternary
{
   private static final String sVERSION = "v0.1.9";
   private static final String sJAVAVER = "(Javac " + System.getProperty("java.runtime.version") + ")";

   public static void main(String[] args)
   {
      System.out.println(String.format("*** Hej, det haer aer `jv127ternary.java` %s -- Ternary operator %s ***", sVERSION, sJAVAVER));  // "Hej, det här är"

      // Preparation
      Random random = new Random();
      int iRand = random.nextInt(6) + 1;
      System.out.println("(1.1) You rolled " + String.valueOf(iRand));

      // The demo line
      System.out.println("(1.2) You rolled greater three: " + (iRand > 3 ? "Yes" : "No"));


      System.out.println("Adjoe.");  // "Adjö"
   }
}
