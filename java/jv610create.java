// THIS FILE IS AN EMPTY STUB, IT IS YET TO BE FILLED WITH CODE

/**
 *  file       : id 20220831°1515 — gitlab.com/normai/cheeseburger …/java/jv610create.java
 *  version    : • 20xxxxxx°xxxx v0.x.x Filling • 20220831°1515 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate polymorphism
 *  summary    :
 */

class jv610create
{
   private static final String sVERSION = "v0.0.0";
   private static final String sJAVAVER = "(Javac " + System.getProperty("java.runtime.version") + ")";

   public static void main(String[] args)
   {
      System.out.println(String.format("*** Hallo, hier ist `jv610create.java` %s -- %s ***", sVERSION, sJAVAVER));





      System.out.println("Auf Wiedersehen.");
   }
}
