/**
 *  file       : id 20220826°1115 — gitlab.com/normai/cheeseburger …/java/jv125bitwise.java
 *  version    : • 20241004°1701 v0.1.8 Fix alignment • 20220908°1751 v0.1.7 Filling • 20220826°1115 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate bitwise operators
 *  summary    :
 *  ref        : https://www.w3schools.in/java/operators/bitwise [ref 20220907°1032]
 *  ref        : https://www.baeldung.com/java-bitwise-operators [ref 20220907°1033]
 *  ref        : https://docs.oracle.com/javase/tutorial/java/nutsandbolts/op3.html [ref 20220907°1034]
 *  ref        : https://stackoverflow.com/questions/1841461/unsigned-short-in-java [ref 20220907°1045]
 *  ref        : https://www.tutorialspoint.com/java-program-to-convert-int-to-binary-string [ref 20220907°1046]
 *  ref        : https://www.delftstack.com/howto/java/java-convert-int-to-binary/ [ref 20220907°1047]
 */

class jv125bitwise
{
    private static final String sVERSION = "v0.1.8";
    private static final String sJAVAVER = "(Javac " + System.getProperty("java.runtime.version") + ")";

    public static void main(String[] args)
    {
        int i1 = 43690;                                         // Java has no unsigned ints except the 16-bit char. The short cannot store 43000, so we use int
        int i2 = 3855;

        System.out.println(String.format("*** Hei, taemae on `jv125bitwise.java` %s -- Bitwise operators %s ***", sVERSION, sJAVAVER));  // "Hei, tämä on"

        System.out.println("(1) Bitwise AND");
        System.out.println("(1.1) i1        = " + String.format("%33s", Integer.toBinaryString(i1))      + " " + String.format("%9x", i1)      + " " + String.format("%8d", i1));
        System.out.println("(1.2) i2        = " + String.format("%33s", Integer.toBinaryString(i2))      + " " + String.format("%9x", i2)      + " " + String.format("%8d", i2));
        System.out.println("(1.3) i1 & i2   = " + String.format("%33s", Integer.toBinaryString(i1 & i2)) + " " + String.format("%9x", i1 & i2) + " " + String.format("%8d", i1 & i2));

        System.out.println("(2) Bitwise OR");
        System.out.println("(2.1) i1        = " + String.format("%33s", Integer.toBinaryString(i1))      + " " + String.format("%9x", i1)      + " " + String.format("%8d", i1));
        System.out.println("(2.2) i2        = " + String.format("%33s", Integer.toBinaryString(i2))      + " " + String.format("%9x", i2)      + " " + String.format("%8d", i2));
        System.out.println("(2.3) i1 | i2   = " + String.format("%33s", Integer.toBinaryString(i1 | i2)) + " " + String.format("%9x", i1 | i2) + " " + String.format("%8d", i1 | i2));

        System.out.println("(3) Bitwise XOR");
        System.out.println("(3.1) i1        = " + String.format("%33s", Integer.toBinaryString(i1))      + " " + String.format("%9x", i1)      + " " + String.format("%8d", i1));
        System.out.println("(3.2) i2        = " + String.format("%33s", Integer.toBinaryString(i2))      + " " + String.format("%9x", i2)      + " " + String.format("%8d", i2));
        System.out.println("(3.3) i1 ^ i2   = " + String.format("%33s", Integer.toBinaryString(i1 ^ i2)) + " " + String.format("%9x", i1 ^ i2) + " " + String.format("%8d", i1 ^ i2));

        System.out.println("(4) Bitwise NOT");
        System.out.println("(4.1) i1        = " + String.format("%33s", Integer.toBinaryString(i1))      + " " + String.format("%9x", i1)      + " " + String.format("%8d", i1));
        System.out.println("(4.2) ~i1       = " + String.format("%33s", Integer.toBinaryString(~i1))     + " " + String.format("%9x", ~i1)     + " " + String.format("%8d", ~i1));

        System.out.println("(5) Shift Left");
        System.out.println("(5.1) i1        = " + String.format("%33s", Integer.toBinaryString(i1))      + " " + String.format("%9x", i1)      + " " + String.format("%8d", i1));
        System.out.println("(5.2) i1 << 1   = " + String.format("%33s", Integer.toBinaryString(i1 << 1)) + " " + String.format("%9x", i1 << 1) + " " + String.format("%8d", i1 << 1));

        System.out.println("(6) Shift Right");
        System.out.println("(6.1) i1        = " + String.format("%33s", Integer.toBinaryString(i1))      + " " + String.format("%9x", i1)      + " " + String.format("%8d", i1));
        System.out.println("(6.2) i1 >> 1   = " + String.format("%33s", Integer.toBinaryString(i1 >> 1)) + " " + String.format("%9x", i1 >> 1) + " " + String.format("%8d", i1 >> 1));

        System.out.println("Naekemiin.");  // "Näkemiin"
    }
}
