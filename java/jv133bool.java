/**
 *  file       : id 20221012°0815 — gitlab.com/normai/cheeseburger …/java/jv133bool.java
 *  version    : • 20221014°1115 v0.1.8 Filling • 20221012°0815 v0.1.7 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate Boolean Type
 *  userstory  :
 *  summary    :
 *  ref        : https://stackabuse.com/java-check-if-array-contains-value-or-element/ [ref 20221014°0933] Nice article, but they miss providing the import directives
 */

import java.util.*;  // List

class jv133bool
{
   private static final String sVERSION = "v0.1.8";
   private static final String sJAVAVER = "(Javac " + System.getProperty("java.runtime.version") + ")";

   public static void main(String[] args)
   {
      System.out.println(String.format("*** Pryvit, tse `jv133bool.java` %s -- Boolean Type %s ***", sVERSION, sJAVAVER));

      // (1) The naive test series
      System.out.println("(1) Naive test series");
      boolean bFalse = false;
      boolean bTrue = true;
      int iMinus = -1;
      int iZero = 0;
      int iOne = 1;
      int iTwo = 2;
      double fMinus = -0.1;
      double fZero = 0.0;
      double fOne = 0.1;
      double fTwo = 1.23;
      //System.out.println("(1.1)  (boolean) None = " + (boolean) bNone);
      System.out.println("(1.2)  False          = " + bFalse);
      System.out.println("(1.3)  True           = " + bTrue);
      //System.out.println("(1.4)  (boolean) -1   = " + (boolean) iMinus); // Compiler error "incompatible types: int cannot be converted to boolean"
      //System.out.println("(1.5)  (boolean) 0    = " + (boolean) iZero);
      //System.out.println("(1.6)  (boolean) 1    = " + (boolean) iOne);
      //System.out.println("(1.7)  (boolean) 2    = " + (boolean) iTwo);
      //System.out.println("(1.8)  (boolean) -0.1 = " + (boolean) fMinus);  // Compiler error "incompatible types: double cannot be converted to boolean"
      //System.out.println("(1.9)  (boolean) 0.0  = " + (boolean) fZero);
      //System.out.println("(1.10) (boolean) 0.1  = " + (boolean) fOne);
      //System.out.println("(1.11) (boolean) 1.23 = " + (boolean) fTwo);
      //System.out.println("(1.12) (boolean) ''   = " + (boolean) "");     // Compiler error "incompatible types: string cannot be converted to boolean"
      //System.out.println("(1.13) (boolean) 'x'  = " + (boolean) "x");

      // (2) Targeted conversion
      System.out.println("(2) Targeted conversion:");
      String[] inputs = { "", "Bla", "Ja", "No", "True", "Yes" };
      String[] trues = new String[]{ "1", "j", "ja", "t", "true", "y", "yes" };
      List<String> vals2 = new ArrayList<>(Arrays.asList(trues));
      for (String x : inputs) {
         boolean b = vals2.contains(x.toLowerCase());
         System.out.println(" -" + x + " -> " + b);
      }

      System.out.println("Do pobachennya.");
   }
}
