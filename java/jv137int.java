/**
 *  file       : id 20221012°1015 — gitlab.com/normai/cheeseburger …/java/jv137int.java
 *  version    : • 20221018°1115 v0.1.8 Filling • 20221012°1015 v0.1.7 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate Integer Type
 *  summary    :
 *  compile    : > javac.exe jv137int.java
 *  userstory  :
 *  ref        : https://www.demo2s.com/java/java-integer-types.html [ref 20221016°1222]
 *  ref        : https://docs.oracle.com/javase/8/docs/api/java/lang/Integer.html [ref 20221016°1232]
 *  ref        : docs.oracle.com/javase/tutorial/java/nutsandbolts/datatypes.html [ref 20221015°1122]
 *  ref        : https://www.delftstack.com/howto/java/java-unsigned-int/ [ref 20221016°1233]
 *  ref        : https://stackoverflow.com/questions/9854166/declaring-an-unsigned-int-in-java [ref 20221016°1234]
 *  status     :
 */

class jv137int
{
   private static final String sVERSION = "v0.1.8";
   private static final String sJAVAVER = "(Javac " + System.getProperty("java.runtime.version") + ")";

   public static void main(String[] args)
   {
      System.out.println(String.format("*** Ni hao, zhe shi `jv137int.java` %s -- Integer Type %s ***", sVERSION, sJAVAVER));  // "N_ h_o, zh_ sh_" Compiler errors "unmappable character (0x90) for encoding windows-1252"

      byte i11 = -128;
      byte i12 = 127;
      byte i13 = (byte) (i12 + 1);
      short i21 = -32_768;
      short i22 = 32_767;
      short i23 = (short) (i22 + 1);
      int i31 = -2_147_483_648;
      int i32 = 2_147_483_647;
      int i33 = (int) (i32 + 1);
      long i41 = Long.MIN_VALUE;                                        // Curiously, using "-9_223_372_036_854_775_808" yields compiler error 'too long'
      long i42 = Long.MAX_VALUE;                                        // Curiously, using "9_223_372_036_854_775_807" yields compiler error 'too long'
      long i43 = (long) (i42 + 1);

      System.out.println("(1.1) i11     = " + i11 + " " + ((Object) i11).getClass().getSimpleName());
      System.out.println("(1.2) i12     = " + i12 + " " + ((Object) i12).getClass().getSimpleName());
      System.out.println("(1.3) i12 + 1 = " + i13 + " " + ((Object) i13).getClass().getSimpleName());
      System.out.println("(2.1) i21     = " + i21 + " " + ((Object) i21).getClass().getSimpleName());
      System.out.println("(2.2) i22     = " + i22 + " " + ((Object) i22).getClass().getSimpleName());
      System.out.println("(2.3) i22 + 1 = " + i23 + " " + ((Object) i23).getClass().getSimpleName());
      System.out.println("(3.1) i31     = " + i31 + " " + ((Object) i31).getClass().getSimpleName());
      System.out.println("(3.2) i32     = " + i32 + " " + ((Object) i32).getClass().getSimpleName());
      System.out.println("(3.3) i32 + 1 = " + i33 + " " + ((Object) i33).getClass().getSimpleName());
      System.out.println("(4.1) i41     = " + i41 + " " + ((Object) i41).getClass().getSimpleName());
      System.out.println("(4.2) i42     = " + i42 + " " + ((Object) i42).getClass().getSimpleName());
      System.out.println("(4.3) i42 + 1 = " + i43 + " " + ((Object) i43).getClass().getSimpleName());

      System.out.println("(5.1) Byte.MIN/MAX_VALUE = " + Byte.MIN_VALUE + " / " + Byte.MAX_VALUE);
      System.out.println("(5.2) Small.MIN/MAX_VALUE = " + Short.MIN_VALUE + " / " + Short.MAX_VALUE);
      System.out.println("(5.3) Integer.MIN/MAX_VALUE = " + Integer.MIN_VALUE + " / " + Integer.MAX_VALUE);
      System.out.println("(5.4) Long.MIN/MAX_VALUE = " + Long.MIN_VALUE + " / " + Long.MAX_VALUE);

      System.out.println("Zaijian.");                                // "xxx (Zaijian)" compiler errors "unmappable character (0x81) for encoding windows-1252"
   }
}
