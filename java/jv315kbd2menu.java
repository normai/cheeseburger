// THIS FILE IS AN EMPTY STUB, IT IS YET TO BE FILLED WITH CODE

/**
 *  file       : id 20221116°1515 — gitlab.com/normai/cheeseburger …/java/jv315kbd2menu.java
 *  version    : • 20xxxxxx°xxxx v0.1.8 Filling • 20221116°1515 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate keyboard menu without pressing Enter
 *  userstory  :
 *  summary    :
 */

class jv315kbd2menu
{
   private static final String sVERSION = "v0.0.0";
   private static final String sJAVAVER = "(Javac " + System.getProperty("java.runtime.version") + ")";

   public static void main(String[] args)
   {
      System.out.println(String.format("*** Hier ist `jv315kbd2menu.java` %s -- Keyboard menu (no Enter) %s ***", sVERSION, sJAVAVER));





      System.out.println("Auf Wiedersehen.");
   }
}
