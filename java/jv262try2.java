/**
 *  file       : id 20220827°0915 — gitlab.com/normai/cheeseburger …/java/jv262try2.java
 *  version    : • 20220915°1021 v0.1.8 Filling • 20220827°0915 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate try/except/finally
 *  summary    : This is one single try block with a sequence of tasks. With
 *               each pass, one more case succeeds to execute. The sequence is:
 *                • (1) Division (fails if divisor is zero)
 *                • (2) Calculate square root (fails if done with negative number)
 *                • (3) Array access (fails if index out of bounds)
 *                • (4) Open file (fails if file does not exist)
 *                • (5) Output all runs fine (only reached if non of above failed)
 *               The catch clause is divided into a catch cascade for specific exceptions.
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

class jv262try2
{
   private static final String sVERSION = "v0.1.8";
   private static final String sJAVAVER = "(Javac " + System.getProperty("java.runtime.version") + ")";

   public static void main(String[] args) throws IOException
   {
      System.out.println(String.format("*** Sveiki, sis ir `jv262try2.java` %s -- Try/Catch (full) %s ***", sVERSION, sJAVAVER));  // "Sveiki, šis ir" does not print nice


      // Process five cases
      int canaries[] = {0, -2, 3, 4, 1};
      for (int iCanary : canaries)
      {
         System.out.println();
         System.out.println("Case with number = " + Integer.toString(iCanary) + ":");

         try
         {
            //----------------------------------------------
            // (1) Try division
            System.out.println("(1.1) Try division");
            Double nRatio = 123.4 / iCanary;
            System.out.println("(1.2) Calculation: " + Double.toString(123.4) + " / " + Integer.toString(iCanary) + " = " + Double.toString(nRatio));
            if (Double.isInfinite(nRatio))
            {
               throw new DivisionByZeroException("Division yielded unusable result.");
            }

            //----------------------------------------------
            // (2) Try calculate square root
            System.out.println("(2.1) Try square root");
            double dSqrt = Math.sqrt(iCanary);
            System.out.println("(2.2) Calculation: Square root of " + Integer.toString(iCanary) + " = " + Double.toString(dSqrt));
            if (Double.isNaN(dSqrt))
            {
               throw new NumberIsNanException("Calculation yielded unusable result.");
            }

            //----------------------------------------------
            // (3) Array out of bounds
            System.out.println("(3) Array bounds");
            String[] ar = {"Oins", "Zwoi", "Droi"};
            if (iCanary > ar.length)                                            // Missing the true boundary by one
            {
               System.out.println("(3.1)   Array access skipped because index is greater than array.");
            }
            else
            {
               System.out.println("(3.2)   ar[" + Integer.toString(iCanary) + "] = " + ar[iCanary]);
            }

            //----------------------------------------------
            // (4) Try open file
            String sFile = iCanary == 4 ? ".\\detjibbetnich.txt" : ".\\jv262try2.java";
            System.out.println("(4.1) Try open file \"" + sFile);

            File fFile = new File(sFile);
            Scanner scnr = new Scanner(fFile);
            ////while (scnr.hasNextLine();)
            ////{
            ////   String sLine = scnr.nextLine();
            ////   System.out.println(sLine);
            ////}
            String sLine = scnr.nextLine();
            System.out.println("(4.2) Read first line: \"" + sLine + "\"");

            //----------------------------------------------
            // (5) All fine
            System.out.println("(5) All was fine, no exception was thrown.");

         }
         catch (DivisionByZeroException ex)
         {
            System.out.println("(6.1) Caught " + ex);
         }
         catch (Exception ex)
         {
            System.out.println("(6.2) Caught " + ex);
         }
         finally {
            System.out.println("(7) Finally.");
         }
      }

      System.out.println("Uz redzesanos.");  // "Uz redzēšanos" does not print nice
   }
}

class DivisionByZeroException extends Exception
{ 
   public DivisionByZeroException(String sMsg)
   {
      super(sMsg);
   }
}

class NumberIsNanException extends Exception
{ 
   public NumberIsNanException(String sMsg)
   {
      super(sMsg);
   }
}
