/**
 *  file       : id 20221215°1115 — gitlab.com/normai/cheeseburger …/java/jvfactorial.java
 *  version    : • 20221216°0922 v0.1.8 Filling • 20221215°1115 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate factorial calculation
 *  status     : Not ready yet. Wrong results are not yet checked.
 */

class jvfactorial
{
   private static final String sVERSION = "v0.1.8";
   private static final String sJAVAVER = "(Javac " + System.getProperty("java.runtime.version") + ")";


   // Function 20221215°1341 Recursion naive
   static long Factorial(long iVal) {
      if (iVal > 1) {
         iVal = iVal * Factorial(iVal - 1);
         return iVal;
      }
      else {
         return 1;
      }
   }

   public static void main(String[] args)
   {
      System.out.println(String.format("*** Sveiki, tai `jvfactorial.java` %s -- Factorial %s ***", sVERSION, sJAVAVER));
      System.out.println("Hint: Java max. long = " + Long.MAX_VALUE + ".");

      for (long i = -1; i < 24; i++) {
          long iFact = Factorial(i);
          System.out.println(" - " + i + "! = " + iFact);
      }

      System.out.println("Iki pasimatymo.");
   }
}
