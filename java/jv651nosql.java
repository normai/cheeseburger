// EMPTY STUB, YET TO BE FILLED WITH CODE

/**
 *  file       : id 20221223°1215 — gitlab.com/normai/cheeseburger …/java/jv651nosql.java
 *  version    : • 20xxxxxx°xxxx v0.x.x Filling • 20221223°1215 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate NoSQL database
 *  status     :
 */

class jv651nosql
{
   private static final String sVERSION = "v0.0.0";
   private static final String sJAVAVER = "(Javac " + System.getProperty("java.runtime.version") + ")";

   public static void main(String[] args)
   {
      System.out.println(String.format("*** To je `jv651nosql.java` %s -- NoSQL %s ***", sVERSION, sJAVAVER));





      System.out.println("Nasvidenje.");
   }
}
