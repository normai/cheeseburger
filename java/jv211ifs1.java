/**
 *  file       : id 20220826°1215 — gitlab.com/normai/cheeseburger …/java/jv211ifs1.java
 *  version    : • 20220922°1815 v0.1.8 Filling • 20220826°1215 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate Python branching
 *  summary    : This demonstrates If/ElseIf/Else with four cases:
 *                • Unilateral branching
 *                • Bilateral branching
 *                • Multilateral branching
 *                • Wrong branching logic
 *               The sequence goes like this (1) Create random integer (2) Let the user
 *               overwrite this number (3) Do four different evaluations on that number
 *  usage      : Commands "javac.exe jv211ifs1.java", "java.exe jv211ifs1"
 *  ref        : https://www.baeldung.com/java-check-string-number []
 */

import java.util.Random;

class jv211ifs1
{
   private static final String sVERSION = "v0.1.8";
   private static final String sJAVAVER = "(Javac " + System.getProperty("java.runtime.version") + ")";

   public static void main(String[] args)
   {
      System.out.println(String.format("*** Hallo, hier ist `jv211ifs1.java` %s -- If/ElseIf/Else %s ***", sVERSION, sJAVAVER));

      // (A.1) Preparation — Get a random number rolled
      Random random = new Random();
      int randi = random.nextInt(6) + 1;
      System.out.println("You rolled number " + String.valueOf(randi));

      // (A.2) Optionally allow to manually choose the number
      if (false) {                                                  // Toggle this
         System.out.print("Overwrite the rolled number with 1..6 >");
         java.util.Scanner input = new java.util.Scanner(System.in);
         String s = input.nextLine();
         try {
            randi = Integer.parseInt(s);
            randi = randi < 1 ? 1 : randi;
            randi = randi > 6 ? 6 : randi;
         }
         catch (NumberFormatException nfe) {
            System.out.println("Your input was not a number.");
         }
         System.out.println("Your number now is '" + String.valueOf(randi) + "'.");
      }

      // (B.1) Unilateral branching
      System.out.println("(1) Unilateral branching");
      if (randi > 3) {
         System.out.println("(1.1) : Your number is bigger than three");
      }

      // (B.2) Two-way branching
      System.out.println("(2) Bilateral branching");
      if (randi == 3) {
         System.out.println("(2.1) : The number is three");
      }
      else {
         System.out.println("(2.2) : The number is not three");
      }

      // (B.3) Multilateral branching — Only one branch will be executed
      System.out.println("(3) Multilateral branching");
      if (randi > 5) {
         System.out.println("(3.1) : The number is six");
      }
      else if (randi > 4) {
         System.out.println("(3.2) : The number is five");
      }
      else if (randi > 3) {
         System.out.println("(3.3) : The number is four");
      }
      else if (randi > 2) {
         System.out.println("(3.4) : The number is three");
      }
      else {
         System.out.println("(3.5) : The number is one or two");
      }

      // (B.4) A typical beginners mistake, looks very similar like above,
      //  has multiple if instead elif statements, will not work as intended.
      System.out.println("(4) Wrong logic");
      if (randi > 5) {
         System.out.println("(4.1) : The number is six");
      }
      if (randi > 4) {
         System.out.println("(4.2) : The number is five or six");
      }
      if (randi > 3) {
         System.out.println("(4.3) : The number is four to six");
      }
      if (randi > 2) {
         System.out.println("(4.4) : The number is three to six");
      }
      else {
         System.out.println("(4.5) : The number is one or two");
      }

      System.out.println("Auf Wiedersehen.");
   }
}
