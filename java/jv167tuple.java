/**
 *  file       : id 20220829°0915 — gitlab.com/normai/cheeseburger …/java/jv167tuple.java
 *  versions   : • 20220906°0820 v0.1.7 Filling • 20220829°0915 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate Tuple in Java
 *  prerequist : Library javatuples-1.2.jar
 *  usage      : Run with jv167tuple.java.bat since classpath must be set
 *  summary    : Java tuples are a bit different from Python tuples
 */

import org.javatuples.*;

class jv167tuple
{
    private static final String sVERSION = "v0.1.7";
    private static final String sJAVAVER = "(Javac " + System.getProperty("java.runtime.version") + ")";

    public static void main(String[] args)
    {
        System.out.println(String.format("*** Hallo, dit is `jv167tuple.java` %s -- Tuples %s ***", sVERSION, sJAVAVER));

        // (1) Create
        Unit<String> ding = new Unit<>("Ding");
        Quintet<String, Integer, Double, Boolean, Unit> tpl = new Quintet<>("Values", 1234, 2.345, true, ding);
        System.out.println("(1) tpl = " + tpl);

        // (2) Info
        System.out.println("(2.1) Size = " + tpl.getSize());
        System.out.println("(2.2) Type = " + tpl.getClass());

        // (3) Iterate
        System.out.println("(3) Iterate:");
        for (Object o : tpl)
        {
            System.out.println("     - " + o + " (type = " + o.getClass() + ")");
        }

        // (4) Pick by index
        System.out.println("(4.1) Third item = " + tpl.getValue(2));
        Object oUnit = tpl.getValue(4);                         // "Unit<String> oUnit1 = tpl.getValue(4)" yields compiler error "Incompatible types. Object cannot be converted to Unit<String>" -- Todo: Try to get oUnit1 as Unit<String>, so one can extract the String.
        System.out.println("(4.2) First of fifth = " + oUnit);

        System.out.println("Tot ziens.");
    }
}
