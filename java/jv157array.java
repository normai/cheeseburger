/**
 *  file       : id 20220831°0815 — gitlab.com/normai/cheeseburger …/java/jv157array.java
 *  version    : • 20220908°1131 v0.1.7 Filling • 20220831°0815 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    :
 *  summary    :
 */

import java.util.ArrayList;

class jv157array
{
   private static final String sVERSION = "v0.1.7";
   private static final String sJAVAVER = "(Javac " + System.getProperty("java.runtime.version") + ")";

    public static void main(String[] args)
    {
        System.out.println(String.format("*** Zdraveite `jv157array.java` %s -- Arrays %s ***", sVERSION, sJAVAVER));  // "Здравейте [Zdraveĭte]"

        // =====================================================
        // (1) Archaic array, separate declaration plus bulky single-elements-definition
        // (1.1) Define some array
        String[] mountains = new String[4];
        mountains[0] = "Zugspitze";
        mountains[1] = "Schneefernerkopf";
        mountains[2] = "Weather spike";
        mountains[3] = "Middle Höllental Peak";                        // BTW. "Hochwanner" is missing

        // (1.2) Retrieve size info
        System.out.println( "(1.2.1) Mountains array memory consumption: "  // Memory consumption of managed array is hard to retrieve
                           /* + Marshal.SizeOf(mountains) */ + "?" + " bytes"  // Marshal.SizeOf() works only with unmanaged objects
                            );
        int iNumberOfMountains = mountains.length;
        System.out.println("(1.2.2) Number of mountains: " + iNumberOfMountains);

        // (1.3) Iterate over and output all elements
        for (int i = 0; i < iNumberOfMountains; i++)
        {
           System.out.println(" - " + mountains[i]);
        }

        // =====================================================
        // (2) Archaic array, declaration and definition in one line
        // (2.1) Define some array
        String[] rivers = {"Danube", "Elbe", "Main", "Rhine", "Weser"};

        // (2.2) Retrieve size info
        System.out.println("(2.2.1) Rivers array memory consumption: ? bytes");  // This information is hard to retrieve for managed objects
        int iNumberOfRivers = rivers.length;
        System.out.println("(2.2.2) Number of rivers: " + iNumberOfRivers);

        // (2.3) Iterate over and output all elements
        for (int i = 0; i < rivers.length; i++)
        {
           System.out.println(" - " + rivers[i]);
        }

        // =====================================================
        // (3) List class, the nearest container class array equivalent
        // (3.1) Create list of lakes
        ArrayList<String> lakes = new ArrayList<String>();
        lakes.add("Bodensee");
        lakes.add("Müritz");
        lakes.add("Chiemsee");
        lakes.add("Schweriner See");
        lakes.add("Starnberger See");
        lakes.add("Ammersee");

        // (3.2) Retrieve size info
        System.out.println( "(3.2.1) Lakes ArrayList memory consumption: "
                          /* + sizeof(lakes) */ + "?" + " bytes"
                           );
        int iNumberOfLakes = lakes.size();
        System.out.println("(3.2.2) Number of lakes: " + iNumberOfLakes);

        // (3.3) Iterate over and output all elements
        for (String s : lakes)
        {
           System.out.println(" - " + s);
        }

        System.out.println("Dovizhdane.");  // "Довиждане [Dovizhdane]."
    }
}
