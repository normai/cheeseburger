// THIS FILE IS AN EMPTY STUB, IT IS YET TO BE FILLED WITH CODE

/**
 *  file       : id 20220827°1215 — gitlab.com/normai/cheeseburger …/java/jv286compars.java
 *  version    : • 20xxxxxx°xxxx v0.x.x Filling • 20220827°1215 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate some comparison features
 *  summary    :
 */

class jv286compars
{
   private static final String sVERSION = "v0.0.0";
   private static final String sJAVAVER = "(Javac " + System.getProperty("java.runtime.version") + ")";

   public static void main(String[] args)
   {
      System.out.println(String.format("*** Kia ora, ko tenei `jv286compars.java` %s -- Comparisons %s ***", sVERSION, sJAVAVER));





      System.out.println("Tena koe.");  // Need hex representation, otherwise compiler error "unmappable character …"
   }
}
