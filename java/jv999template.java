// EMPTY STUB, YET TO BE FILLED WITH CODE

/**
 *  file       : id 20220828°1615 — gitlab.com/normai/cheeseburger …/java/jv999template.java
 *  version    : • 20xxxxxx°xxxx v0.x.x Filling • 20220828°1615 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate ...
 *  status     :
 */

class jv999template
{
   private static final String sVERSION = "v0.0.0";
   private static final String sJAVAVER = "(Javac " + System.getProperty("java.runtime.version") + ")";

   public static void main(String[] args)
   {
      System.out.println(String.format("*** Buna, acesta este `jv999template.java` %s -- Template code %s ***", sVERSION, sJAVAVER));  // "Bună, acesta este"





      System.out.println("La revedere.");
   }
}
