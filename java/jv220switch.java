/**
 *  file       : id 20220826°1415 — gitlab.com/normai/cheeseburger …/java/jv220switch.java
 *  version    : • 20221001°1015 v0.1.8 Filling • 20220826°1415 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate a switch
 *  summary    :
 */

import java.util.Random;

class jv220switch
{
   private static final String sVERSION = "v0.1.8";
   private static final String sJAVAVER = "(Javac " + System.getProperty("java.runtime.version") + ")";

   public static void main(String[] args)
   {
      System.out.println(String.format("*** Aloha, o keia `jv220switch.java` %s -- Java Switch %s ***", sVERSION, sJAVAVER));  // "Aloha, ʻo kēia"

      Random random = new Random();
      System.out.println("Your dice is rolling ...");
      int randi = random.nextInt(7) + 1;                    // Numbers 1 through 7
      switch (randi)
      {
         case 1 : System.out.println("Look, a one"); break;
         case 2 : System.out.println("Whoops, a two"); break;
         case 3 : System.out.println("Uh-huh, a three"); break;
         case 4 : System.out.println("Holla, a four"); break;
         case 5 : System.out.println("Oh, a five"); break;
         case 6 : System.out.println("Hooray, a six"); break;
         default : System.out.println("Your dice seems broken, you rolled " + randi);
      }

      System.out.println("Aloha mai.");
   }
}
