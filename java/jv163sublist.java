/**
 *  file       : id 20220828°1821 — gitlab.com/normai/cheeseburger …/java/jv163sublist.java
 *  version    : • 20220908°1331 v0.1.7 Filling • 20220828°1821 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : This demonstrates sublists
 *  summary    :
 *  reference  : https://docs.oracle.com/javase/7/docs/api/java/util/ArrayList.html []
 */

import java.util.ArrayList;

class jv163sublist
{
   private static final String sVERSION = "v0.1.7";
   private static final String sJAVAVER = "(Javac " + System.getProperty("java.runtime.version") + ")";

   public static void main(String[] args)
   {
      System.out.println(String.format("*** Dobry den, toto je `jv163sublist.java` %s -- Sublists .. %s ***", sVERSION, sJAVAVER));  // "Dobrý den, toto je"

      ArrayList<String> al = new ArrayList<String>();
      al.add ("Uno");
      al.add ("Due");
      al.add ("Tre");
      al.add ("Quattro");
      al.add ("Cinque");
      al.add ("Sei");
      al.add ("Sette");

      System.out.println("(1) al                            = " + al);
      System.out.println("(2) al.subList(0, 2)              = " + new ArrayList<String>(al.subList(0, 2)));  // Params 'from index', 'to index'
      System.out.println("(3) al.subList(2, al.size())      = " + new ArrayList<String>(al.subList(2, al.size())));
      System.out.println("(4) al.subList(0, (al.size() - 2) = " + new ArrayList<String>(al.subList(0, (al.size() - 2))));
      System.out.println("(5) al.subList(2, 5))             = " + new ArrayList<String>(al.subList(2, 5)));

      System.out.println("Nashledanou.");
   }
}
