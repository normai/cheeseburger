/**
 *  file       : id 20221012°1615 — gitlab.com/normai/cheeseburger …/java/jv153stripol.java
 *  version    : • 20221013°1015 v0.1.8 Filling • 20221012°1615 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate String Interpolation
 *  userstory  :
 *  summary    :
 *  ref        : https://www.thejavaprogrammer.com/java-string-interpolation/ [ref 20221010°0932]
 *  ref        : https://www.delftstack.com/howto/java/java-string-interpolation/ [ref 20221010°0924]
 *  ref        : https://www.delftstack.com/howto/java/java-string-template/ [ref 20221010°0925]
 *  ref        : https://docs.oracle.com/html/E79064_01/Content/Technical%20Reference/Velocity_Templates_DG.htm [ref 20221010°0926]
 */

import java.text.MessageFormat;

class jv153stripol
{
   private static final String sVERSION = "v0.1.8";
   private static final String sJAVAVER = "(Javac " + System.getProperty("java.runtime.version") + ")";

   public static void main(String[] args)
   {
      System.out.println(String.format("*** Hej, det er `jv153stripol.java` %s -- String Interpolation %s ***", sVERSION, sJAVAVER));

      // Fodder
      String sHello = "Hello";
      String sJenny = "Jenny";
      int iX = 234;

      // () String interpolation seven flavours
      // (.1)
      System.out.println("(1) Concatenation    : " + sHello + " " + Integer.toString(iX) + " " + sJenny + ".");  // Integer.toString() seems superflouos

      // (.2)
      System.out.println(String.format("(2) Format method    : %s %s %s.", sHello, iX, sJenny));

      // (.3)
      System.out.println(MessageFormat.format("(3) MessageFormat    : {0} {1} {2}.", sHello, iX, sJenny));

      // (.4)
      String sFm = "(4) Formatted method : %s %s %s.".formatted(sHello, iX, sJenny);
      System.out.println(sFm);

      // (.5)
      StringBuilder sbldr = new StringBuilder("(5) StringBuilder    : ").append(sHello).append(" ").append(iX).append(" ").append(sJenny).append(".");
      System.out.println(sbldr);

      // (.6)
      StringBuffer sbfr = new StringBuffer("(6) StringBuffer     : ").append(sHello).append(" ").append(iX).append(" ").append(sJenny).append(".");
      System.out.println(sbfr);

      // (.7)
      System.out.println("(7) Velocity Templates : ... (No demo yet)");

      System.out.println("Farvel.");
   }
}
