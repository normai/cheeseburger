// EMPTY STUB, YET TO BE FILLED WITH CODE

/**
 *  file       : id 20221213°1615 — gitlab.com/normai/cheeseburger …/java/jv276anonfs.java
 *  version    : • 20xxxxxx°xxxx v0.x.x Filling • 20221213°1615 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate anonymous functions
 *  userstory  :
 *  summary    :
 *  status     :
 *  ref        :
 */

class jv276anonfs
{
   private static final String sVERSION = "v0.0.0";
   private static final String sJAVAVER = "(Javac " + System.getProperty("java.runtime.version") + ")";

   public static void main(String[] args)
   {
      System.out.println(String.format("*** Halo, ini adalah `jv276anonfs.java` %s -- Anonymous functions %s ***", sVERSION, sJAVAVER));





      System.out.println("Alavida.");  //"अलविदा [Alavida]"
   }
}
