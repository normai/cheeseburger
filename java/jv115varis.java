/**
 *  file       : id 20220215°0931 — gitlab.com/normai/cheeseburger …/java/jv115varis.java
 *  version    : • 20220908°0931 v0.1.7 Filling • 20220215°0931 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate variables
 */

class jv115varis
{
   private static final String sVERSION = "v0.1.7";
   private static final String sJAVAVER = "(Javac " + System.getProperty("java.runtime.version") + ")";

   public static void main(String[] args)
   {
      System.out.println(String.format("*** Hello, this is `jv115varis.java` %s -- Variables %s ***", sVERSION, sJAVAVER));

      int a = 123;                                             // Integer
      double b = 2.34;                                         // Float
      String c = "Ahoj";                                       // String
      String d = c + " = ";
      double e = a + b;
      System.out.println(d + Double.toString(e));

      System.out.println("Good bye.");
   }
}
