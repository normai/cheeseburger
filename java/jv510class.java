// THIS FILE IS AN EMPTY STUB, IT IS YET TO BE FILLED WITH CODE

/**
 *  file       : id 20220829°1415 — gitlab.com/normai/cheeseburger …/java/jv510class.java
 *  version    : • 20xxxxxx°xxxx v0.x.x Filling • 20220829°1415 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    :
 *  summary    :
 */

class jv510class
{
   private static final String sVERSION = "v0.0.0";
   private static final String sJAVAVER = "(Javac " + System.getProperty("java.runtime.version") + ")";

   public static void main(String[] args)
   {
      System.out.println(String.format("*** Dobry den, toto je `jv510class.java` %s -- %s ***", sVERSION, sJAVAVER));  // "Dobrý den, toto je"





      System.out.println("Nashledanou.");
   }
}
