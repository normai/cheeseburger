/**
 *  file       : id 20220826°1915 — gitlab.com/normai/cheeseburger …/java/jv261try1.java
 *  version    : • 20220910°1515 v0.1.7 Filling • 20220826°1915 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate Java Exception Handling
 *  summary    :
 *  ref        : https://www.scaler.com/topics/square-root-in-java/ [ref 20220910°1412]
 *  ref        : https://www.w3schools.com/java/ref_keyword_try.asp [ref 20220910°1413]
 *  ref        : https://www.w3schools.com/java/ref_keyword_throw.asp [ref 20220910°1414]
 *  status     : Example stories not yet settled
 */

import java.util.Random;

class jv261try1
{
   private static final String sVERSION = "v0.1.7";
   private static final String sJAVAVER = "(Javac " + System.getProperty("java.runtime.version") + ")";

   public static void main(String[] args)
   {
      System.out.println(String.format("*** Kon'nichiwa, desu `jv261try1.java` %s -- Try/Catch %s ***", sVERSION, sJAVAVER));  // Errors "unmappable character …" in "xxxxxxxx [Kon'nichiwa, desu] …" 


      // (1) Try adding mismatching types
      System.out.println("(1) Adding mismatching types cannot be demonstrated in Java because of strict typing");

      // (2) Calculate square root, sometimes succeed, sometimes fail
      Random random = new Random();
      int iRand = random.nextInt(19) -9;                                        // Get integers from -9 to +9
      System.out.println("\n(2) Calculate sqare root of " + iRand);
      double fRoot;
      try
      {
          if (iRand < 0)
          {
             throw new ArithmeticException("The root of a negative number is NaN, not a number.");  // Workaround. Todo: Craft another example [todo 20220910°1421]
          }
          fRoot = Math.sqrt(iRand);
          System.out.println("    => Sqare root of " + iRand + " is " + fRoot);
      }
      catch (Exception e)
      {
          System.out.println("    => Sqare root of " + iRand + " calculation failed");
      }

      System.out.println("Sayonara.");  // Errors "unmappable character …" in "xxxxx ~[Sayonara]." 
   }
}
