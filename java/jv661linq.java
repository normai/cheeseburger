// EMPTY STUB, YET TO BE FILLED WITH CODE

/**
 *  file       : id 20221223°1315 — gitlab.com/normai/cheeseburger …/java/jv661linq.java
 *  version    : • 20xxxxxx°xxxx v0.x.x Filling • 20221223°1315 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate LINQ
 *  status     :
 */

class jv661linq
{
   private static final String sVERSION = "v0.0.0";
   private static final String sJAVAVER = "(Javac " + System.getProperty("java.runtime.version") + ")";

   public static void main(String[] args)
   {
      System.out.println(String.format("*** Esto es `jv661linq.java` %s -- LINQ %s ***", sVERSION, sJAVAVER));





      System.out.println("Adios.");  // "Adiós"
   }
}
