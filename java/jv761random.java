// THIS FILE IS AN EMPTY STUB, IT IS YET TO BE FILLED WITH CODE

/**
 *  file       : id 20220910°1315 — gitlab.com/normai/cheeseburger …/java/jv761random.java
 *  version    : • 20xxxxxx°xxxx v0.x.x Filling • 20220910°1315 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate Java Random Functions
 *  summary    :
 *  ref        : https://docs.oracle.com/javase/8/docs/api/java/util/Random.html [ref 20220910°0943]
 *  ref        : https://www.codecademy.com/resources/docs/java/random [ref 20220910°0944]
 *  ref        : https://www.freecodecamp.org/news/generate-random-numbers-java/ [ref 20220910°0945]
 *  ref        : https://www.w3schools.blog/java-random-class-tutorial [ref 20220910°0946]
 *  ref        : https://www.w3schools.com/java/java_math.asp [ref 20220910°0947]
 *  ref        : https://www.w3schools.com/java/java_ref_math.asp [ref 20220910°0948]
 *  ref        : https://www.scaler.com/topics/tostring-method-in-java/ [ref 20220910°1112]
 */

class jv761random
{
   private static final String sVERSION = "v0.0.0";
   private static final String sJAVAVER = "(Javac " + System.getProperty("java.runtime.version") + ")";

   public static void main(String[] args)
   {
      System.out.println(String.format("*** Privet, eto `jv761random.java` %s -- Random Functions %s ***", sVERSION, sJAVAVER));  // Unmappable characters …





      System.out.println("Do svidaniya.");  // Unmappable characters …
   }
}
