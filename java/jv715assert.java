/**
 *  file       : id 20221224°1215 — gitlab.com/normai/cheeseburger …/java/jv715assert.java
 *  subject    : Demonstrate assertions
 *  version    : • 20221224°1215 v0.1.9 Filling • 20221224°1215 v0.1.7 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  compile    : > javac.exe jv715assert.java
 *  run        : > java.exe -enableassertions jv715assert
 *  run        : > java.exe -ea jv715assert
 *  status     :
 *  note       : Demo sequence so far identical with 20221217°0915 jv117keywords.java
 */

class jv715assert
{
   private static final String sVERSION = "v0.1.9";
   private static final String sJAVAVER = "(Javac " + System.getProperty("java.runtime.version") + ")";


   public static void main(String[] args)
   {
      System.out.println(String.format("*** Bu `jv715assert.java` %s -- Assertions %s ***", sVERSION, sJAVAVER));


      System.out.println("(1) Probe keyword 'assert'");

      try {
         int i = 10;
         assert i < 20 : "(1.1) i smaller 10";
         assert i > 20 : "(1.2) i greater 10";
      }
      catch (Throwable e) {                                  // Throwable <- Error <- AssertionError
         System.out.println("(1.3) Exception thrown : \"" + e.getMessage() + "\"");
      }



      System.out.println("Guele guele.");  // "Güle güle"
   }
}
