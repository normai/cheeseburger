// THIS FILE IS AN EMPTY STUB, IT IS YET TO BE FILLED WITH CODE

/**
 *  file       : id 20220828°1215 — gitlab.com/normai/cheeseburger …/java/jv420export.java
 *  version    : • 20xxxxxx°xxxx v0.x.x Filling • 20220828°1215 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Module with functions to be called from elsewhere
 *  summary    :
 */

class jv420export
{
   private static final String sVERSION = "v0.0.0";
   private static final String sJAVAVER = "(Javac " + System.getProperty("java.runtime.version") + ")";

   public static void main(String[] args)
   {
      System.out.println(String.format("*** Hola, este es `jv420export.java` %s -- %s ***", sVERSION, sJAVAVER));





      System.out.println("Adios.");  // "Adiós"
   }
}
