/**
 *  file       : id 20220828°1715 — gitlab.com/normai/cheeseburger …/java/jv161list.java
 *  version    : • 20220908°1231 v0.1.7 Filling • 20220828°1715 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate Java ArrayList
 *  summary    :
 *  ref        : https://www.w3schools.com/java/java_arraylist.asp []
 *  ref        : https://stackoverflow.com/questions/13395114/how-to-initialize-liststring-object-in-java [ref 20221014°0932] Lists many possibilities
 *  todo       : Demonstrate difference between ArrayList and ~LinkedList [todo 20220902°1141]
 */

import java.util.ArrayList;

class jv161list
{
   private static final String sVERSION = "v0.1.7";
   private static final String sJAVAVER = "(Javac " + System.getProperty("java.runtime.version") + ")";

   public static void main(String[] args)
   {
      System.out.println(String.format("*** Ni hao `jv161list.java` %s -- %s ***", sVERSION, sJAVAVER));  // "~(Ni hao)" Compiler errors "unmappable character (0x90) for encoding windows-1252"

      // (1) Simple list
      // (1.1) Create
      ArrayList<String> islands = new ArrayList<String>();
      islands.add ("Rügen");
      islands.add ("Usedom");
      islands.add ("Fehmarn");
      islands.add ("Sylt");
      islands.add ("Föhr");
      islands.add ("Pellworm");

      // (1.2) Print some infos
      System.out.println("(1.1) Type = " + islands.getClass());
      System.out.println("(1.2) Length =" + islands.size());
      System.out.println("(1.3) Content = " + islands);

      // (1.3) Iterate
      for (String s : islands)
      {
         System.out.println("   - " + s);
      }

      // (2) Sophisticated list
      // (2.1) Create
      ArrayList<Object> mixture = new ArrayList<Object>();             // Use Object as a workaround
      mixture.add("Pink");
      mixture.add(true);
      mixture.add(123);
      mixture.add(2.34);
      ArrayList<String> helplist = new ArrayList<String>();
      helplist.add("Aha");
      helplist.add("Oha");
      helplist.add("Uhu");
      mixture.add(helplist);

      // (2.2) Print some infos
      System.out.println("(2.1) Type = " + mixture.getClass());
      System.out.println("(2.2) Length =" + mixture.size());
      System.out.println("(2.3) Content = " + mixture);

      // (2.3) Iterate
      for (Object o : mixture)
      {
         System.out.println("   - " + o + " (type = " + o.getClass() + ")");
      }

      System.out.println("Zaijian.");                                  // "xxx (Zaijian)" compiler errors "unmappable character (0x81) for encoding windows-1252"
   }
}
