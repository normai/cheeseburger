// EMPTY STUB, YET TO BE FILLED WITH CODE

/**
 *  file       : id 20221227°1015 — gitlab.com/normai/cheeseburger …/java/jv735yield.java
 *  version    : • 20xxxxxx°xxxx v0.x.x Filling • 20221227°1015 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate the yield keyword
 *  status     :
 */

class jv735yield
{
   private static final String sVERSION = "v0.0.0";
   private static final String sJAVAVER = "(Javac " + System.getProperty("java.runtime.version") + ")";

   public static void main(String[] args)
   {
      System.out.println(String.format("*** Tse `jv735yield.java` %s -- Yield %s ***", sVERSION, sJAVAVER));  // "Це [Tse]"





      System.out.println("Do pobachennya.");  // Unmappable characters ..
   }
}
