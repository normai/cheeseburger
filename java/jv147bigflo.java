/**
 *  file       : id 20221117°1115 — gitlab.com/normai/cheeseburger …/java/jv147bigflo.java
 *  version    : • 20221227°1731 v0.1.9 Filling • 20221117°1115 v0.1.7 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate big floatingpoint numbers
 *  userstory  : Calculate and output some popular non-terminating decimals
 */

import java.math.BigDecimal;
import java.math.RoundingMode;

class jv147bigflo
{
    private static final String sVERSION = "v0.1.9";
    private static final String sJAVAVER = "(Javac " + System.getProperty("java.runtime.version") + ")";


    public static void main(String[] args)
    {
        System.out.println(String.format("*** Hailo, yah hai `jv147bigflo.java` %s -- Big integers %s ***", sVERSION, sJAVAVER));  // "हैलो, यह है [Hailo, yah hai]"


        double d1 = 10.0 / 7.0;
        double d2 = 10.0 / 3.0;
        double d3 = 10.0 / 1.5;
        System.out.println("(1) Some doubles:");
        System.out.printf("(1.1.1) d1  = %f%n", d1);
        System.out.printf("(1.1.2) d1  = %s%n", Double.toString(d1));
        System.out.printf("(1.2.1) d2  = %f%n", d2);
        System.out.printf("(1.2.2) d2  = %s%n", Double.toString(d2));
        System.out.printf("(1.3.1) d3  = %f%n", d3);
        System.out.printf("(1.3.2) d3  = %s%n", Double.toString(d3));

        System.out.println("(2) Some BigDecimals:");
        BigDecimal bd1 = new BigDecimal("10.0");
        bd1 = bd1.divide(new BigDecimal("7.0"), 56, RoundingMode.HALF_UP);  // Precision needed, otherwise "ArithmeticException: Non-terminating decimal expansion"
        BigDecimal bd2 = new BigDecimal("10.0");
        bd2 = bd2.divide(new BigDecimal("3.0"), 56, RoundingMode.HALF_UP);
        BigDecimal bd3 = new BigDecimal("10.0");
        bd3 = bd3.divide(new BigDecimal("1.5"), 56, RoundingMode.HALF_UP);

        System.out.printf("(2.1.1) bd1 = %f%n", bd1);
        System.out.printf("(2.1.2) bd1 = %s%n", bd1.toString());
        System.out.printf("(2.2.1) bd1 = %f%n", bd2);
        System.out.printf("(2.2.2) bd2 = %s%n", bd2.toString());
        System.out.printf("(2.3.1) bd1 = %f%n", bd3);
        System.out.printf("(2.3.2) bd3 = %s%n", bd3.toString());


        System.out.println("Alavida.");                                // "अलविदा [Alavida]"
    }
}
