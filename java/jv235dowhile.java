/**
 *  file       : id 20220826°1615 — gitlab.com/normai/cheeseburger …/java/jv235dowhile.java
 *  version    : • 20221001°1115 v0.1.8 Filling • 20220826°1615 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Foot-controlled Do/While loop
 *  summary    :
 */

import java.util.Random;

class jv235dowhile
{
   private static final String sVERSION = "v0.1.8";
   private static final String sJAVAVER = "(Javac " + System.getProperty("java.runtime.version") + ")";

   public static void main(String[] args)
   {
      System.out.println(String.format("*** Hello, ez `jv235dowhile.java` %s -- Do/While loop %s ***", sVERSION, sJAVAVER));

      System.out.println("Loop until you rolled a six :");
      Random random = new Random();
      int randi;                                                    // Variable needs still be known behind the do block
      do
      {
          randi = random.nextInt(6) + 1;                            // Numbers 1 through 6
          System.out.println(" - You rolled " + randi);
      }
      while (randi != 6);

      System.out.println("Viszlat.");  // "Viszlát." does not print nice
   }
}
