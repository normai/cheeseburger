// EMPTY STUB, YET TO BE FILLED WITH CODE

/**
 *  file       : id 20221223°0815 — gitlab.com/normai/cheeseburger …/java/jv355attribs.java
 *  version    : • 20xxxxxx°xxxx v0.x.x Filling • 20221223°0815 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate file attributes
 *  status     :
 */

class jv355attribs
{
   private static final String sVERSION = "v0.0.0";
   private static final String sJAVAVER = "(Javac " + System.getProperty("java.runtime.version") + ")";

   public static void main(String[] args)
   {
      System.out.println(String.format("*** Este e `jv355attribs.java` %s -- File attributes %s ***", sVERSION, sJAVAVER));  // "Este é"





      System.out.println("Adeus.");
   }
}
