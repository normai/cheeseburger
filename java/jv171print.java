// THIS FILE IS AN EMPTY STUB, IT IS YET TO BE FILLED WITH CODE

/**
 *  file       : id 20220827°1315 — gitlab.com/normai/cheeseburger …/java/jv171print.java
 *  version    : • 20xxxxxx°xxxx v0.x.x Filling • 20220827°1315 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    :
 *  summary    :
 */

class jv171print
{
   private static final String sVERSION = "v0.0.0";
   private static final String sJAVAVER = "(Javac " + System.getProperty("java.runtime.version") + ")";

   public static void main(String[] args)
   {
      System.out.println(String.format("*** Namaste, yo ho `jv171print.java` %s -- Print function %s ***", sVERSION, sJAVAVER));  // Errors "unmappable character …"





      System.out.println("Namaste.");  // Errors "unmappable character …"
   }
}
