// THIS FILE IS AN EMPTY STUB, IT IS YET TO BE FILLED WITH CODE

/**
 *  file       : id 20220828°1015 — gitlab.com/normai/cheeseburger …/java/jv335format.java
 *  version    : • 20xxxxxx°xxxx v0.x.x Filling • 20220828°1015 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate JavaScript string formatting
 *  summary    :
 *  ref        : https://attacomsian.com/blog/java-string-format [ref 20220907°1042]
 *  ref        : https://www.javatpoint.com/java-string-format [ref 20220907°1043]
 *  ref        : https://blog.udemy.com/java-format-string/ [ref 20220907°1044]
 *  ref        :
 */

class jv335format
{
   private static final String sVERSION = "v0.0.0";
   private static final String sJAVAVER = "(Javac " + System.getProperty("java.runtime.version") + ")";

   public static void main(String[] args)
   {
      System.out.println(String.format("*** Dobry den, toto je `jv335format.java` %s -- Format functions %s ***", sVERSION, sJAVAVER));  // "Dobrý deň, toto je"





      System.out.println("Dovidenia.");
   }
}
