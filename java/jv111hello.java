/**
 *  file       : id 20140729°1311 — gitlab.com/normai/cheeseburger …/java/jv111hello.java
 *  version    : • 20220908°0731 v0.1.7 Filling • 20140729°1311 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate a most simple Java program with user input
 *  issue      : Getting the JRE version is easy, but I'd rather like the compiler version. [issue 20221228°0951]
 */

class jv111hello
{
   private static final String sVERSION = "v0.1.7";
   private static final String sJAVAVER = "(Javac " + System.getProperty("java.runtime.version") + ")";

   public static void main(String[] args)
   {
      System.out.println(String.format("*** This is `jv111hello.java` %s -- Hello %s ***", sVERSION, sJAVAVER));

      System.out.print("Please enter your name >");
      java.util.Scanner input = new java.util.Scanner(System.in);
      String s = input.nextLine();
      System.out.println("Good morning \"" + s + "\"!");

      System.out.println("Bye.");
   }
}
