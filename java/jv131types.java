/**
 *  file       : id 20220215°0951 — gitlab.com/normai/cheeseburger …/java/jv131types.java
 *  version    : • 20220908°1031 v0.1.7 Filling • 20220215°0951 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  summary    : Demonstrate some types
 *  reference  : E.g. • www.w3schools.com/java/java_data_types.asp [ref 20220825°1012]
 */

class jv131types
{
   private static final String sVERSION = "v0.1.7";
   private static final String sJAVAVER = "(Javac " + System.getProperty("java.runtime.version") + ")";

    public static void main(String[] args)
    {
        System.out.println(String.format("*** Abhinandan `jv131types.java` %s -- Types %s ***", sVERSIO, sJAVAVERN));  // " अभिनंद  [abhinandan]"

        // Some declarations
        byte iByte1 = 127;
        short iShort1 = 32767;
        int iInt1 = 2147483647;
        long iLong1 = Long.MAX_VALUE;
        float f1 = 1.23f;                              // The 'f' postfix prevents error 'lossy conversion'
        double d1 = Double.MAX_VALUE;
        boolean b1 = true;
        char c1 = 'x';
        String s1 = "Holla";

        // Some operations
        byte iByte2 = (byte) (iByte1 + ((byte) 1));    // Force the lossy conversion from int to byte
        short iShort2 = (short) (iShort1 + ((short) 1)); // Force lossy conversion from int to short
        int iInt2 = iInt1 + 1;
        long iLong2 = iLong1 + 1;
        double f2 = f1 + 1;
        double d2 = d1 + 1;
        boolean b2 = !b1;                              // The plus operator is not possible with boolean
        char c2 = 'x' + 1;
        String s2 = "Holla" + 1;                       // Note the automatic conversion of '1' from int to String

        // Display the results
        System.out.println("(1) " + iByte1 + " + 1 = " + iByte2 + " (Byte.MAX_VALUE = " + Byte.MAX_VALUE + ")");
        System.out.println("(2) " + iShort1 + " + 1 = " + iShort2 + " (Short.MAX_VALUE = " + Short.MAX_VALUE + ")");
        System.out.println("(3) " + iInt1 + " + 1 = " + iInt2);
        System.out.println("(4) " + iLong1 + " + 1 = " + iLong2);
        System.out.println("(5) " + f1 + " + 1 = " + f2);
        System.out.println("(6) " + d1 + " + 1 = " + d2);
        System.out.println("(7) !" + b1 + " = " + b2);
        System.out.println("(8) " + s1 + " + 1 = " + s2);

        System.out.println("Alavida.");  // "अलविदा [Alavida]."
    }
}
