/**
 *  file       : id 20221217°0915 — gitlab.com/normai/cheeseburger …/java/jv117keywords.java
 *  version    : • 20221224°1213 v0.1.9 Filling • 20221217°0915 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  compile    : > javac.exe jv117keywords.java
 *  run        : > java.exe -enableassertions jv117keywords
 *  run        : > java.exe -ea jv117keywords
 *  subject    : Demonstrate key words on the example of assert
 *  note       : Demo sequence is so far identical with 20221224°1215 jv715assert.java
 *  status     :
 */

class jv117keywords
{
    private static final String sVERSION = "v0.1.9";
    private static final String sJAVAVER = "(Javac " + System.getProperty("java.runtime.version") + ")";


    public static void main(String[] args)
    {
        System.out.println(String.format("*** Ko tenei `jv117keywords.java` %s -- Keywords %s ***", sVERSION, sJAVAVER));
        System.out.println("Java version = " + System.getProperty("java.version"));


        System.out.println("(1) Probe keyword 'assert'");

        try {
            int i = 10;
            assert i < 20 : "(1.1) i smaller 10";
            assert i > 20 : "(1.2) i greater 10";
        }
        catch (Throwable e) {                                  // Throwable <- Error <- AssertionError
            System.out.println("(1.3) Exception thrown : \"" + e.getMessage() + "\"");
        }


        System.out.println("Tena koe.");  // Original chars cause error "unmappable character (0x81) for encoding windows-1252"
    }
}
