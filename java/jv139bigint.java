/**
 *  file       : id 20221117°1015 — gitlab.com/normai/cheeseburger …/java/jv139bigint.java
 *  version    : • 20221216°0912 v0.1.8 Filling • 20221117°1015 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate big integer types
 *  status     : Too simple. Some more intelligent example(s) were nice.
 */

import java.math.BigInteger;

class jv139bigint
{
   private static final String sVERSION = "v0.1.8";
   private static final String sJAVAVER = "(Javac " + System.getProperty("java.runtime.version") + ")";


   public static void main(String[] args)
   {
      System.out.println(String.format("*** Hello, ez `jv139bigint.java` %s -- Big integers %s ***", sVERSION, sJAVAVER));

      // (1) Series One
      int i1 = 123_000_456;
      long i2 = Long.MAX_VALUE;                                // 9223372036854775807 = 2^63 - 1
      BigInteger i3 = new BigInteger("123000000000000000000000456");
      BigInteger i4 = new BigInteger("123000000000000000000000000000000456");
      System.out.println("1.1 - " + i1 + " type = " + ((Object) i1).getClass().getSimpleName());
      System.out.println("1.2 - " + i2 + " type = " + ((Object) i2).getClass().getSimpleName());
      System.out.println("1.3 - " + i3 + " type = " + ((Object) i3).getClass().getSimpleName());
      System.out.println("1.4 - " + i4 + " type = " + ((Object) i4).getClass().getSimpleName());

      // (2) Series Two
      int j1 = 10 ^ 9 + 234;
      BigInteger j2 = new BigInteger("10").pow(27).add(new BigInteger("234"));
      BigInteger j3 = new BigInteger("10").pow(54).add(new BigInteger("234"));
      System.out.println("2.1 - " + j1 + " type = " + ((Object) i1).getClass().getSimpleName());
      System.out.println("2.2 - " + j2 + " type = " + ((Object) j2).getClass().getSimpleName());
      System.out.println("2.3 - " + j3 + " type = " + ((Object) j3).getClass().getSimpleName());

      System.out.println("Viszlat.");  // Viszlát
   }
}
