// THIS FILE IS AN EMPTY STUB, IT IS YET TO BE FILLED WITH CODE

/**
 *  file       : id 20220829°1115 — gitlab.com/normai/cheeseburger …/java/jv343textfile.java
 *  version    : • 20xxxxxx°xxxx v0.x.x Filling • 20220829°1115 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate file create/read/write
 *  summary    :
 */

class jv343textfile
{
   private static final String sVERSION = "v0.0.0";
   private static final String sJAVAVER = "(Javac " + System.getProperty("java.runtime.version") + ")";

   public static void main(String[] args)
   {
      System.out.println(String.format("*** Pryvit, tse `jv343textfile.java` %s -- Textfile %s ***", sVERSION, sJAVAVER));  // Errors "unmappable character …"





      System.out.println("Do pobachennya.");  // Errors "unmappable character …"
   }
}
