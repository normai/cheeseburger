/**
 *  file       : id 20220826°1515 — gitlab.com/normai/cheeseburger …/java/jv230while.java
 *  version    : • 20220910°1015 v0.1.7 Filling • 20220826°1515 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate while loop
 *  summary    :
 *  ref        : About random see jv761random.java
 */

import java.util.Random;

class jv230while
{
   private static final String sVERSION = "v0.1.7";
   private static final String sJAVAVER = "(Javac " + System.getProperty("java.runtime.version") + ")";

   public static void main(String[] args)
   {
      System.out.println(String.format("*** Hailo, yah hai `jv230while.java` %s -- While Loop %s ***", sVERSION, sJAVAVER));  // "*** हैलो, यह है  [Hailo, yah hai]" is not printed nice

      // (1) Roll dice first time
      System.out.println("Roll the number six!");
      Random random = new Random();
      int iDicenum = random.nextInt(6) + 1;
      int iTimes = 1;
      System.out.println("Your first number is " + String.valueOf(iDicenum));

      // (2) Continue as long no six is rolled
      while (iDicenum != 6)
      {
         iDicenum = random.nextInt(6) + 1;
         iTimes += 1;
         System.out.println(" - then " + String.valueOf(iDicenum));
      }

      System.out.println("You rolled " + Integer.toString(iTimes) + " times to yield the six.");

      System.out.println("Alavida.");  // "अलविदा  [Alavida]." is not printed nice
   }
}
