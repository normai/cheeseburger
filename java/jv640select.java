// THIS FILE IS AN EMPTY STUB, IT IS YET TO BE FILLED WITH CODE

/**
 *  file       : id 20220831°1815 — gitlab.com/normai/cheeseburger …/java/jv640select.java
 *  version    : • 20xxxxxx°xxxx v0.x.x Filling • 20220831°1815 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate polymorphism
 *  summary    :
 */

class jv640select
{
   private static final String sVERSION = "v0.0.0";
   private static final String sJAVAVER = "(Javac " + System.getProperty("java.runtime.version") + ")";

   public static void main(String[] args)
   {
      System.out.println(String.format("*** Hailo, yah hai `jv640select.java` %s -- %s ***", sVERSION, sJAVAVER));  // " हैलो, यह है  [Hailo, yah hai]"





      System.out.println("Alavida.");  // "अलविदा [Alavida]."
   }
}
