// EMPTY STUB, YET TO BE FILLED WITH CODE

/**
 *  file       : id 20221224°1115 — gitlab.com/normai/cheeseburger …/java/jv711mthread.java
 *  version    : • 20xxxxxx°xxxx v0.x.x Filling • 20221224°1115 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate multithreading
 *  status     :
 */

class jv711mthread
{
   private static final String sVERSION = "v0.0.0";
   private static final String sJAVAVER = "(Javac " + System.getProperty("java.runtime.version") + ")";

   public static void main(String[] args)
   {
      System.out.println(String.format("*** Det haer aer `jv711mthread.java` %s -- Multithreading %s ***", sVERSION, sJAVAVER));  // "Det här är"





      System.out.println("Adjoe.");  // "Adjö"
   }
}
