/**
 *  file       : id 20220826°1715 — gitlab.com/normai/cheeseburger …/java/jv240for.java
 *  version    : • 20220919°1915 v0.1.8 Filling • 20220826°1715 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate for loop
 *  summary    :
 *  ref        : About the Java equivalent for the Python range function, see e.g.
 *               • https://github.com/mherrmann/java-generator-functions [ref 20220825°1951] (Not used here)
 *               • https://stackoverflow.com/questions/11570132/generator-functions-equivalent-in-java [ref 20220825°1953] (Not used here)
 *  ref        : https://www.javatpoint.com/java-math-pow-method [ref 20220919°1743]
 */

class jv240for
{
   private static final String sVERSION = "v0.1.8";
   private static final String sJAVAVER = "(Javac " + System.getProperty("java.runtime.version") + ")";

   public static void main(String[] args)
   {
      System.out.println(String.format("*** Halo, ini adalah `jv240for.java` %s -- For loop %s ***", sVERSION, sJAVAVER));

      System.out.println("(1) Two power 6 through 10");
      for (int i = 6; i <= 10; i++)
      {
         int iPow = (int) java.lang.Math.pow(2, i);
         System.out.println("   - " + Integer.toString(i) + " " + Integer.toString(iPow));
      }

      System.out.println("(2) And the same backward");
      for (int i = 10; i >= 6; i--)
      {
         int iPow = (int) java.lang.Math.pow(2, i);
         System.out.println("   - " + Integer.toString(i) + " " + Integer.toString(iPow));
      }

      System.out.println("Alavida.");  // "अलविदा" does not print nice
   }
}
