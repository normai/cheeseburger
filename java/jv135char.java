/**
 *  file       : id 20221012°0915 — gitlab.com/normai/cheeseburger …/java/jv135char.java
 *  version    : • 20221016°1115 v0.1.8 Filling • 20221012°0915 v0.1.7 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate Character Type
 *  userstory  :
 *  summary    :
 *  ref        : https://www.javastring.net/java/string/java-string-chars-method-examples [ref 20221015°1112]
 *  ref        : https://docs.oracle.com/javase/tutorial/java/nutsandbolts/datatypes.html [ref 20221015°1122]
 *  ref        : https://www.programiz.com/java-programming/variables-primitive-data-types [ref 20221015°1123]
 *  ref        : https://stackoverflow.com/questions/4329275/get-char-value-in-java [ref 20221015°1124]
 *  status     : Not ready.
 */

import java.util.ArrayList;
import java.util.List;

class jv135char
{
   private static final String sVERSION = "v0.1.8";
   private static final String sJAVAVER = "(Javac " + System.getProperty("java.runtime.version") + ")";

   public static void main(String[] args)
   {
      System.out.println(String.format("*** Zdraveite, tova e `jv135char.java` %s -- Character Type %s ***", sVERSION, sJAVAVER));  // "Здравейте, това е [Zdraveĭte, tova e]" does not print nice

      // (A)
      // (A.1)
      char c1 = 'A';
      System.out.println("(A.1.1) char c1 : " + c1 + " -- " + ((Object) c1).getClass().getSimpleName());  // Does autoboxing.
      System.out.println("(A.1.2) char c1 : " + c1 + " -- " + ((Object) c1).getClass().isPrimitive());  // There is no way to get the primitive type directly.

      // (A.2)
      char c2 = 65;
      System.out.println("(A.2.1) c2                            : " + c2);
      System.out.println("(A.2.2) (int) c2                      : " + (int) c2);
      System.out.println("(A.2.3) Character.getNumericValue(c2) : " + Character.getNumericValue(c2));  // Nonsense

      // (A.3)
      char c3 = '\u00e9';                                               // 'é'
      System.out.println("(A.3.1) c3                            : " + c3);
      System.out.println("(A.3.2) (int) c3                      : " + (int) c3);
      System.out.println("(A.3.3) Character.getNumericValue(c3) : " + Character.getNumericValue(c3));  // Nonsense

      // (B)
      // (B.1) Simple string
      String str_1 = "Abcd…wxyz";
      System.out.println("(B.1) Simple string : " + str_1);

      // (B.2) Get character codes
      List<Integer> chars = new ArrayList<Integer>();
      System.out.print("(B.2) Codes from chars: ");
      for(char c : str_1.toCharArray()) {
         System.out.print((int) c + " ");
         chars.add ((int) c);
      }
      System.out.println();

      // (B.3) Get character codes
      System.out.print("(B.3) Chars from codes : ");
      for(char c : str_1.toCharArray()) {
         System.out.print((int) c + " ");
      }
      System.out.println();

      System.out.println("Dovizhdane.");  // "Довиждане [Dovizhdane]" does not print nice
   }
}
