// THIS FILE IS AN EMPTY STUB, IT IS YET TO BE FILLED WITH CODE

/**
 *  file       : id 20220830°1915 — gitlab.com/normai/cheeseburger …/java/jv810window.java
 *  version    : • 20xxxxxx°xxxx v0.x.x Filling • 20220830°1915 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate a plain window with a button
 *  summary    :
 */

class jv810window
{
   private static final String sVERSION = "v0.0.0";
   private static final String sJAVAVER = "(Javac " + System.getProperty("java.runtime.version") + ")";

   public static void main(String[] args)
   {
      System.out.println(String.format("*** Namaste, yo ho `jv810window.java` %s -- %s ***", sVERSION, sJAVAVER));  // Unmappable characters …





      System.out.println("Namaste.");  // Unmappable characters …
   }
}
