/**
 *  file       : id 20221012°1815 — gitlab.com/normai/cheeseburger …/java/jv159date.java
 *  version    : • 20221230°1331 v0.1.9 Filling • 20221012°1815 v0.1.7 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate Date and Time Types
 *  userstory  : Set and print dates in various formats and calculate time spans
 */

import java.time.Duration;
import java.time.format.DateTimeFormatter;
import java.time.LocalDate;                                    // java.time new in Java 8
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.Period;
import java.time.ZonedDateTime;                                // NOT YET USED HERE
import java.util.Date;                                         // Old, partially deprecated

class jv159date
{
   private static final String sVERSION = "v0.1.9";
   private static final String sJAVAVER = "(Javac " + System.getProperty("java.runtime.version") + ")";

   public static void main(String[] args)
   {
      System.out.println(String.format("*** Tere, see on `jv159date.java` %s -- Date and Time Types %s ***", sVERSION, sJAVAVER));

      LocalDateTime ldtStart = LocalDateTime.now();

      // Limits
      System.out.println("(1) LocalDateTime Limits:");
      LocalDateTime ldtMin = LocalDateTime.MIN;
      LocalDateTime ldtMax = LocalDateTime.MAX;
      System.out.println("   1. LDT Min = " + ldtMin + "               y/m/d = " + ldtMin.getYear() + "/" + ldtMin.getMonth() + "/" + ldtMin.getDayOfMonth());
      System.out.println("   2. LDT Max = " + ldtMax + "  y/m/d = " + ldtMax.getYear() + "/" + ldtMax.getMonth() + "/" + ldtMax.getDayOfMonth());


      // Galileo
      System.out.println("(2) Galileo birthday:");
      LocalDate daGal = LocalDate.of(1564, 2, 16);
      LocalDateTime dtGal1 = LocalDateTime.of(1564, Month.FEBRUARY, 16, 13, 14);  // Minutes only, no seconds
      LocalDateTime dtGal2 = LocalDateTime.of(1564, 2, 16, 13, 14, 15);           // With seconds
      LocalDateTime dtGal3 = LocalDateTime.of(1564, 2, 16, 13, 14, 15, 123456789);  // With nanosec
      //String sCustom = dtGal3.format(DateTimeFormatter.ofPattern("EEEE, MMMM dd, yyyy hh:mm:ss a"));
      String sCustom = dtGal3.format(DateTimeFormatter.ofPattern("yyyy-MM-dd`HH:mm:ss, 'a' EEEE 'in' MMMM"));
      System.out.println("    1. LD     = " + daGal);
      System.out.println("    2. LDT 1  = " + dtGal1 + "                 -- with minutes");
      System.out.println("    3. LDT 2  = " + dtGal2 + "              -- with seconds");
      System.out.println("    4. LDT 3  = " + dtGal3 + "    -- with nanoseconds");
      System.out.println("    5. Custom = " + sCustom);

      // Now
      System.out.println("(3) Now:");
      LocalDate ldaNow = LocalDate.now();
      LocalTime ltiNow1 = LocalTime.now();
      LocalDateTime ldtNow = LocalDateTime.now();
      String sNow = ldtNow.format(DateTimeFormatter.ofPattern("yyyy-MM-dd`HH:mm:ss, 'a' EEEE 'in' MMMM"));
      System.out.println("    1. LD/LT  = " + ldaNow + " / " + ltiNow1);
      System.out.println("    2. LDT    = " + ldtNow);
      System.out.println("    3. Custom = " + sNow);

      // Halley
      System.out.println("(4) Next Halley approach:");
      LocalDate ldaHal = LocalDate.of(2061, Month.JULY, 29);
      LocalDateTime ldtHal = LocalDateTime.of(2061, 7, 29, 12, 13);
      String sHalley = ldtHal.format(DateTimeFormatter.ofPattern("yyyy-MM-dd`HH:mm:ss, 'a' EEEE 'in' MMMM"));
      System.out.println("    1. LD    = " + ldaHal);
      System.out.println("    2. LDT   = " + ldtHal);
      System.out.println("    3. Custom = " + sHalley);


      // Calculate large scale time span
      Period duWait = Period.between(LocalDate.now(), ldaHal);
      System.out.println("(5) Wait for Halley  : " + duWait.getYears()
         + " years, " + duWait.getMonths() + " month, " + duWait.getDays() + " days");


      // Calculate short scale time span
      // Empirical finding. Successive runs result in either 46 ms or
      //    62 ms, but noting in between. This indicates some artifact
      //    being at work here. [issue 20221230°1311 'java nanosecs']
      LocalDateTime ldtStop = LocalDateTime.now();
      Duration dura = Duration.between(ldtStart, ldtStop);
      int iGetNano = dura.getNano();                   // e.g. 31241700
      int iMil = iGetNano / 1000_000;                  // 31
      int iMic = iGetNano % 1000_000 / 1000;           // 241
      int iNan = iGetNano % 1000;                      // 700
      System.out.println("(6) Runtime duration : " + iGetNano + " ns = " + iMil + " ms + " + iMic + " us + " + iNan + " ns");

      System.out.println("Huevasti.");  // "Hüvasti"
   }
}
