// EMPTY STUB, YET TO BE FILLED WITH CODE

/**
 *  file       : id 20221213°1715 — gitlab.com/normai/cheeseburger …/java/jv278lambda.java
 *  version    : • 20xxxxxx°xxxx v0.x.x Filling • 20221213°1715 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate lambda expressions
 *  userstory  :
 *  summary    :
 *  status     :
 *  ref        :
 */

class jv278lambda
{
   private static final String sVERSION = "v0.0.0";
   private static final String sJAVAVER = "(Javac " + System.getProperty("java.runtime.version") + ")";

   public static void main(String[] args)
   {
      System.out.println(String.format("*** Bongiorno, sono `jv278lambda.java` %s -- Lambda expressions %s ***", sVERSION, sJAVAVER));





      System.out.println("Arrivederci.");
   }
}
