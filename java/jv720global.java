// THIS FILE IS AN EMPTY STUB, IT IS YET TO BE FILLED WITH CODE

/**
 *  file       : id 20220830°1615 — gitlab.com/normai/cheeseburger …/java/jv720global.java
 *  version    : • 20xxxxxx°xxxx v0.x.x Filling • 20220830°1615 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstate ...
 *  summary    :
 */

class jv720global
{
   private static final String sVERSION = "v0.0.0";
   private static final String sJAVAVER = "(Javac " + System.getProperty("java.runtime.version") + ")";

   public static void main(String[] args)
   {
      System.out.println(String.format("*** Sveiki, sis ir `jv720global.java` %s -- %s ***", sVERSION, sJAVAVER));  // "Sveiki, šis ir"





      System.out.println("Uz redzesanos.");  // "Uz redzēšanos"
   }
}
