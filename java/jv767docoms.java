// EMPTY STUB, YET TO BE FILLED WITH CODE

/**
 *  file       : id 20221227°1115 — gitlab.com/normai/cheeseburger …/java/jv767docoms.java
 *  version    : • 20xxxxxx°xxxx v0.x.x Filling • 20221227°1115 v0.1.7 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate documentation comments
 *  status     :
 */

class jv767docoms
{
   private static final String sVERSION = "v0.0.0";
   private static final String sJAVAVER = "(Javac " + System.getProperty("java.runtime.version") + ")";

   public static void main(String[] args)
   {
      System.out.println(String.format("*** Tova e `jv767docoms.java` %s -- Documentation comments %s ***", sVERSION, sJAVAVER));  // "Това е [Tova e]"





      System.out.println("Dovizhdane.");  // "Довиждане [Dovizhdane]."
   }
}
