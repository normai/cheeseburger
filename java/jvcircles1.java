/**
 *  file       : id 20220928°1815 — gitlab.com/normai/cheeseburger … py/jvcircles1.py
 *  version    : • 20220928°1815 v0.1.8 Initial
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Draw ASCII Circles and other patterns
 *  summary    :
 *  encoding   : UTF-8-without-BOM
 *  reference  : https://www.onlinemath4all.com/how-to-determine-if-a-point-is-inside-or-outside-a-circle.html [ref 20220928°1822]
 */

class jvcircles1
{
   private static final String sVERSION = "v0.0.0";
   private static final String sJAVAVER = "(Javac " + System.getProperty("java.runtime.version") + ")";

   public static boolean IsPointOnCirlce(int r, int x, int y) {
      int tolerance = r;                                            // Empirical value for line width
      if (Math.abs(Math.pow(x, 2) + Math.pow(y, 2) - Math.pow(r, 2)) < tolerance) {
         return true;
      }
      else {
         return false;
      }
   }

   public static boolean IsPointOnDiagonal(int x, int y, int size) {
      if (x == size - y + 1) {                                      // Add 1 empirically
         return true;
      }
      else {
         return false;
      }
   }

   public static void main(String[] args)
   {
      System.out.println(String.format("*** Dobry den, toto je `jvcircles1.java` %s -- Draw ASCII Circles %s ***", sVERSION, sJAVAVER));  // "Dobrý deň, toto je"

      int[] sizes = {7, 17};
      for (int size : sizes) {                                      // Paint as many rectangles as sizes are given

         // Prepare convenient variables
         // Note how the integer division for the radius looks different in each language
         int radius = (size / 2) - 1;                               // Make size of circle slightly smaller than the rectangle
         int shiftx = radius + 2;                                   // Shift circle center from rectangle left to rectangle center
         int shifty = radius + 2;                                   // Shift circle center from rectangle top to rectangle center

         // Iterate over lines and columns
         for (int line = 1; line <= size; line++) {
            for (int col = 1; col <= size; col++) {
               if (IsPointOnCirlce(radius, line - shiftx, col - shifty)) {  // Is this point on the circle?
                  System.out.print("@ ");
               }
               else {
                  if (IsPointOnDiagonal(line, col, size)) {         // Is this point on the diagonal?
                     System.out.print("# ");
                  }
                  else {                                            // All other cases
                     System.out.print(". ");
                  }
               }
            }
            System.out.println();                                   // Goto next line
         }
      }

      System.out.println("Dovidenia.");
   }
}
