:: file 20220215`1921 (after 20211103`1121) -- Run all
:: j11calc.java
@echo off

set list=
set list=%list%;jv111hello
set list=%list%;jv113aloha
set list=%list%;jv115varis
set list=%list%;jv117keywords
set list=%list%;jv121operas
set list=%list%;jv123compare
set list=%list%;jv125bitwise
set list=%list%;jv127ternary
set list=%list%;jv131types
set list=%list%;jv133bool
set list=%list%;jv135char
set list=%list%;jv137int
set list=%list%;jv139bigint
set list=%list%;jv145float
set list=%list%;jv147bigflo
set list=%list%;jv151string
set list=%list%;jv153stripol
set list=%list%;jv155strfunc
set list=%list%;jv157array
set list=%list%;jv161list
set list=%list%;jv163sublist
set list=%list%;jv165dict
rem set list=%list%;.\libs\javatuples-1.2.jar jv167tuple
set list=%list%;jv171print
set list=%list%;jv173input
set list=%list%;jv211ifs1
set list=%list%;jv220switch
set list=%list%;jv230while
set list=%list%;jv235dowhile
set list=%list%;jv240for
set list=%list%;jv250foreach
set list=%list%;jv261try1
set list=%list%;jv262try2
set list=%list%;jv267goto
set list=%list%;jv271funcs
set list=%list%;jv273recurs
set list=%list%;jv276anonfs
set list=%list%;jv278lambda
set list=%list%;jv281fntional
set list=%list%;jv286compars
set list=%list%;jv311kbd1menu
set list=%list%;jv313getkey
set list=%list%;jv315kbd2menu
set list=%list%;jv317keypress
set list=%list%;jv319kbd3menu
set list=%list%;jv335format
set list=%list%;jv338stdio
set list=%list%;jv341dir
set list=%list%;jv343textfile
set list=%list%;jv346binfile
set list=%list%;jv350modes
set list=%list%;jv410import
set list=%list%;jv420export
set list=%list%;jv430import2
set list=%list%;jv440main
set list=%list%;jv450lib
set list=%list%;jv510class
set list=%list%;jv520access
set list=%list%;jv530inherit
set list=%list%;jv540aggregate
set list=%list%;jv550polymo
set list=%list%;jv560iface
set list=%list%;jv610create
set list=%list%;jv620insert
set list=%list%;jv630upsala
set list=%list%;jv640select
set list=%list%;jv642relation
set list=%list%;jv644many
set list=%list%;jv646joins
set list=%list%;jv651nosql
set list=%list%;jv661linq
set list=%list%;jv711mthread
set list=%list%;jv715assert
set list=%list%;jv720global
set list=%list%;jv722varargs
set list=%list%;jv724range
set list=%list%;jv726regex
set list=%list%;jv730del
set list=%list%;jv733sleep
set list=%list%;jv735yield
set list=%list%;jv740args
set list=%list%;jv751bubblesort
set list=%list%;jv753humansort
set list=%list%;jv761random
set list=%list%;jv765unittest
set list=%list%;jv771sound
set list=%list%;jv810window
set list=%list%;jv813input1
set list=%list%;jv815input2
set list=%list%;jv830treeview
set list=%list%;jv999template
set list=%list%;jvcircles1
set list=%list%;jvfactorial

goto fin
:fin

@echo off
for %%a in (%list%) do (
   echo --------------------- %%a.java ---------------------
   javac.exe %%a.java
   java.exe -ea %%a
   echo.
   pause
)
jv167tuple.java.bat

echo --------------------------------------------------------
pause
