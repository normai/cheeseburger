// EMPTY STUB, YET TO BE FILLED WITH CODE

/**
 *  file       : id 20221213°1815 — gitlab.com/normai/cheeseburger …/java/jv281fntional.java
 *  version    : • 20xxxxxx°xxxx v0.x.x Filling • 20221213°1815 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate functional programming
 *  userstory  :
 *  summary    :
 *  status     :
 *  ref        :
 */

class jv281fntional
{
   private static final String sVERSION = "v0.0.0";
   private static final String sJAVAVER = "(Javac " + System.getProperty("java.runtime.version") + ")";

   public static void main(String[] args)
   {
      System.out.println(String.format("*** Kon'nichiwa, desu `jv281fntional.java` %s -- Functional programming %s ***", sVERSION, sJAVAVER));  // [Kon'nichiwa, desu]





      System.out.println("Sayonara.");  // [Sayonara]
   }
}
