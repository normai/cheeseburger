// THIS FILE IS AN EMPTY STUB, IT IS YET TO BE FILLED WITH CODE

/**
 *  file       : id 20221116°1715 — gitlab.com/normai/cheeseburger …/java/jv722varargs.java
 *  version    : • 20xxxxxx°xxxx v0.x.x Filling • 20221116°1715 v0.1.8 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate VarArgs
 *  userstory  :
 *  summary    :
 */

class jv722varargs
{
   private static final String sVERSION = "v0.0.0";
   private static final String sJAVAVER = "(Javac " + System.getProperty("java.runtime.version") + ")";

   public static void main(String[] args)
   {
      System.out.println(String.format("*** Aloha, o keia `jv722varargs.java` %s -- VarArgs %s ***", sVERSION, sJAVAVER));  // "Aloha, ʻo kēia"





      System.out.println("Aloha mai.");
   }
}
