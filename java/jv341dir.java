// EMPTY STUB, YET TO BE FILLED WITH CODE

/**
 *  file       : id 20221222°1915 — gitlab.com/normai/cheeseburger …/java/jv341dir.java
 *  version    : • 20xxxxxx°xxxx v0.x.x Filling • 20221222°1915 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate directory listing
 *  status     :
 */

class jv341dir
{
   private static final String sVERSION = "v0.0.0";
   private static final String sJAVAVER = "(Javac " + System.getProperty("java.runtime.version") + ")";


   public static void main(String[] args)
   {
      System.out.println(String.format("*** To jest `jv341dir.java` %s -- Directory listing %s ***", sVERSION, sJAVAVER));





      System.out.println("Zegnajcie.");  // "Żegnajcie."
   }
}
