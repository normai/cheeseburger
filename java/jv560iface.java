// THIS FILE IS AN EMPTY STUB, IT IS YET TO BE FILLED WITH CODE

/**
 *  file       : id 20220830°1415 — gitlab.com/normai/cheeseburger …/java/jv560iface.java
 *  version    : • 20xxxxxx°xxxx v0.x.x Filling • 20220830°1415 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate polymorphism
 *  summary    :
 */

class jv560iface
{
   private static final String sVERSION = "v0.0.0";
   private static final String sJAVAVER = "(Javac " + System.getProperty("java.runtime.version") + ")";

   public static void main(String[] args)
   {
      System.out.println(String.format("*** Hei, taemae on `jv560iface.java` %s -- %s ***", sVERSION, sJAVAVER));  // "Hei, tämä on"





      System.out.println("Naekemiin.");  // "Näkemiin"
   }
}
