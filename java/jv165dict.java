/**
 *  file       : id 20220828°1915 — gitlab.com/normai/cheeseburger …/java/jv165dict.java
 *  version    : • 20220908°1431 v0.1.7 Filling • 20220828°1915 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    :
 *  summary    :
 *  ref        : https://www.w3schools.com/java/java_hashmap.asp []
 *  ref        : https://sharkysoft.com/archive/printf/docs/javadocs/lava/clib/stdio/doc-files/specification.htm [] — About string format and padding
 */

import java.util.HashMap;

class jv165dict
{
   private static final String sVERSION = "v0.1.7";
   private static final String sJAVAVER = "(Javac " + System.getProperty("java.runtime.version") + ")";

   public static void main(String[] args)
   {
      System.out.println(String.format("*** Hej, das ist `jv165dict.java` %s -- Dictionaries %s ***", sVERSION, sJAVAVER));

      // (1) Create
      HashMap<String, String> being = new HashMap<String, String>();
      being.put("kingdom", "Animals");
      being.put("class", "Reptiles");
      being.put("genus", "Crocodile");
      being.put("species", "Nile crocodile");
      being.put("name", "Schnappi");
      being.put("color", "Green");
      being.put("legs", "Four");
      being.put("food", "Meat");

      // (2) Info
      System.out.println("(2) HashMap size = " + Integer.toString(being.size()));

      // (3) Iterate
      System.out.println("(3) Iterate:");
      for (String key : being.keySet())
      {
         System.out.println(String.format(" - %-8s : %s", key, being.get(key)));
      }

      System.out.println("Farvel.");
   }
}
