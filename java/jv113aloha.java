/**
 *  file       : id 20220215°0915 — gitlab.com/normai/cheeseburger …/java/jv113aloha.java
 *  version    : • 20220908°0831 v0.1.7 Filling • 20220215°0915 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate basic program building elements like • Variables • Operators • Strings • Built-in function calls
 */

import java.lang.reflect.Field; // getType()

class jv113aloha
{
   private static final String sVERSION = "v0.1.7";
   private static final String sJAVAVER = "(Javac " + System.getProperty("java.runtime.version") + ")";

   public static void main(String[] args)
   {
      System.out.println(String.format("*** O keia `jv113aloha.java` %s -- Aloha %s ***", sVERSION, sJAVAVER));  // "ʻO kēia"

      // Define some variables and use an operator
      int a = 123;
      int b = 345;
      int c = a + b;

      // Output with a bulky statement
      System.out.println( "(1) Sum of " + String.valueOf(a)
                         + " and " + String.valueOf(b)
                          + " is " + String.valueOf(c)
                           );

      // Output with a summarized statement
      String s = "(2) Sum of " + String.valueOf(a)
                + " and " + String.valueOf(b)
                 + " is " + String.valueOf(c)
                  ;
      System.out.println(s);

      // Output numbers in various notations
      String sBin = Integer.toBinaryString(a * -1);
      String sHex = String.format("0x%08X", a);
      System.out.println("(3) Bin : " + sBin + ", Hex : " + sHex);

      // Show difference between integers, floats and strings
      System.out.println("(4) Get variable types:");
      int i = 12345;
      double f = 12.345;
      s = "A character chain";
      System.out.println(" - i = " + String.valueOf(i) + " " + ((Object) i).getClass().getName());
      System.out.println(" - f = " + String.valueOf(f) + " " + ((Object) f).getClass().getName());
      System.out.println(" - s = \"" + String.valueOf(s) + "\" " + ((Object) s).getClass().getName());  // Using escape sequence '\"'

      System.out.println("Aloha mai.");
   }
}
