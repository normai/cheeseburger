/**
 *  file       : id 20220215°0941 — gitlab.com/normai/cheeseburger …/java/jv121operas.java
 *  version    : • 20220908°1551 v0.1.7 Filling • 20220215°0941 v0.1.6 Stub
 *  license    : BSD 3-Clause | © 2022 - 2024 Norbert C. Maier
 *  subject    : Demonstrate some Java operators
 *  summary    :
 *  ref        : https://stackoverflow.com/questions/7277757/why-cant-i-import-static-java-lang-system-out-println [ref 20220806°1232]
 *  ref        : https://www.delftstack.com/howto/java/java-format-specifiers-for-integral-numbers/ [ref 20220909°0913]
 *  ref        : https://www.delftstack.com/howto/java/java-format-specifiers-for-floating-numbers/ [ref 20220909°0914]
 */

import static java.lang.System.out;

class jv121operas
{
   private static final String sVERSION = "v0.1.7";
   private static final String sJAVAVER = "(Javac " + System.getProperty("java.runtime.version") + ")";

   public static void main(String[] args)
   {
      out.println(String.format("*** Hello, this is `j121operas.java` %s -- Operators %s ***", sVERSION, sJAVAVER));

      // Assignment operators
      int v, w, x, y, z;
      v = w = x = y = z = 5;
      w += 2;
      x -= 2;
      y *= 2;
      z /= 2;
      out.println("(1.1) 'v = w = x = y = z = 5            ': " + String.valueOf(v));
      out.println("(1.2) 'w = w + 2' identical with 'w += 2': " + String.valueOf(w));
      out.println("(1.3) 'x = x - 2' identical with 'x -= 2': " + String.valueOf(x));
      out.println("(1.4) 'y = y * 2' identical with 'y *= 2': " + String.valueOf(y));
      out.println("(1.5) 'x = x / 2' identical with 'x /= 2': " + String.valueOf(z));
      out.println();

      // Remarkable operators
      double x1 = (double) 100 / 30;                            // Division. Note the extra cast "(double)"
      int x2 = 100 / 30;                                        // Integer division, in Python "//"
      double x3 = 100 % 30;                                     // Modulo or reminder operator
      double x4 = Math.pow(16, 3);                              // Power, in Python this were operator "16 ** 3"
      int x5 = 1 > 2 ? 1234 : 5678;                             // Ternary operator

      out.println("(2.1) 100 / 30 = (double) " + String.valueOf(x1) + " " + ((Object) x1).getClass().getName());
      out.println("(2.2) 100 / 30 = " + String.valueOf(x2) + " " + ((Object) x2).getClass().getName());
      out.println("(2.3) 100 % 30 = " + String.valueOf(x3) + " " + ((Object) x3).getClass().getName());
      out.println("(2.4) 16 ** 3 = " + String.valueOf(x4) + " " + ((Object) x4).getClass().getName());
      out.println("(2.5) 1 > 2 ? 1234 : 5678 = " + String.valueOf(x5 ) + " " + ((Object) x5).getClass().getName());
      out.println();

      // The format function introduces the member operator '.'
      int i1 = 1; float f1 = 2.3f; double d1 = 3.456789;
      String s = String.format("(3) Take %d or %.3f or %.3f", i1, f1, d1);
      out.println(s);

      out.println("Good bye.");
   }
}
